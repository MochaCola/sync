/* 
Kenny Blum
CS 11
3/2/2018
Dave Harden
File: MyString.cpp

Class Invariant:
Private Data Members:
string - The C-String of any length that is attached to the MyString object.
This member is used (not modified) when constructing MyString objects, comparing
MyString objects, and outputting/inputting MyString objects. This member is
modified when concatenating MyString objects, destructing MyString objects,
and reading MyString objects. MyString objects will always contain a dynamic
C-String.
*/


#include <iostream>
#include <string>
#include <cassert>
#include <cstring>
#include "MyString.h"
using namespace std;

namespace cs_mystring {
	MyString::MyString() {
		string = new char[1];
		strcpy(string, "");
	}

	MyString::MyString(const char* p_string) {
		string = new char[strlen(p_string) + 1];
		strcpy(string, p_string);
	}

	MyString::MyString(const MyString& right) {
		string = new char[strlen(right.string) + 1];
		strcpy(string, right.string);
	}

	MyString::~MyString() {
		delete[] string;
	}

	ostream& operator<<(ostream& out, const MyString& printMe) {
		out << printMe.string;
		return out;
	}

	istream& operator>>(istream& in, MyString& s) {
		while (isspace(in.peek()))
			in.ignore();
		char temp[128];
		if (in.peek() != ' ' || in.peek() != '\n')
			in >> temp;
		delete[] s.string;
		s.string = new char[strlen(temp) + 1];
		strcpy(s.string, temp);
		return in;
	}

	void MyString::read(istream& in, char limit) {
		char temp[128];
		in.getline(temp, 127, limit);
		delete[] string;
		string = new char[strlen(temp) + 1];
		strcpy(string, temp);
	}

	MyString operator+(const MyString& s1, const MyString& s2) {
		MyString result = MyString();
		result.string = new char[strlen(s1.string) + strlen(s2.string) + 1];
		strcpy(result.string, s1.string);
		strcat(result.string, s2.string);
		return result;
	}

	MyString MyString::operator+=(const MyString& right) {
		*this = *this + right;
		return *this;
	}

	bool operator<(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) < 0)
			return true;
		else
			return false;
	}

	bool operator<=(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) <= 0)
			return true;
		else
			return false;
	}

	bool operator>(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) > 0)
			return true;
		else
			return false;
	}

	bool operator>=(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) >= 0)
			return true;
		else
			return false;
	}

	bool operator==(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) == 0)
			return true;
		else
			return false;
	}

	bool operator!=(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) != 0)
			return true;
		else
			return false;
	}

	MyString MyString::operator=(const MyString& right) {
		if (this != &right) {
			delete[] string;
			string = new char[strlen(right.string) + 1];
			strcpy(string, right.string);
		}
		return *this;
	}

	char& MyString::operator[](int index) {
		assert(index >= 0 && index < strlen(string));
		return string[index];
	}

	char MyString::operator[](int index) const {
		assert(index >= 0 && index < strlen(string));
		return string[index];
	}

	int MyString::length() const {
		return strlen(string);
	}
}