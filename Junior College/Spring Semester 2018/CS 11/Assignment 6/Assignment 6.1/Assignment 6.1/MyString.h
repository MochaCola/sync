/*
Kenny Blum
CS 11
3/2/2018
Dave Harden
File: MyString.h

This class handles MyString objects. There are three constructors in the class, and one destructor.
MyString objects can be created with a C-String, another MyString object, or nothing (default).
MyString objects can be both outputted and inputted. They can also be read up to a specific
character, at which point the reading will cease. MyString objects can be concatenated as well.
This class is capable of the six comparison operators: less than (<), less than or equal to (<=), 
greater than (>), greater than or equal to (>=), equal to (==), and not equal to (!=). MyString
objects can be assigned to other MyString objects, as well as C-Strings and strings. You can also
retrieve characters at specific indexes. You can also retrieve the length of a MyString object.

Pre/Post Conditions:

MyString();

Pre: Empty C-String object
Post: Empty C-String object is assigned to MyString object

MyString(const char* p_string);

Pre: User has a C-String
Post: C-String is assigned to MyString object

MyString(const MyString& right);

Pre: User has a MyString object
Post: Another MyString object is created from the MyString parameter

~MyString();

Pre: User has MyString object
Post: Memory of the MyString object is de-allocated

friend std::ostream& operator<<(std::ostream& out, const MyString& printMe);

Pre: User has MyString object
Post: The content of that MyString object is displayed

friend std::istream& operator>>(std::istream& in, MyString& s);

Pre: MyString object is empty
Post: A string is read, and that string is assigned to the MyString object

void read(std::istream& in, char limit);

Pre: A string is given
Post: The string is read up until a specific character 'limit'

friend MyString operator+(const MyString& s1, const MyString& s2);

Pre: User has two strings, C-Strings, and/or MyString objects
Post: The objects are concatenated into one object

MyString operator+=(const MyString& right);

Pre: User has two strings, C-Strings, and/or MyString objects
Post: The objects are concatenated into one object

friend bool operator<(const MyString& s1, const MyString& s2);

Pre: MyString objects are given
Post: Returns true if MyString s1 is lower alphabetically than MyString s2

friend bool operator<=(const MyString& s1, const MyString& s2);

Pre: MyString objects are given
Post: Returns true if MyString s1 is lower or equal alphabetically to MyString s2

friend bool operator>(const MyString& s1, const MyString& s2);

Pre: MyString objects are given
Post: Returns true if MyString s1 is higher alphabetically than MyString s2

friend bool operator>=(const MyString& s1, const MyString& s2);

Pre: MyString objects are given
Post: Returns true if MyString s1 is lower or equal alphabetically to MyString s2

friend bool operator==(const MyString& s1, const MyString& s2);

Pre: MyString objects are given
Post: Returns true if MyString s1 is equal to MyString s2

friend bool operator!=(const MyString& s1, const MyString& s2);

Pre: MyString objects are given
Post: Returns true if MyString s1 is not equal to MyString s2

MyString operator=(const MyString& right);

Pre: MyString objects are given
Post: Assigns right-hand object to MyString object

char& operator[](int index);

Pre: MyString object is given
Post: Can retrieve characters from MyString at specific indexes

char operator[](int index) const;

Pre: MyString object is given
Post: Can retrieve characters from MyString at specific indexes

int length() const;

Pre: MyString object is given
Post: Can retrieve the length of the MyString object

*/

#ifndef MYFRACTION_H
#define MYFRACTION_H

#include <iostream>

namespace cs_mystring {
	class MyString {
	public:
		MyString();
		MyString(const char* p_string);
		MyString(const MyString& right);
		~MyString();
		friend std::ostream& operator<<(std::ostream& out, const MyString& printMe);
		friend std::istream& operator>>(std::istream& in, MyString& s);
		void read(std::istream& in, char limit);
		friend MyString operator+(const MyString& s1, const MyString& s2);
		MyString operator+=(const MyString& right);
		friend bool operator<(const MyString& s1, const MyString& s2);
		friend bool operator<=(const MyString& s1, const MyString& s2);
		friend bool operator>(const MyString& s1, const MyString& s2);
		friend bool operator>=(const MyString& s1, const MyString& s2);
		friend bool operator==(const MyString& s1, const MyString& s2);
		friend bool operator!=(const MyString& s1, const MyString& s2);
		MyString operator=(const MyString& right);
		char& operator[](int index);
		char operator[](int index) const;
		int length() const;
	private:
		char* string;
	};
}

#endif