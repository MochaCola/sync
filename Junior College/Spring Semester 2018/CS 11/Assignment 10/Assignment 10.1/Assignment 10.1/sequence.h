#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <iostream>
using namespace std;

namespace cs_sequence {
	class sequence {
	public:
		typedef int value_type;
		typedef std::size_t size_type;
		sequence();
		sequence(const sequence& item);
		~sequence();
		void start();
		void advance();
		void insert(const value_type& entry);
		void attach(const value_type& entry);
		void remove_current();
		size_type size() const;
		bool is_item() const;
		value_type current() const;
		sequence operator=(const sequence& item);	//assignment operator
	private:
		struct node {
			value_type data;
			node* next;
		};
		node* cursor;
		node* headptr;
		node* tailptr;
		node* precursor;
		size_type numitems;
		void copy(const sequence& item);			//copy constructor
		void clear();								//destructor
	};
}

#endif