#include "sequence.h"
#include <vector>
#include <iostream>
using namespace std;

namespace cs_sequence {
	typedef int value_type;
	typedef std::size_t size_type;

	sequence::sequence() {
		cursor = nullptr;
		headptr = nullptr;
		tailptr = nullptr;
		precursor = nullptr;
		numitems = size_type();
	}

	sequence::sequence(const sequence& item) {
		copy(item);
	}
	
	sequence::~sequence() {
		clear();
	}

	void sequence::start() {
		cursor = headptr;
		precursor = nullptr;
	}

	void sequence::advance() {
		if (is_item()) {
			if (cursor != tailptr) {
				precursor = cursor;
				cursor = cursor->next;
			}
			else {
				cursor = nullptr;
				precursor = nullptr;
			}
		}
	}

	void sequence::insert(const value_type& entry) {
		node* temp = new node;
		temp->data = entry;
		numitems++;
		if (cursor == headptr || cursor == nullptr) {
			temp->next = headptr;
			headptr = temp;
			if (numitems == 1) {
				tailptr = temp;
			}
		}
		else {
			temp->next = cursor;
			precursor->next = temp;
		}
		cursor = temp;
	}

	void sequence::attach(const value_type& entry) {
		node* temp = new node;
		temp->data = entry;
		temp->next = nullptr;
		numitems++;
		if (cursor == nullptr)
			cursor = tailptr;
		if (numitems == 1) {
			headptr = temp;
			tailptr = temp;
		}
		else if (numitems > 1 && cursor == tailptr) {
			cursor->next = temp;
			precursor = cursor;
			tailptr = temp;
		}
		else {
			temp->next = cursor->next;
			cursor->next = temp;
			precursor = cursor;
		}
		cursor = temp;
	}

	void sequence::remove_current() {
		if (is_item()) {
			if (numitems == 1) {
				delete[] cursor;
				numitems--;
				cursor = nullptr;
				headptr = nullptr;
				tailptr = nullptr;
			}
			else if (numitems > 1 && cursor == headptr) {
				headptr = cursor->next;
				delete[] cursor;
				numitems--;
				cursor = headptr;
			}
			else {
				precursor->next = cursor->next;
				if (cursor == tailptr)
					tailptr = precursor;
				delete[] cursor;
				numitems--;
				if (precursor->next != nullptr)
					cursor = precursor->next;
				else {
					cursor = nullptr;
					precursor = nullptr;
				}
			}
		}
	}

	size_type sequence::size() const {
		return numitems;
	}

	bool sequence::is_item() const {
		if (cursor == nullptr)
			return false;
		else
			return true;
	}

	value_type sequence::current() const {
		return cursor->data;
	}

	sequence sequence::operator=(const sequence& item) {
		if (this != &item) {
			clear();
			copy(item);
		}
		return *this;
	}

	void sequence::copy(const sequence& item) {
		if (item.headptr == nullptr) {
			cursor = nullptr;
			headptr = nullptr;
			tailptr = nullptr;
			precursor = nullptr;
			numitems = 0;
		}
		else {
			headptr = new node;
			numitems = 1;
			headptr->data = item.headptr->data;
			node* newListPtr = headptr;
			node* sourcePtr = item.headptr->next;
			while (sourcePtr != nullptr) {
				newListPtr->next = new node;
				numitems++;
				newListPtr = newListPtr->next;
				newListPtr->data = sourcePtr->data;
				sourcePtr = sourcePtr->next;
			}
			newListPtr->next = nullptr;
			node* traveler = headptr;
			while (traveler != nullptr) {
				if (item.precursor != nullptr && traveler->data == item.precursor->data)
					precursor = traveler;
				if (item.cursor != nullptr && traveler->data == item.cursor->data)
					cursor = traveler;
				if (item.tailptr != nullptr && traveler->data == item.tailptr->data)
					tailptr = traveler;
				traveler = traveler->next;
			}
			if (item.cursor == nullptr) {
				cursor = nullptr;
				precursor = nullptr;
			}
		}
	}

	void sequence::clear() {
		start();
		while (is_item()) {
			node* temp = cursor->next;
			delete[] cursor;
			numitems--;
			cursor = temp;
		}
		headptr = nullptr;
		tailptr = nullptr;
		precursor = nullptr;
	}
}