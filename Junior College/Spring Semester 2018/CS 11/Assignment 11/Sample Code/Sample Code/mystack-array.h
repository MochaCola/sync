class mystack {
	public:
		mystack();
		void push(char ch);
		void pop();
		bool empty();
		char top();
	private:
		char array[100];
		int numItems;
};