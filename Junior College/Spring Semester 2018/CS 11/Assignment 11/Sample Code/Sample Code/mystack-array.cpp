#include "mystack.h"
#include <cassert>
#include <cstdlib>

mystack::mystack() {
	numItems = 0;
}

void mystack::push(char ch){
	array[numItems] = ch;
	numItems++;
}


void mystack::pop(){
	assert(!empty());
	numItems--;
}


char mystack::top(){
	assert(!empty());
	return array[numItems - 1];
}

bool mystack::empty(){
	return numItems == 0;
}