#include "sequence.h"
#include <stack>
#include <cassert>
#include <cstdlib>
using namespace std;

namespace cs_sequence {
	typedef int value_type;
	typedef std::size_t size_type;


	void sequence::start() {
		if (stack1.empty()) {
			stack1.push(stack2.top);			//error
		}
		else {
			while (stack1.size() != 1) {
				stack2.push(stack1.top);		//error
			}
		}
	}






	void sequence::advance() {
		assert(!stack2.empty());
		stack1.push(stack2.top);				//error
	}






	void sequence::insert(const value_type& entry) {
		value_type temp = stack1.top;			//error
		stack1.pop();
		stack1.push(entry);
		stack2.push(temp);
	}






	void sequence::attach(const value_type& entry) {
		stack1.push(entry);
	}






	value_type sequence::current() const {
		return stack1.top;						//2 errors
	}






	void sequence::remove_current() {
		stack1.pop();
	}






	size_type sequence::size() const {
		return stack1.size() + stack2.size();
	}






	bool sequence::is_item() const {
		return stack1.top == NULL;				//error
	}






}