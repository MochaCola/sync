#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <cstdlib>
#include <stack>
using namespace std;

namespace cs_sequence {
	class sequence {
		public:
			typedef int value_type;
			typedef std::size_t size_type;
			void start();
			void advance();
			void insert(const value_type& entry);
			void attach(const value_type& entry);
			value_type current() const;
			void remove_current();
			size_type size() const;
			bool is_item() const;

		private:
			stack<value_type> stack1;
			stack<value_type> stack2;
			size_type numitems;
	};
}

#endif