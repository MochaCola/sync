#include <iostream>
#include "binarytree.h"
#include "LL.h"
using namespace std;

typedef std::size_t size_type;

binarytree::binarytree() {
	root = nullptr;
	mSize = 0;
}


LL<int> binarytree::toLL() {
	toLL_aux(root);
}


void binarytree::print() const {
	print_aux(root);
}


void binarytree::insert(int item) {
	insert_aux(root, item);
	mSize++;
}


binarytree::size_type binarytree::size() const {
	return mSize;
}


int binarytree::find(int target, bool& found) const {
	return find_aux(root, target, found);
}


void binarytree::del(int target, bool& found) {
	del_aux(root, target, found);
	mSize--;
}


size_type binarytree::numPrimes() {
	return numPrimes_aux(root);
}


LL<int> binarytree::toLL_aux(treenode* root) {
	LL<int> l;
	LL<int> right;
	LL<int> left;
	if (root == nullptr) {
		return l;
	}
	else {
		l.push_front(root->data);
	}
	right = toLL_aux(root->right);
	left = toLL_aux(root->left);
	LL<int>::iterator i1 = right.begin();
	LL<int>::iterator i2 = left.begin();
	for (int i = 0; i < right.size(); i++) {
		l.push_front(*i1);
	}
	for (int i = 0; i < left.size(); i++) {
		l.push_front(*i2);
	}
	return l;
}


void binarytree::del_aux(binarytree::treenode*& root,
	int target,
	bool& found) {

	if (root == nullptr) {
		found = false;
	}
	else if (target < root->data) {
		del_aux(root->left, target, found);
	}
	else if (target > root->data) {
		del_aux(root->right, target, found);
	}
	else if (root->left == nullptr) {
		binarytree::treenode* tempptr = root;
		root = root->right;
		delete tempptr;
		found = true;
	}
	else {
		int max;
		remove_max(root->left, max);
		root->data = max;
		found = true;
	}
}

// pre: root != nullptr

void binarytree::remove_max(binarytree::treenode*& root, int& max) {
	if (root->right == nullptr) {
		max = root->data;
		binarytree::treenode* tempptr = root;
		root = root->left;
		delete tempptr;
	}
	else {
		remove_max(root->right, max);
	}
}



int binarytree::find_aux(const treenode* root,
	int target,
	bool& found) {

	if (root == nullptr) {
		found = false;
		return 0;
	}
	else if (target == root->data) {
		found = true;
		return root->data;
	}
	else if (target < root->data) {
		return find_aux(root->left, target, found);
	}
	else {
		return find_aux(root->right, target, found);
	}
}


void binarytree::insert_aux(treenode*& root, int item) {
	if (root == nullptr) {
		root = new binarytree::treenode;
		root->data = item;
		root->left = nullptr;
		root->right = nullptr;
	}
	else if (item <= root->data) {
		insert_aux(root->left, item);
	}
	else {
		insert_aux(root->right, item);
	}
}


void binarytree::print_aux(const treenode* root) {
	if (root != nullptr) {
		print_aux(root->left);
		cout << root->data << " ";
		print_aux(root->right);
	}
}

int binarytree::numPrimes_aux(treenode* root) {
	int numPrimes = 0;
	if (root == nullptr) {
		return 0;
	}
	else if (root->data == 2) {
		numPrimes++;
	}
	else if (root->data > 2) {
		int counter = 0;
		for (int i = 2; i < root->data; i++) {
			if (isdigit(root->data / i))
				counter++;
		}
		if (counter == root->data - 2)
			numPrimes++;
	}
	return numPrimes + numPrimes_aux(root->left) + numPrimes_aux(root->right);
}