#include <iostream>
#include "binarytree.h"
using namespace std;



binarytree::binarytree() {
	root = nullptr;
	mSize = 1;
}


void binarytree::print() const {
	print_aux(root);
}


void binarytree::insert(int item) {
	insert_aux(root, item);
	mSize++;
}


binarytree::size_type binarytree::size() const {
	return mSize;
}


int binarytree::find(int target, bool& found) const {
	return find_aux(root, target, found);
}


void binarytree::del(int target, bool& found) {
	del_aux(root, target, found);
	mSize--;
}


void binarytree::del_aux(binarytree::treenode*& root,
	int target,
	bool& found) {

	if (root == nullptr) {
		found = false;
	}
	else if (target < root->data) {
		del_aux(root->left, target, found);
	}
	else if (target > root->data) {
		del_aux(root->right, target, found);
	}
	else if (root->left == nullptr) {
		binarytree::treenode* tempptr = root;
		root = root->right;
		delete tempptr;
		found = true;
	}
	else {
		int max;
		remove_max(root->left, max);
		root->data = max;
		found = true;
	}
}

// pre: root != nullptr

void binarytree::remove_max(binarytree::treenode*& root, int& max) {
	if (root->right == nullptr) {
		max = root->data;
		binarytree::treenode* tempptr = root;
		root = root->left;
		delete tempptr;
	}
	else {
		remove_max(root->right, max);
	}
}



int binarytree::find_aux(const treenode* root,
	int target,
	bool& found) {

	if (root == nullptr) {
		found = false;
		return 0;
	}
	else if (target == root->data) {
		found = true;
		return root->data;
	}
	else if (target < root->data) {
		return find_aux(root->left, target, found);
	}
	else {
		return find_aux(root->right, target, found);
	}
}


void binarytree::insert_aux(treenode*& root, int item) {
	if (root == nullptr) {
		root = new binarytree::treenode;
		root->data = item;
		root->left = nullptr;
		root->right = nullptr;
	}
	else if (item <= root->data) {
		insert_aux(root->left, item);
	}
	else {
		insert_aux(root->right, item);
	}
}


void binarytree::print_aux(const treenode* root) {
	if (root != nullptr) {
		print_aux(root->left);
		cout << root->data << " ";
		print_aux(root->right);
	}
}

bool binarytree::is_prime(size_type , int prime) {
	if (root->data == prime) {
		return true;
	}
	else if (isdigit(root->data / prime)) {
		return false;
	}
	else {
		is_prime(root, prime + 1);
	}
}