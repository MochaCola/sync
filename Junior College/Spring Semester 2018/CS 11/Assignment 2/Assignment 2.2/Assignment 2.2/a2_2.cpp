/*
	Kenny Blum
	2/2/2018
	Assignment 2.2

	This program starts by creating an empty 26-element array list[] of struct Lettercount. The struct Lettercount has two attributes: 
	letter & count. The letter attributes of the members of the array are intialized with  the letters of the alphabet, and the count 
	attributes for all of the members are initialized with a value of 0. This program prompts the user to enter a sequence of 
	characters ending in a period. There is no limit to how many characters the user can input, and the user can also enter multiple 
	lines of input by pressing enter. Each individual character that is inputted is first tested to see whether or not is a letter. 
	If so, the count attribute of array element corresponding to that letter is incremented by one. The program continues reading 
	character inputs until it reaches a period, then it stops. Any characters inputted after the period will be neglected. Once it is
	done reading inputs, the program sorts the array by count from greatest to least, and then displays the letters and how many times
	each letter occurred.
*/

#include <iostream>
using namespace std;

struct Lettercount {														//Struct that holds both a letter and an integer
	char letter;
	int count;
	
	Lettercount();
	Lettercount(char p_letter, int p_num);
};

void initializeArray(Lettercount list[], int numItems);						//Function 1
int indexOfSmallest(Lettercount list[], int startingIndex, int numItems);	//Function 2
void sort(Lettercount list[], int numItems);								//Function 3
void displayList(Lettercount list[], int numItems);							//Function 4

int main() {
	const int SIZE = 26;													//26, one for each letter of the alphabet
	Lettercount list[SIZE];
	initializeArray(list, SIZE);

	char ch;
	cout << "Enter a sequence of characters (end with '.'): ";
	do {
		cin >> ch;
		if (isalpha(ch)) {
			char element = tolower(ch);
			list[static_cast<int>(element) - 97].count++;
		}
	} while (ch != '.');

	sort(list, SIZE);
	displayList(list, SIZE);
	return 0;
}


/*
	This is the default constructor for the struct Lettercount. The letter is set to a null character, and the count is set to zero.
	This default constructor is not used in this program.
*/

Lettercount::Lettercount() {
	letter = '\0';
	count = 0;
}






/*
	This is the parameterized constructor for the struct Lettercount. The letter is set to the parameter p_letter, and the count is
	set to the parameter p_count. This is only used in the initializeArray function.
*/

Lettercount::Lettercount(char p_letter, int p_count) {
	letter = p_letter;
	count = p_count;
}






/*
	This function initializes each member in the Lettercount array list[] with lowercase letters a-z and a count of zero.
*/

void initializeArray(Lettercount list[], int numItems) {
	for (int i = 0; i < numItems; i++)
		list[i] = Lettercount(static_cast<char>(97 + i), 0);
}






/*
	This function finds the smallest count number in the Lettercount array list[]. This is only used in the sort function.
*/

int indexOfSmallest(Lettercount list[], int startingIndex, int numItems)
{
	int targetIndex = startingIndex;
	for (int count = startingIndex + 1; count < numItems; count++) {
		if (list[count].count > list[targetIndex].count)
			targetIndex = count;
	}
	return targetIndex;
}






/*
	This function sorts the Lettercount array list[] by count from greatest to least.
*/

void sort(Lettercount list[], int numItems) {
	int count = 0;
	while (count < numItems - 1) {
		swap(list[indexOfSmallest(list, count, numItems)], list[count]);
		count++;
	}
}






/*
	This function displays the members of Lettercount array list[] if they do not have a count value of zero.
*/

void displayList(Lettercount list[], int numItems) {
	cout << "Letter: \tNumber of Occurences\n";
	for (int i = 0; i < numItems; i++) {
		if (list[i].count != 0)
			cout << list[i].letter << "\t\t" << list[i].count << "\n";
	}
}


/* Sample Output 1:
Enter a sequence of characters (end with '.'): Doug Dimmadome,
Owner of the Dimmsdale Dimmadome.
Letter:         Number of Occurences
m               8
d               7
e               5
o               5
i               3
a               3
g               1
h               1
l               1
f               1
n               1
r               1
s               1
t               1
u               1
w               1
 
   Sample Output 2:
Enter a sequence of characters (end with '.'): doooo
deeee
daaaa. duuuu diiii
Letter:         Number of Occurences
a               4
e               4
o               4
d               3
*/