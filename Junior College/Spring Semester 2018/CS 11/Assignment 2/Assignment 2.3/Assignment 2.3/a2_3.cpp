/*
	Kenny Blum
	2/2/2018
	Assignment 2.3

	This program receives scores and the names of people they belong to and then sorts them in descending order by score amount.
	It uses a dynamic arrays of Highscore structs. It first asks how many scores will be entered, and then the array is created with 
	the specified size. Afterwards, the program prompts the user to enter names and scores until the array is are filled. 
	The program then sorts the arrays in order of greatest score. Finally, the program displays the scores and then names of the 
	people that achieved those scores like a leaderboard.
*/

#include <iostream>
using namespace std;

struct Highscore {
	int score;
	char name[24];
};

void initializeData(Highscore scores[], int size);
void sortData(Highscore scores[], int size);
void displayData(const Highscore scores[], int size);

int main() {
	int size;

	cout << "How many scores will you enter? ";
	cin >> size;
	Highscore* scores = new Highscore[size];
	initializeData(scores, size);
	sortData(scores, size);
	displayData(scores, size);
	return 0;
}


/*
This function prompts the user to enter a name and a score. The entered name is inserted as the name attribute of the Highscore
struct in the array at index i, and the entered score is inserted as the score attribute of the first Highscore struct in the array
at index i.
*/

void initializeData(Highscore scores[], int size) {
	for (int i = 0; i < size; i++) {
		scores[i] = Highscore();
		cout << "Enter the name for score #" << i + 1 << ": ";
		cin >> scores[i].name;
		cout << "Enter the score for score #" << i + 1 << ": ";
		cin >> scores[i].score;
	}
	return;
}






/*
This function sorts the elements of the Highscore array by score from greatest to smallest.
*/

void sortData(Highscore scores[], int size) {
	int i, j;
	for (i = 0; i < size - 1; i++) {
		for (j = 0; j < size - i - 1; j++) {
			if (scores[j].score < scores[j + 1].score)
				swap(scores[j], scores[j + 1]);
		}
	}
}






/*
This function displays the array, first displaying the scores and the names of the people who earned those scores.
It displays like a leaderboard on an arcade machine.
*/

void displayData(const Highscore scores[], int size) {
	cout << "\nTop Scorers:" << endl;
	for (int i = 0; i < size; i++)
		cout << scores[i].name << ": " << scores[i].score << endl;
}


/* Output:
How many scores will you enter? 4
Enter the name for score #1: Suzy
Enter the score for score #1: 600
Enter the name for score #2: Kim
Enter the score for score #2: 9900
Enter the name for score #3: Armando
Enter the score for score #3: 8000
Enter the name for score #4: Tim
Enter the score for score #4: 514

Top Scorers:
Kim: 9900
Armando : 8000
Suzy : 600
Tim : 514
*/