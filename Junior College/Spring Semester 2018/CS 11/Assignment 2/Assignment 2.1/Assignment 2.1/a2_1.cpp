/*
	Kenny Blum
	2/2/2018
	Assignment 1.1

	This program takes C-Strings and performs various different functions on them. First, a function searches the C-Strings for a
	specific character and outputs the last index of that character in the C-String. Second, the C-Strings are reversed. Third,
	the function searches for a specific character and replaces all instances of that character with another specified character.
	Fourth, the C-Strings are tested to see if they are palindromes. Fifth, all the letters of the C-Strings are converted to 
	uppercase. Finally, the number of letters in the C-Strings are returned. This program works as intended.
*/

#include <iostream>
using namespace std;

int lastIndexOf(const char* inString, char target);						//Function 1
void reverse(char* inString);											//Function 2
int replace(char* inString, char target, char replacementChar);			//Function 3
bool isPalindrome(const char* inString);								//Function 4
void toupper(char* inString);											//Function 5
int numLetters(const char* inString);									//Function 6

int main() {
	char str1[] = "C-Strings are sweet!";								//20 characters
	char str2[] = "rac3car";											//7 characters
	
	cout << "String 1: " << str1 << endl;
	cout << "String 2: " << str2 << endl;

	cout << "Last index of 'e' in String 1: " << lastIndexOf(str1, 'e') << endl;
	cout << "Last index of 'x' in String 1: " << lastIndexOf(str1, 'x') << endl;

	reverse(str1);
	reverse(str2);
	cout << "String 1 Reversed: " << str1 << endl;						//Testing to be sure that the reverse function works with
	cout << "String 2 Reversed: " << str2 << endl;						//C-Strings containing both an odd & even number of chars
	reverse(str1);
	reverse(str2);														//Returning str1 & str2 to original form

	cout << "Replacing all 's' characters in String 1 with '#'... \n";
	cout << "Replacements made: " << replace(str1, 's', '#') << "\t" << str1 << endl;
	cout << "Replacing all 'c' characters in String 2 with '#'... \n";
	cout << "Replacements made: " << replace(str2, 'c', '#') << "\t" << str2 << endl;
	replace(str1, '#', 's');
	replace(str2, '#', 'c');											//Returning str1 & str2 to original form

	cout << "Testing to see if str1 is a palindrome... " << isPalindrome(str1) << endl;
	cout << "Testing to see if str2 is a palindrome... " << isPalindrome(str2) << endl;

	toupper(str1);
	cout << "Converting String 1 to uppercase... " << str1 << endl;
	toupper(str2);
	cout << "Converting String 2 to uppercase... " << str2 << endl;

	cout << "There are " << numLetters(str1) << " number(s) in String 1\n";
	cout << "There are " << numLetters(str2) << " number(s) in String 2\n";
	return 0;
}


/*
	This function searches the C-Strings for a specific character and outputs the last index of that character in the C-String.
	If the character does not appear in the C-String, a value of -1 is returned. I used a for-loop because the function must check
	every element of the C-String for the specified character. This function works as intended.

	Parameters.
	inString	The C-String that searched for instances of a specific character.
	target		The specific character that is searched for.
*/

int lastIndexOf(const char* inString, char target) {
	int size = strlen(inString);
	int index = 0;
	for (int i = 0; i <= size; i++) {
		if (inString[i] == target)
			index = i;
		if (inString[i] == '\0' && index == 0)
			index = -1;
	}
	return index;
}






/*
	This function reverses the C-Strings. It swaps the first element of the C-String with the last element, followed by the the
	second element with the second-to-last element, and so on until it reaches the middle of the C-String. The while-loop used in
	this function only reaches to the middle of the string and then stops. This function works as intended and results in a 
	reversed C-String.

	Parameters.
	inString	The C-String that will be reversed.
*/

void reverse(char* inString) {
	int size = strlen(inString);
	int halfmark = size / 2;
	int index = 0;
	while (index < halfmark) {
		swap(inString[index], inString[size - (index + 1)]);
		index++;
	}
}






/*
	This function searches the C-Strings for a specific character and replaces those character(s) with another specified character.
	I used a for-loop because it must check every element of the C-String for the specified character so that all instances of that
	character are replaced. This function works as intended.

	Parameters.
	inString			The C-String that is searched for instances of a specific character.
	target				The character that is searched for.
	replacementChar		The character that replaces the target character.
*/

int replace(char* inString, char target, char replacementChar) {
	int size = strlen(inString);
	int counter = 0;
	for (int i = 0; i < size; i++) {
		if (inString[i] == target) {
			inString[i] = replacementChar;
			counter++;
		}
	}
	return counter;
}






/*
	This function uses the same while-loop method as Function 2, but rather than swapping the elements, it compares them and increments
	a counter variable if they are the same. If a C-String is a palindrome, then by the end of the while-loop, the counter variable
	should be equal to half the length of the C-String. This function works as intended.

	Parameters.
	inString	The C-String that is checked to see whether or not it is a palindrome.
*/

bool isPalindrome(const char* inString) {
	int size = strlen(inString);
	int halfmark = size / 2;
	int index = 0, counter = 0;
	while (index < halfmark) {
		if (inString[index] == inString[size - (index + 1)]);
		index++, counter++;
	}
	if (counter == halfmark)
		return true;
	else
		return false;
}






/*
	This function converts all letters in the C-String to the uppercase forms of those letters. Rather than using the toupper()
	function, I opted to use static_cast to obtain the ASCII values of the each letter and convert them to the uppercase form of
	that letter by subtracting the ASCII value by 32. I used a for-loop because every character in the C-String must be changed if
	it is a letter. This function works as intended.

	Parameters.
	inString	The C-string that is traversed through as each letter element is converted to uppercase.
*/

void toupper(char* inString) {
	int size = strlen(inString);
	for (int i = 0; i < size; i++) {
		if (islower(inString[i]))
			inString[i] = static_cast<char>(static_cast<int>(inString[i]) - 32);		//avoided using toupper()
	}
}






/*
	This function returns the amount of numeric characters in the C-String. I used a for-loop because every element must be checked
	by the function isdigit(). If no numbers occur in the C-String a value of 0 is returned. This function works as intended.

	Parameters.
	inString	The C-String that is traversed in search of numbers.
*/

int numLetters(const char* inString) {
	int size = strlen(inString);
	int counter = 0;
	for (int i = 0; i < size; i++) {
		if (isdigit(inString[i]))
			counter++;
	}
	return counter;
}


/* Output:
String 1: C-Strings are sweet!
String 2: rac3car
Last index of 'e' in String 1: 17
Last index of 'x' in String 1: -1
String 1 Reversed: !teews era sgnirtS-C
String 2 Reversed: rac3car
Replacing all 's' characters in String 1 with '#'...
Replacements made: 2    C-String# are #weet!
Replacing all 'c' characters in String 2 with '#'...
Replacements made: 2    ra#3#ar
Testing to see if str1 is a palindrome... 0
Testing to see if str2 is a palindrome... 1
Converting String 1 to uppercase... C-STRINGS ARE SWEET!
Converting String 2 to uppercase... RAC3CAR
There are 0 number(s) in String 1
There are 1 number(s) in String 2
*/