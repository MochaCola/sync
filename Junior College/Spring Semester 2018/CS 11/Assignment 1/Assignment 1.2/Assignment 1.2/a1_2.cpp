/*
	Kenny Blum
	1/27/2018
	Assignment 1.2

	This program receives scores and the names of people they belong to and then sorts them in descending order by score amount.
	It uses two dynamic arrays: an integer array for scores, and a string array for names. It first asks how many scores will be entered,
	and then the arrays for scores and names are created with the specified size. Afterwards, the program prompts the user to enter names
	and scores until the arrays are filled. The program then sorts the arrays in order of greatest score. Finally, the program displays the
	scores and then names of the people that achieved those scores like a leaderboard.

*/

#include <iostream>
#include <string>
using namespace std;

void initializeArrays(string names[], int scores[], int size);
void sortData(string names[], int scores[], int size);
void displayData(const string names[], const int scores[], int size);

int main() {
	int size;
	string* nameArray;
	int* scoreArray;

	cout << "How many scores will you enter? ";
	cin >> size;
	string* nameArray = new string[size];
	int* scoreArray = new int[size];
	initializeArrays(nameArray, scoreArray, size);
	sortData(nameArray, scoreArray, size);
	displayData(nameArray, scoreArray, size);
	return 0;
}

/*
	This function prompts the user to enter a name and a score. The entered name is made a member of the name array,
	and the entered score is made a member of the score array.
*/

void initializeArrays(string names[], int scores[], int size) {
	for (int i = 0; i < size; i++) {
		cout << "Enter the name for score #" << i + 1 << ": ";
		cin >> names[i];
		cout << "Enter the score for score #" << i + 1 << ": ";
		cin >> scores[i];
	}
	return;
}

/*
	This function sorts the score array from greatest to smallest. It then rearranges the name array in the same manner.
*/

void sortData(string names[], int scores[], int size) {
	int i, j;
	for (i = 0; i < size - 1; i++) {
		for (j = 0; j < size - i - 1; j++) {
			if (scores[j] < scores[j + 1]) {
				swap(scores[j], scores[j + 1]);
				swap(names[j], names[j + 1]);
			}
		}
	}
}

/*
	This function displays the sorted arrays, first displaying the scores and the names of the people who earned those scores.
	It displays like a leaderboard on an arcade machine.
*/

void displayData(const string names[], const int scores[], int size) {
	cout << "\nTop Scorers:" << endl;
	for (int i = 0; i < size; i++)
		cout << names[i] << ": " << scores[i] << endl;
}

/* Output:
How many scores will you enter? 4
Enter the name for score #1: Suzy
Enter the score for score #1: 600
Enter the name for score #2: Kim
Enter the score for score #2: 9900
Enter the name for score #3: Armando
Enter the score for score #3: 8000
Enter the name for score #4: Tim
Enter the score for score #4: 514

Top Scorers:
Kim: 9900
Armando : 8000
Suzy : 600
Tim : 514
*/