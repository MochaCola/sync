#include <iostream>
using namespace std;

void noNegatives(int *x) {													//Task 11
	if (*x < 0)
		*x = 0;
}

void swap(int *x, int *y) {													//Task 21
	int temp = *x;
	*x = *y;
	*y = temp;
}

int main() {
	int x, y;																//Task 1
	int* p1;																//Task 2
	p1 = &x;																//Task 3
	*p1 = 99;																//Task 4
	cout << "x is: " << x << endl;											//Task 5
	cout << "x is: " << *p1 << endl;										//Task 6
	p1 = &y;																//Task 7
	*p1 = -300;																//Task 8
	int temp, *p2 = &x;														//Task 9
	temp = *p1;																//Task 10
	*p1 = *p2;
	*p2 = temp;
	noNegatives(&x);														//Task 12
	noNegatives(&y);
	cout << "x is: " << *p2 << endl;										//Task 13
	p2 = &y;
	cout << "y is: " << *p2 << endl;
	int a[2];																//Task 14
	p2 = a;		//equivalent to p2 = &a[0];
	*p2 = x;																//Task 15
	p2[1] = y;																//Task 16
	cout << "Address of the first element of a[]: " << &p2[0] << endl;		//Task 17
	cout << "Address of the first element of a[]: " << &p2[1] << endl;		//Task 18
	p1 = &a[0];																//Task 19
	p2 = &a[1];
	temp = *p1;
	*p1 = *p2;
	*p2 = temp;
	cout << "x is: " << *p1 << endl;										//Task 20
	cout << "y is: " << *p2 << endl;
	swap(&x, &y);															//Task 22
	cout << "x is: " << x << endl;											
	cout << "y is: " << y << endl;
	swap(&a[0], &a[1]);														//Task 23
	cout << "a[0] is: " << a[0] << endl;
	cout << "a[1] is: " << a[1] << endl;
	return 0;
}

/* Output:
x is: 99
x is: 99
x is: 0
y is: 99
Address of the first element of a[]: 0016FE0C
Address of the first element of a[]: 0016FE10
x is: 99
y is: 0
x is: 99
y is: 0
a[0] is: 0
a[1] is: 99
*/