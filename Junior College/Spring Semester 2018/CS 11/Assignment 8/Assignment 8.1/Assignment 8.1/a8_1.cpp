/*
Kenny Blum
3/27/2018
CS 11
Dave Harden
Assignment 8.1
*/

#include <iostream>
#include <vector>
using namespace std;

struct Highscore {
	int score;
	char name[24];
};

void initializeData(vector<Highscore>& scores);
void sortData(vector<Highscore>& scores);
void displayData(const vector<Highscore>& scores);

int main() {
	int size;

	cout << "How many scores will you enter? ";
	cin >> size;
	vector<Highscore> scores(size);
	initializeData(scores);
	sortData(scores);
	displayData(scores);
	return 0;
}


void initializeData(vector<Highscore>& scores) {
	int counter = 0;
	for (vector<Highscore>::iterator i = scores.begin(); i != scores.end(); i++) {
		counter++;
		cout << "Enter the name for score #" << counter << ": ";
		cin >> i->name;
		cout << "Enter the score for score #" << counter << ": ";
		cin >> i->score;
	}
	return;
}


void sortData(vector<Highscore>& scores) {
	int i, j;
	for (vector<Highscore>::iterator i = scores.begin(); i != scores.end(); i++) {
		vector<Highscore>::iterator j = i;
		while (j != scores.begin() && j->score > (j - 1)->score) {
			Highscore temp = *j;
			*j = *(j - 1);
			*(j - 1) = temp;
			j--;
		}
	}
}


void displayData(const vector<Highscore>& scores) {
	cout << "\nTop Scorers:" << endl;
	for (vector<Highscore>::const_iterator i = scores.begin(); i != scores.end(); i++)
		cout << i->name << ": " << i->score << endl;
}


/* Output:
How many scores will you enter? 4
Enter the name for score #1: Suzy
Enter the score for score #1: 600
Enter the name for score #2: Kim
Enter the score for score #2: 9900
Enter the name for score #3: Armando
Enter the score for score #3: 8000
Enter the name for score #4: Tim
Enter the score for score #4: 514

Top Scorers:
Kim: 9900
Armando : 8000
Suzy : 600
Tim : 514
*/