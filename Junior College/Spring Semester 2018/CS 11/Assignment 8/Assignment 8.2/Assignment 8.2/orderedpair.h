#include <iostream>

namespace cs_pairs {
	template <class T>
	class OrderedPair {
	public:
		typedef T value_type;
		static const T DEFAULT_VALUE = T();
		class DuplicateMemberError {};
		OrderedPair(T newFirst = DEFAULT_VALUE, T newSecond = DEFAULT_VALUE);
		void setFirst(T newFirst);
		void setSecond(T newSecond);
		T getFirst() const;
		T getSecond() const;
		OrderedPair operator+(const OrderedPair& right) const;
		bool operator<(const OrderedPair& right) const;
		void print() const;
	private:
		T first;
		T second;
	};
	
	template <class T>
	OrderedPair<T>::OrderedPair(T newFirst = DEFAULT_VALUE, T newSecond = DEFAULT_VALUE) {
		setFirst(newFirst);
		setSecond(newSecond);
	}

	template <class T>
	void OrderedPair<T>::setFirst(T newFirst) {
		if (newFirst != second)
			first = newFirst;
		else if (newFirst == second && newFirst == DEFAULT_VALUE && second == DEFAULT_VALUE)
			first = newFirst;
		else
			throw DuplicateMemberError();
	}

	template <class T>
	void OrderedPair<T>::setSecond(T newSecond) {
		if (newSecond != first)
			second = newSecond;
		else if (first == newSecond && first == DEFAULT_VALUE && newSecond == DEFAULT_VALUE)
			second = newSecond;
		else
			throw DuplicateMemberError();
	}

	template <class T>
	T OrderedPair<T>::getFirst() const {
		return first;
	}

	template <class T>
	T OrderedPair<T>::getSecond() const {
		return second;
	}

	template <class T>
	OrderedPair<T> OrderedPair<T>::operator+(const OrderedPair& right) const {
		return OrderedPair(first + right.first, second + right.second);
	}

	template <class T>
	bool OrderedPair<T>::operator<(const OrderedPair& right) const {
		return first + second < right.first + right.second;
	}

	template <class T>
	void OrderedPair<T>::print() const {
		cout << "(" << first << ", " << second << ")";
	}
}