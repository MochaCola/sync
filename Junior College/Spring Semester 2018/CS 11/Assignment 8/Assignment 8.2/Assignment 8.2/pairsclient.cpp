/*
Kenny Blum
3/27/2018
CS 11
Dave Harden
Assignment 8.2
*/

#include <iostream>
#include <ctime>
#include <cstdlib>
#include "orderedpair.h"
using namespace std;
using namespace cs_pairs;

void intMain();
void stringMain();

int main() {
	int num1, num2;
	OrderedPair<int> myList[10];

	srand(static_cast<unsigned>(time(0)));
	cout << "default value: ";
	myList[0].print();
	cout << endl;

	for (int i = 0; i < 10; i++) {
		try {
			myList[i].setFirst(rand() % 50);
			myList[i].setSecond(rand() % 50 + 50);
		}
		catch (OrderedPair<int>::DuplicateMemberError e) {
			cout << "ERROR: DuplicateMemberError - the values of the first and second cannot be equal, except when they are equal to DEFAULT_VALUE.";
		}
	}

	myList[2] = myList[0] + myList[1];

	if (myList[0] < myList[1]) {
		myList[0].print();
		cout << " is less than ";
		myList[1].print();
		cout << endl;
	}

	for (int i = 0; i < 10; i++) {
		myList[i].print();
		cout << endl;
	}

	cout << "Enter two numbers to use in an OrderedPair.  Make sure they are different numbers: ";
	cin >> num1 >> num2;
	OrderedPair<int> x;
	try {
		x.setFirst(num1);
		x.setSecond(num2);
	}
	catch (OrderedPair<int>::DuplicateMemberError e) {
		x.setFirst(0);
		x.setSecond(0);
	}


	cout << "The resulting OrderedPair: ";
	x.print();
	cout << endl;
}

/*	Output:

default value: (0, 0)
(17, 84)
(21, 59)
(38, 143)
(28, 76)
(14, 94)
(8, 60)
(3, 77)
(31, 81)
(42, 68)
(3, 66)
Enter two numbers to use in an OrderedPair.  Make sure they are different number
s: 10 20
The resulting OrderedPair: (10, 20)

*/