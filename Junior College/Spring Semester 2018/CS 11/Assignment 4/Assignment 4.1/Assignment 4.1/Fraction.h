/*
Kenny Blum
CS 11
2/17/2018
Dave Harden
File: Fraction.h

This class handles Fraction objects. There is a single constructor in this class, allowing a Fraction to be created with 2, 1, or 0 arguments.
Fraction objects from this class are capable of being outputted in reduced form & mixed number form. Fraction objects can also be read by this
class. This class is capable of the six comparison operators: less than (<), less than or equal to (<=), greater than (>), greater than or equal 
to (>=), equal to (==), and not equal to (!=). This class is also capable of performing basic arithmetic operations, including: addition, 
subtraction, multiplication, division, addition-assignment, subtraction-assignment, multiplication-assignment, division-assignment, pre/post 
increment, and pre/post decrement.

Pre/Post Conditions:

Fraction(int num = 0, int den = 1);

Pre: Numerator equals zero, denominator equals one
Post: Numerator equals a specified value, denominator equals a specified value

friend std::ostream& operator<<(std::ostream& out, const Fraction& printMe);

Pre: Fraction is given
Post: Fraction is displayed

friend std::istream& operator>>(std::istream& in, Fraction& f);

Pre: Fraction f is set to default constructor values
Post: A fraction is read, and the values of that fraction are inputted into Fraction f.

friend bool operator<(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns true if Fraction f1 is less than Fraction f2

friend bool operator<=(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns true if Fraction f1 is less than or equal to Fraction f2

friend bool operator>(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns true if Fraction f1 is greater than Fraction f2

friend bool operator>=(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns true if Fraction f1 is greater than or equal to Fraction f2

friend bool operator==(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns true if Fraction f1 is equal to Fraction f2

friend bool operator!=(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns true if Fraction f1 is not equal to Fraction f2

friend Fraction operator+(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns the sum of Fraction f1 and Fraction f2 in reduced form

friend Fraction operator-(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns the difference of Fraction f1 and Fraction f2 in reduced form

friend Fraction operator*(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns the product of Fraction f1 and Fraction f2 in reduced form

friend Fraction operator/(const Fraction& f1, const Fraction& f2);

Pre: Fractions are given
Post: Returns the quotient of Fraction f1 and Fraction f2 in reduced form

Fraction operator+=(const Fraction& f);

Pre: Fraction is given
Post: Assigns the sum of Fraction f and another number/fraction to the value of Fraction f

Fraction operator-=(const Fraction& f);

Pre: Fraction is given
Post: Assigns the difference of Fraction f and another number/fraction to the value of Fraction f

Fraction operator*=(const Fraction& f);

Pre: Fraction is given
Post: Assigns the product of Fraction f and another number/fraction to the value of Fraction f

Fraction operator/=(const Fraction& f);

Pre: Fraction is given
Post: Assigns the quotient of Fraction f and another number/fraction to the value of Fraction f

Fraction operator++();

Pre: User has a Fraction object
Post: Pre-increments Fraction by 1

Fraction operator++(int);

Pre: User has a Fraction object
Post: Post-increments Fraction by 1

Fraction operator--();

Pre: User has a Fraction object
Post: Pre-decrements Fraction by 1

Fraction operator--(int);

Pre: User has a Fraction object
Post: Post-decrements Fraction by 1

void simplify();

Pre: User has an unsimplified Fraction object
Post: Fraction is simplified

*/

#ifndef FRACTION_h
#define FRACTION_h

#include <iostream>

namespace cs_Fraction {
	class Fraction {
		public:
			Fraction(int num = 0, int den = 1);
			friend std::ostream& operator<<(std::ostream& out, const Fraction& printMe);
			friend std::istream& operator>>(std::istream& in, Fraction& f);
			friend bool operator<(const Fraction& f1, const Fraction& f2);
			friend bool operator<=(const Fraction& f1, const Fraction& f2);
			friend bool operator>(const Fraction& f1, const Fraction& f2);
			friend bool operator>=(const Fraction& f1, const Fraction& f2);
			friend bool operator==(const Fraction& f1, const Fraction& f2);
			friend bool operator!=(const Fraction& f1, const Fraction& f2);
			friend Fraction operator+(const Fraction& f1, const Fraction& f2);
			friend Fraction operator-(const Fraction& f1, const Fraction& f2);
			friend Fraction operator*(const Fraction& f1, const Fraction& f2);
			friend Fraction operator/(const Fraction& f1, const Fraction& f2);
			Fraction operator+=(const Fraction& f);
			Fraction operator-=(const Fraction& f);
			Fraction operator*=(const Fraction& f);
			Fraction operator/=(const Fraction& f);
			Fraction operator++();
			Fraction operator++(int);
			Fraction operator--();
			Fraction operator--(int);
		private:
			void simplify();
			int num;
			int den;
	};
}
#endif