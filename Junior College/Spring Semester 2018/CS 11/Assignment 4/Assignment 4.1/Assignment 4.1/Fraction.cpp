/* Here is my first attempt at a class invariant:
Private Data Members:
num - The numerator value.
den - The denominator value
Both of these private members are used (but not modified) when constructing a Fraction objects, comparing Fraction objects,
as well as outputting/inputting Fraction objects. These members are modified when performing arithmetic operations, and when
simplifying Fraction objects. Fraction objects will always be in lowest terms, and will always be written as mixed numbers
when the numerator exceeds the denominator.
*/

#include <iostream>
#include "Fraction.h"
using namespace std;

namespace cs_Fraction {
	Fraction::Fraction(int p_num, int p_den) {
		num = p_num;
		den = p_den;
		simplify();
	}

	ostream& operator<<(ostream& out, const Fraction& printMe) {
		if (printMe.num % printMe.den == 0)
			out << printMe.num / printMe.den;
		else if (abs(printMe.num) > printMe.den)
			out << printMe.num / printMe.den << "+" << abs(printMe.num) % printMe.den << "/" << printMe.den;
		else
			out << printMe.num << "/" << printMe.den;
		return out;
	}

	istream& operator>>(istream& in, Fraction& f) {
		int temp;
		in >> temp;
		int whole = 0, num = 0, den = 1;
		if (in.peek() == '+') {
			whole = temp;
			in.ignore();
			in >> temp;
		}
		if (in.peek() == '/') {
			if (whole > 0)
				num = temp;
			else
				num = -temp;
			in.ignore();
			in >> temp;
			den = temp;
			f = Fraction((whole * den) + num, den);
			return in;
		}
		f = Fraction(temp);
		return in;
	}

	bool operator<(const Fraction& f1, const Fraction& f2) {
		if (f1.den == f2.den) {
			if (f1.num < f2.num)
				return true;
			else
				return false;
		}
		else {
			if ((f1.num * f2.den) < (f2.num * f1.den))
				return true;
			else
				return false;
		}
	}

	bool operator<=(const Fraction& f1, const Fraction& f2) {
		if (f1.den == f2.den) {
			if (f1.num <= f2.num)
				return true;
			else
				return false;
		}
		else {
			if ((f1.num * f2.den) <= (f2.num * f1.den))
				return true;
			else
				return false;
		}
	}

	bool operator>(const Fraction& f1, const Fraction& f2) {
		if (f1.den == f2.den) {
			if (f1.num > f2.num)
				return true;
			else
				return false;
		}
		else {
			if ((f1.num * f2.den) > (f2.num * f1.den))
				return true;
			else
				return false;
		}
	}

	bool operator>=(const Fraction& f1, const Fraction& f2) {
		if (f1.den == f2.den) {
			if (f1.num >= f2.num)
				return true;
			else
				return false;
		}
		else {
			if ((f1.num * f2.den) >= (f2.num * f1.den))
				return true;
			else
				return false;
		}
	}

	bool operator==(const Fraction& f1, const Fraction& f2) {
		if (f1.den == f2.den) {
			if (f1.num == f2.num)
				return true;
			else
				return false;
		}
		else {
			if ((f1.num * f2.den) == (f2.num * f1.den))
				return true;
			else
				return false;
		}
	}

	bool operator!=(const Fraction& f1, const Fraction& f2) {
		if (f1.den == f2.den) {
			if (f1.num != f2.num)
				return true;
			else
				return false;
		}
		else {
			if ((f1.num * f2.den) != (f2.num * f1.den))
				return true;
			else
				return false;
		}
	}

	Fraction operator+(const Fraction& f1, const Fraction& f2) {
		Fraction sum;
		if (f1.den != f2.den)
			sum = Fraction((f1.num * f2.den) + (f2.num * f1.den), f1.den * f2.den);
		else
			sum = Fraction(f1.num + f2.num, f1.den);
		sum.simplify();
		return sum;
	}

	Fraction operator-(const Fraction& f1, const Fraction& f2) {
		Fraction difference;
		if (f1.den != f2.den)
			difference = Fraction((f1.num * f2.den) - (f2.num * f1.den), f1.den * f2.den);
		else
			difference = Fraction(f1.num - f2.num, f1.den);
		difference.simplify();
		return difference;
	}

	Fraction operator*(const Fraction& f1, const Fraction& f2) {
		Fraction product = Fraction(f1.num * f2.num, f1.den * f2.den);
		product.simplify();
		return product;
	}

	Fraction operator/(const Fraction& f1, const Fraction& f2) {
		Fraction quotient = Fraction(f1.num * f2.den, f1.den * f2.num);
		quotient.simplify();
		return quotient;
	}

	Fraction Fraction::operator+=(const Fraction& f) {
		*this = *this + f;
		return *this;
	}

	Fraction Fraction::operator-=(const Fraction& f) {
		*this = *this - f;
		return *this;
	}

	Fraction Fraction::operator*=(const Fraction& f) {
		*this = *this * f;
		return *this;
	}

	Fraction Fraction::operator/=(const Fraction& f) {
		*this = *this / f;
		return *this;
	}

	Fraction Fraction::operator++() {
		num += den;
		return *this;
	}

	Fraction Fraction::operator++(int) {
		Fraction temp(num, den);
		num += den;
		return temp;
	}

	Fraction Fraction::operator--() {
		num -= den;
		return *this;
	}

	Fraction Fraction::operator--(int) {
		Fraction temp(num, den);
		num -= den;
		return temp;
	}

	void Fraction::simplify() {
		if (den < 0)
			num *= -1, den *= -1;
		if (den == 0)
			den = 1;
		int start;
		if (num >= den)
			start = num;
		else
			start = den;
		for (int i = start; i > 1; i--) {
			if ((num % i == 0) && (den % i == 0)) {
				num /= i;
				den /= i;
			}
		}
	}
}