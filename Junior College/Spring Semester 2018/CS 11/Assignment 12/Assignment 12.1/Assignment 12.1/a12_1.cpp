/*

Kenny Blum
CS 11
4/21/2018
Dave Harden
a12_1.cpp

This program asks the user to input a sequence of characters without any spaces, and then
asks the user to choose a lower and upper bound. This program reverses the characters 
in the sequence between the lower and upper indices. If either the lower or upper bound 
are invalid, the program will automatically change them to the nearest acceptable value.

*/

#include <iostream>
#include <string>
using namespace std;

void reverseWithinBounds(char* c, int lower, int upper);

int main() {
	string temp;
	cout << "Enter a sequence of characters with no spaces: ";
	cin >> temp;
	char* charArray = new char[temp.length() + 1];
	strcpy(charArray, temp.c_str());
	int upper, lower;
	cout << "Enter a lower and upper bound between 0 and " << temp.length() - 1 << ": ";
	cin >> lower >> upper;
	if (upper < lower)
		swap(upper, lower);
	if (upper > temp.length() - 1) {
		cout << "Invalid upper bound. Setting upper equal to " << temp.length() - 1 << "...\n";
		upper = temp.length() - 1;
	}
	if (lower < -1) {
		cout << "Invalid lower bound. Setting lower equal to 0...\n";
		lower = 0;
	}
	reverseWithinBounds(charArray, lower, upper);
	for (int i = 0; i < temp.length(); i++) {
		cout << charArray[i];
	}
	return 0;
}




/*
This recursive function takes an array of characters and reverses the characters within a lower and 
upper bound of indices. It first swaps the characters at the lower and upper bounds, and then
recursively calls itself again with a lower bound one index higher, and an upper bound one index lower.
This repeats until the lower and upper bound are either the same, or directly next to each other.
*/

void reverseWithinBounds(char* c, int lower, int upper) {
	swap(c[lower], c[upper]);
	if ((upper - 1) - (lower + 1) > 0)
		reverseWithinBounds(c, lower + 1, upper - 1);
}