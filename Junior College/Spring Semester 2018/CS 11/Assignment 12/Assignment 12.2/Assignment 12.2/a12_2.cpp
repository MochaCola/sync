#include <iostream>
#include <string>
#include <vector>
#include "MyString.h"
using namespace cs_mystring;
using namespace std;

bool isAPalindrome(MyString s, int lower, int upper);

int main() {
	string entry;
	vector<char> charVec;
	int lower, upper;

	cout << "Enter a string: ";
	getline(cin, entry);
	if (entry == "quit")
		return 0;
	for (int i = 0; i < entry.length(); i++) {
		if (!isspace(entry[i]) && !ispunct(entry[i]))
			charVec.push_back(toupper(entry[i]));
	}
	char* charArray = new char[charVec.size()];
	for (int i = 0; i < charVec.size(); i++)
		charArray[i] = charVec.at(i);
	charArray[charVec.size()] = '\0';
	MyString s(charArray);

	cout << "Enter a lower and upper bound between 0 and " << s.length() - 1 << ": ";
	cin >> lower >> upper;
	if (upper < lower)
		swap(upper, lower);
	if (upper > s.length() - 1) {
		cout << "Invalid upper bound. Setting upper equal to " << s.length() - 1 << "...\n";
		upper = s.length() - 1;
	}
	if (lower < -1) {
		cout << "Invalid lower bound. Setting lower equal to 0...\n";
		lower = 0;
	}



	bool palindrome = isAPalindrome(s, lower, upper);
	if (palindrome)
		cout << entry << " is a palidrome.";
	else
		cout << entry << " is not a palindrome.";
	return 0;
}









bool isAPalindrome(MyString s, int lower, int upper) {
	if (s[lower] == s[upper]) {
		if (upper - lower <= 1)
			return true;
		else
			if (isAPalindrome(s, lower + 1, upper - 1))
				return true;
	}
	else
		return false;
}