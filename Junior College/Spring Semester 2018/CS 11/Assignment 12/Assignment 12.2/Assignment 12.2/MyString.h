/*

Name: Kenny Blum
Date: Apr. 21, 2018
Assignment Number: 12
Instructor:  Dave Harden
File: mystring.h


The MyString class is designed to make working with strings easier and less
error-prone than working with traditional null-terminated C-strings.  The client
can declare and use MyStrings freely without concern for memory management
issues or the size of the MyString.  Operations for input/output, construction,
indexing, comparison, and concatenation of MyStrings are provided.  Assignment
and copying of MyString objects is allowed.

MyString(const char* inString);
post: a MyString object is created and initialized to "inString".

MyString();
post: a MyString object is created and initialized to the empty string.

MyString(const MyString& copyMe);
post: a MyString object is created and initialized to "copyMe".

friend ostream& operator<<(ostream& out, const MyString& printMe);
pre: "out" is ready for writing.
post: The contents of "printMe" have been inserted into "out".

char operator[] (int index) const;
pre: 0 <= index < length()
post: The character at position "index" (counting from 0) has been returned.

char& operator[](int index);
pre: 0 <= index < length()
post: The character at position "index" (counting from 0) has been returned.


friend bool operator<(const MyString& left, const MyString& right);
post: true is returned if left < right; false otherwise.

friend bool operator>(const MyString& left, const MyString& right);
post: true is returned if left > right; false otherwise.

friend bool operator<=(const MyString& left, const MyString& right);
post: true is returned if left <= right; false otherwise.

friend bool operator>=(const MyString& left, const MyString& right);
post: true is returned if left >= right; false otherwise.

friend bool operator==(const MyString& left, const MyString& right);
post: true is returned if left == right; false otherwise.

friend bool operator!=(const MyString& left, const MyString& right);
post: true is returned if left != right; false otherwise.

MyString operator=(const MyString& right);
post: A copy of "right" is stored in the calling object.

int length() const;
post: the number of characters in the calling object is returned.

*/

#ifndef MYSTRING_H
#define MYSTRING_H

#include <iostream>
namespace cs_mystring {
	class MyString {
	public:
		MyString(const char* inString);
		MyString();
		MyString(const MyString& copyMe);
		~MyString();
		friend std::ostream& operator<<(std::ostream& out, const MyString& printMe);
		char operator[] (int index) const;
		char& operator[](int index);
		friend bool operator<(const MyString& left, const MyString& right);
		friend bool operator>(const MyString& left, const MyString& right);
		friend bool operator<=(const MyString& left, const MyString& right);
		friend bool operator>=(const MyString& left, const MyString& right);
		friend bool operator==(const MyString& left, const MyString& right);
		friend bool operator!=(const MyString& left, const MyString& right);
		MyString operator=(const MyString& right);
		int length() const;
	private:
		char *str;
	};
}

#endif
