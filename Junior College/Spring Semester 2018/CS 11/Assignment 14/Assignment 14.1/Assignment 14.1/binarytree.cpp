#include <iostream>
#include "binarytree.h"
#include "LL.h"
using namespace std;

typedef std::size_t size_type;

binarytree::binarytree() {
	root = nullptr;
	mSize = 0;
}


binarytree::binarytree(const binarytree &item) {
	copy(this->root, item.root);
}


binarytree::~binarytree() {
	clear(this->root);
}


binarytree binarytree::operator=(const binarytree& right) {
	clear(this->root);
	*this = binarytree(right);
	return *this;
}


LL<int> binarytree::toLL() const {
	return toLL_aux(root);
}


void binarytree::print() const {
	print_aux(root);
}


void binarytree::insert(int item) {
	insert_aux(root, item);
	mSize++;
}


binarytree::size_type binarytree::size() const {
	return mSize;
}


int binarytree::find(int target, bool& found) const {
	return find_aux(root, target, found);
}


void binarytree::del(int target, bool& found) {
	del_aux(root, target, found);
	mSize--;
}


size_type binarytree::numPrimes() const {
	return numPrimes_aux(root);
}


LL<int> binarytree::toLL_aux(const treenode* root) {
	LL<int> l;
	if (root == nullptr) {
		return l;
	} else l.push_front(root->data);
	if (root->right != nullptr) {
		LL<int> right = toLL_aux(root->right);
		for (LL<int>::iterator i = right.begin(); i != right.end(); i++) {
			l.push_front(*i);
		}
	}
	if (root->left != nullptr) {
		LL<int> left = toLL_aux(root->left);
		for (LL<int>::iterator i = left.begin(); i != left.end(); i++) {
			l.push_front(*i);
		}
	}
	return l;
}


void binarytree::copy(treenode* destinationRoot, treenode* copyRoot) {
	if (copyRoot != nullptr) {
		destinationRoot->data = copyRoot->data;
	}
	if (copyRoot->left != nullptr) {
		destinationRoot->left = new treenode;
		copy(destinationRoot->left, copyRoot->left);
	}
	if (copyRoot->right != nullptr) {
		destinationRoot->right = new treenode;
		copy(destinationRoot->right, copyRoot->right);
	}
}


void binarytree::clear(treenode* root) {
	if (root != nullptr) {
		clear(root->left);
		clear(root->right);
		delete[] root;
	}
}


void binarytree::del_aux(binarytree::treenode*& root,
	int target,
	bool& found) {

	if (root == nullptr) {
		found = false;
	}
	else if (target < root->data) {
		del_aux(root->left, target, found);
	}
	else if (target > root->data) {
		del_aux(root->right, target, found);
	}
	else if (root->left == nullptr) {
		binarytree::treenode* tempptr = root;
		root = root->right;
		delete tempptr;
		found = true;
	}
	else {
		int max;
		remove_max(root->left, max);
		root->data = max;
		found = true;
	}
}


void binarytree::remove_max(binarytree::treenode*& root, int& max) {
	if (root->right == nullptr) {
		max = root->data;
		binarytree::treenode* tempptr = root;
		root = root->left;
		delete tempptr;
	}
	else {
		remove_max(root->right, max);
	}
}


int binarytree::find_aux(const treenode* root,
	int target,
	bool& found) {

	if (root == nullptr) {
		found = false;
		return 0;
	}
	else if (target == root->data) {
		found = true;
		return root->data;
	}
	else if (target < root->data) {
		return find_aux(root->left, target, found);
	}
	else {
		return find_aux(root->right, target, found);
	}
}


void binarytree::insert_aux(treenode*& root, int item) {
	if (root == nullptr) {
		root = new binarytree::treenode;
		root->data = item;
		root->left = nullptr;
		root->right = nullptr;
	}
	else if (item <= root->data) {
		insert_aux(root->left, item);
	}
	else {
		insert_aux(root->right, item);
	}
}


void binarytree::print_aux(const treenode* root) {
	if (root != nullptr) {
		print_aux(root->left);
		cout << root->data << " ";
		print_aux(root->right);
	}
}


int binarytree::numPrimes_aux(const treenode* root) {
	int numPrimes = 0;
	if (root == nullptr) {
		return 0;
	}
	else if (root->data == 2) {
		numPrimes++;
	}
	else if (root->data > 2) {
		bool prime = true;
		for (int i = 2; i < root->data; i++) {
			if (root->data % i == 0)
				prime = false;
		}
		if (prime)
			numPrimes++;
	}
	return numPrimes + numPrimes_aux(root->left) + numPrimes_aux(root->right);
}