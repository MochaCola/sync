#ifndef BINARYTREE_H
#define BINARYTREE_H
#include <cstdlib>	// for size_t
#include "LL.h"

class binarytree {

public:
	typedef std::size_t size_type;
	binarytree();
	binarytree(const binarytree &item);
	~binarytree();
	binarytree operator=(const binarytree& right);
	LL<int> toLL() const;
	void insert(int item);
	void print() const;
	size_type size() const;
	int find(int target, bool& found) const;
	void del(int target, bool& found);
	size_type numPrimes() const;
private:
	struct treenode {
		int data;
		treenode* left;
		treenode* right;
	};
	size_type mSize;
	treenode* root;
	static LL<int> toLL_aux(const treenode* root);
	static void copy(treenode* destination, treenode* copyObject);
	static void clear(treenode* root);
	static void insert_aux(treenode*& root, int item);
	static void print_aux(const treenode* root);
	static int find_aux(const treenode* root, int target, bool& found);
	static void del_aux(treenode*& root, int target, bool& found);
	static void remove_max(treenode*& root, int& max);
	static int numPrimes_aux(const treenode* root);
};

#endif