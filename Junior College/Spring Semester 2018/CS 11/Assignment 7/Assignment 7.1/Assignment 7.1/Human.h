#ifndef HUMAN_H
#define HUMAN_H
#include "Creature.h"
#include <string>
using namespace std;

namespace cs_creature {
	class Human : public Creature {
	public:
		Human();				// initialize to 10 strength, 10 hitpoints
		Human(int newStrength, int newHitpoints);
		string getSpecies();    // returns the type of the species
		int getDamage();		// returns the amount of damage this Human inflicts in one round of combat
	};
}

#endif