#include "Creature.h"
#include <string>
#include <iostream>
using namespace std;

namespace cs_creature {
	Creature::Creature() {
		strength = 10;
		hitpoints = 10;
	}

	Creature::Creature(int newStrength, int newHitpoints) {
		strength = newStrength;
		hitpoints = newHitpoints;
	}

	string Creature::getSpecies() {
		return "Creature";
	}

	int Creature::getDamage() {
		int damage = (rand() % strength) + 1;
		cout << "The " << getSpecies() << " attacks for " << damage << " points!" << endl;
		return damage;
	}

	int Creature::getStrength() {
		return strength;
	}

	int Creature::getHitpoints() {
		return hitpoints;
	}

	void Creature::setStrength(int newStrength) {
		strength = newStrength;
	}

	void Creature::setHitpoints(int newHitpoints) {
		hitpoints = newHitpoints;
	}
}