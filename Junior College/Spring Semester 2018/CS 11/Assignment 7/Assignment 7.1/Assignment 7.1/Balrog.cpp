#include "Balrog.h"
#include "Demon.h"
#include <string>
#include <iostream>
using namespace std;

namespace cs_creature {
	Balrog::Balrog() {
	}

	Balrog::Balrog(int newStrength, int newHitpoints)
		: Demon(newStrength, newHitpoints)
	{
	}

	string Balrog::getSpecies() {
		return "Balrog";
	}

	/*
	int Balrog::getDamage() {
		int damage = Demon::getDamage();
		return damage;
	}
	*/
}