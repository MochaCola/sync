#include "Human.h"
#include "Creature.h"
#include <string>
#include <iostream>
using namespace std;

namespace cs_creature {
	Human::Human() {
	}

	Human::Human(int newStrength, int newHitpoints)
		: Creature(newStrength, newHitpoints)
	{
	}

	string Human::getSpecies() {
		return "Human";
	}

	int Human::getDamage() {
		int damage = Creature::getDamage();
		return damage;
	}
}