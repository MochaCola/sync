#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Creature.h"
#include "Human.h"
#include "Elf.h"
#include "Demon.h"
#include "Balrog.h"
#include "Cyberdemon.h"
using namespace std;
using namespace cs_creature;

void battleArena(Creature &Creature1, Creature& Creature2);

int main()
{
	srand((time(0)));

	Elf e(50, 50);
	Balrog b(50, 50);;

	battleArena(e, b);
	return 0;
}


void battleArena(Creature &Creature1, Creature& Creature2)
{
	while (Creature1.getHitpoints() > 0 && Creature2.getHitpoints() > 0) {
		int damage1 = Creature1.getDamage();
		int damage2 = Creature2.getDamage();
		Creature1.setHitpoints(Creature1.getHitpoints() - damage2);
		if (Creature1.getHitpoints() < 0)
			Creature1.setHitpoints(0);
		cout << Creature1.getSpecies() << " has " << Creature1.getHitpoints() << " HP left!\n";
		Creature2.setHitpoints(Creature2.getHitpoints() - damage1);
		if (Creature2.getHitpoints() < 0)
			Creature2.setHitpoints(0);
		cout << Creature2.getSpecies() << " has " << Creature2.getHitpoints() << " HP left!\n";
		
		if (Creature2.getHitpoints() == 0 && Creature1.getHitpoints() > 0)
			cout << "The " << Creature1.getSpecies() << " wins!" << endl;
		else if (Creature1.getHitpoints() == 0 && Creature2.getHitpoints() > 0)
			cout << "The " << Creature2.getSpecies() << " wins!" << endl;
		else if (Creature1.getHitpoints() == 0 && Creature2.getHitpoints() == 0)
			cout << "The result is a tie!" << endl;
	}
}

/* Sample Outputs: (some close matches here!!)

Sample Output 1:
The Elf attacks for 21 points!
Magical attack inflicts 21 additional damage points!
The Balrog attacks for 46 points!
Elf has 4 HP left!
Balrog has 8 HP left!
The Elf attacks for 1 points!
Magical attack inflicts 1 additional damage points!
The Balrog attacks for 48 points!
Elf has 0 HP left!
Balrog has 6 HP left!
The Balrog wins!

Sample Output 2:
The Elf attacks for 44 points!
Magical attack inflicts 44 additional damage points!
The Balrog attacks for 49 points!
Elf has 1 HP left!
Balrog has 0 HP left!
The Elf wins!

Sample Output 3:
The Elf attacks for 1 points!
The Balrog attacks for 43 points!
Elf has 7 HP left!
Balrog has 49 HP left!
The Elf attacks for 35 points!
Magical attack inflicts 35 additional damage points!
The Balrog attacks for 10 points!
Elf has 0 HP left!
Balrog has 0 HP left!
The result is a tie!

*/