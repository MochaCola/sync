#ifndef CYBERDEMON_H
#define CYBERDEMON_H
#include "Demon.h"
#include <string>
using namespace std;

namespace cs_creature {
	class Cyberdemon : public Demon {
	public:
		Cyberdemon();				// initialize to 10 strength, 10 hitpoints
		Cyberdemon(int newStrength, int newHitpoints);
		string getSpecies();    // returns the type of the species
		//int getDamage();		// returns the amount of damage this Human inflicts in one round of combat
	};
}

#endif