#ifndef DEMON_H
#define DEMON_H
#include "Creature.h"
#include <string>
using namespace std;

namespace cs_creature {
	class Demon : public Creature {
	public:
		Demon();				// initialize to 10 strength, 10 hitpoints
		Demon(int newStrength, int newHitpoints);
		string getSpecies();    // returns the type of the species
		int getDamage();		// returns the amount of damage this Human inflicts in one round of combat
	};
}

#endif