#include "Demon.h"
#include "Creature.h"
#include <string>
#include <iostream>
using namespace std;

namespace cs_creature {
	Demon::Demon() {
	}

	Demon::Demon(int newStrength, int newHitpoints)
		: Creature(newStrength, newHitpoints)
	{
	}

	string Demon::getSpecies() {
		return "Demon";
	}

	int Demon::getDamage() {
		int damage = Creature::getDamage();
		if (rand() % 4 == 0) {
			damage += 50;
			cout << "Demonic attack inflicts 50 additional damage points!" << endl;
		}
		return damage;
	}
}