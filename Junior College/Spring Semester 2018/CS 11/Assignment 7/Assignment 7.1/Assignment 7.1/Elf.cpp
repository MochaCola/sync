#include "Elf.h"
#include "Creature.h"
#include <string>
#include <iostream>
using namespace std;

namespace cs_creature {
	Elf::Elf() {
	}

	Elf::Elf(int newStrength, int newHitpoints)
		: Creature(newStrength, newHitpoints)
	{
	}

	string Elf::getSpecies() {
		return "Elf";
	}

	int Elf::getDamage() {
		int damage = Creature::getDamage();
		if ((rand() % 2) == 0) {
			cout << "Magical attack inflicts " << damage << " additional damage points!" << endl;
			damage *= 2;
		}
		return damage;
	}
}