#include "Cyberdemon.h"
#include "Demon.h"
#include <string>
#include <iostream>
using namespace std;

namespace cs_creature {
	Cyberdemon::Cyberdemon() {
	}

	Cyberdemon::Cyberdemon(int newStrength, int newHitpoints)
		: Demon(newStrength, newHitpoints)
	{
	}

	string Cyberdemon::getSpecies() {
		return "Cyberdemon";
	}

	/*
	int Cyberdemon::getDamage() {
		int damage = Demon::getDamage();
		return damage;
	}
	*/
}