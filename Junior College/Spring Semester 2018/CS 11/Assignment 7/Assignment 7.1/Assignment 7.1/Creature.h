#ifndef CREATURE_H
#define CREATURE_H
#include <string>
using namespace std;

namespace cs_creature {
	class Creature {
	public:
		Creature();				// initialize to 10 strength, 10 hitpoints
		Creature(int newStrength, int newHitpoints);
		virtual string getSpecies() = 0;		// returns the type of the species
		virtual int getDamage();				// returns the amount of damage this Creature inflicts in one round of combat
		int getStrength();		// returns the strength rating of this Creature
		int getHitpoints();		// returns the amount of HP this Creature has
		void setStrength(int newStrength);		//sets the Creature's strength to specified value
		void setHitpoints(int newHitpoints);	//sets the Creature's HP to specified value
	private:
		int strength;           // how much damage this Creature inflicts
		int hitpoints;          // how much damage this Creature can sustain
	};
}

#endif