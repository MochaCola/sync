#ifndef MYFRACTION_H
#define MYFRACTION_H

#include <iostream>

namespace cs_mystring {
	class MyString {
	public:
		MyString();
		MyString(const char* p_string);
		MyString(const MyString& right);
		~MyString();
		friend std::ostream& operator<<(std::ostream& out, const MyString& printMe);
		friend bool operator<(const MyString& f1, const MyString& f2);
		friend bool operator<=(const MyString& f1, const MyString& f2);
		friend bool operator>(const MyString& f1, const MyString& f2);
		friend bool operator>=(const MyString& f1, const MyString& f2);
		friend bool operator==(const MyString& f1, const MyString& f2);
		friend bool operator!=(const MyString& f1, const MyString& f2);
		MyString operator=(const MyString& right);
		char& operator[](int index);
		char operator[](int index) const;
		int length() const;
	private:
		char* string;
	};
}

#endif