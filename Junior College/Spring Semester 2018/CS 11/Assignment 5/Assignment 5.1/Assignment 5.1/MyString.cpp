#include <iostream>
#include <cassert>
#include <cstring>
#include "MyString.h"
using namespace std;

namespace cs_mystring {
	MyString::MyString() {
		string = new char[1];
		strcpy(string, "");
	}

	MyString::MyString(const char* p_string) {
		string = new char[strlen(p_string) + 1];
		strcpy(string, p_string);
	}

	MyString::MyString(const MyString& right) {
		string = new char[strlen(right.string) + 1];
		strcpy(string, right.string);
	}

	MyString::~MyString() {
		delete[] string;
	}

	ostream& operator<<(ostream& out, const MyString& printMe) {
		out << printMe.string;
		return out;
	}

	bool operator<(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) < 0)
			return true;
		else
			return false;
	}

	bool operator<=(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) <= 0)
			return true;
		else
			return false;
	}

	bool operator>(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) > 0)
			return true;
		else
			return false;
	}

	bool operator>=(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) >= 0)
			return true;
		else
			return false;
	}

	bool operator==(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) == 0)
			return true;
		else
			return false;
	}

	bool operator!=(const MyString& s1, const MyString& s2) {
		if (strcmp(s1.string, s2.string) != 0)
			return true;
		else
			return false;
	}

	MyString MyString::operator=(const MyString& right) {
		if (this != &right) {
			delete[] string;
			string = new char[strlen(right.string) + 1];
			strcpy(string, right.string);
		}
		return *this;
	}

	char& MyString::operator[](int index) {
		assert(index >= 0 && index < strlen(string));
		return string[index];
	}

	char MyString::operator[](int index) const {
		assert(index >= 0 && index < strlen(string));
		return string[index];
	}

	int MyString::length() const {
		return strlen(string);
	}
}