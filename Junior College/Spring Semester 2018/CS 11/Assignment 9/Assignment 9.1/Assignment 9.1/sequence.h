#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <iostream>
using namespace std;

namespace cs_sequence {
	class sequence {
		public:
			typedef int value_type;
			typedef std::size_t size_type;
			sequence();
			void start();
			void advance();
			void insert(const value_type& entry);
			size_type size() const;
			bool is_item() const;
			value_type current() const;
		private:
			struct node {
				value_type data;
				node* next;
			};
			node* cursor;
			node* headptr;
			node* tailptr;
			node* precursor;
			size_type numitems;
	};
}

#endif