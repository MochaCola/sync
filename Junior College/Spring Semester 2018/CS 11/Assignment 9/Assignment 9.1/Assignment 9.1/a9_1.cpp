#include "sequence.h"
#include <iostream>
using namespace std;
using namespace cs_sequence;

int main() {
	sequence s;
	for (int i = 0; i < 6; i++) {
		s.insert(i);
	}

	s.start();
	while (s.is_item()) {
		cout << s.current() << " ";
		s.advance();
	}

	cout << endl;
}