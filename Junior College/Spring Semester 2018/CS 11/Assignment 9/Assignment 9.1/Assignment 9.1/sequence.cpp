#include "sequence.h"
#include <typeinfo>
#include <iostream>
using namespace std;

namespace cs_sequence {
	typedef int value_type;
	typedef std::size_t size_type;
	
	sequence::sequence() {
		cursor = nullptr;
		headptr = nullptr;
		tailptr = nullptr;
		precursor = nullptr;
		numitems = size_type();
	}

	void sequence::start() {
		cursor = headptr;
	}

	void sequence::advance() {
		size_type counter = size_type();
		if (is_item()) {
			counter++;
			if (counter == numitems) {
				return;
			}
			else {
				precursor = cursor;
				cursor = cursor->next;
			}
		}
	}

	void sequence::insert(const value_type& entry) {
		node* temp = new node;
		temp->data = entry;
		numitems++;
		if (cursor == headptr || cursor == nullptr) {
			temp->next = headptr;
			headptr = temp;
			if (numitems == 1) {
				tailptr = temp;
			}
		}
		else {
			temp->next = cursor;
			precursor->next = temp;
		}
		cursor = temp;
	}

	size_type sequence::size() const {
		return numitems;
	}

	bool sequence::is_item() const {
		if (cursor == nullptr) {
			return false;
		}
		value_type data = current();
		if (data >= value_type()) {
			return true;
		}
		else {
			return false;
		}
	}

	value_type sequence::current() const {
		return cursor->data;
	}
}