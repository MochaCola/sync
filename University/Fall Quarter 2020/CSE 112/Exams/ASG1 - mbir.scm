#!/afs/cats.ucsc.edu/courses/cse112-wm/usr/racket/bin/mzscheme -qr
;; $Id: mbir.scm,v 1.2 2020-09-06 20:23:54-07 - - $
;;
;; Kenny Blum - kpblum@ucsc.edu - 1641667
;;
;; NAME
;;    mbir.scm filename.mbir
;;
;; SYNOPSIS
;;    mbir.scm - mini basic interper
;;
;; DESCRIPTION
;;    The file mentioned in argv[1] is read and assumed to be an mbir
;;    program, which is the executed.  Currently it is only printed.
;;

(define *DEBUG* #f)
(define *STDIN* (current-input-port))
(define *STDOUT* (current-output-port))
(define *STDERR* (current-error-port))
(define *ARG-LIST* (vector->list (current-command-line-arguments)))

(define *stmt-table*     (make-hash))
(define *function-table* (make-hash))
(define *var-table*      (make-hash))
(define *array-table*    (make-hash))
(define *label-table*    (make-hash))

(define nan (/ 0.0 0.0))
(define file (car *ARG-LIST*))
(define linen '0)

(define *RUN-FILE*
    (let-values
        (((dirpath basepath root?)
            (split-path (find-system-path 'run-file))))
        (path->string basepath))
)

(define (die list)
    (for-each (lambda (item) (fprintf *STDERR* "~s " item)) list)
    (fprintf *STDERR* "~n")
    (when (not *DEBUG*) (exit 1)))

(define (out-error func type val)
    (when (equal? file "-d") (set! file (cadr *ARG-LIST*)))
    (fprintf *STDERR* "||| ERROR in file ~s, at line ~a:~n" file linen)
    (fprintf *STDERR* "|||\tin ~a: invalid ~a: [~a]~n" func type val)
    (exit 1))

(define (dump . args)
    (when *DEBUG*
        (printf "DEBUG:")
        (for-each (lambda (arg) (printf " ~s" arg)) args)
        (printf "~n")))

(define (usage-exit)
    (die `("Usage: " ,*RUN-FILE* " [-d] filename"))
)

(define (line-number line)
    (car line))

(define (line-label line)
    (let ((tail (cdr line)))
         (and (not (null? tail))
              (symbol? (car tail))
              (car tail))))

(define (line-stmt line)
    (let ((tail (cdr line)))
         (cond ((null? tail) #f)
               ((pair? (car tail)) (car tail))
               ((null? (cdr tail)) #f)
               (else (cadr tail)))))

(define (array-set args)
    (when (eqv? (caar args) 'asub)
          (let ((array (hash-ref *array-table* (cadar args) #f))
                (index (exact-round (eval-expr (caddar args))))
                (value (eval-expr (cadr args))))
               (if (not (>= index (vector-length array)))
                   (vector-set! array index value)
               (out-error "asub" "index" index)))))

(define (eval-expr expr)
    (cond [(number? expr) (+ expr 0.0)]
          [(symbol? expr)
                (let ((var (hash-ref *var-table* expr #f))
                      (vec (hash-ref *array-table* expr #f)))
                     (cond (var var) (vec vec)
                     (else (out-error "eval-expr" "symbol" expr))))]
          [(pair? expr)
           (let ((func (hash-ref *function-table* (car expr) #f))
                 (opnds (map eval-expr (cdr expr))))
                (if (not func)
                    (out-error "eval-expr" "function" (car expr))
                    (apply func opnds)))]
          [(vector? expr) expr]
          (else (out-error "eval-expr" "type" expr))))

(define (interp-dim args continuation)
    (when (eqv? (caar args) 'asub)
        (let ((name (cadar args))
              (size (exact-round (eval-expr (caddar args)))))
             (when (not (null? (and name size))) 
              (hash-set! *array-table* name (make-vector size 0.0)))))
    (interp-program continuation))

(define (interp-let args continuation)
    (cond [(symbol? (car args))
           (hash-set! *var-table* (car args) (eval-expr (cadr args)))]
          [(pair? (car args)) 
           (when (eqv? (caar args) 'asub) (array-set args))]
           (else (out-error "let" "argument" (car args))))
    (interp-program continuation))

(define (interp-goto args continuation)
    (if (not (symbol? (car args)))
        (out-error "goto" "label" (car args))
        (let ((cont (hash-ref *label-table* (car args) #f)))
             (if (eqv? #f cont) (out-error "goto" "label" (car args))
                 (interp-program cont)))))

(define (interp-if args continuation)
    (if (eval-expr (car args))
          (interp-goto (cdr args) continuation)
    (when (not (null? continuation))
          (interp-program continuation))))

(define (interp-print args continuation)
    (define (print item)
        (if (string? item)
            (printf "~a" item)
            (printf " ~a" (eval-expr item))))
    (for-each print args)
    (printf "~n")
    (interp-program continuation))

(define (interp-input args continuation)
    (when (not (null? (car args)))
      (let ((input (read)))
        (cond [(eof-object? input)
               (interp-let '(eof 1) '()) nan]
              [(number? input)
               (when (pair? (car args))
                     (array-set (cons (car args) (cons input '()))))
               (hash-set! *var-table* (car args) (+ input 0.0))]
              (else (out-error "input" "input" input))))
      (when (not (null? (cdr args)))
        (interp-input (cdr args) continuation)))
    (interp-program continuation))

(define (fill-labels program)
    (when (not (null? program))
          (let ((label (line-label (car program))))
               (when label
                 (let ((cont (cons (cdar program) (cdr program))))
                 (hash-set! *label-table* label cont))))
          (fill-labels (cdr program))))

(define (interp-program program)
    (when (not (null? program))
          (set! linen (line-number (car program)))
          (let ((line (line-stmt (car program)))
                (cont (cdr program)))
               (if line
                   (let ((func (hash-ref *stmt-table* (car line) #f)))
                        (func (cdr line) cont))
                   (interp-program cont)))))

(define (readlist filename)
    (let ((inputfile (open-input-file filename)))
         (if (not (input-port? inputfile))
             (die `(,*RUN-FILE* ": " ,filename ": open failed"))
             (let ((program (read inputfile)))
                  (close-input-port inputfile)
                         program))))

(define (dump-program filename program)
    (define (dump-line line)
        (dump (line-number line) (line-label line) (line-stmt line)))
    (dump *RUN-FILE* *DEBUG* filename)
    (dump program)
    (for-each (lambda (line) (dump-line line)) program))

(for-each (lambda (st) (hash-set! *stmt-table* (car st) (cadr st)))
   `(
        (dim   ,interp-dim)
        (let   ,interp-let)
        (goto  ,interp-goto)
        (if    ,interp-if)
        (print ,interp-print)
        (input ,interp-input)
    ))

(define (getindex a i)
    (if (not (>= i (vector-length a)))
             (vector-ref a (exact-round i))
        (out-error "asub" "index" (exact-round i))))

(for-each (lambda (fn) (hash-set! *function-table* (car fn) (cadr fn)))
   `(
        (+     ,+)
        (-     ,-)
        (*     ,*)
        (/     ,/)
        (=     ,eqv?)
        (!=    ,(lambda (x y) (not (eqv? x y))))
        (<     ,<)
        (<=    ,<=)
        (>     ,>)
        (>=    ,=)
        (^     ,expt)
        (sqrt  ,sqrt)
        (exp   ,exp)
        (log   ,log)
        (sin   ,sin)
        (cos   ,cos)
        (tan   ,tan)
        (acos  ,acos)
        (asin  ,asin)
        (atan  ,atan)
        (abs   ,abs)
        (ceil  ,ceiling)
        (floor ,floor)
        (round ,round)
        (asub  ,(lambda (a i) (getindex a i)))
    ))

(for-each (lambda (var) (hash-set! *var-table* (car var) (cadr var)))
   `(
        (e    ,(exp 1.0))
        (eof  0.0)
        (nan  ,(/ 0.0 0.0))
        (pi   ,(acos -1.0))
    ))

(define (main arglist)
    (cond ((null? arglist)
                (usage-exit))
          ((string=? (car arglist) "-d")
                (set! *DEBUG* #t)
                (printf "~a: ~s~n" *RUN-FILE* *ARG-LIST*)
                (main (cdr arglist)))
          ((not (null? (cdr  arglist)))
                (usage-exit))
          (else (let* ((mbprogfile (car arglist))        
                       (program (readlist mbprogfile)))
                (begin (when *DEBUG* (dump-program mbprogfile program))
                       (fill-labels program)
                       (interp-program program))))))

(main *ARG-LIST*)

