# Question 2A i
addi x5, x0, 16		# a = 16
addi x6, x0, 8		# b = 8
addi x7, x0, 4		# c = 4
add x8, x5, x6		# x8 = a + b  ->  24
sub x9, x8, x7		# y = a + b - c  ->  20

# Question 2A ii
jal  occupyx10
lui x10, 0x10010	# let x10 = base address of array, type doubleword (64 bits)
addi x5, x0, 0x420	# let total = 0x420
addi x6, x0, 1		# let i = 1 (meaning, access doubleword #i after base address)

slli x7, x6, 3		# x7 = i * 8 bytes (64 bits)
add  x7, x10, x7	# x7 = base address + 64 bits
ld   x6, 0(x7)		# x6 = 0xEEDDCCBBAA998877
add  x5, x5, x6		# total (0x420) += 0xEEDDCCBBAA998877   ->   0xEEDDCCBBAA998C97

# Question 2B - 2C: 	simple translations, no test :F

j done

occupyx10:
lui  x10, 0x11223				#  x10:   x0000000011223000
addi x10, x10, 0x344				#  x10:   x0000000011223344
slli x10, x10, 32				#  x10:   x1122334400000000
lui  x5, 0x55667				#  x5:    x0000000055667000
addi x5, x5, 0x788				#  x5:    x0000000055667788
add  x10, x10, x5				#  x10:   x1122334455667788
lui  x5, 0x10010
addi x6, x5, 8					
sd   x10, 0(x5)					# Store x10 in address 0x10010
xori x10, x10, -1				# Invert x10
sd   x10, 0(x6)					# Store inverted x10 in address 0x10018
jr   ra

done: