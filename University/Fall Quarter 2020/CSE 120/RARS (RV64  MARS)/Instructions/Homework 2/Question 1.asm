# Question 1A
lui  x10, 0x11223				#  x10:   x0000000011223000
addi x10, x10, 0x344				#  x10:   x0000000011223344
slli x10, x10, 32				#  x10:   x1122334400000000
lui  x5, 0x55667					#  x5:    x0000000055667000
addi x5, x5, 0x788				#  x5:    x0000000055667788
add  x10, x10, x5				#  x10:   x1122334455667788

# Question 1B
xori x5, x10, -1

# Question 1C
ld   x5, 8(x7)
xor  x5, x5, x6

# Question 1D
addi x10, x10, 8
sd   x11, 16(x10)