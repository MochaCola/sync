fib:    addi    t0, x0, 1
	bgt   	a0, t0, body	# If n > 1, enter the recursive body
	add	a1, a0, x0	# Else, let a1 hold the value of n
	jr	ra		# And jump back to caller
body:	addi  	sp, sp, -24	# Clear stack space for 3 items
	sd    	ra, 0(sp)	# Store the return address
	sd	a0, 8(sp)	# Store the value of n
	addi	a0, a0, -1	# Decrement the value of n by 1
	jal	fib		# Jump to fib,
	ld	a0, 8(sp)	# Restore n
	sd	a1, 16(sp)	# Save the return value of fib(n-1)
	addi	a0, a0, -2	# Decrement the value of n by 2
	jal	fib
	ld	t0, 16(sp)	# Save the return value of fib(n-2)
	add	a1, t0, a1	
	ld	ra, 0(sp)
	addi	sp, sp, 24
	jr	ra