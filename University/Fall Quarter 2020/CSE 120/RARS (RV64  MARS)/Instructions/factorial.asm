Func:	addi	t0, x0, 1	# i = 1
	addi	a1, x0, 1	# result = 1
Loop:	slt	t1, t0, a0	# set $t1 to 1 if (i <= arg)
	beq	t1, x0, Exit	# exit loop if (i > arg)
	mul	a1, a1, t0	# result *= i
	addi	t0, t0, 1	# i++
	j	Loop		# loop
Exit:	jr	ra