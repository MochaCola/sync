lui  a0, 0x11223	#  a0:    x0000000011223000
addi a0, a0, 0x344	#  a0:    x0000000011223344
slli a0, a0, 32		#  a0:    x1122334400000000
lui  t0, 0x55667	#  t0:    x0000000055667000
addi t0, t0, 0x788	#  t0:    x0000000055667788
add  a0, a0, t0		#  a0:    x1122334455667788

lui  a1, 0x10010	#  a1:    x0000000010010000		A1 will represent the base address of array 1
lui  a2, 0x10020	#  a2:	  x0000000010020000		A2 will represent the base address of array 2

sd   a0, 0(a1)		# Store   x1122334455667788	into	x0000000010010000 - x0000000010010004
xori a0, a0, -1		# Bitwise inversion of x10
sd   a0, 8(a1)		# Store	  xEEDDCCBBAA998877	into	x0000000010010008 - x000000001001000C

ld   t0, 0(a1)		# Load    x1122334455667788	into	t0
addi t0, t0, 1		# t0 += 1
sd   t0, 0(a2)		# Store   x1122334455667789	into    x0000000010020000 - x0000000010020004

ld   t1, 8(a1)		# Load    xEEDDCCBBAA998877	into	t1
addi t1, t1, -1		# t1 -= 1
sd   t1, 8(a2)		# Store   xEEDDCCBBAA998876	into	x0000000010020008 - x000000001002000C