class Fraction:
    def __init__(self, num, den):
        self.numerator = num
        self.denominator = den
        self.eval()
    def set_num(self, num):
        self.numerator = num
        self.eval()
    def set_den(self, den):
        self.denominator = den
        self.eval()
    def eval(self):
        try: self.value = self.numerator / self.denominator
        except ZeroDivisionError: print("oops!")
    def add(self, frac):
        self.numerator += frac.numerator
        self.denominator += frac.denominator
        self.eval()

total = Fraction(0, 0)

for i in range(1000):
    if i <= 1: total.add(Fraction(1, 5))
    else: total.add(Fraction(3, 5))
    print("Iteration %d = %f  (%d, %d)" % (i, total.value, 
                        total.numerator, total.denominator))