Note(s) viewed:
https://drive.google.com/drive/u/0/folders/1iSjSz_4iqNC3ALXx2x-tDA9Oul_7dePK
--------------------------------------------------------------------------------------------------------------------
More on the Corrected Datapath

 - As seen in the last image, the green line which is passed through each pipeline register after IF/ID is used to
   preserve the instruction in each stage.

    - This is done to assist in the prevention of data hazards - because pipeline stages are sometimes used by             different instructions when processing other, previously-called instruction(s) - certain values in functional        blocks (such as the RegFile or Data Memory) may be overwritten by other instructions.

    - Thus, by passing the entire 32-bit instruction through each stage, information can be pulled from the       instruction itself and returned to a previous stage if needed.

       - Example: In a load instruction, during the WB stage, the "Write Register" in the RegFile may not be the                   register that the instruction specified - due to the ID stage being used by another instruction.

                   - Thus, the "Write Register" (or rd) can be pulled from the instruction during the WB phase, and
                     looped back into the RegFile.

--------------------------------------------------------------------------------------------------------------------
Visualizing Multiple Instructions in the Same Pipeline (simplified)
(also see Lec11 Slide 9 for a resource-based visualization - preferred to simplified if you have the time)

  Clock Cycles:		CC1	CC2	CC3	CC4	CC5	CC6	CC7	CC8	CC9

 ld  x10, 40(x1)     [  IF   |  ID   |  EX   |  MEM  |  WB   ]
 sub x11, x2, x3	     [  IF   |  ID   |  EX   |  MEM  |  WB   ]
 add x12, x3, x4		     [  IF   |  ID   |  EX   |  MEM  |  WB   ]
 ld  x13, 48(x1)			     [  IF   |  ID   |  EX   |  MEM  |  WB   ]
 add x14, x5, x6				     [  IF   |  ID   |  EX   |  MEM  |  WB   ]

 - At Clock Cycle 5, every single pipeline stage is being used by a different instruction!
    - The pipeline is being "filled" during Clock Cycles 1 - 4
    - The pipeline is full during Clock Cycle 5
    - The pipeline is being "drained" during Clock Cycles 6 - 9 (nice)

--------------------------------------------------------------------------------------------------------------------
Pipelining Control [ see Lecture 11, Datapath Pipeline Control1.png ]
		   [ and Lecture 11, Datapath Pipeline Control2.png ]
                   [ and Lecture 11, Datapath Pipeline Control3.png ]

 - Need to control functional units
    - But they are working on different instructions!

 - Not a problem
    - Just pipeline the control signals forward into their respective upcoming stages, along with data

--------------------------------------------------------------------------------------------------------------------
Pipeline Performance

 - Given the following instructions:

    ld  x1, 100(x4)
    ld  x2, 200(x4)
    ld  x3, 400(x4)

 - Single Cycle (2400ps)				vs.	- Pipelined (1400ps)

   [ IF |ID| EX | MEM| WB ]					  [ IF |ID|#| EX | MEM| WB ]
			  [ IF |ID| EX | MEM| WB ]		       [ IF |ID|#| EX | MEM| WB ]
						 [ ... ]		    [ IF |ID|#| EX | MEM| WB ]

--------------------------------------------------------------------------------------------------------------------
Unpipelined vs. Pipelined Periods

 - Let's remember Iron Law:	Time = IC * CPI * Period

 - For the following, let:

     t_IF = 60ns, t_ID = 30ns, t_EX = 50ns, t_MEM = 80ns, t_WB = 20ns

     - Unpipelined: 	Period: 60ns + 30ns + 50ns + 80ns + 20ns = 240ns

     - Pipelined:	Period: 80ns, the longest stage

 - Speedup:	(unpipelined exec. time / pipelined exec. time)

    - This will vary with both the number of stages and the number of instructions, as well as the number of
      hazards encountered (discussed next lecture)

--------------------------------------------------------------------------------------------------------------------
Types of Hazards

 - Hazards: When the next instruction in sequence cannot execute in the following clock cycle due to inter-   dependence between the execution of the consecutive instructions.

    - Structural Hazards:
       - Two or more instructions in the pipeline require the same hardware resource to progress
       - Most common instance is multiplier or floating point unit

    - Data Hazards:
       - One instruction has a source operand that is the result of a previous instruction in the pipeline

    - Control Hazards:
       - The execution of an instruction depends on the resolution of a previous branch instruction in the pipeline
       - Becomes a big problem with deep pipelines

--------------------------------------------------------------------------------------------------------------------
Types of Data Dependencies

 - True dependence  ->  Read After Write (RAW) hazard		(focus on t0)		(Most likely to be hazard)

	add t0, t1, t2
	sub t3, t4, t0

 - Output dependence  ->  Write After Write (WAW) hazard	(focus on t0)		(Unlikely to be hazardous)

        add t0, t1, t2
        sub t0, t4, t5

 - Anti-dependence  ->  Write After Read (WAR) hazard		(focus on t1)		(Unlikely to be hazardous)

	add t0, t1, t2
        sub t1, t4, t5

--------------------------------------------------------------------------------------------------------------------
Dealing with RAW Hazards [ see Lecture 11, RAW Hazard.png ]

 - Must keep our "promise" to the instruction set
    - ISA says instructions execute sequentially, one at a time
    - All data dependencies (including RAW) are respected

 - Pipelining (without fixes) may break this promise
    - Overlapping instruction "i" and instruction "j" (parallelism)
    - "i" writes late in the pipeline (WB); "j" reads early (ID)
    - Don't want "j" to read wrong (old value) for a register

 - Must ensure that programmers cannot observe this behavior
    - Without necessarily reverting to single-cycle design...

 - When you read a register, what is the correct value to return?

--------------------------------------------------------------------------------------------------------------------