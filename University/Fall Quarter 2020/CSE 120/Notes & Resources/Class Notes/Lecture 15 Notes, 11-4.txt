Note(s) viewed:
https://drive.google.com/drive/u/1/folders/1iSjSz_4iqNC3ALXx2x-tDA9Oul_7dePK
--------------------------------------------------------------------------------------------------------------------
Refresher on Static Branch Predictions		(before covering Dynamic Branch Predictions)

 - Recall the three branch strategies:
    1: MEM stage - stall until branch resolved (unconditional stall) - always stalled for 2 cycles.
    2: ID stage  - stall until branch resolved (unconditional stall) - always stalled for 1 cycle.
    3: ID stage  - predict branch not taken - if correct, no stall; if incorrect, stall for 1 cycle.

 - Only stage three makes a branch prediction. In general, when predicting branches, there will only be a penalty
   if the prediction is incorrect.

--------------------------------------------------------------------------------------------------------------------
Proper Semantics for Branch Hazard

 Note the following: "n" varies depending on Control Point (ex. ID, EX, MEM)

 - "Predict Taken"
     - i)  if prediction correct (branch taken)     : Pipeline not stalled
     - ii) if prediction wrong (branch not taken)   : Pipeline stalled for "n" cycles

 - "Predict Not Taken"  (ex. Strategy 3)
     - i)  if prediction correct (branch not taken) : Pipeline not stalled
     - ii) if prediction wrong (branch taken)       : Pipeline stalled for "n" cycles

 - "Unconditional Stall" (ex. Strategy 1 & 2)
     - Pipeline always stalled for "n" cycles, no prediction involved

--------------------------------------------------------------------------------------------------------------------Static vs. Dynamic Branch Prediction

 - Previously, we've seen Static Branch prediction, where predictions are either "branch taken", or "not taken"

 - Dynamic Branch Prediction
    - Dynamically change prediction between "taken" and "not taken" based on the past history of branch behavior.
       - Given the same instance of a branch instruction repeatedly (such as in a for-loop):
          - The prediction should be whatever the most common occurence has been for the given instruction.

--------------------------------------------------------------------------------------------------------------------
Motivation for Dynamic Branch Predictions

 - After our improvements in Static Branch Prediction, only 1 stall cycle per wrongly predicted branch. (Strat 3)
    - Was sufficient for a single 5-stage pipeline to deal with control hazard
       - For industry standards (with 20 - 30 pipeline stages) this can come with heavy cycle penalties.

 - As processor gets bigger, branch penalty worsens.
    - Deeper pipeline (more pipeline stages) -> resolve branch later into the pipeline and have to flush more
      cycles for each wrong prediction

 - Bigger branch penalty -> higher accuracy needed

 - Two ways to lessen the problem:
    - 1: Predict earlier - reduce number of instructions to flush
    - 2: Improve prediction accuracy - if wrong less, flush less often

--------------------------------------------------------------------------------------------------------------------
An Advisory on the Usage of the Word "Prediction" for CSE120

 - Branch prediction is NOT to be confused with Machine Learning!
    - Do not try to associate any concepts of Machine Learning into trying to understand branch prediction
    - Machine Learning is a form of prediction, but not all predictions are machine learning!

 - Branch prediction has no direct correlation with the concept of a "training set" and/or "validation set"

--------------------------------------------------------------------------------------------------------------------
The "Boxing" Analogy for Dynamic Prediction	["squirrel girl" vs. "dishwasher material man"] LMAO
 (One specific TYPE of Dynamic Prediction)         Player One		     Player Two

 - For this kind of Dynamic Branch Prediction, only the previous outcome is referenced for match 'HISTORY'
    - This is known as a "1 Bit Predictor" Scheme, covered in the next note sections
  ______________________________________________________________________________________________________________
 | Match Number	| Match Winner (Prediction)			| Match Winner (Result)				|
 |--------------+-----------------------------------------------+-----------------------------------------------|
 | 1        	| Uncertain (b/c no match 'HISTORY')    	| P1 (arbitrary prediction)			|
 |--------------+-----------------------------------------------+-----------------------------------------------|
 | 2     	| P1 (b/c she won the previous match)		| P1 						|
 |--------------+-----------------------------------------------+-----------------------------------------------|
 | 3     	| P1 (b/c she won the previous match)		| P1 						|
 |--------------+-----------------------------------------------+-----------------------------------------------|
 | 4     	| P1 (b/c she won the previous match)  		| P2 (prediction shift!)			|
 |--------------+-----------------------------------------------+-----------------------------------------------|
 | 5     	| P2 (b/c he won the previous match) 		| P2						|
 |--------------+-----------------------------------------------+-----------------------------------------------|
 | 6     	| P2 (b/c he won the previous match)		| P1 (prediction shift!) (prediction 7 = P1)	|
 |______________|_______________________________________________|_______________________________________________|

--------------------------------------------------------------------------------------------------------------------
FSM of a "1 Bit Predictor" Dynamic Branch Prediction Scheme

       Taken			      Not Taken
	___				______
	| |				|    |
  ______|_v________   Not Taken	 _______|____v________
 |  Predict Taken  |----------->|  Predict Not Taken  |
 |_________________|<-----------|_____________________|
		        Taken

 - The diagram above is an example of a "Finite State Machine" (FSM)     (only two states, so = 1 bit)
    - Assume prediction begins with prediction: "not taken" (right box)
    - If branch is taken, follow the arrow which points to the left box - prediction "taken"
    - If branch is not taken, follow the cyclic arrow which points back to same box - prediction "not taken"

 - The current prediction for branch behavior is considered a state
 - The arrows indicate the actual observation of the branch (ex: whether it actually branched or not)

 - This state diagram can be represented by a 1 bit FSM
    - Bit "1": Predict branch taken, Bit "0": Predict branch not taken

 - Separate Hardware Block dedicated to implementing this FSM on our processor (beyond scope of course)

 - This FSM dynamically alters the processor's Control Hazard Strategy (Predict Taken / Predict Not Taken)

--------------------------------------------------------------------------------------------------------------------
1 Bit Predictor Accuracy (with Example) and Score Card

 - Example RISC-V Code:

 	 Outer Loop:
	      addi x2, x0, 9
	      addi x3, x0, 0

	 Inner Loop:
	      beq  x2, x3, Outer Loop
 	      addi x3, x3, 1
  	      jal  x1, Inner Loop

  ______________________________________________________________________________________________________________
 | Inner Loop   |			|				|					|
 | Iteration #	| Branch Prediction	| Actual Branch Behavior	| Prediction Accuracy			|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 1 (x3 = 0)   | Taken (T)   		| Not Taken (NT)		| INCORRECT	0%			|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 2 (x3 = 1)   | NT   		 	| NT				| CORRECT	1/2 = 50%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 3 (x3 = 2)	| NT   		 	| NT				| CORRECT	2/3 = 66.7%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 4 (x3 = 3)	| NT   		 	| NT				| CORRECT	3/4 = 75%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 5 (x3 = 4)	| NT   		 	| NT				| CORRECT	4/5 = 80%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 6 (x3 = 5)	| NT   		 	| NT				| CORRECT	5/6 = 83.3%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 7 (x3 = 6)	| NT   		 	| NT				| CORRECT	6/7 = 85.7%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 8 (x3 = 7)	| NT   		 	| NT				| CORRECT	7/8 = 87.5%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 9 (x3 = 8)	| NT   		 	| NT				| CORRECT	8/9 = 88.9%		|
 |--------------+-----------------------+-------------------------------+---------------------------------------|
 | 10 (x3 = 9) 	| NT   		 	| T				| INCORRECT	8/10 = 80%		|
 |______________|_______________________|_______________________________|_______________________________________|

 - The "SCORE CARD" of a 1-Bit Predictor for the previously shown RISC-V code is 8/10, or 80%.
 - What would the Score Card be if the Outer Loop is iterated 100 times?

    - Score Card: (Correct Predictions / Total Predictions)

    - (100 * 8) / (100 * 10)     ->     80/100     ->     8/10     ->     80%

    - It remains the same score card! This is because the first iteration (of the inner loop) will always
      predict the branch to be taken (due to the fact that the prediction is changed when the branch of the
      inner loop is taken during the last iteration)

--------------------------------------------------------------------------------------------------------------------
1 Bit Predictor - Branch History Table (BHT)

 - Imagine your program has thousands of branch instructions located at different memory addresses.
    - How do you keep track of the behavior of all the branch instructions?

 - Solution: Create "LOOKUP TABLE" in memory that maps each branch instruction address to the prediction bit (1/0)

 - This "LOOKUP TABLE" is called Branch History Table (BHT)
    - The BHT is implemented separately from main memory
    - If implemented in main memory, the processor will be slowed down by it.

 - Whenever PC increments to a branch instruction, the address is looked up in the BHT and the corresponding
   Prediction bit is accessed. If Prediction bit is correct, the bit is unchanged. If incorrect, bit value is
   updated (1->0 or 0->1). If a Prediction bit change occurs, it will occur prior to PC being updated to the next
   instruction. 

 - In practice, the BHT cannot accomodate ALL branch instruction addresses in our program in the table
 - A large BHT will cause slower memory access, thus reducing CPU performance
 - Commonly, a small BHT with a limited number of rows is used. (say for example, 10 rows)
    - Then, how do we map thousands of entries to a table with only 10 spaces? Using 'INDEXING'


 - A branch instruction address is looked up by INDEXING only its lower significant bits
    - EX: if branch instruction is '8020', the index used to lookup in the table will be '20'
    - This will be explored further in the next lecture

 - EXAMPLE:						      Branch History Table
  ______________________________		 _______________________________________________________
 | Addr.  RISC-V Code		|		| Branch Instr. Address	| Branch Prediction Bit		|
 | ...    ...			|		|                       | (will dynamically change)     |
 | ...    ...			|		|-----------------------+-------------------------------|
 | 8000:  beq x2, x3, L1	|		| 8000			| 1				|
 | ...	  ...			|		|-----------------------+-------------------------------|
 | ...    ...			|		| 8040			| 0				|
 | 8040:  beq x5, x3, L2	|		|-----------------------+-------------------------------|
 | ...    ...			|		| 8080			| 0				|
 | ...    ...			|		|-----------------------+-------------------------------|
 | 8080:  beq x10, x11, L3	|		| ...			| ...				|
 | ...    ...			|		|_______________________|_______________________________|
 |______________________________|

--------------------------------------------------------------------------------------------------------------------