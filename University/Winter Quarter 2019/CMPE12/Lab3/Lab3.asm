################################################################################
# Created by:	Blum, Kenny						       #
#		kpblum@ucsc.edu						       #
#		2/12/2019						       #
#									       #
# Assignment:	Lab 3: MIPS Looping ASCII Art				       #
#		CMPE012, Computer Systems and Assembly Language		       #
#		UC Santa Cruz, Winter 2019				       #
# 									       #
# Description:	This program creates a user-specified number of triangles.     #
#		Each triangle also has a user-specified size. The triangles    #
#		are made out of spaces, forward slashes, and backward slashes. #
################################################################################

.data
     prompt1:  .asciiz "Enter the length of one of the triangle legs: "
     prompt2:  .asciiz "Enter the number of triangles to print: "
     newline:  .asciiz "\n"
.text
	# Prompt user to enter length of triangle legs
	li $v0, 4
	la $a0, prompt1
	syscall
	
	# Get the user's input and save it in $t0
	li $v0, 5
	syscall
	move $t0, $v0
	
	# Prompt the user to enter the number of triangles
	li $v0, 4
	la $a0, prompt2
	syscall
	
	# Get the user's input and save it in $t1
	li $v0, 5
	syscall
	move $t1, $v0
	
	# New line
	li $v0, 4
	la $a0, newline
	syscall
	
	# Constructing the triangles with while loops
	addi $t2, $zero, 0	# Ensuring that $t2 begins at 0 (counter for loop 1)
	while1:			# Loop 1 controls the number of triangles
	      addi $t3, $zero, 0        # Ensuring that $t3 begins at 0 (counter for loop 2 & 4)
	      subi $t4, $t0, 1		# Ensuring that $t4 equals leg length - 1 (exit condition for loop 3 & 5)
	      addi $t5, $zero, 0	# Ensuring that $t5 begins at 0 (decrement / increment amount for loop 3 & 5)
	      addi $t6, $t4, 0		# Ensuring that $t6 begins at $t4 (counter for loop 3 & 5)
	      beq $t2, $t1, exit1	# If loop 1 counter = number of triangles, then exit loop 3
	      while2:			# Loop 2 controls printing of the top of an individual triangle
	            beq $t3, $t0, exit2		# If loop 2 counter = length of triangle leg, then exit loop
	            while3:			# Loop 3 controls printing of spaces for top of triangle
	                   beq $t6, $t4, exit3		# If loop 3 counter = length of legs - 1, then exit loop 3
	                   li $v0, 11
	                   la $a0, ' '
	                   syscall			# Print space character
	                   addi $t6, $t6, 1		# Increment loop 3 counter by 1
	                   j while3			# Loop through loop 3 again
	            exit3:
	                   addi $t5, $t5, 1		# Increment the decrementation amount by 1
	                   sub  $t6, $t6, $t5		# Decrement loop 3 counter by decrementation amount
	            li $v0, 11
	            la $a0, '\\'
	            syscall			# Print backwards slash character
	            li $v0, 4
	            la $a0, newline
	            syscall			# Print new line
	            addi $t3, $t3, 1		# Increment loop 2 counter by 1
	            j while2
	      exit2:			# Loop 2 exit condition
	            addi $t3, $zero, 0		# Reset loop 2 counter to 0 so it can be reused in loop 4
	            subi $t5, $t0, 1		# Set loop 3's decrementation amount to leg length - 1 so it can be used in loop 6
	            subi $t6, $t0, 1		# Set loop 3's counter to leg length - 1 so it can be used in loop 6
	      while4:			# Loop 4 controls printing of the bottom of an individual triangle
	             beq $t3, $t0, exit4	# If loop 4 counter = length of triangle leg, then exit loop
	             while5:		# Loop 5 controls printing of spaces for bottom of triangle
	                    beq $t6, $zero, exit5	# If loop 5 counter = 0, then exit loop 5
	                    li $v0, 11
	                    la $a0, ' '
	                    syscall
	                    subi $t6, $t6, 1
	                    j while5
	             exit5:
	                    subi $t5, $t5, 1		# Decrement the incrementation amount by 1
	                    add  $t6, $t6, $t5		# Increment loop 6 counter by incrementation amount
	             li $v0, 11
	             la $a0, '/'
	             syscall			# Print forwards slash character
	             li $v0, 4
	             la $a0, newline
	             syscall			# Print new line
	             addi $t3, $t3, 1		# Increment loop 4 counter by 1
	             j while4
	      exit4:
	      addi $t2, $t2, 1		# Increment loop 1 counter by 1
	      j while1			# Loop through loop 1 again
	exit1:				# Loop 1 exit condition
	      li $v0, 10
	      syscall			# Exit the program
