-----------------------------
Lab 3: MIPS Looping ASCII Art
CMPE 012 Winter 2019

Blum, Kenny
kpblum
-----------------------------

The text of your prompts are stored in the processor's memory. After
assembling your program, what is the range of addresses in which these strings
are stored?

The prompts are stored in the hexadecimal addresses in the range:
0x10010000	to	0x10010058	(in .data)

What were the learning objectives of this lab?

To become more familiar with assembly language by taking a hands-on approach, 
as well as to give us a good introduction to looping structures.

Did you encounter any issues? Were there parts of this lab you found enjoyable?

Overall, I found this entire lab to be enjoyable (at least, far more enjoyable
than working in MML with logic gates). I appreciated how we could see the output
of our program, so that helped me to troubleshoot a lot. I ran into issues with
infinite loops, but all it took to fix those problems was printing the iteration
values of each loop. Overall, I had a very good time and I'm glad we're moving on
from logic gates!! :)

How would you redesign this lab to make it better?

Honestly, I found no issue with this lab. I think it's perfectly fine as is.
It was a very good introduction to MIPS and I think adding anything more would
have been too much for our first introduction to it.

Did you collaborate with anyone on this lab?

No, I did not, unless you count watching Amell Peralta's videos on youtube as
collaboration. :P
His videos helped a lot!