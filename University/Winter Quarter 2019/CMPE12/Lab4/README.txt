------------------------
Lab 4: ASCII (HEX or 2SC) to Base 4
CMPE 012 Winter 2019

Blum, Kenny
kpblum
-------------------------

What was your approach to converting each ASCII input to two�s complement form?
For binary inputs, if the MSB was 1, I would invert the bits and add a 1, and then tack on a negative sign at the end.
For hexadecimal inputs, if the MSB was 8 or greater, I would perform the same operation.

What did you learn in this lab?
My knowledge of program arguemtns, subroutines, memory manipulation, and just general proficiency at programming in MIPS improved a lot during this lab.

Did you encounter any issues? Were there parts of this lab you found enjoyable?
I ran into issues with converting binary arguments to hexadecimal arguments. I wanted to convert it to a hexadecimal string, which meant I had to insert specific characters into the exactly correct place, and then append a null terminator. For a long time, the memory addresses of the two .byte variables (value1 & value2) would collide, resulting in a mess of an output. Eventually I figured out that I had to specify specific bytes I wanted to start with, otherwise changing those variables would change the other. I was stuck on that for a long time!

How would you redesign this lab to make it better?
I would remove the aspect of sign-extending. I chose not to do it for this lab, as I felt it needlessly overcomplicated things. My output comes out just fine without sign-extending, and also gave me less of a headache. Also, I feel like restraining us from using certain syscalls made this lab needlessly convoluted.

Did you collaborate with anyone on this lab? Please list who you collaborated with and the nature of your collaboration.
No, I found out everything I needed to from going to lab sections and stack overflow.