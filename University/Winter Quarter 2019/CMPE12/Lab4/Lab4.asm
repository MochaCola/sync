###################################################################################
# Created by:	Blum, Kenny						          #
#		kpblum@ucsc.edu						          #
#		2/25/2019						          #
#									          #
# Assignment:	Lab 4: ASCII Conversion					          #
#		CMPE012, Computer Systems and Assembly Language		          #
#		UC Santa Cruz, Winter 2019				          #
# 									          #
# Description:	This program reads in ASCII strings that represent hexadecimal,	  #
#		binary, or decimal values. It will convert any binary strings to  #
#		hex strings, and then find the integer value of any hex strings   #
#		and add it in a total sum register. It will also convert any      #
#		decimal strings to integer values and add it to the total sum 	  #
#		register. After every argument has been converted to an integer   #
#		and added to the total sum register, the total sum is then        #
#		converted to base 4 and outputted.				  #
#										  #
# Note:		The lab instructions did not state that we are allowed to use     #
#		register $a1. However, that is where program arguments are	  #
# 		stored, so I was forced to use it. Hope that's okay!		  #
###################################################################################

# Register usages:
# $v0: syscall values
# $a0: addresses for syscalls
# $a1: used to get user inputs from memory, also to pass arguments to subroutines
# $t0 - $t6: registers used in loops for various purposes
# $s0: sum of 2SC values
# $s1: first used to store first user input
# $s2: first used to store second user input

.data

output1: .asciiz "You entered the numbers:\n"
output2: .asciiz "\nThe sum in base 4 is:\n"
value1: .byte 0x0, 0x0, 0x0, 0x0, 0x0		# used to create strings for $s1
value2: .byte 0x0, 0x0, 0x0, 0x0, 0x0		# used to create strings for $s2

.text

# Store the user arguments in $s1 and $s2 & print them

lw $s1, ($a1)			# grab first argument, set to $s1
lw $s2, 4($a1)			# grab second argument, set to $s2
li $v0, 4			# 4 = string print
la $a0, output1
syscall				# print "You entered the numbers:\n"
move $a0, $s1
syscall				# print the contents of $s1
li $v0, 11			# 11 = character print
la $a0, ' '
syscall				# print a space to separate outputs
li $v0, 4			# 4 = string print
move $a0, $s2
syscall				# print the contents of $s2
li $v0, 11			# 11 = character print
la $a0, '\n'
syscall				# print newline escape sequence

# Determine the inputs as binary, hexadecimal, or decimal
# If an input is hexadecimal, it will remain the same for now
# If an input is decimal, it will remain the same for now
# If an input is binary, convert it into a hexadecimal string

lb $t0, 1($s1)			# store the second character of $s1 in $t0
beq $t0, 'b', setup1		# if $t0 = 'b', branch to setup1
lb $t0, 1($s2)			# else, store the second character of $s2 in $t0
beq $t0, 'b', setup2		# if $t0 = 'b', branch to setup2
j exit1				# else, jump to exit1
setup1:
	add $a1, $zero, $s1	# store $s1 in argument $a1
	add $a1, $a1, 2		# trim prefix off of argument
	add $t1, $zero, 16	# let $t1 = 16 (powers of 2, first loop counter)
	add $t3, $zero, 0	# let $t3 = 0 (byte sum)
	add $t4, $zero, 2	# let $t4 = 2 (indices of array, second loop counter)
	add $t5, $zero, 0	# let $t5 = 0 (third loop counter)
	j convert		# jump to convert
setup2:
	add $a1, $zero, $s2	# store $s2 in argument $a1
	add $a1, $a1, 2		# trim prefix off of argument
	add $t1, $zero, 16	# let $t1 = 16 (powers of 2, loop counter)
	add $t3, $zero, 0	# let $t3 = 0 (byte sum)
	add $t4, $zero, 2	# let $t4 = 2 (second loop counter)
	add $t5, $zero, 1	# let $t5 = 1 (third loop counter)
	j convert
convert:
	beq $t1, 1, converted	# if power of 2 = 1, branch to converted
	div $t1, $t1, 2		# power of 2 = (power of 2) / 2
	lb $t2, 0($a1)		# store the first char of argument in $t2
	add $a1, $a1, 1		# shift argument left by 1
	subi $t2, $t2, 48	# get int equivalent of char in $t2
	mul $t2, $t2, $t1	# bit value = int value * (power of 2)
	add $t3, $t3, $t2	# byte sum = byte sum + bit value
	j convert		# jump back to convert
converted:
	bge $t3, 10, letter	# if $t3 >= 10, branch to letter
	blt $t3, 10, number	# if $t3 < 10, branch to number
letter:
	addi $t3, $t3, 55	# add 55 to obtain appropriate ASCII
	beq $t5, 1, createHex2	# if third loop counter = 1, branch to createHex2
	beq $t5, 0, createHex1	# else, jump to createHex1
number:
	addi $t3, $t3, 48	# add 48 to obtain appropriate ASCII
	beq $t5, 1, createHex2	# if third loop counter = 1, branch to createHex2
	beq $t5, 0, createHex1	# else, jump to createHex1
createHex1:
	sb $t3, value1($t4)	# insert the byte sum in value1 at index $t4
	addi $t4, $t4, 1	# increment second loop counter by 1
	add $t1, $zero, 16	# let $t1 = 16 (powers of 2, loop counter)
	add $t3, $zero, 0	# let $t3 = 0 (byte sum)
	beq $t4, 3, convert	# if second loop counter = 3, branch to convert
	addi $t3, $zero, '0'	# set $t3 equal to '0'
	addi $t4, $zero, 0	# set array index = 0
	sb $t3, value1($t4)	# insert '0' at index 0 in value1
	addi $t3, $zero, 'x'	# set $t3 equal to 'x'
	addi $t4, $zero, 1	# set array index = 1
	sb $t3, value1($t4)	# insert 'x' at index 1 in value1
	addi $t4, $zero, 4	# set array index = 4
	sb $zero, value1($t4)	# insert '\0' at index 4 in value1
	j setS1			# jump to setS1
createHex2:
	li $v0, 11
	sb $t3, value2($t4)	# insert the byte sum in value2 at index $t4
	addi $t4, $t4, 1	# increment second loop counter by 1
	add $t1, $zero, 16	# let $t1 = 16 (powers of 2, loop counter)
	add $t3, $zero, 0	# let $t3 = 0 (byte sum)
	beq $t4, 3, convert	# if second loop counter = 3, branch to convert
	addi $t3, $zero, '0'	# set $t3 equal to '0'
	addi $t4, $zero, 0	# set array index = 0
	sb $t3, value2($t4)	# insert '0' at index 0 in value2
	addi $t3, $zero, 'x'	# set $t3 equal to 'x'
	addi $t4, $t4, 1	# increment array index by 1
	sb $t3, value2($t4)	# insert 'x' at index 1 in value2
	addi $t4, $zero, 4	# set array index = 4
	sb $zero, value2($t4)	# insert '\0' at index 4 in value2
	j setS2			# jump to setS2
setS1:
	la $s1, value1		# set $s1 to value1
	lb $t0, 1($s2)		# else, store the second character of $s2 in $t0
	beq $t0, 'b', setup2	# if $t0 = 'b', branch to setup2
	j exit1			# else, jump to exit1
setS2:
	la $s2, value2		# set $s2 to value2
	j exit1			# jump to exit1
exit1:

# At this point, arguments are either decimal or hex
# If an input is decimal string, format it in a specific way: (sign, 3 values, \0)
# If an input is hexadecimal, it will remain the same for now

lb $t0, 1($s1)		# store the first char of $s1 in $t0
bne $t0, 'x', setup3	# if $t0 = 'x', branch to setup3
lb $t0, 1($s2)		# else, store the first char of $s2 in $t0
bne $t0, 'x', setup4	# if $t0 = 'x', branch to setup4
j exit2			# else, jump to exit2
setup3:
	move $a1, $s1			# let subroutine argument be $s1
	la $t0, value1			# set $t0 equal to mem address of value1
	addi $t2, $zero, 0		# set $t2 = 0 (length variable)
	addi $t3, $zero, 0		# set $t3 = 0 (loop counter)
	j checkSign1			# jump to checkSign1
setup4:
	move $a1, $s2			# let subroutine argument be $s1
	la $t0, value2			# set $t0 equal to mem address of value2
	addi $t2, $zero, 0		# set $t2 = 0 (length variable)
	addi $t3, $zero, 1		# set $t3 = 1 (loop counter)
	j checkSign1			# jump to checkSign1
checkSign1:
	lb $t1, 0($a1)			# load 1st char of arg into $t1
	beq $t1, '-', insertMinus	# if $t1 = '-', branch to insertMinus
	j insertPlus			# else, jump to insertPlus
insertMinus:
	sb $t1, 0($t0)			# insert '-' at 1st character of value1
	addi $a1, $a1, 1		# shift arg string left by 1
	j length			# jump to length
insertPlus:
	add $t1, $zero, '+'		# store '+' at $t1
	sb $t1, 0($t0)			# insert '+' at 1st character of value1
	j length			# jump to length
length:
	lb $t4, 0($a1)			# load 1st char of arg into $t1
	beq $t4, '\0', insertType	# if $t1 = '\0', branch to insertType
	addi $a1, $a1, 1		# shift arg string left by 1
	addi $t2, $t2, 1		# increment length by 1
	lb $t5, 0($a1)			# load 1st char of arg into $t1
	beq $t5, '\0', insertType	# if $t1 = '\0', branch to insertType
	addi $a1, $a1, 1		# shift arg string left by 1
	addi $t2, $t2, 1		# increment length by 1
	lb $t6, 0($a1)			# load 1st char of arg into $t1
	beq $t6, '\0', insertType	# if $t1 = '\0', branch to insertType
	addi $a1, $a1, 1		# shift arg string left by 1
	addi $t2, $t2, 1		# increment length by 1
insertType:
	addi $t1, $zero, '0'		# store '0' at $t1
	beq $t2, 1, insertNums1		# if length = 1, branch to insertNums1
	beq $t2, 2, insertNums2		# if length = 2, branch to insertNums2
	beq $t2, 3, insertNums3		# if length = 3, branch to insertNums3
insertNums1:
	sb $t1, 1($t0)			# insert '0' at 2nd char of value1
	sb $t1, 2($t0)			# insert '0' at 3rd char of value1
	sb $t4, 3($t0)			# insert $t4 at 4th char of value1
	j nullTerminate			# jump to nullTerminate
insertNums2:
	sb $t1, 1($t0)			# insert '0' at 2nd char of value1
	sb $t4, 2($t0)			# insert $t4 at 3rd char of value1
	sb $t5, 3($t0)			# insert $t5 at 4th char of value1
	j nullTerminate			# jump to nullTerminate
insertNums3:
	sb $t4, 1($t0)			# insert $t4 at 2nd char of value1
	sb $t5, 2($t0)			# insert $t5 at 3rd char of value1
	sb $t6, 3($t0)			# insert $t6 at 4th char of value1
	j nullTerminate			# jump to nullTerminate
nullTerminate:
	add $t1, $zero, $zero		# store '\0' at $t1
	sb $t1, 4($t0)			# insert '\0' at 5th char of value1
	beq $t3, 0, modifyS1		# if loop counter = 1, branch to modifyS1
	j modifyS2			# else, jump to modifyS2
modifyS1:
	la $s1, value1			# set $s1 to value1
	lb $t0, 1($s2)			# store the first char of $s2 in $t0
	bne $t0, 'x', setup4		# if $t0 = 'x', branch to setup2
	j exit2				# else, jump to exit2
modifyS2:
	la $s2, value2			# set $s1 to value2
	j exit2				# jump to exit2
exit2:

# If an input is decimal, turn it into an integer and add it to $s0
# If an input is hexadecimal, it will remain the same for now

lb $t0, 0($s1)		# store the first char of $s1 in $t0
beq $t0, '-', setup5	# if $t0 = '-', branch to setup5
beq $t0, '+', setup5	# if $t0 = '+', branch to setup5
lb $t0, 0($s2)		# else, store the first char of $s2 in $t0
beq $t0, '-', setup6	# if $t0 = '-', branch to setup6
beq $t0, '+', setup6	# if $t0 = '+', branch to setup6	
j exit3			# else, jump to exit3
setup5:
	move $a1, $s1			# set subroutine arg to $s1
	lb $t0, 0($a1)			# let $t0 = 1st char of $s1 (polarity)
	addi $a1, $a1, 1		# shift $a1 left by 1
	addi $t1, $zero, 1000		# let $t1 = 1000 (powers of 10, loop counter)
	addi $t2, $zero, 0		# let $t2 = 0 (sum)
	addi $t5, $zero, 0		# let $t5 = 0 (loop counter)
	j toInt				# jump to toInt
setup6:
	move $a1, $s2			# set subroutine arg to $s2
	lb $t0, 0($a1)			# let $t0 = 1st char of $s2 (polarity)
	addi $a1, $a1, 1		# shift $a1 left by 1
	addi $t1, $zero, 1000		# let $t1 = 1000 (powers of 10, loop counter)
	addi $t2, $zero, 0		# let $t2 = 0 (sum)
	addi $t5, $zero, 1		# let $t5 = 1 (loop counter)
	j toInt				# jump to toInt
toInt:
	beq $t1, 1, checkSign2		# if power of 10 = 1, branch to checkNext2
	div $t1, $t1, 10		# power of 10 = power of 10 / 10
	lb $t3, 0($a1)			# load 1st char of arg into $t3
	addi $a1, $a1, 1		# shift arg left by 1
	subi $t3, $t3, 48		# subtract $t3 by 48 to obtain int equivalent
	mul $t4, $t3, $t1		# $t4 = int equivalent * power of 10
	add $t2, $t2, $t4		# add $t4 to sum
	j toInt				# jump to toInt
checkSign2:
	beq $t0, '-', negate		# if $t0 = '-', branch to negate
	beq $t5, 0, finalizeS1		# if second loop counter = 0, branch to finalizeS1
	j finalizeS2			# else, jump to finalizeS2
negate:
	mul $t2, $t2, -1		# negate the sum
	beq $t5, 0, finalizeS1		# if second loop counter = 0, branch to finalizeS1
	j finalizeS2			# else, jump to finalizeS2
finalizeS1:
	move $s1, $t2			# set $s1 to sum
	add $s0, $s0, $s1		# set $s0 = $s0 + $s1
	la $s1, value1			# set $s1 back to value1
	lb $t0, 0($s2)			# store the first char of $s2 in $t0
	beq $t0, '-', setup6		# if $t0 = '-', branch to setup6
	beq $t0, '+', setup6		# if $t0 = '+', branch to setup6
	j exit3				# else, jump to exit3
finalizeS2:
	move $s2, $t2			# set $s2 to sum
	add $s0, $s0, $s2		# set $s0 = $s0 + $s2
	la $s2, value2			# set $s2 back to value2
	j exit3				# jump to exit3
exit3:

# If an input is hex, turn it into an integer and add it to $s0

lb $t0, 1($s1)		# store the second char of $s1 in $t0
beq $t0, 'x', setup7	# if $t0 = 'x', branch to setup7
lb $t0, 1($s2)		# else, store the second char of $s2 in $t0
beq $t0, 'x', setup8	# if $t0 = 'x', branch to setup8
j exit4			# else, jump to exit4
setup7:
	move $t0, $s1		# set $t0 to $s1
	add $t0, $t0, 2		# trim prefix off of $t0
	addi $t1, $zero, 256	# let $t1 = 256 (loop counter)
	addi $t3, $zero, 0	# let $t3 = 0 (sum)
	addi $t4, $zero, 0	# let $t4 = 0 (invert boolean)
	addi $t5, $zero, 0	# let $t5 = 0 (second loop counter)
	j while			# jump to while
setup8:
	move $t0, $s2		# set $t0 to $s2
	add $t0, $t0, 2		# trim prefix off of $t0
	addi $t1, $zero, 256	# let $t1 = 256 (loop counter)
	addi $t3, $zero, 0	# let $t3 = 0 (sum)
	addi $t4, $zero, 0	# let $t4 = 0 (invert boolean)
	addi $t5, $zero, 1	# let $t5 = 1 (second loop counter)
	j while			# jump to while
while:
	beq $t1, 1, finish	# if $t1 = 1, branch to finish
	lb $t2, 0($t0)		# store first character of $t0 in $t2
	add $t0, $t0, 1		# shift $t0 left by 1
	div $t1, $t1, 16	# $t1 = $t1 / 16
	ble $t2, 57, toint1	# if $t2 <= '9', branch to toint1
	bge $t2, 65, toint2	# if $t2 >= 'A', branch to toint2
	toint1:
		sub $t2, $t2, 48	# subtract $t2 by 48, gives int equivalent
		bge $t4, 1, invert	# if $t4 >= 1, jump to invert
		bne $t1, 16, else	# if $t1 != 16, branch to else
		blt $t2, 8, else	# if $t2 < 8, branch to else
		j invert		# else, jump to invert
		else:
			j sum		# jump to sum
	toint2:
		sub $t2, $t2, 55	# subtract $t2 by 55, gives int equivalent
		bge $t4, 1, invert	# if $t4 >= 1, jump to invert
		beq $t1, 16, invert	# if $t1 = 16, branch to invert
		j sum			# else, jump to sum
	sum:
		mul $t2, $t1, $t2	# set $t2 to counter * int value
		bge $t4, 1, negateSum	# if $t4 >= 1, branch to negateSum
		add $t3, $t3, $t2	# $t3 = $t3 + $t2
		j while			# jump back to while
	negateSum:
		mul $t2, $t2, -1	# negate $t2
		add $t3, $t3, $t2	# $t3 = $t3 + $t2
		j while			# jump back to while
	invert:
		addi $t4, $t4, 1	# set $t4 to one if invert is visited
		addi $t2, $t2, -15	# subtract int value by 15
		mul $t2, $t2, -1	# multiply int value by -1
		beq $t1, 1, addone	# if counter = 1, branch to addone
		j sum			# jump back to sum
	addone:
		addi $t2, $t2, 1	# add one
		j sum			# jump back to sum
finish:
	add $s0, $s0, $t3	# let $s0 = $s0 + $t3
	beq $t5, 1, exit4	# if $t5 = 1, branch to exit4
	lb $t0, 1($s2)		# store the first char of $s2 in $t0
	beq $t0, 'x', setup8	# if $t0 = 'x', branch to setup8
	j exit4			# jump to exit4
exit4:

# At this point, every argument's integer equivalent has been added to $s0
# Print $s0 as a base 4 value

base4setup:
	li $v0, 4		# 4 = string print
	la $a0, output2
	syscall			# print "\nThe sum in base 4 is:\n"
	j base4			# jump to base4
base4:
	beq $s0, 0, printZero	# if $s0 = 0, branch to printZero
	blt $s0, 0, printNeg	# if $s0 < 0, branch to printNeg
	addi $t0, $zero, 4096	# let $t0 = 4096 (powers of 4, loop counter)
	addi $t3, $zero, 0	# let $t3 = 0 (print determination)
	while2:
		beq $t0, 1, done	# if power of 4 = 1, branch to done
		div $t0, $t0, 4		# power of 4 = (power of 4) / 4
		div $t1, $s0, $t0	# divide $s0 by power of 4, store in $t1
		bgt $t1, 0, notZero	# if $t1 > 0, branch to notZero
		j next			# else, jump to next
	next:
		mul $t2, $t1, $t0	# set $t2 to ($t1 * $t0)
		sub $s0, $s0, $t2	# subtract $s0 by $t2 to get remainder
		add $t1, $t1, 48	# add 48 to $t1 to get ASCII value
		bne $t1, '0', print	# if $t1 != '0', branch to print
		bgt $t3, 0, print	# if $t3 > 0, branch to print
		j while2		# jump back to while2
	printNeg:
		li $v0, 11		# 11 = character print
		la $a0, '-'
		syscall			# print a negative sign
		mul $s0, $s0, -1	# turn $s0 to positive
		j base4			# jump to base4
	printZero:
		li $v0, 11		# 11 = character print
		la $a0, '0'		# print a zero
		syscall
		j done			# jump to done
	notZero:
		addi $t3, $t3, 1	# increment $t3 by 1
		j next			# jump to next
	print:
		li $v0, 11		# 11 = character print
		move $a0, $t1
		syscall			# print the character in $t1
		j while2		# jump back to while2
done:
	li $v0, 10		# 10 = program end
	syscall			# end the program
