.data
nofile: .asciiz "No secondary file found. Exiting...\n"
output: .asciiz "\nHere is the encrypted and decrypted string\n"
encrypted: .asciiz "<Encrypted> "
decrypted: .asciiz "<Decrypted> "
invalid: .asciiz "\nInvalid input: Please input E, D, or X. "
input: .byte 1				# user character input
array1: .space 100			# key string array
array2: .space 100			# user string array input
array3: .space 100			# encrypted/decrypted string array

# Extra Credit Variables
extracredit: .asciiz "\nExtra Credit\n"
cipherprint: .asciiz "<Cipher>\n"
ciphertext: .asciiz "<Cipher Text> "
nullcipher: .asciiz "Input cannot be represented using the railfence cipher"
array4: .space 100			# uppercase string w/o punctuation (char_amount)
array5: .space 100			# railfence cipher text

.text

# This is included so that the file does not assemble with an error on its own.
la $a0, nofile	# load the string "No secondary file found. Exiting...\n"
li $a1, 3	# load a value of 3 into $a1

#########################################################################################
# give_prompt										#
#											#
# This function should print the string in $a0 to the user, store the user�s input in	#
# an array, and return the address of that array in $v0. Use the prompt number in $a1	#
# to determine which array to store the user�s input in. ?Include error checking? for	#
# the first prompt to see if user input E, D, or X if not print error message and ask	#
# again.										#
#											#
# arguments:  $a0 - address of string prompt to be printed to user			#
#             $a1 - prompt number (0, 1, or 2)						#
#											#
# note:		prompt 0: Do you want to (E)ncrypt, (D)ecrypt, or e(X)it?		#
#		prompt 1: What is the key?						#
#		prompt 2: What is the string?						#
#											#
# return:     $v0 - address of the corresponding user input data			#
#########################################################################################
give_prompt:
	li $v0, 4
	syscall			# print $a0 (prompt 0, 1, or 2)
	beq $a1, 0, p0		# if $a1 = 0, branch to p0
	beq $a1, 1, p1		# if $a1 = 1, branch to p1
	beq $a1, 2, p2		# if $a1 = 2, branch to p2
	beq $a1, 3, nofile_exit	# if $a1 = 3, branch to nofile_exit
	
	p0:
		li $v0, 12		# store user character input in $v0
		syscall
		beq $v0, 69, correct	# if input = 'E', branch to correct
		beq $v0, 68, correct	# if input = 'D', branch to correct
		beq $v0, 88, correct	# if input = 'X', branch to correct
		li $v0, 4		# else, print invalid string,
		la $a0, invalid
		syscall
		j p0			# and jump back up to prompt0 routine
	
	correct:
		sb $v0, input		# store user input in input
		li $v0, 11
		la $a0, 10		# ASCII value 10 = \n
		syscall			# print new line
		la $v0, input		# set $v0 to user char input
		jr $ra			# jump to return address (line 36 of Lab5Main)

	p1:
		li $v0, 8		# store user string input in $v0
		la $a0, array1		# store input in array1
		li $a1, 100		# maximum input: 100 characters
		syscall
		la $v0, array1		# set $v0 to address of key string array
		jr $ra			# jump to return address (line 44 of Lab5Main)

	p2:
		li $v0, 8		# store user string input in $v0
		la $a0, array2		# store input in array2
		li $a1, 100		# maximum input: 100 characters
		syscall
		la $v0, array2		# set $v0 to address of user input string
		jr $ra			# jump to return address (line 49 of Lab5Main)

#########################################################################################
# cipher										#
#											#
# Calls compute_checksum and encrypt or decrypt depending on if the user input E or	#
# D. The numerical key from compute_checksum is passed into either encrypt or decrypt	#
#											#
# note: this should call compute_checksum and then either encrypt or decrypt		#
#											#
# arguments:  $a0 - address of E or D character						#
#             $a1 - address of key string						#
#             $a2 - address of user input string					#
#											#
# return:     $v0 - address of resulting encrypted/decrypted string			#
#########################################################################################
cipher:
	sw $ra, ($sp)		# push the current return address to the stack
	subi $sp, $sp, 4	# move down the stack
	move $a0, $a1		# let $a0 contain address of key string
	lb $v0, ($a0)		# set $v0 to first char in key string
	jal compute_checksum	# jump and link to compute_checksum
	la $t1, input		# set $t1 to address of character input
	lb $a0, ($t1)		# set $a0 to E, D character
	move $a1, $v0		# set $a1 to checksum result
	la $a3, array3		# set $a3 to array3
	beq $a0, 69, encrypting	# if $a0 = 'E', branch to encrypting
	beq $a0, 68, decrypting	# if $a0 = 'D', branch to decrypting
	
	encrypting:
		lb $a0, ($a2)			# store the character to encrypt in $a0
		addi $a2, $a2, 1		# increment user input string by 1
		beq $a0, 10, finish		# if $a0 = '\n', branch to finish
		jal encrypt			# else, jump and link to encrypt,
		sb $v0, ($a3)			# load encrypted char into $a3,
		addi $a3, $a3, 1		# increment encrypted string by 1,
		j encrypting			# and jump back to top of this loop
	
	decrypting:
		lb $a0, ($a2)			# store the character to decrypt in $a0
		addi $a2, $a2, 1		# increment user input string by 1
		beq $a0, 10, finish		# if $a0 = '\n', branch to finish
		jal decrypt			# else, jump and link to decrypt,
		sb $v0, ($a3)			# load decrypted char into $a3,
		addi $a3, $a3, 1		# increment decrypted string by 1,
		j decrypting			# and jump back to top of this loop

	finish:
		addi $t1, $zero, 0		# let $t0 = 0 ('\0')
		sb $t1, ($a3)			# null-terminate $a3 (so it is a string)
		la $v0, array3			# set $v0 to array3
		addi $sp, $sp, 4		# move up the stack
		lw $ra, ($sp)			# load the return address in the stack
		jr $ra				# jump to $ra (Line 60 of Lab5Main)

#########################################################################################
# compute_checksum									#
#											#
# Computes the checksum by xor�ing each character in the key together. Then,		#
# use mod 26 in order to return a value between 0 and 25.				#
#											#
# arguments:  $a0 - address of key string						#
#											#
# return:     $v0 - numerical checksum result (value should be between 0 - 25)		#
#########################################################################################
compute_checksum:
	addi $a0, $a0, 1	# increment key string array by 1
	lb $t1, ($a0)		# set $t1 to next character in key string array
	beq $t1, 10, done	# if $t1 = '\n', jump to done
	xor $v0, $v0, $t1	# XOR $v0 with $t1
	j compute_checksum	# else, jump back to the top of this loop
	done:
		div $v0, $v0, 26	# divide $v0 by 26 (remainder stored in mfhi)
		mfhi $v0		# set $v0 to remainder (mfhi)
		jr $ra			# jump to return address (Line 107)

#########################################################################################
# encrypt										#
#											#
# Uses a Caesar cipher to encrypt a character using the key returned from		#
# compute_checksum. This function should call check_ascii.				#
#											#
# arguments:  $a0 - character to encrypt						#
#             $a1 - checksum result							#
#											#
# return:     $v0 - encrypted character							#
#########################################################################################
encrypt:
	sw $ra, ($sp)			# push the current return address to the stack
	jal check_ascii			# jump to check_ascii
	beq $v0, 0, encrypt_upper	# if $v0 = 0, branch to encrypt_upper
	beq $v0, 1, encrypt_lower	# if $v0 = 1, branch to encrypt_lower
	move $v0, $a0			# else, set $v0 to $a0,
	lw $ra, ($sp)			# load the return address in the stack,
	jr $ra				# and jump to return address (Line 119)

	encrypt_upper:
		subi $t1, $a0, 65	# set $t1 to $a0 - 65 (alphabetical index)
		add $t1, $t1, $a1	# add $a1 to $t1 to get new alphabetical index
		div $t1, $t1, 26	# divide $t1 by 26
		mfhi $t1		# set $t1 to remainder of divison
		addi $t1, $t1, 65	# add 65 to $t1 to get new encrypted ASCII value
		move $v0, $t1		# set $v0 to $t1
		lw $ra, ($sp)		# load the return address in the stack
		jr $ra			# jump to return address (Line 119)

	encrypt_lower:
		subi $t1, $a0, 97	# set $t1 to $a0 - 97 (alphabetical index)
		add $t1, $t1, $a1	# add $a1 to $t1 to get new alphabetical index
		div $t1, $t1, 26	# divide $t1 by 26
		mfhi $t1		# set $t1 to remainder of division
		addi $t1, $t1, 97 	# add 96 to $t1 to get new encrypted ASCII value
		move $v0, $t1		# set $v0 to $t1
		lw $ra, ($sp)		# load the return address in the stack
		jr $ra			# jump to return address (Line 119)

#########################################################################################
# decrypt										#
#											#
# Uses a Caesar cipher to decrypt a character using the key returned from		#
# compute_checksum. This function should call check_ascii.				#
#											#
# arguments:  $a0 - character to decrypt						#
#             $a1 - checksum result							#
#											#
# return:     $v0 - decrypted character							#
#########################################################################################
decrypt:
	sw $ra, ($sp)			# push the current return address to the stack
	jal check_ascii			# jump and link to check_ascii
	beq $v0, 0, decrypt_upper	# if $v0 = 0, branch to decrypt_upper
	beq $v0, 1, decrypt_lower	# if $v0 = 1, branch to decrypt_lower
	move $v0, $a0			# else, set $v0 to $a0,
	lw $ra, ($sp)			# load the return address in the stack,
	jr $ra				# and jump to return address (Line 128)
	
	decrypt_upper:
		subi $t1, $a0, 65	# set $t1 to $a0 - 65 (alphabetical index)
		sub $t1, $t1, $a1	# subt $t1 by $a1 to get new alphabetical index
		blt $t1, 0, negIndex1	# if $t1 is negative, branch to negIndex1
		j posIndex1		# else, jump to posIndex1
		negIndex1:
			addi $t1, $t1, 26	# add 26 to $t1
			j continue1		# jump to continue1
		posIndex1:
			div $t1, $t1, 26	# divide $t1 by 26
			mfhi $t1		# set $t1 to remainder of division
			j continue1		# jump to continue1
		continue1:
			addi $t1, $t1, 65	# add 65 to $t1 to get new decrypted ASCII value
			move $v0, $t1		# set $v0 to $t1
			lw $ra, ($sp)		# load the return address in the stack
			jr $ra			# jump to return address (Line 128)

	decrypt_lower:
		subi $t1, $a0, 97	# set $t1 to $a0 - 97 (alphabetical index)
		sub $t1, $t1, $a1	# subt $t1 by $a1 to get new alphabetical index
		blt $t1, 0, negIndex2	# if $t1 is negative, branch to negIndex2
		j posIndex2		# else, jump to posIndex2
		negIndex2:
			addi $t1, $t1, 26	# add 26 to $t1
			j continue2		# jump to continue2
		posIndex2:
			div $t1, $t1, 26	# divide $t1 by 26
			mfhi $t1		# set $t1 to remainder of division
			j continue2		# jump to continue2
		continue2:
			addi $t1, $t1, 97	# add 97 to $t1 to get new decrypted ASCII value
			move $v0, $t1		# set $v0 to $t1
			lw $ra, ($sp)		# load the return address in the stack
			jr $ra			# jump to return address (Line 128)

#########################################################################################
# check_ascii										#
#											#
# This checks if a character is an uppercase letter, lowercase letter, or		#
# not a letter at all. Returns 0, 1, or -1 for each case, respectively.			#
#											#
# arguments:  $a0 - character to check							#
#											#
# return:     $v0 - 0 if uppercase, 1 if lowercase, -1 if not letter			#
#########################################################################################
check_ascii:
	bge $a0, 97, lowercheck		# if $a0 >= 'a', branch to lowercheck
	ble $a0, 90, uppercheck		# if $a0 <= 'Z', branch to uppercheck
	addi $v0, $zero, -1		# else, set $v0 = -1,
	jr $ra				# and jump to return address (Line 175/215)
	
	lowercheck:
		ble $a0, 122, lowercase		# if $a0 <= 'z', branch to lowercase
		addi $v0, $zero, -1		# else, set $v0 = -1,
		jr $ra				# and jump to return address (Line 175/215)

	uppercheck:
		bge $a0, 65, uppercase		# if $a0 >= 'A', branch to uppercase
		addi $v0, $zero, -1		# else, set $v0 = -1,
		jr $ra				# and jump to return address (Line 175/215)

	lowercase:
		addi $v0, $zero, 1		# set $v0 = 1,
		jr $ra				# and jump to return address (Line 175/215)

	uppercase:
		addi $v0, $zero, 0		# set $v0 = 0,
		jr $ra				# and jump to return address (Line 175/215)

#########################################################################################
# print_strings										#
#											#
# Determines if user input is the encrypted or decrypted string in order		#
# to print accordingly. Prints encrypted string and decrypted string.			#
# At the end, the program will jump to railfence.					#
#											#
# arguments:  $a0 - address of user input string to be printed				#
#             $a1 - address of resulting encrypted/decrypted string to be printed	#
#             $a2 - address of E or D character						#
#											#
# return:     prints to console								#
#########################################################################################
print_strings:
	sw $ra, ($sp)		# push the return address to the stack
	subi $sp, $sp, 4	# move down the stack
	move $t1, $a0		# move $a0 to $t1 so we can use $a0 to print things
	li $v0, 4		# print "\nHere is the encrypted and decrypted string\n"
	la $a0, output
	syscall
	move $t2, $a2		# set $t2 to $a2
	lb $a2, ($t2)		# set $a2 to the first char of $t2
	beq $a2, 69, output1	# if $a2 = 'E', branch to output1
	beq $a2, 68, output2	# if $a2 = 'D', branch to output2
	
	output1:
		la $a0, encrypted	# print "<Encrypted> "
		syscall
		move $a0, $a1		# print the encrypted string
		syscall
		li $v0, 11		# print new line
		la $a0, '\n'
		syscall
		li $v0, 4		# print "<Decrypted> "
		la $a0, decrypted
		syscall
		move $a0, $t1		# print the decrypted string
		syscall
		jal railfence		# jump and link to railfence
		addi $sp, $sp, 4	# move up the stack
		lw $ra, ($sp)		# load the return address
		jr $ra			# jump to return address (Line 70 of Lab5Main)
	
	output2:
		la $a0, encrypted	# print "<Encrypted> "
		syscall
		move $a0, $t1		# print the encrypted string
		syscall
		li $v0, 4		# print "<Decrypted> "
		la $a0, decrypted
		syscall
		move $a0, $a1		# print the decrypted string
		syscall
		li $v0, 11		# print new line
		la $a0, '\n'
		syscall
		move $a0, $a1		# set $a0 back to $a1 so railfence works
		jal railfence		# jump and link to railfence
		addi $sp, $sp, 4	# move up the stack
		lw $ra, ($sp)		# load the return address
		jr $ra			# jump to return address (Line 70 of Lab5Main)

#########################################################################################
# railfence										#
#											#
# Takes the decrypted string and performs the rail fence cipher on it. This will call	#
# counter_size to determine loop amounts for each rail, then perform loops. It will 	#
# store	the successful cipher in array4. At the end, it will call print_cipher.		#
#											#
# arguments: 	$a0 - address of decrypted string					#
#											#
# returns:	None									#
#########################################################################################
railfence:
	sw $ra, ($sp)		# store the return address in the stack
	subi $sp, $sp, 4	# move down the stack
	addi $v0, $zero, 0	# set $v0 to 0
	la $a1, array4		# let $a1 = array4
	jal char_amount		# jump and link to char_amount
	move $a1, $v0		# store char amount in $a1
	li $a2, 1		# set $a2 to 1
	jal counter_size	# jump and link to counter_size
	move $t1, $v0		# set $t1 to loop amount
	move $t2, $t1		# set $t2 to $t1 (for print_cipher)
	la $t0, array4		# let $t0 = array4
	lb $a0, ($t0)		# let $a0 = first char of array4
	la $a3, array5		# let $a3 = array5
	j loop1			# jump to loop1
	
	loop1:
		beq $t1, 0, loop1_end		# if counter = 0, exit loop1
		sb $a0, ($a3)			# insert char at array4 pos at array5 pos
		addi $t0, $t0, 4		# increment array4 pos by 4
		lb $a0, ($t0)			# let $a0 = char at $t0
		addi $a3, $a3, 1		# increment array5 pos by 1
		subi $t1, $t1, 1		# decrement $t1 by 1
		j loop1				# jump to loop1

	loop1_end:
		la $t0, array4			# set $t0 to array4
		addi $t0, $t0, 1		# set $t0 to 2nd pos in array4
		lb $a0, ($t0)			# set $a0 to 2nd char of array4
		li $a2, 2			# set $a2 to 2
		jal counter_size		# jump and link to counter_size
		move $t1, $v0			# set $t1 to loop amount
		move $t3, $t1			# set $t3 to $t1 (for print_cipher)
		j loop2				# jump to loop2

	loop2:
		beq $t1, 0, loop2_end		# if counter = 0, exit loop2
		sb $a0, ($a3)			# insert char at array4 pos at array5 pos
		addi $t0, $t0, 2		# increment array4 pos by 2
		lb $a0, ($t0)			# let $a0 = char at $t0
		addi $a3, $a3, 1		# increment array5 pos by 1
		subi $t1, $t1, 1		# decrement $t1 by 1
		j loop2				# jump to loop2

	loop2_end:
		la $t0, array4			# set $t0 to array4
		addi $t0, $t0, 2		# set $t0 to 3rd pos in array4
		lb $a0, ($t0)			# set $a0 to 3rd char of array4
		li $a2, 3			# set $a2 to 3
		jal counter_size		# jump and link to counter_size
		move $t1, $v0			# set $t1 to loop amount
		move $t4, $t1			# set $t4 to $t1 (for print_cipher)
		j loop3				# jump to loop3

	loop3:
		beq $t1, 0, loop3_end		# if counter = 0, exit loop3
		sb $a0, ($a3)			# insert char at array4 pos at array5 pos
		addi $t0, $t0, 4		# increment array4 pos by 4
		lb $a0, ($t0)			# let $a0 = char at $t0
		addi $a3, $a3, 1		# increment array5 pos by 1
		subi $t1, $t1, 1		# decrement $t1 by 1
		j loop3				# jump to loop3

	loop3_end:
		sb $t1, ($a3)			# null-terminate array5 ($t1 should be 0)
		la $a0, array5			# set $a0 to address of array5
		jal print_cipher		# jump to print_cipher
		addi $sp, $sp, 4		# move up the stack
		lw $ra, ($sp)			# load the return address (print_strings)
		jr $ra				# jump to return address (Line 330/349)

#########################################################################################
# char_amount										#
#											#
# Determines the number of alphabetical characters in the decrypted string and		#
# returns it, and stores the cipher text in array4.					#
#											#
# arguments:	$a0 - address of decrypted string					#
#		$a1 - address of array4							#
#											#
# returns:	$v0 - number of alphabetical characters					#
#########################################################################################
char_amount:
	lb $t1, ($a0)			# load current char of decrypted string into $t1
	beq $t1, 0, return		# if $t1 = '\0', branch to return
	beq $t1, 10, return		# if $t1 = '\n', branch to return
	bge $t1, 97, lower1		# else, if $t1 >= 'a', branch to lower1
	ble $t1, 90, upper1		# else, if $t1 <= 'Z', branch to upper1
	addi $a0, $a0, 1		# else, increment decrypted string by 1,
	j char_amount			# and jump to char_amount

	lower1:
		ble $t1, 122, convert		# if $t1 <= 'z', branch to convert
		addi $a0, $a0, 1		# else, increment decrypted string by 1,
		j char_amount			# and jump to char_amount

	upper1:
		bge $t1, 65, alpha		# if $t1 >= 'A', branch to alpha
		addi $a0, $a0, 1		# else, increment decrypted string by 1,
		j char_amount			# and jump to char_amount

	convert:
		subi $t1, $t1, 32		# subtract $t1 by 32 to get uppercase
		j alpha				# jump to alpha

	alpha:
		sb $t1, ($a1)			# store uppercase in array4
		addi $v0, $v0, 1		# increment $v0 by 1
		addi $a0, $a0, 1		# increment decrypted string by 1
		addi $a1, $a1, 1		# increment array4 by 1
		j char_amount			# jump to char_amount

	return:
		jr $ra				# jump to return address (Line 370)
		
#########################################################################################
# counter_size										#
#											#
# Takes the current loop value & character amount from railfence and determines the 	#
# number of loops that should occur, and returns it.					#
#											#
# arguments:	$a1 - character amount							#
#		$a2 - loop value							#
#											#
# returns:	$v0 - loop amount for argument loop value				#
#########################################################################################
counter_size:
	beq $a2, 1, loopsize1		# if $a2 = 1, branch to loopsize1
	beq $a2, 2, loopsize2		# if $a2 = 2, branch to loopsize2
	beq $a2, 3, loopsize3		# if $a2 = 3, branch to loopsize3

	loopsize1:
		div $v0, $a1, 4		# set $v0 = character amount / 4
		mfhi $t1		# set $t1 = remainder of division
		bne $t1, 0, increment	# if $t1 != 0, branch to increment
		jr $ra			# jump to return address (Line 372)

	loopsize2:
		div $v0, $a1, 2		# set $v0 = character amount / 2
		jr $ra			# jump to return address (Line 372)

	loopsize3:
		subi $a1, $a1, 2	# let $a1 = character amount - 2
		div $v0, $a1, 4		# set $v0 = character amount / 4
		addi $a1, $a1, 2	# restore $a1
		mfhi $t1		# set $t1 = remainder of division
		bne $t1, 0, increment	# if $t1 != 0, branch to increment
		jr $ra			# else, jump to return address (Line 372)

	increment:
		addi $v0, $v0, 1	# increment $v0 by 1
		jr $ra			# jump to return address (Line 372)

#########################################################################################
# print_cipher										#
#											#
# Takes cipher text & loop values for each rail and outputs it in a railfence format.	#
#											#
# arguments:	$a0 - address of railfence cipher text					#
#		$a1 - character amount							#
#		$t2 - rail 1 loop value							#
#		$t3 - rail 2 loop value							#
#		$t4 - rail 3 loop value							#
#											#
# returns:	prints to console							#
#########################################################################################
print_cipher:
	move $t1, $a0			# move $a0 to $t1
	move $t5, $t2			# set $t5 to $t2
	move $t6, $t3			# set $t6 to $t3
	move $t7, $t4			# set $t7 to $t4
	li $v0, 4			# print "\nExtra Credit\n"
	la $a0, extracredit
	syscall
	la $a0, cipherprint		# print "<Cipher>\n"
	syscall
	lb $a0, ($t1)			# store the first char of $t1 in $a0
	beq $a0, 0, nullend		# if $a0 = '\0', branch to nullend	

	rail_1:
		li $v0, 11
		lb $a0, ($t1)		# print current char of cipher text
		syscall
		addi $t1, $t1, 1	# increment cipher text by 1
		subi $t2, $t2, 1	# decrement $t2 by 1
		beq $t2, 0, railend_1	# if $t2 = 0, branch to railend_1
		li $a0, 46		# set $a0 to '.'
		syscall			# print '.' three times
		syscall
		syscall
		j rail_1		# jump to rail_1

	railend_1:
		mul $t5, $t5, 4		# $t5 = $t5 * 4
		sub $t5, $a1, $t5	# $t5 = char amount - $t5
		addi $t5, $t5, 4	# $t5 = $t5 + 4
		subi $t5, $t5, 1	# $t5 = $t5 - 1

		rail1:			# print remaining periods, if any
			beq $t5, 0, pre1	# if $t5 = 0, branch to pre1
			li $a0, 46		# set $a0 to '.'
			syscall			# print '.'
			subi $t5, $t5, 1	# decrement $t5 by 1
			j rail1			# jump to rail1

	pre1:
		li $a0, 10		# print '\n'
		syscall
		li $a0, 46		# print '.' once
		syscall

	rail_2:
		lb $a0, ($t1)		# print current char of cipher text
		syscall
		addi $t1, $t1, 1	# increment cipher text by 1
		subi $t3, $t3, 1	# decrement $t3 by 1
		beq $t3, 0, railend_2	# if $t3 = 0, branch to railend_2
		li $a0, 46		# set $a0 to '.'
		syscall			# print '.' once
		j rail_2		# jump to rail_2

	railend_2:
		mul $t6, $t6, 2		# $t6 = $t6 * 2
		sub $t6, $a1, $t6	# $t6 = char amount - $t6
		
		rail2:			# print remaining periods, if any
			beq $t6, 0, pre2	# if $t6 = 0, branch to pre2
			li $a0, 46		# set $a0 to '.'
			syscall			# print '.'
			subi $t6, $t6, 1	# decrement $t6 by 1
			j rail2			# jump to rail2

	pre2:
		li $a0, 10		# print '\n'
		syscall
		li $a0, 46		# print '.' two times
		syscall
		syscall

	rail_3:
		lb $a0, ($t1)		# print current char of cipher text
		syscall
		addi $t1, $t1, 1	# increment cipher text by 1
		subi $t4, $t4, 1	# decrement $t4 by 1
		beq $t4, 0, railend_3	# if $t4 = 0, branch to railend_3
		li $a0, 46		# set $a0 to '.'
		syscall			# print '.' three times
		syscall
		syscall
		j rail_3		# jump to rail_3

	railend_3:
		mul $t7, $t7, 4		# $t7 = $t7 * 4
		sub $a1, $a1, 2		# subtract character amount by 2
		sub $t7, $a1, $t7	# $t7 = char amount - $t7
		addi $t7, $t7, 4	# $t7 = $t7 + 4
		subi $t7, $t7, 1	# $t7 = $t7 - 1

		rail3:			# print remaining periods, if any
			beq $t7, 0, end		# if $t7 = 0, branch to end
			li $a0, 46		# set $a0 to '.'
			syscall			# print '.'
			subi $t7, $t7, 1	# decrement $t7 by 1
			j rail3			# jump to rail3

	end:
		li $a0, 10		# print '\n'
		syscall
		li $v0, 4		# print "<Cipher Text> "
		la $a0, ciphertext
		syscall
		la $a0, array5		# print cipher text
		syscall
		li $v0, 11
		li $a0, 10		# print '\n'
		syscall
		jr $ra			# jump to return address (Line 431)

	nullend:
		li $v0, 4		# print nullcipher
		la $a0, nullcipher
		syscall
		li $v0, 11		# print '\n'
		li $a0, 10
		syscall
		li $v0, 4		# print "<Cipher Text> "
		la $a0, ciphertext
		syscall
		la $a0, nullcipher	# print nullcipher
		syscall
		li $v0, 11
		li $a0, 10		# print '\n'
		syscall
		jr $ra			# jump to return address (Line 431)

	nofile_exit:
		li $v0, 10
		syscall			# end the program if no second file present
