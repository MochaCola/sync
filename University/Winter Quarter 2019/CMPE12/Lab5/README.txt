------------------------
Lab 5: Subroutines
CMPE 012 Winter 2019

Blum, Kenny
kpblum
-------------------------
What was your design approach?
I worked through encryption/decryption by hand, on paper. Then I simply translated those operations
into assembly code. As for extra credit, I did the same, and tried to distinguish patterns of
operations that would help me.

What did you learn in this lab?
I learned about using the stack in assembly, as well as jumping back and forth between various
different subroutines. I also learned for the first time what a Caesar Cipher is, as well as
a railfence cipher!

Did you encounter any issues? Were there parts of this lab you found enjoyable?
I didn't run into many issues. The most notable problem I ran into was data carryover from previous
encryptions and/or decryptions, which was solved by adding a null terminator to the array. Generally
I found all of this lab to be enjoyable!

How would you redesign this lab to make it better?
I would give looser requirements for each of the subroutines. Being required to use specific
registers made things a little more complicated than I think it had to be. But overall, I think
this lab was very doable, and not in much need of improvement.

Did you collaborate with anyone on this lab? Please list who you collaborated with and the nature of your collaboration.
No, I didn't collaborate with anybody. The only person I really enlisted the help of was a friend of
mine who isn't even a CS/CE major. They just really like math and have a very logically-oriented
mind, and they were interested in hearing about my program, and they threw out ideas as to how I
might be able to fix some of the minor issues I was having. One of their ideas actually did work!