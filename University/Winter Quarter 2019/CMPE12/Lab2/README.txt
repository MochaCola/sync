------------------------
Lab 2: Whack-A-Mole
CMPE 012 Winter 2019

Blum, Kenny
kpblum
-------------------------

What were the learning objectives of this lab?

To understand the functionality of adders, registers, as well as how to create and use them. Also, this lab helped me to understand two's complement numbers better.

What was your design approach?

I started by creating a truth table and attempting to find what logical operations would yield these desirable 4-bit values for the four potential cases of the whack and mole. I was able to yield these values with the following logical operations:

Bit 0: ~(M and W) or W			Bits 1 - 3: ~(M and W) and W

From there, I created the 4-bit registry, and then created my adder. The result from the adder would be input into my registry, and the outputted values from the registry would be used in ensuing operations in my adder.

Did you encounter any issues?

Unfortunately, yes. It seems that my adder would continue to take outputs from the registry and continue to increase in value, even if I was not whacking. I mulled this over for many hours across multiple days but could not seem to find an alternative solution. :(

Were there parts of this lab you found enjoyable?

I enjoyed the truth table at the beginning the most, followed by creating the adder. I found those processes understandable, and the results were satisfying, for the most part.

How would you redesign this lab assignment to make it better?

I think it would have been better if we were given an example of a 4-bit registry, and how to withdraw bit values from it. Some extra instruction in that area may have resulted in me seeing this lab to 100% completion.