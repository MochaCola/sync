from pacai.util import reflection, counter
from pacai.agents.capture.reflex import ReflexCaptureAgent
from pacai.agents.base import BaseAgent
from pacai.agents.search.multiagent import MultiAgentSearchAgent
from pacai.core.distance import manhattan
from pacai.core.directions import Directions

import random

"""
Useful methods to (maybe) use:
    chooseAction(self, gameState)
    getAction(self, gameState)
    getFood(self, gameState)
    getFoodYouAreDefending(self, gameState)
    getCapsulesYouAreDefending(self, gameState)
    getOpponents(self, gameState)
    getTeam(self, gameState)
    getScore(self, gameState)
    getMazeDistance(self, pos1, pos2)
    getPreviousObservation(self)
    getCurrentObservation
    registerTeam(self, agentsOnTeam)
    observationFunction(self, state)
"""

i_count = 0
attackPrint = False
defendPrint = False

def getAttackFeatures(self, gameState, action):

    ###################################################
    # Used for understanding the state of agent modes
    global i_count, attackPrint, defendPrint

    if not attackPrint:
        if i_count != 0: print("Agent %d's defense lasted %d iterations" % (self.index, i_count))
        i_count = 0
        print("Agent %d is ATTACKING!" % self.index)
        attackPrint = True
        defendPrint = False
    
    i_count += 1
    ###################################################

    features = counter.Counter()
    successor = self.getSuccessor(gameState, action)
    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    features['successorScore'] = self.getScore(successor)

    # Compute distance to the nearest food.
    foodList = self.getFood(successor).asList()

    # This should always be True, but better safe than sorry.
    if (len(foodList) > 0):
        minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
        features['distanceToFood'] = minDistance

    """
    minDist = float("inf")
    for capsule in self.getCapsules(successor):
        minDist = min(minDist, manhattan(myPos, capsule))
    features['capsule'] = minDist
    """
    
    enemies = [gameState.getAgentState(i) for i in self.getOpponents(gameState)]
    closest = float("inf")

    for g in enemies:

        closest = min(manhattan(myPos, g.getPosition()), closest)

        # if closest == 0 and myState.isPacman():
        #     print("Attacker died! Defending now")
        #     self.defending = True
        #     break

        if closest <= 1:
            # To handle attacker ghost on our side
            # We want our agent to pursue attacker ghost if within 4 units
            if g.isBraveGhost() and not myState.isPacman():
                features["closestPacman"] = closest
            
            # To handle defender ghost on their side
            if g.isBraveGhost() and myState.isPacman():
                features["closestGhost"] = 1 / closest
    
    return features


def getDefendFeatures(self, gameState, action):

    ###################################################
    # Used for understanding the state of agent modes
    global i_count, attackPrint, defendPrint

    if not defendPrint:
        if i_count != 0: print("Agent %d's attack lasted %d iterations" % (self.index, i_count))
        i_count = 0
        print("Agent %d is DEFENDING!" % self.index)
        defendPrint = True
        attackPrint = False

    i_count += 1
    ###################################################

    features = counter.Counter()
    successor = self.getSuccessor(gameState, action)
    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    # Computes whether we're on defense (1) or offense (0).
    features['onDefense'] = 1
    if (myState.isPacman()):
        features['onDefense'] = 0

    # Computes distance to invaders we can see.
    enemies = [gameState.getAgentState(i) for i in self.getOpponents(gameState)]
    invaders = [a for a in enemies if a.isPacman()]
    features['numInvaders'] = len(invaders)

    # First focus on invaders if there are any
    if (len(invaders) > 0):
        closest_dist = float("inf")
        for pacman in invaders:
            invader_dist = self.getMazeDistance(myPos, pacman.getPosition())
            # if invader_dist == 0:
            #     self.defending = False
            #     break
            if closest_dist > invader_dist : # inf > dist
                closest_dist = invader_dist
        features['invaderDistance'] = closest_dist
    
    # Otherwise, seek to be close to the nearest enemy
    else:
        closest_dist = float("inf")
        for ghost in enemies:
            enemy_dist = self.getMazeDistance(myPos, ghost.getPosition())
            if  closest_dist > enemy_dist:
                closest_dist = enemy_dist
        features['enemyDistance'] = closest_dist

    # Punishment for stopping
    if (action == Directions.STOP):
        features['stop'] = 1

    return features


class PogAgent(ReflexCaptureAgent):
    def __init__(self, index, **kwargs):
        self.defending = False
        super().__init__(index)
    
    def evalFunc(self, gameState, action):
        features = self.getFeatures(gameState, action)
        return features * self.getWeights(gameState, action)
    
    def getFeatures(self, gameState, action):
        features = None
        if self.defending: features = getDefendFeatures(self, gameState, action)
        else: features = getAttackFeatures(self, gameState, action)
        return features
    
    def getWeights(self, gameState, action):
        weights = None
        if self.defending:
            weights = {
                'numInvaders': -1000,
                'onDefense': 100,
                'invaderDistance': -10,
                'enemyDistance': -15,
                'stop': -100,
            }
        elif not self.defending:
            weights = {
                'successorScore': 100,
                'distanceToFood': -3,
                'closestGhost': -1000,
                'closestPacman': -10,
                'capsule': -50
            }
        return weights



def createTeam(firstIndex, secondIndex, isRed,
        first = 'pacai.student.myTeam.PogAgent',
        second = 'pacai.student.myTeam.PogAgent'):
    """
    This function should return a list of two agents that will form the capture team,
    initialized using firstIndex and secondIndex as their agent indexed.
    isRed is True if the red team is being created,
    and will be False if the blue team is being created.
    """

    firstAgent = PogAgent
    secondAgent = PogAgent

    return [
        firstAgent(firstIndex),
        secondAgent(secondIndex),
    ]
