from pacai.agents.learning.reinforcement import ReinforcementAgent
from pacai.util import reflection, counter, probability
import random

class QLearningAgent(ReinforcementAgent):
    """
    I love Python! Using some clever list comprehension and conditional return statments,
    I made my code very compact! Minimum of two lines per method! (awesome geek moment)
    Note: Code may use variable names 's' & 'a' to represent state & action, respectively.

    getQValue - simply returns the value in counter q_values for state,action

    getAction - with probability epsilon, return a random action, otherwise follow policy

    getValue - retrieves q-values for each action using list comprehension
               maximum element of q-value list is return as long as actions are valid

    getPolicy - for each legal action in state, if q-value == state's value, put in list
                yields single-element list with best action, return element if actions valid

    update - adjusts q-value with "sample", using alpha-proportion learning rate
                    Q(s,a) = [a * (R + yV(s))] + [(1 - a) * Q(s, a)]
                                   (sample)       (current Q-value)
    """

    def __init__(self, index, **kwargs):
        super().__init__(index, **kwargs)

        self.q_values = counter.Counter()

    def getQValue(self, state, action):
        """ Get the Q-Value for a state & action.
        getQValue - simply returns the value in counter q_values for state,action
        """

        return self.q_values[(state, action)]

    def getAction(self, s):
        """
        Compute the action to take in the current state.
        getAction - with probability epsilon, return random action, else follow policy
        """

        prob = probability.flipCoin(self.getEpsilon())
        return random.choice(self.getLegalActions(s)) if prob else self.getPolicy(s)

    def getValue(self, state):
        """
        Return the value of the best action in a state.
        getValue - retrieves q-values for each action using list comprehension
                   maximum element of q-value list is return as long as actions are valid
        """

        acts = self.getLegalActions(state)
        return max([self.getQValue(state, action) for action in acts]) if acts else 0.0

    def getPolicy(self, s):
        """
        Return the best action in a state.

        getPolicy - for each legal action in state, if q-value == state's value, put in list
                    yields single-element list with best action, return element if actions valid
        """

        acts = self.getLegalActions(s)
        return [a for a in acts if self.getValue(s) == self.getQValue(s, a)][0] if acts else None

    def update(self, s, a, next, reward):
        """
        The parent class calls this to observe a state transition and reward.
        update - adjusts q-value with "sample", using alpha-proportion learning rate
                    Q(s,a) = [a * (R + yV(s))] + [(1 - a) * Q(s, a)]
                                   (sample)       (current Q-value)
        """

        learn = self.getAlpha() * (reward + self.getDiscountRate() * self.getValue(next))
        self.q_values[(s, a)] = learn + (1 - self.getAlpha()) * self.getQValue(s, a)

class PacmanQAgent(QLearningAgent):
    """
    Exactly the same as `QLearningAgent`, but with different default parameters.
    """

    def __init__(self, index, epsilon = 0.05, gamma = 0.8, alpha = 0.2, numTraining = 0, **kwargs):
        kwargs['epsilon'] = epsilon
        kwargs['gamma'] = gamma
        kwargs['alpha'] = alpha
        kwargs['numTraining'] = numTraining

        super().__init__(index, **kwargs)

    def getAction(self, state):
        """
        Simply calls the super getAction method and then informs the parent of an action for Pacman.
        Do not change or remove this method.
        """

        action = super().getAction(state)
        self.doAction(state, action)

        return action

class ApproximateQAgent(PacmanQAgent):
    """
    An approximate Q-learning agent.

    Here is a short description: (also found in the methods)

    - getQValue: Return the summation of the features multiplied by the weights.

    - update: For every feature, get the correction, then update the weight for the feature
              Variables s = state, a = action, r = reward
    """

    def __init__(self, index,
            extractor = 'pacai.core.featureExtractors.IdentityExtractor', **kwargs):
        super().__init__(index, **kwargs)
        self.featExtractor = reflection.qualifiedImport(extractor)
        self.weights = counter.Counter()

    def final(self, state):
        """
        Called at the end of each game.
        """

        # Call the super-class final method.
        super().final(state)

    def getQValue(self, state, action):
        """
        Return the summation of the features multiplied by the weights.
        """

        features = self.featExtractor.getFeatures(self.featExtractor, state, action)
        return sum([features[feat] * self.weights[feat] for feat in features])

    def update(self, s, a, next, r):
        """
        Variables s = state, a = action, r = reward
        For every feature, get the correction, then update the weight for the feature
        """

        features = self.featExtractor.getFeatures(self.featExtractor, s, a)
        for feature in features:
            correction = (r + self.getDiscountRate() * self.getValue(next)) - self.getQValue(s, a)
            self.weights[feature] += self.getAlpha() * correction * features[feature]
