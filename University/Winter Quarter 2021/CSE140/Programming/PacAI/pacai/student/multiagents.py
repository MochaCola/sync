import random

from pacai.agents.base import BaseAgent
from pacai.agents.search.multiagent import MultiAgentSearchAgent
from pacai.core.distance import manhattan, euclidean, maze
from pacai.core.directions import Directions

class ReflexAgent(BaseAgent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.
    You are welcome to change it in any way you see fit,
    so long as you don't touch the method headers.
    """

    def __init__(self, index, **kwargs):
        super().__init__(index)

    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        `ReflexAgent.getAction` chooses among the best options according to the evaluation function.

        Just like in the previous project, this method takes a
        `pacai.core.gamestate.AbstractGameState` and returns some value from
        `pacai.core.directions.Directions`.
        """

        # Collect legal moves.
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions.
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best.

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        The evaluation function takes in the current `pacai.bin.pacman.PacmanGameState`
        and an action, and returns a number, where higher numbers are better.

        Evaluation scores fit into the following categories:
        | Nearby 'brave' ghost  =  distance to nearest ghost
        | Nearby scared ghost  =  1000 / distance to nearest ghost
        | Stopped or trashing  =  -100
        | Food pellet  =  100
        | If none of the above, then:
        | | If getting closer to nearest food  =  50 / distance to nearest food
        | | If getting farther from nearest food  =  -50
        """

        # Useful information you can extract.
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        oldPos = currentGameState.getPacmanPosition()
        newPos = successorGameState.getPacmanPosition()
        oldFood = currentGameState.getFood().asList()
        newFood = successorGameState.getFood().asList()
        newGhosts = successorGameState.getGhostStates()

        # Get the lowest euclidean distance (with respect to pacman) of all ghosts on the grid
        ghost_distances = [manhattan(newPos, ghost._position) for ghost in newGhosts]
        min_ghost_distance = min(ghost_distances)
        min_ghost_index = ghost_distances.index(min_ghost_distance)

        # If pacman is trashing or not moving, GREATLY DE-MOTIVATE!
        if oldPos == newPos or action == Directions.STOP:
            eval = -100

        # If the nearest ghost is scared, EXTREMELY MOTIVATE pacman to chase!
        elif newGhosts[min_ghost_index].getScaredTimer() > 0:
            eval = 1000 / min_ghost_distance

        # If the closest 'brave' ghost is <= 3 units away from successor state, focus on avoidance
        elif min_ghost_distance <= 3:
            eval = min_ghost_distance

        # Otherwise, if ghosts are not within 3 units of pacman,
        else:
            # If the new position is a food pellet, GREATLY MOTIVATE!
            if newPos in oldFood and newPos not in newFood:
                eval = 100

            # If not food, determine the closest food
            else:
                closest = None
                for food_state in newFood:
                    dist = euclidean(newPos, food_state)
                    if closest is None or euclidean(newPos, closest) > dist:
                        closest = food_state

                new_proximity = maze(newPos, closest, successorGameState)
                old_proximity = maze(oldPos, closest, currentGameState)

                # If newPos is CLOSER (MAZE) to the CLOSEST FOOD than oldPos, motivate!
                if new_proximity < old_proximity:
                    eval = 50 / new_proximity

                # If newPos is FARTHER (MAZE) from the CLOSEST FOOD than oldPos, de-motivate!
                else:
                    eval = -50

            # Always consider the minimum ghost distance in the evaluation
            eval += min([euclidean(newPos, ghost._position) for ghost in newGhosts])

        return eval


class MinimaxAgent(MultiAgentSearchAgent):
    """
    A minimax agent.

    """

    def __init__(self, index, **kwargs):
        super().__init__(index, **kwargs)

    # Returns the minimax action from the current gameState using treeDepth
    def getAction(self, state):
        return self.minimax(state, self._treeDepth)

    # Returns the evaluation function
    def getEvaluationFunction(self):
        return self._evaluationFunction

    # Used to determine if the state is at an end state
    def is_terminal(self, state, depth):
        if depth == 0 or state.isWin() or state.isLose():
            return True
        return False

    # Used to remove DONE from the list of legal directions
    def remove_done(self, actions):
        if Directions.STOP in actions:
            del actions[actions.index(Directions.STOP)]
        return actions

    # The minimax algorithm
    def minimax(self, state, depth):

        # We want to return the best minimax value & action
        best_value = None
        best_action = None

        # Remove STOP from the list of legal actions
        legal_actions = self.remove_done(state.getLegalActions(0))

        # Iterate through the adjusted legal actions
        for action in legal_actions:

            # Get the value of successor, given action
            succ = state.generateSuccessor(0, action)

            # Get the maximum of the successor minimums
            new_value = self.max_value(succ, depth)

            # Update the best_value & best_action, if applicable
            if best_value is None or new_value > best_value:
                best_action = action
                best_value = new_value

        return best_action

    def max_value(self, state, depth):
        # If the game is at an end state, return utility
        if self.is_terminal(state, depth):
            return self.getEvaluationFunction()(state)

        # Otherwise, we seek to determine the maximum value
        max_value = None

        # Remove STOP from the list of legal actions for pacman
        legal_actions = self.remove_done(state.getLegalActions(0))

        # Iterate through the adjusted legal actions for pacman
        for action in legal_actions:

            # Get each of pacman's successors, given action
            succ = state.generateSuccessor(0, action)

            # For each successor, get all minimums for ghosts
            new_value = self.min_value(succ, depth, 1)

            # Update the max_value, if applicable
            if max_value is None or new_value > max_value:
                max_value = new_value

        return max_value

    def min_value(self, state, depth, agent):
        # If the game is at an end state, return utility
        if self.is_terminal(state, depth):
            return self.getEvaluationFunction()(state)

        # Otherwise, we seek to determine the minimum value
        min_value = None

        # Remove STOP from the list of legal actions
        legal_actions = self.remove_done(state.getLegalActions(agent))

        # Iterate through the adjusted legal actions
        for action in legal_actions:

            # Get the value of agent's successor, given action
            succ = state.generateSuccessor(agent, action)

            # Handling for multiple agents
            # If the agent is not the last agent,
            if agent < state.getNumAgents() - 1:

                # Get the minimum value per ghost agent
                new_value = self.min_value(succ, depth, agent + 1)

                # Update the min_value, if applicable
                if min_value is None or min_value > new_value:
                    min_value = new_value

            # Otherwise, if the ghost is the last agent,
            else:

                # Update the maximum value of the predecessor
                new_value = self.max_value(succ, depth - 1)

                # Update the min_value, if applicable
                if min_value is None or min_value > new_value:
                    min_value = new_value

        return min_value


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    A minimax agent with alpha-beta pruning.

    """

    def __init__(self, index, **kwargs):
        super().__init__(index, **kwargs)

    # Returns the minimax action from the current gameState using treeDepth
    def getAction(self, state):
        return self.alpha_beta_search(state, self._treeDepth)

    # Returns the evaluation function
    def getEvaluationFunction(self):
        return self._evaluationFunction

    # Used to determine if the state is at an end state
    def is_terminal(self, state, depth):
        if depth == 0 or state.isWin() or state.isLose():
            return True
        return False

    # Used to remove STOP from the list of legal directions
    def remove_done(self, actions):
        if Directions.STOP in actions:
            del actions[actions.index(Directions.STOP)]
        return actions

    # The minimax algorithm with alpha beta pruning
    def alpha_beta_search(self, state, depth):

        # We want to return the best minimax value & action
        best_value = None
        best_action = None

        # Remove STOP from the list of legal actions
        legal_actions = self.remove_done(state.getLegalActions(0))

        # Iterate through the adjusted legal actions
        for action in legal_actions:

            # Get the value of successor, given action
            succ = state.generateSuccessor(0, action)

            # Get the maximum of the successor
            new_value = self.max_value(succ, depth, float('-inf'), float('inf'))

            # Update the best_value & best_action
            if best_value is None or new_value > best_value:
                best_action = action
                best_value = new_value

        return best_action

    def max_value(self, state, depth, alpha, beta):
        # If the game is at an end state, return utility
        if self.is_terminal(state, depth):
            return self.getEvaluationFunction()(state)

        # Otherwise, we seek to determine the maximum value
        max_value = None

        # Remove STOP from the list of legal actions for pacman
        legal_actions = self.remove_done(state.getLegalActions(0))

        # Iterate through the adjusted legal actions for pacman
        for action in legal_actions:

            # Get each of pacman's successors, given action
            succ = state.generateSuccessor(0, action)

            # For each successor, get all minimums for ghosts
            new_value = self.min_value(succ, depth, 1, alpha, beta)

            # Update the max_value, if applicable
            if max_value is None or new_value > max_value:
                max_value = new_value

            # Alpha-Beta pruning
            alpha = max(alpha, new_value)
            if alpha >= beta:
                break

        return max_value

    def min_value(self, state, depth, agent, alpha, beta):
        # If the game is at an end state, return utility
        if self.is_terminal(state, depth):
            return self.getEvaluationFunction()(state)

        # Otherwise, we seek to determine the minimum value
        min_value = None

        # Remove STOP from the list of legal actions
        legal_actions = self.remove_done(state.getLegalActions(agent))

        # Iterate through the adjusted legal actions
        for action in legal_actions:

            # Get the value of agent's successor, given action
            succ = state.generateSuccessor(agent, action)

            # Handling for multiple agents
            # If the agent is not the last agent,
            if agent < state.getNumAgents() - 1:

                # Get the minimum value per ghost agent
                new_value = self.min_value(succ, depth, agent + 1, alpha, beta)

                # Update the min_value, if applicable
                if min_value is None or min_value > new_value:
                    min_value = new_value

            # Otherwise, if the ghost is the last agent,
            else:

                # Get the maximum value per ghost agent
                new_value = self.max_value(succ, depth - 1, alpha, beta)

                # Update the min_value, if applicable
                if min_value is None or min_value > new_value:
                    min_value = new_value

            # Alpha-Beta pruning
            beta = min(beta, new_value)
            if alpha >= beta:
                break

        return min_value


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
    An expectimax agent.
    All ghosts should be modeled as choosing uniformly at random from their legal moves.
    """

    def __init__(self, index, **kwargs):
        super().__init__(index, **kwargs)

    # Returns the minimax action from the current gameState using treeDepth
    def getAction(self, state):
        # We want to return the best minimax value & action
        best_value = None
        best_action = None

        # Remove STOP from the list of legal actions
        legal_actions = self.remove_done(state.getLegalActions(0))

        # For each available action
        for action in legal_actions:
            # Generate the successor for the action, and get its value
            successor = state.generateSuccessor(0, action)
            new_value = self.expectimax_search(successor, self.getTreeDepth(), 1)

            # Update the value / score if applicable
            if best_value is None or new_value > best_value:
                best_value = new_value
                best_action = action

        return best_action

    # Returns the evaluation function
    def getEvaluationFunction(self):
        return self._evaluationFunction

    # Used to determine if the state is at an end state
    def is_terminal(self, state, depth):
        if depth == 0 or state.isWin() or state.isLose():
            return True
        return False

    # Used to remove STOP from the list of legal directions
    def remove_done(self, actions):
        if Directions.STOP in actions:
            del actions[actions.index(Directions.STOP)]
        return actions

    # The minimax algorithm with alpha beta pruning
    def expectimax_search(self, state, depth, agent):
        # If the game is at an end state, return utility
        if self.is_terminal(state, depth):
            return self.getEvaluationFunction()(state)

        # Remove STOP from the list of legal actions
        legal_actions = self.remove_done(state.getLegalActions(agent))

        # We want to return the best expectimax value
        best_value = None

        # If the agent is not the first, (so, if agent is a ghost)
        if agent != 0:

            # For each available action
            for action in legal_actions:
                # Generate the successor for the action, and get its value
                succ = state.generateSuccessor(agent, action)

                # Update the agent value - increase until end, then return to pacman
                new_agent = agent
                if agent != state.getNumAgents() - 1:
                    new_agent += 1
                else:
                    new_agent = 0

                # Get the expectimax value for each agent
                new_value = self.expectimax_search(succ, depth - 1, new_agent)

                # Update the value
                if best_value is None:
                    best_value = new_value
                else:
                    best_value += new_value

            # Return the average of the values
            return best_value / len(legal_actions)

        # Otherwise, if agent is the first (so, if agent is pacman)
        else:

            # For each available action
            for action in legal_actions:
                # Generate the successor for the action, and get its value
                succ = state.generateSuccessor(agent, action)

                # Get the expectimax value for each agent after pacman
                new_value = self.expectimax_search(succ, depth - 1, agent + 1)

                # Update the best_value, if applicable
                if best_value is None or best_value < new_value:
                    best_value = new_value

            return best_value


def betterEvaluationFunction(currentGameState):
    """
    The evaluation function takes in the current PacmanGameState
    and returns a number, where higher numbers are better.

    Evaluation scores fit into the following categories:
    | Nearby 'brave' ghost  =  distance to nearest ghost
    | Nearby scared ghost  =  1000 / distance to nearest ghost
    | Stopped or trashing  =  -100
    | Food pellet  =  100
    | If none of the above, then:
    | | If getting closer to nearest food  =  50 / distance to nearest food
    | | If getting farther from nearest food  =  -50
    """

    # Useful information to extract
    currPos = currentGameState.getPacmanPosition()
    currFood = currentGameState.getFood().asList()
    currGhosts = currentGameState.getGhostStates()

    # Get the lowest manhattan distance (with respect to pacman) of all ghosts on the grid
    ghost_distances = [manhattan(currPos, ghost._position) for ghost in currGhosts]
    min_ghost_distance = min(ghost_distances)
    min_ghost_index = ghost_distances.index(min_ghost_distance)

    # Else if the nearest ghost is scared, EXTREMELY MOTIVATE pacman to chase!
    if currGhosts[min_ghost_index].getScaredTimer() > 0:
        eval = (1000 / min_ghost_distance)

    # If the closest 'brave' ghost is <= 3 units away from successor state, focus on avoidance
    elif min_ghost_distance <= 3:
        eval = min_ghost_distance

    # Otherwise, if ghosts are not within 3 units of pacman,
    else:
        # If the new position is a food pellet, GREATLY MOTIVATE!
        if currPos in currFood:
            eval = 100

        # If not food, determine the closest food
        else:
            closest = None
            for food_state in currFood:
                dist = manhattan(currPos, food_state)
                if closest is None or manhattan(currPos, closest) > dist:
                    closest = food_state

            if closest is not None:
                curr_proximity = maze(currPos, closest, currentGameState)
            else:
                curr_proximity = 1

            # Motivate towards the closest food!
            eval = 50 / curr_proximity

        # Always consider the minimum ghost distance in the evaluation
        eval += min([manhattan(currPos, ghost._position) for ghost in currGhosts])

    return eval

class ContestAgent(MultiAgentSearchAgent):
    """
    Your agent for the mini-contest.

    You can use any method you want and search to any depth you want.
    Just remember that the mini-contest is timed, so you have to trade off speed and computation.

    Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
    just make a beeline straight towards Pacman (or away if they're scared!)

    Method to Implement:

    `pacai.agents.base.BaseAgent.getAction`
    """

    def __init__(self, index, **kwargs):
        super().__init__(index)
