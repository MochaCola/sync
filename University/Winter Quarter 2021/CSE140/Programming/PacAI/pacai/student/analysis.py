"""
Analysis question.
Change these default values to obtain the specified policies through value iteration.
If any question is not possible, return just the constant NOT_POSSIBLE:
```
return NOT_POSSIBLE
```
"""

NOT_POSSIBLE = None

def question2():
    """
    With a discount of 0.8, by lowering the noise to zero, the agent will
    never fall off a cliff and will instead go straight for the end!

    This is because a noise of zero means no random actions - the agent
    will always follow the action it expects to take. Thus, the agent will
    never randomly experience a deduction of 100 - and so the Q-values for
    moving East and West will be unaffected by the Q-values for moving North
    or South. Thus E&W directions will be high-valued while N&S actions will
    be very low-valued.
    """

    answerDiscount = 0.9
    answerNoise = 0.0

    return answerDiscount, answerNoise

def question3a():
    """
    With a discount of (0 < discount <= 0.3), a noise of 0, and a living
    reward of (-10 < living_reward <= 0), the agent will move towards
    the nearest positive reward.
    (Some limitations apply with the value combos of the ranges above.)

    By keeping the discount in the range (0 < discount <= 0.3), more
    distant rewards have very little effect on Q-values.
    If discount > 0.3, farther rewards will factor more into the Q-values,
    pulling the agent away from the nearest reward.

    By keeping the living reward in the range (-10 < living_reward <= 0),
    the agent is extra incentivized to seek out the nearest reward.
    If the living reward is changed positively, the agent will then be
    incentivized to seek the farthest / highest payoff reward.

    By keeping the noise at zero, the agent is not threatened by the cliffs.
    A non-zero noise would result in the agent taking the longest path to
    the reward of 1, in order to prevent huge negative payoff.
    """

    answerDiscount = 0.2
    answerNoise = 0.0
    answerLivingReward = 0.0

    return answerDiscount, answerNoise, answerLivingReward

def question3b():
    """
    With a discount of (0.1 <= d <= 0.3), a noise of 0.1, and a L_reward of
    (-3 < L_reward <= 0.1), the agent will move towards the closest reward,
    while taking the path which avoids the -10 cliffs.
    (Some limitations apply with the value combos of the ranges above.)

    By keeping the discount at (0.1 <= d <= 0.3), the values from neighboring
    states creep in just enough such that the values of the 10 reward do not
    motivate the agent closer to it. Instead, the agent is more motivated
    towards the reward of 1.

    By keeping the noise at 0.1, the agent is fearful enough of the
    cliffside that it will steer clear of it. If noise is adjusted
    higher, trashing begins to occur.

    A living reward is feasible, so long as it not exceedingly negative
    (would motivate off the cliff!), and so long as it is not greater than
    0.1 (if so, agent is more inclined towards the larger reward!)
    """

    answerDiscount = 0.3
    answerNoise = 0.1
    answerLivingReward = 0.0

    return answerDiscount, answerNoise, answerLivingReward

def question3c():
    """
    With a discount of (0.01 <= d < 0.9), a noise of 0, and a L_reward of
    (1 <= L_reward < 9), the agent will move to the 10 goal, risking cliffs.
    (Some limitations apply with the value combos of the ranges above.)

    By keeping the discount between [0.01, 0.9), the trickle-down of values
    from the 10 reward is more effective upon the agent than the trickle-down
    from the 10, avoiding the cliffs.

    By keeping the noise at zero, the agent is not threatened by the cliffs.
    A non-zero noise would result in the agent taking the longest path to
    the reward of 1, in order to prevent huge negative payoff.

    By keeping the living reward between (1 <= L_reward < 9), the values are
    not 'tipped over' into values which would prefer a farther path, or a path
    which favors immediate jumping off a cliff.
    """

    answerDiscount = 0.5
    answerNoise = 0.0
    answerLivingReward = 1.0

    return answerDiscount, answerNoise, answerLivingReward

def question3d():
    """
    With a discount of (0.3 <= d < 1.0), a noise of 0.1, and a L_reward of
    (0.1 <= L_reward < 0.8), the agent will move towards the 10 goal,
    while taking the path which avoids the -10 cliffs.
    (Some limitations apply with the value combos of the ranges above.)

    By keeping the discount at (0.3 <= d <= 1.0), the values from neighboring
    states creep in such that the values of the 10 reward do, in fact,
    motivate the agent closer to it.

    By keeping the noise at 0.1, the agent is fearful enough of the
    cliffside that it will steer clear of it. If noise is adjusted
    higher, trashing begins to occur.

    A living reward from from [0.1, 0.8) is valid, as it gives some extra
    reinforcement such that the agent will take the longest path to 10.
    """

    answerDiscount = 0.8
    answerNoise = 0.1
    answerLivingReward = 0.1

    return answerDiscount, answerNoise, answerLivingReward

def question3e():
    """
    If we want the agent to simply avoid any terminal-states, then
    it seems plausible to simply give the agent a reward for living.
    With discount = 0, noise = 0, and living reward = 1, the agent
    will simply move upwards and stay put, completely avoiding all
    terminal states, cliffs included.
    """

    answerDiscount = 0.0
    answerNoise = 0.0
    answerLivingReward = 1.0

    return answerDiscount, answerNoise, answerLivingReward

def question6():
    """
    With no ability to give a variable such as "living reward", there is
    no easy way for an agent surrounded by death squares to reach the
    optimal path, unless many, many iterations were run.
    If a living reward were in place, epsilon would likely be zero,
    and alpha would be some non-zero value.
    """

    return NOT_POSSIBLE

if __name__ == '__main__':
    questions = [
        question2,
        question3a,
        question3b,
        question3c,
        question3d,
        question3e,
        question6,
    ]

    print('Answers to analysis questions:')
    for question in questions:
        response = question()
        print('    Question %-10s:\t%s' % (question.__name__, str(response)))
