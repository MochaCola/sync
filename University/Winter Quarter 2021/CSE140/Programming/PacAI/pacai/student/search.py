"""
In this file, you will implement generic search algorithms which are called by Pacman agents.
"""


class Path:
    """
    A class which represents a path from one node to another.
    Used primarily for organization of paths and options in search algorithms.
    """

    # Initialization parameters: starting state, list of directions, and cost.
    def __init__(self, state, dirs, cost):
        self.state = state
        self.dirs = dirs
        self.cost = cost

    # A string conversion method utilized for understanding program flow.
    def __str__(self):

        def reformat(msg):
            for sym in ['[', ']']:
                msg = msg.replace(sym, '')
            dirList = msg.split(', ')
            for index, d in enumerate(dirList):
                temp = d.replace('\'', '')
                dirList[index] = temp[0]
            return dirList

        msg = str(reformat(str(self.dirs)))
        if len(msg) > 140:
            msg = '...' + msg[len(msg) - 140:-1]
        msg = "{{ {:s}, {:.12f}, {:s} }}".format(str(self.state), self.cost, msg)
        return msg

    # Used to set the variables (state, direction, cost) for a path.
    def set_vars(self, state, dir, cost):
        self.state = state
        self.dirs.append(dir)
        self.cost += cost

# Removes already visited states from a list of successor states.
def remove_visited(ss, visited):
    result = list(ss)
    for item in ss:
        if item[0] in visited:
            result.remove(item)
    return result

# Adds a state to the fringe list.
def fringe_add(node, fringe):
    if node not in fringe:
        fringe.append(node)

# Removes a path object matching the state 'node' from the paths list.
def remove_paths(paths, node):
    temp = list(paths)
    for item in temp:
        if item.state == node:
            paths.remove(item)

# A print function for the paths list.
# Utilized to better understand the program during development.
def print_paths(paths):
    print(" | Paths:")
    for path in paths:
        print(" | | " + str(path))

def expand(problem, state, paths, fringe, visited):
    """
    A very important function!
    Function args: problem object, state node, paths list, fringe list, visited list
    Given a state node, this function will expand the node's successor states,
    and add / replace / duplicate them in the paths list, depending on the amount of
    unique successor states there are. Also altered: the fringe list, which is appended
    to for new, unvisited states. The visited list is not altered, but referenced.
    """

    # A function which simply prints the successor states.
    # Utilized to better understand the program during development.
    def ss_print(ss):
        msg = " | SS: [ "
        for succ in ss:
            msg = msg + "({:s}, {:s}, {:.12f}), ".format(str(succ[0]), str(succ[1]), succ[2])
        if len(ss) > 0:
            msg = msg[:-2] + ')'
        msg = msg + " ]"
        print(msg)

    # print(('-' * 190) + "\nExpanding node: %s" % str(state))

    ss = problem.successorStates(state)
    ss = remove_visited(ss, visited)

    # ss_print(ss)

    if paths == []:
        for node in ss:
            (nextState, dir, cost) = node
            fringe_add(nextState, fringe)
            paths.append(Path(nextState, [dir], cost))

    # If there are no successors, remove the path from paths
    elif len(ss) == 0:
        remove_paths(paths, state)

    # If there are one or more successors
    elif len(ss) >= 1:
        dupe = None
        (nextState, dir, cost) = ss[0]
        fringe_add(nextState, fringe)

        for path in paths:
            if state == path.state:
                if len(ss) >= 2:
                    dupe = Path(path.state, list(path.dirs), path.cost)
                path.set_vars(nextState, dir, cost)

        # For second & farther successors, add to fringe, create new path
        for item in ss[1:]:
            fringe_add(item[0], fringe)
            paths.append(Path(dupe.state, dupe.dirs[:], dupe.cost))
            paths[-1].set_vars(item[0], item[1], item[2])

# The first searching algorithm, depth first.
# A list is used for the fringe.
# Simply the .pop() method controls the order in which nodes are dealt with.
def depthFirstSearch(problem):

    paths = []
    fringe = [problem.startingState()]
    visited = []

    while(len(fringe) != 0):
        node = fringe.pop()
        visited.append(node)
        if problem.isGoal(node):
            for path in paths:
                if path.state == node:
                    return path.dirs
        else:
            expand(problem, node, paths, fringe, visited)

# The second searching algorithm, breadth first.
# A list is used for the fringe.
# Simply the .pip() method controls the order in which nodes are dealt with.
def breadthFirstSearch(problem):

    # Like .pop(), but removes the first item!
    def pip(aList):
        item = aList[0]
        aList.remove(item)
        return item

    paths = []
    fringe = [problem.startingState()]
    visited = []

    while(len(fringe) != 0):
        # print("Fringe: %s" % str(fringe))
        node = pip(fringe)
        visited.append(node)
        if problem.isGoal(node):
            for path in paths:
                if path.state == node:
                    return path.dirs
        else:
            expand(problem, node, paths, fringe, visited)
            # print_paths(paths)

# The third searching algorithm, uniform cost.
# A list is used for the fringe.
# Simply the .get_cheapest() method controls the order in which nodes are dealt with.
def uniformCostSearch(problem):

    # Returns the state in fringe with the cheapest cost for the associated path.
    def get_cheapest(paths, fringe):
        cheapest = None
        if paths[0].dirs == [None]:
            paths.remove(paths[0])
            cheapest = fringe[0]
            fringe.remove(cheapest)
            return cheapest
        for node in fringe:
            for path in paths:
                if path.state == node:
                    if cheapest is None or path.cost < cheapest.cost:
                        cheapest = path
        if cheapest is None:
            return None
        state = cheapest.state
        fringe.remove(state)
        return state

    paths = [Path(problem.startingState(), [None], 0)]
    fringe = [problem.startingState()]
    visited = []

    goalPath = None
    while(len(paths) != 0):

        if goalPath is not None:
            count = 0
            for path in paths:
                if goalPath.cost <= path.cost:
                    count += 1
            if count == len(paths):
                break

        if len(fringe) != 0:
            node = get_cheapest(paths, fringe)

        visited.append(node)
        if problem.isGoal(node):
            for path in paths:
                if path.state == node:
                    remove_paths(paths, node)
                    goalCost = problem.actionsCost(path.dirs)
                    if goalPath is None or goalCost < goalPath.cost:
                        goalPath = Path(node, path.dirs, goalCost)
        else:
            expand(problem, node, paths, fringe, visited)

    return goalPath.dirs

# The fourth searching algorithm, A*.
# A list is used for the fringe.
# Simply the .get_best() method controls the order in which nodes are dealt with.
def aStarSearch(problem, heuristic):

    # Returns the state in the fringe with the cheapest cost + heuristic for the associated path.
    def get_best(paths, fringe, heur):
        cheapest = None
        if paths[0].dirs == [None]:
            paths.remove(paths[0])
            cheapest = fringe[0]
            fringe.remove(cheapest)
            return cheapest
        for node in fringe:
            for path in paths:
                if path.state == node:
                    if cheapest is None:
                        cheapest = path
                    else:
                        val1 = path.cost + heur(path.state, problem)
                        val2 = cheapest.cost + heur(cheapest.state, problem)
                        if val1 < val2:
                            cheapest = path
        if cheapest is None:
            return None
        state = cheapest.state
        fringe.remove(state)
        return state

    paths = [Path(problem.startingState(), [None], 0)]
    fringe = [problem.startingState()]
    visited = []

    goalPath = None
    while(len(paths) != 0):

        if goalPath is not None:
            count = 0
            for path in paths:
                h_goal = goalPath.cost + heuristic(goalPath.state, problem)
                h_path = path.cost + heuristic(path.state, problem)
                if h_goal <= h_path:
                    count += 1
            if count == len(paths):
                break

        if len(fringe) != 0:
            node = get_best(paths, fringe, heuristic)

        visited.append(node)
        if problem.isGoal(node):
            for path in paths:
                if path.state == node:
                    remove_paths(paths, node)
                    goalCost = problem.actionsCost(path.dirs)
                    if goalPath is None or goalCost < goalPath.cost:
                        goalPath = Path(node, path.dirs, goalCost)
        else:
            expand(problem, node, paths, fringe, visited)

    return goalPath.dirs
