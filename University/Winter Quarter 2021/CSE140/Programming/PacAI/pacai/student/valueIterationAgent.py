from pacai.agents.learning.value import ValueEstimationAgent
from pacai.util import counter

class ValueIterationAgent(ValueEstimationAgent):
    """
    A value iteration agent.

    Make sure to read `pacai.agents.learning` before working on this class.

    A `ValueIterationAgent` takes a `pacai.core.mdp.MarkovDecisionProcess` on initialization,
    and runs value iteration for a given number of iterations using the supplied discount factor.
    """

    def __init__(self, index, mdp, discountRate = 0.9, iters = 100, **kwargs):
        super().__init__(index)

        self.mdp = mdp
        self.discount = discountRate
        self.iters = iters
        self.values = counter.Counter()  # A Counter (V0 = all zeros)
        self.policy = {}                 # A policy dictionary

        # We must iterate the number of times specified by the iters parameter
        states = self.mdp.getStates()

        # For each iteration, we will evaluate the states
        for iter in range(self.iters):
            # We will use a temporary Counter to store values for states
            temp_vals = counter.Counter()

            # And we will use a temporary Dict to store policy actions
            temp_policy = {}

            # Every iteration, we will evaluate all states
            for state in states:

                # Get the possible actions for the state
                actions = self.mdp.getPossibleActions(state)

                # For this state, we wish to find the best Q-val & action
                bestQ_val = float("-inf")
                bestAction = None

                for action in actions:

                    # For every action, calculate the state-action Q-value
                    Q_val = self.getQValue(state, action)

                    # Update the best Q-val, if applicable, for the state iteration
                    if Q_val > bestQ_val:
                        bestQ_val = Q_val
                        bestAction = action

                # Once all actions have been evaluated, store the value & policy action
                temp_vals[state] = 0 if bestQ_val == float("-inf") else bestQ_val
                temp_policy[state] = bestAction

            # Once all states have been evaluated, update self.values & self.policy
            for state in states:
                self.values[state] = temp_vals[state]
                self.policy[state] = temp_policy[state]

    def getValue(self, state):
        """
        Return the value of the state (computed in __init__).
        """

        return self.values[state]

    def getAction(self, state):
        """
        Returns the policy at the state (no exploration).
        """

        return self.getPolicy(state)

    def getPolicy(self, state):
        """
        The policy is the best action in the given state according to the values
        computed by value iteration. If there are no legal actions, which is the
        case at the terminal state, return None.
        """

        if state == 'TERMINAL_STATE':
            return None
        return self.policy[state]

    def getQValue(self, state, action):
        """
        The q-value of the state action pair (after the indicated
        number of value iteration passes).
        """

        # For the state & action, get the transition state & probability
        transitions = self.mdp.getTransitionStatesAndProbs(state, action)

        # We aim to calculate the state-action Q-value
        Q_val = 0

        # Get the next states & probabilities from the transitions
        for pair in transitions:
            (next, t_prob) = pair

            # Get the reward using (state-action-next)
            reward = self.mdp.getReward(state, action, pair[0])

            # The summation part of the Bellman Q-value Equation
            Q_val += t_prob * (reward + (self.discount * self.values[next]))

        return Q_val
