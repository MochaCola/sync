"""
This file contains incomplete versions of some agents that can be selected to control Pacman.
You will complete their implementations.

Good luck and happy searching!
"""

import logging

from pacai.core import distance
from pacai.core.distanceCalculator import Distancer
from pacai.student import search
from pacai.core.actions import Actions
from pacai.core.directions import Directions
from pacai.core.search.position import PositionSearchProblem
from pacai.core.search.problem import SearchProblem
from pacai.agents.base import BaseAgent
from pacai.agents.search.base import SearchAgent

class CornersProblem(SearchProblem):

    def __init__(self, startingGameState):

        super().__init__()

        self.walls = startingGameState.getWalls()
        self.startingPosition = startingGameState.getPacmanPosition()
        self.startingGameState = startingGameState
        top = self.walls.getHeight() - 2
        right = self.walls.getWidth() - 2

        self.corners = [(1, 1), (1, top), (right, 1), (right, top)]
        for corner in self.corners:
            if not startingGameState.hasFood(*corner):
                logging.warning('Warning: no food in corner ' + str(corner))

    def actionsCost(self, actions):

        if (actions is None):
            return 999999

        x, y = self.startingPosition
        for action in actions:
            dx, dy = Actions.directionToVector(action)
            x, y = int(x + dx), int(y + dy)
            if self.walls[x][y]:
                return 999999

        return len(actions)

    def startingState(self):
        # Empty represents whether the corners have been visited
        empty = [0, 0, 0, 0]
        return [self.startingPosition, empty[:]]

    def successorStates(self, state):

        successors = []

        for action in Directions.CARDINAL:
            x, y = state[0]
            dx, dy = Actions.directionToVector(action)
            nextx, nexty = int(x + dx), int(y + dy)

            if (not self.walls[nextx][nexty]):

                cornerVisits = state[1][:]
                nextState = [(nextx, nexty), cornerVisits]

                for index, corner in enumerate(self.corners):
                    if nextState[0] == corner and nextState[1][index] != 1:
                        nextState[1][index] = 1

                successors.append((nextState, action, 1))

        # Bookkeeping for display purposes (the highlight in the GUI).
        self._numExpanded += 1
        if state[0] not in self._visitedLocations:
            self._visitedLocations.add(state[0])
            self._visitHistory.append(state[0])

        return successors

    def isGoal(self, state):
        # If all four corners haven't been visited,
        if state[1] != [1, 1, 1, 1]:
            return False

        # Register the locations we have visited.
        # This allows the GUI to highlight them.
        self._visitedLocations.add(state[0])
        self._visitHistory.append(state[0])

        return True

def cornersHeuristic(state, problem):
    """
    This heuristic takes the average between the manhattan and euclidean heuristics
    in a chain of the remaining corners. 'futureState' begins as the starting state,
    then will change to represent the most recently referenced unvisited corner.

    In other words, this heuristic is an average of average heuristics (man & euc)
    for each corner, in a corner-to-corner style.

    Finally, this total average is multiplied by the remaining corners at the end.
    """

    avgHeurVal = 0.0
    foodCount = 0
    futureState = state[0]

    for index, value in enumerate(state[1]):
        if value == 0:
            foodCount += 1
            manhattan = distance.manhattan(futureState, problem.corners[index])
            euclidean = distance.euclidean(futureState, problem.corners[index])
            futureState = problem.corners[index]
            avgHeurVal += (manhattan + euclidean) / 2
        if foodCount != 0:
            avgHeurVal /= foodCount

    avgHeurVal += (avgHeurVal * foodCount)
    return avgHeurVal

def foodHeuristic(state, problem):
    """
    Heuristic incentive: For every (non-wall) space in the gameState, perform a
    maze distance search to every food. Then, store this information in
    heuristicInfo, so that BFS only has to occur an amount of times equivalent to:

            { non-wall spaces } * { amount of food in the world }

         ( for trickySearch, this would mean: ~780 BFS occurences )

    Future heuristic checks will simply access heuristicInfo to gather values.

    Funny note: This strategy was motivated by my original strategy, which
        utilized .maze (& subsequently, BFS) A WHOPPING 1,000,000+ TIMES
        across about 10 minutes.

        (I really had a lot of difficulty making anything work for this problem.) 
        (I wish I could have something better. But I spent two days on this one )
        (problem and I just can't dedicate any more time to it!  :/             )
    """

    position, foodGrid = state
    foodList = foodGrid.asList()
    walls = problem.walls

    # For logging purposes
    if 'searchCount' not in problem.heuristicInfo:
        problem.heuristicInfo['searchCount'] = 0

    # Needed in order to use the .maze function
    def getWalls():
        return problem.walls
    problem.getWalls = getWalls

    # If the walls have not yet been processed,
    # (this whole if statement only runs once)
    if 'DONE' not in problem.heuristicInfo:
        sequence = []
        for x, col in enumerate(walls):
            for y, wall in enumerate(col):
                if not wall:
                    sequence.append((x, y))

        # Use each non-wall space and measure the
        for (x, y) in sequence:
            for food in foodList:
                curr_state = (x, y)
                food_dist = 0
                if curr_state != food:
                    food_dist = distance.maze(curr_state, food, problem)
                problem.heuristicInfo['searchCount'] += 1
                key = "(" + str(x) + ", " + str(y) + ")->" + str(food)
                problem.heuristicInfo[key] = food_dist
        problem.heuristicInfo['DONE'] = True
        print("Search count: %d" % problem.heuristicInfo['searchCount'])

    # total_dist = 0
    # max_dist = 0
    food_distances = []
    for food in foodList:
        key = str(position) + "->" + str(food)
        # food_dist = problem.heuristicInfo[key]
        food_distances.append(problem.heuristicInfo[key])
        food_distances.sort()
        # total_dist += food_dist

        # if max_dist is None or food_dist > max_dist:
        #     max_dist = food_dist

    heurVal = 0
    # print("food distances: %s\n | " % str(food_distances), end="")
    # for dist in food_distances[len(food_distances)-5:len(food_distances)]:
    if len(food_distances) != 5:
        food_distances = food_distances[:5]
    for dist in food_distances:
        # print(str(dist) + ", ", end="")
        heurVal += dist
    # print()

    # avg_dist = 0
    # if foodGrid.count() != 0:
    #     avg_dist = total_dist / foodGrid.count()

    return heurVal
    # return (foodGrid.count() * avg_dist)
    # return total_dist

class ClosestDotSearchAgent(SearchAgent):
    """
    Search for all food using a sequence of searches.
    """

    def __init__(self, index, **kwargs):
        super().__init__(index)

    def registerInitialState(self, state):
        self._actions = []
        self._actionIndex = 0

        currentState = state

        while (currentState.getFood().count() > 0):
            nextPathSegment = self.findPathToClosestDot(currentState)
            self._actions += nextPathSegment

            for action in nextPathSegment:
                legal = currentState.getLegalActions()
                if action not in legal:
                    raise Exception('findPathToClosestDot returned an illegal move: %s!\n%s' %
                            (str(action), str(currentState)))

                currentState = currentState.generateSuccessor(0, action)

        logging.info('Path found with cost %d.' % len(self._actions))

    def findPathToClosestDot(self, gameState):
        """
        Returns a path (a list of actions) to the closest dot, starting from gameState.
        """

        position = gameState.getPacmanPosition()
        foodList = gameState.getFood().asList()

        for index, food in enumerate(foodList):

            food_dist = distance.manhattan(food, position)
            foodList[index] = [foodList[index], food_dist]

        if len(foodList) >= 5:
            foodList.sort(key=lambda x: x[1])
            foodList = foodList[0:5]

        min_path = None
        for food in foodList:

            food_search = AnyFoodSearchProblem(gameState, position, food[0])
            food_path = search.breadthFirstSearch(food_search)

            if min_path is None or len(food_path) < len(min_path):
                min_path = food_path

        return min_path

class AnyFoodSearchProblem(PositionSearchProblem):
    """
    A search problem for finding a path to any food.

    This search problem is just like the PositionSearchProblem,
    but has a different goal test, which you need to fill in below.
    The state space and successor function do not need to be changed.

    The class definition above, `AnyFoodSearchProblem(PositionSearchProblem)`,
    inherits the methods of `pacai.core.search.position.PositionSearchProblem`.

    You can use this search problem to help you fill in
    the `ClosestDotSearchAgent.findPathToClosestDot` method.

    Additional methods to implement:

    `pacai.core.search.position.PositionSearchProblem.isGoal`:
    The state is Pacman's position.
    Fill this in with a goal test that will complete the problem definition.
    """

    def __init__(self, gameState, start = None, foodState = None):
        super().__init__(gameState, goal = foodState, start = start)

        # Store the food for later reference.
        self.food = gameState.getFood()

    def isGoal(self, state):
        if state == self.goal:
            return True

        self._visitedLocations.add(state[0])
        self._visitHistory.append(state[0])

        return False

class ApproximateSearchAgent(BaseAgent):
    """
    Implement your contest entry here.

    Additional methods to implement:

    `pacai.agents.base.BaseAgent.getAction`:
    Get a `pacai.bin.pacman.PacmanGameState`
    and return a `pacai.core.directions.Directions`.

    `pacai.agents.base.BaseAgent.registerInitialState`:
    This method is called before any moves are made.
    """

    def __init__(self, index, **kwargs):
        super().__init__(index)
