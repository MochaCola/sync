import math
def quadratic_formula(a,b,c):
    x1 = (-b + math.sqrt(b**2 - 4*a*c)) / float(2*a)
    x2 = (-b - math.sqrt(b**2 - 4*a*c)) / float(2*a)
    return (x1,x2)

a = 2
b = -3
c = -77

vals = quadratic_formula(a,b,c)
print(vals)