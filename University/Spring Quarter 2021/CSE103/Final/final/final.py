
def my_range(start,stop,step):

   i = start         # begin at "start"
   while i < stop:   # increment in steps until i >= "stop"
      yield i           # yield as we go
      i += step         # increment by "step"

def interleave(g,h):

   elem_g = next(g)     # get the first elem in g
   elem_h = next(h)     # get the first elem in h

   while True:

      # if both elements are valid (not None)
      if (elem_g is not None and elem_h is not None):

         yield elem_g         # yield elem from g
         yield elem_h         # yield elem from h
         elem_g = next(g)     # progress g
         elem_h = next(h)     # progress h

      # otherwise, if one elem is None,
      else:

         if (elem_g is None):
            yield elem_h         # print the non-None element
            elem_h = next(h)     # progress while applicable
   
         if (elem_h is None):
            yield elem_g         # print the non-None element
            elem_g = next(g)     # progress while applicable



def enqueue(self, x):
   self.back.data = x     # just add the correct data to the back

def dequeue(self):
   temp = self.front.data           # keep front data for after
   self.front = self.front.next     # make front point to next node
   return temp                      # return data from original front


"""
Output:

in A : A
in B : in A : B
in C : in A : C
in D : in B : in C : in A : D

"""


def transpose(D):
   vertices = list(D.keys())                       # get vertex list
   for vertex in vertices:                         # iterate through them
      adjacency_list = D[vertex]                   # grab adjacency list for v
      for i, adj in enumerate(adjacency_list):     # iterate through adjacents
         D[vertex].pop(i)                          # remove adjacents from list by index
         D[adj].append(vertex)                     # append v to adj adjacency lists



def diameter(G):

   diameter = 0                                    # start diameter at 0              
   for start in G.vertices():                      # iterate for our source vertex
      G.BFS(start)                                 # run BFS from the source
      for vertex in G.vertices():                  # iterate for our other vertex
         dist = G.getDistance(vertex)              # get the distance from source to other
         if (diameter < dist): diameter = dist     # if the distance is greatest, update diameter
   return diameter                                 # return diameter