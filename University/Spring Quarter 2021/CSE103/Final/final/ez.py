class A(object):
   def __init__(self):
      self.my_type = type(self).__name__
   def __str__(self):
      return 'in A : ' + self.my_type

class B(A):
   def __str__(self):
      return 'in B : ' + super().__str__()

class C(A):
   def __str__(self):
      return 'in C : ' + super().__str__()

class D(B, C):
   def __str__(self):
      return 'in D : ' + super().__str__()

def main():
   a = A()
   b = B()
   c = C()
   d = D()

   print(a)
   print(b)
   print(c)
   print(d)

if __name__ == '__main__':
   main()