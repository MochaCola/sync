/************************************************************************
 * 
 * CSE130 Assignment 1
 * 
 * UNIX Shared Memory Multi-Process Merge Sort
 * 
 * Copyright (C) 2020-2021 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 *
 ************************************************************************/

#include "merge.h"
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/* 
 * Merge Sort in the current process a sub-array of ARR[] defined by the 
 * LEFT and RIGHT indexes.
 */
void singleProcessMergeSort(int arr[], int left, int right) 
{
  if (left < right) {
    int middle = (left+right)/2;
    singleProcessMergeSort(arr, left, middle); 
    singleProcessMergeSort(arr, middle+1, right); 
    merge(arr, left, middle, right); 
  } 
}

/* 
 * Merge Sort in the current process and at least one child process a 
 * sub-array of ARR[] defined by the LEFT and RIGHT indexes.
 */
void multiProcessMergeSort(int arr[], int left, int right) 
{
  /* Create shared memory - function referenced from:
  https://man7.org/linux/man-pages/man3/ftok.3.html
  https://man7.org/linux/man-pages/man2/shmget.2.html */
  key_t ftok(const char *pathname, int proj_id);
  int shmget(key_t key, size_t size, int shmflg);

  /* Remove shared memory (by name) - function referenced from:
  https://man7.org/linux/man-pages/man2/shmctl.2.html */
  int shmctl(int shmid, int cmd, struct shmid_ds *buf);

  /* Attach to shared memory - function referenced from:
  https://linux.die.net/man/2/shmat */
  void *shmat(int shmid, const void *shmaddr, int shmflg);

  /* Detach from shared memory (necessary only if you attached)
  https://linux.die.net/man/2/shmat */
  int shmdt(const void *shmaddr);


  int middle = (left+right)/2;      // Grab the middle index       

  // Create shared memory
  key_t ipc_key = ftok("shmseg", 65);
  size_t arr_size = (right + 1) * sizeof(int);
  int shmid = shmget(ipc_key, arr_size, 0666 | IPC_CREAT);
  
  // Attach to shared memory
  int *shm = (int*)shmat(shmid, (void*)0,0);

  // Copy all local memory into shared memory
  // WOULD BE BETTER: Copy RIGHT SIDE of local memory into shared memory
  memcpy(shm, arr, (right+1) * sizeof(int));

  // Fork
  switch (fork()) {

    // If error, exit
    case -1:
    
      exit(-1);

    // If child,
    case 0:

      // Attach to shared memory (optional, but good practice)
      shm = (int*)shmat(shmid, (void*)0,0);

      // Sort RIGHT SIDE of SHARED MEMORY
      singleProcessMergeSort(shm, middle+1, right);

      // Detach from shared memory
      shmdt(shm);
      
      // Exit
      exit(-1);
    
    // If parent,
    default:
      
      // Sort the LEFT SIDE of SHARED MEMORY
      // WOULD BE BETTER: Sort the LEFT SIDE of LOCAL MEMORY
      singleProcessMergeSort(shm, left, middle);

      // Wait for child to finish
      wait(NULL);

      // Copy shared memory to local memory
      // WOULD BE BETTER: Copy shared memory to RIGHT SIDE of local memory
      memcpy(arr, shm, (right+1) * sizeof(int));

      // Merge shared memory
      // WOULD BE BETTER: Merge shared memory AFTER DESTROYING
      merge(arr, left, middle, right);

      // Detach from shared memory
      shmdt(shm);

      // Destroy shared memory
      shmctl(shmid, IPC_RMID, NULL);

  }
  
}