/************************************************************************
 * 
 * CSE130 Assignment 1
 *  
 * POSIX Shared Memory Multi-Process Merge Sort
 * 
 * Copyright (C) 2020-2021 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 *
 ************************************************************************/

#include "merge.h"
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>

/* 
 * Merge Sort in the current process a sub-array of ARR[] defined by the 
 * LEFT and RIGHT indexes.
 */
void singleProcessMergeSort(int arr[], int left, int right) 
{
  if (left < right) {
    int middle = (left+right)/2;
    singleProcessMergeSort(arr, left, middle); 
    singleProcessMergeSort(arr, middle+1, right); 
    merge(arr, left, middle, right); 
  } 
}

/* 
 * Merge Sort in the current process and at least one child process a 
 * sub-array of ARR[] defined by the LEFT and RIGHT indexes.
 */
void multiProcessMergeSort(int arr[], int left, int right) 
{

  /* Create shared memory - function referenced from:
  https://man7.org/linux/man-pages/man3/shm_open.3.html */
  int shm_open(const char *name, int oflag, mode_t mode);

  /* Remove shared memory (by name) - function referenced from:
  https://man7.org/linux/man-pages/man3/shm_open.3.html */
  int shm_unlink(const char *name);

  /* Set the size of shared memory - function referenced from:
  https://linux.die.net/man/2/ftruncate */
  int ftruncate(int fd, off_t length);

  /* Attach to shared memory - function referenced from:
  https://man7.org/linux/man-pages/man2/mmap.2.html */
  void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);

  /* Detach from shared memory (necessary only if you attached)
  https://man7.org/linux/man-pages/man2/mmap.2.html */
  int munmap(void *addr, size_t length);

  /* Close file descriptor allocated by shm_open - function referenced from:
  https://linux.die.net/man/3/close */
  int close(int fd);

  /* Additionally, some references were made to a brief POSIX API
  written by GeeksforGeeks, at the weblink:
  https://www.geeksforgeeks.org/posix-shared-memory-api/ */


  int middle = (left+right)/2;      // Grab the middle index

  // Create shared memory
  const char* name = "sharedmem";
  int shm = shm_open(name, O_CREAT | O_RDWR, 0666);

  // Set the size of the shared memory
  const size_t SIZE = (right + 1) * sizeof(int);
  ftruncate(shm, SIZE);
  
  // Attach to shared memory
  void* shm_ptr = mmap(0, SIZE, PROT_WRITE, MAP_SHARED, shm, 0);

  // Copy all local memory into shared memory
  // WOULD BE BETTER: Copy RIGHT SIDE of local memory into shared memory
  memcpy(shm_ptr, arr, (right+1) * sizeof(int));

  // Fork
  switch (fork()) {

    // If error, exit
    case -1:
    
      exit(-1);

    // If child,
    case 0:

      // Attach to shared memory (optional, but good practice)
      shm_ptr = mmap(0, SIZE, PROT_WRITE, MAP_SHARED, shm, 0);

      // Sort RIGHT SIDE of SHARED MEMORY
      singleProcessMergeSort(shm_ptr, middle+1, right);

      // Detach from shared memory
      munmap(0, SIZE);
      
      // Exit
      exit(-1);
    
    // If parent,
    default:
      
      // Sort the LEFT SIDE of SHARED MEMORY
      // WOULD BE BETTER: Sort the LEFT SIDE of LOCAL MEMORY
      singleProcessMergeSort(shm_ptr, left, middle);

      // Wait for child to finish
      wait(NULL);

      // Copy shared memory to local memory
      // WOULD BE BETTER: Copy shared memory to RIGHT SIDE of local memory
      memcpy(arr, shm_ptr, (right+1) * sizeof(int));

      // Merge shared memory
      // WOULD BE BETTER: Merge shared memory AFTER DESTROYING
      merge(arr, left, middle, right);

      // Detach from shared memory
      munmap(0, SIZE);

      // Destroy shared memory
      close(shm);

  }
}
