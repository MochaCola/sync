#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// Global int variables
int status = 0;
int wpid;

// Global parent
int parent = -1;


// Prints out a message about the fork based on pid
void print_fork(int pid) {
   switch (pid) {
      case -1: /*error*/
         fprintf(stderr, "fork failed\n" );
         exit(0);
      case 0: /* we are the child */
         printf("Child Running PID %d\n", getpid());
         break;
      default: /* we are the parent */
         printf("Parent Running PID %d\n", getpid());
         while ((wpid = wait(&status)) > 0) {
            printf("Exit status of %d was %d\n", wpid, status);
         }
   }
}


int main (int argc , char* argv []) {

   int pid1 = fork();
   print_fork(pid1);

   /*
   int pid2 = fork();
   print_fork(pid2);

   int pid3 = fork();
   print_fork(pid3);
   */

   return (EXIT_SUCCESS);
}