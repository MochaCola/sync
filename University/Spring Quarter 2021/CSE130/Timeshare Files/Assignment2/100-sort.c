#include "merge.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

/* LEFT index and RIGHT index of the sub-array of ARR[] to be sorted */
void singleThreadedMergeSort(int arr[], int left, int right) 
{
  if (left < right) {
    int middle = (left+right)/2;
    singleThreadedMergeSort(arr, left, middle); 
    singleThreadedMergeSort(arr, middle+1, right); 
    merge(arr, left, middle, right); 
  } 
}


void multiThreadedMergeSort(int arr[], int left, int right) 
{

  // P_thread create syntax referenced here:
  // http://www.cse.cuhk.edu.hk/~ericlo/teaching/os/lab/9-PThread/Pass.html
  // int pthread_create(pthread_t tid, void optional,
  //                   void function, void pass_value);
  
  // P_thread join syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_join.3.html
  // int pthread_join(pthread_t tid, void **retval);

  // P_thread exit syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_exit.3.html
  // void pthread_exit(void *retval);


  // Defining a structure to pass multiple arguments

  struct args {
    int* subarr;
    int left;
    int right;
  };

  // --------------------------------------------------------------------------
  // Helper function used to create & return structs

  struct args* struct_helper(int arr[], int left, int right) {
    struct args* Structure = (struct args *)malloc(sizeof(struct args));
    Structure->subarr = arr;
    Structure->left = left;
    Structure->right = right;
    return Structure;
  }

  // --------------------------------------------------------------------------
  // Helper function for sorting subarrays in pthread_create
  
  void* in_sorter(void *input) {
    struct args* arg = (struct args*)input;
    if (arg->left < arg->right) {
      singleThreadedMergeSort(arg->subarr, arg->left, arg->right);
    }
    return NULL;
  }

  // --------------------------------------------------------------------------
  // Helper function for sorting subarrays in pthread_create
  
  void* sorter(void *input) {

    struct args* arg = (struct args*)input;

    int mid = (arg->left + arg->right) / 2;
    struct args* strcts[2];
    strcts[0] = struct_helper(arg->subarr, arg->left, mid);
    strcts[1] = struct_helper(arg->subarr, mid+1, arg->right);

    pthread_t tids[2];

    // int lefts[2] = {arg->left, mid};
    // int rights[2] = {mid, arg->right};

    for (int i = 0; i < 2; i++) {
      // strcts[i] = struct_helper(arg->subarr, lefts[i], rights[i]);
      pthread_create(&tids[i], NULL, in_sorter, (void *)strcts[i]);
    }

    for (int i = 0; i < 2; i++) {
      pthread_join(tids[i], NULL);
    }

    merge(arg->subarr, arg->left, mid, arg->right);
    free(strcts[0]);
    free(strcts[1]);
    
    return NULL;
  }

  int mid = (left+right)/2;
  struct args* strcts[2];
  strcts[0] = struct_helper(arr, left, mid);
  strcts[1] = struct_helper(arr, mid+1, right);

  // P creates two threads to sort half of the array each
  pthread_t tids[2];
  // int lefts[2] = {left, mid+1};
  // int rights[2] = {mid, right};
  // int lefts[4] = {left, l_mid + 1, mid + 1, r_mid + 1};
  // int rights[4] = {l_mid, mid, r_mid, right};
  
  // Create two struct args & pthreads (one for each half of array)
  for (int i = 0; i < 2; i++) {
    // strcts[i] = struct_helper(arr, lefts[i], rights[i]);
    pthread_create(&tids[i], NULL, sorter, (void *)strcts[i]);
  }

  // P waits for both threads to finish
  for (int i = 0; i < 2; i++) {
    pthread_join(tids[i], NULL);
  }

  // P merges left & right
  merge(arr, left, mid, right);
  free(strcts[0]);
  free(strcts[1]);

}
