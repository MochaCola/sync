#include "merge.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>


// Global variables
pthread_t tids[3];
int lefts[4] = {0};
int rights[4] = {0};
int mid, l_mid, r_mid;

// --------------------------------------------------------------------------
/* LEFT index and RIGHT index of the sub-array of ARR[] to be sorted */
void singleThreadedMergeSort(int arr[], int left, int right) 
{
  if (left < right) {
    int middle = (left+right)/2;
    singleThreadedMergeSort(arr, left, middle); 
    singleThreadedMergeSort(arr, middle+1, right); 
    merge(arr, left, middle, right); 
  } 
}

// --------------------------------------------------------------------------
void multiThreadedMergeSort(int arr[], int left, int right) 
{

  // P_thread create syntax referenced here:
  // http://www.cse.cuhk.edu.hk/~ericlo/teaching/os/lab/9-PThread/Pass.html
  // int pthread_create(pthread_t tid, void optional,
  //                   void function, void pass_value);
  
  // P_thread join syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_join.3.html
  // int pthread_join(pthread_t tid, void **retval);

  // P_thread exit syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_exit.3.html
  // void pthread_exit(void *retval);


  // Defining a structure to pass multiple arguments

  struct args {
    int* subarr;
    int left;
    int right;
    int thread;
  };

  // --------------------------------------------------------------------------
  // Helper print function for arrays
  void print_array(int arr[], int left, int right) {
    printf("{ ");
    for (int i = left; i <= right; i++) {
      printf("%d", arr[i]);
      if (i < right) {
        printf(", ");
      }
    }
    printf(" }\n\n");
  }

  // --------------------------------------------------------------------------
  // Helper function for sorting subarrays in pthread_create
  void* sorter(void *input) {
    struct args* arg = (struct args*)input;

    printf("T%d | Pre-sort: \n", arg->thread);
    print_array(arr, left, right);
    printf("T%d | Sorting 1/4 of array...\n", arg->thread);
    singleThreadedMergeSort(arr, arg->left, arg->right);
    printf("T%d | Post-sort: \n", arg->thread);
    print_array(arr, left, right);
    printf("\n\n");

    return NULL;
  }

  // --------------------------------------------------------------------------
  // Helper function used to create & return structs
  struct args* struct_helper(int arr[], int left, int right, int thread) {
    struct args* Structure = (struct args *)malloc(sizeof(struct args));
    Structure->subarr = arr;
    Structure->left = left;
    Structure->right = right;
    Structure->thread = thread;
    return Structure;
  }

  // --------------------------------------------------------------------------
  // Array subset boundary indexes (very useful)
  int mid = (left+right)/2, l_mid = (left+mid)/2, r_mid = ((mid+1)+right)/2;

  // P creates four threads to sort one 1/4 of the array each
  int lefts[4] = {left, l_mid + 1, mid + 1, r_mid + 1};
  int rights[4] = {l_mid, mid, r_mid, right};

  // P sorts the remaining 1/4
  printf("P  | Pre-sort: \n");
  print_array(arr, left, right);
  printf("P  | Sorting first 1/4 of array...\n");
  singleThreadedMergeSort(arr, lefts[0], rights[0]);
  printf("P  | Post-sort: \n");
  print_array(arr, left, right);
  printf("\n\n");
  
  // // Create struct args & pthread for each subarray
  // P creates three threads to sort one 1/4 of the array each
  for (int i = 1; i < 4; i++) {
    printf("Creating thread %d\n", i);
    struct args* str_args = struct_helper(arr, lefts[i], rights[i], i);
    pthread_create(&tids[i-1], NULL, sorter, (void *)str_args);
    // T2 waits for T3 to finish, then merges right-left and right-right
    if (i == 2) {
      pthread_join(tids[2], NULL);
      printf("T%d | Pre-merge: \n", i);
      print_array(arr, left, right);
      printf("T%d | Merging right-left and right-right...\n", i);
      merge(arr, mid+1, r_mid, right);
      printf("T%d | Post-merge: \n", i);
      print_array(arr, left, right);
      printf("\n\n");
    }
  }
  printf("\n\n");

  // P waits for T1 to finish, then merges left-left and left-right
  printf("P  | Waiting for T1...\n");
  pthread_join(tids[0], NULL);
  printf("P  | Pre-merge: \n");
  print_array(arr, left, right);  // mid
  printf("P  | Merging left-left and left-right...\n");
  merge(arr, left, l_mid, mid);
  printf("P  | Post-merge: \n");
  print_array(arr, left, right);  // mid
  printf("\n\n");

  // P waits for T2 to finish, then merges left and right
  printf("P  | Waiting for T2...\n");
  pthread_join(tids[1], NULL);
  printf("P  | Pre-merge: \n");
  print_array(arr, left, right);  // mid
  printf("P  | Merging left and right...\n");
  merge(arr, left, mid, right);
  printf("P  | Post-merge: \n");
  print_array(arr, left, right);
  printf("\n\n");

}
