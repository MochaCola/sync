#include "merge.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

/* LEFT index and RIGHT index of the sub-array of ARR[] to be sorted */
void singleThreadedMergeSort(int arr[], int left, int right) 
{
  if (left < right) {
    int middle = (left+right)/2;
    singleThreadedMergeSort(arr, left, middle); 
    singleThreadedMergeSort(arr, middle+1, right); 
    merge(arr, left, middle, right); 
  } 
}


void multiThreadedMergeSort(int arr[], int left, int right) 
{ 

  // Helper function for sorting subarrays in pthread_create
  void* sorter(void *input) {
    int size = (int *) input;
    singleThreadedMergeSort(arr, left, left + size);
    return NULL;
  }

  // --------------------------------------------------------------------------
  // Array subset boundary indexes (very useful)
  int mid = (left+right)/2, l_mid = (left+mid)/2, r_mid = ((mid+1)+right)/2;

  // P creates four threads to sort one 1/4 of the array each
  pthread_t tids[4];
  int elems[4] = {(l_mid-left)+1, (mid-(l_mid+1))+1, (r_mid-(mid+1))+1, (right-(r_mid+1))+1};
  
  // Create struct args & pthread for each subarray
  for (int i = 0; i < 4; i++) {
    pthread_create(&tids[i], NULL, sorter, (void *)elems[i]);
    left += elems[i];
  }

  // --------------------------------------------------------------------------
  // P waits for all four threads to finish
 
  for (int i = 0; i < 4; i++) {
    pthread_join(tids[i], NULL);
  }

  // P merges left-left & left-right
  merge(arr, left, l_mid, mid);

  // P merges right-left & right-right
  merge(arr, mid + 1, r_mid, right);

  // P merges left & right
  merge(arr, left, mid, right);
      

}
