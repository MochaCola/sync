#include "merge.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

/* LEFT index and RIGHT index of the sub-array of ARR[] to be sorted */
void singleThreadedMergeSort(int arr[], int left, int right) 
{
  if (left < right) {
    int middle = (left+right)/2;
    singleThreadedMergeSort(arr, left, middle); 
    singleThreadedMergeSort(arr, middle+1, right); 
    merge(arr, left, middle, right); 
  } 
}


void multiThreadedMergeSort(int arr[], int left, int right) 
{

  // P_thread create syntax referenced here:
  // http://www.cse.cuhk.edu.hk/~ericlo/teaching/os/lab/9-PThread/Pass.html
  // int pthread_create(pthread_t tid, void optional,
  //                   void function, void pass_value);
  
  // P_thread join syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_join.3.html
  // int pthread_join(pthread_t tid, void **retval);

  // P_thread exit syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_exit.3.html
  // void pthread_exit(void *retval);


  // Defining a structure to pass multiple arguments

  struct args {
    int* subarr;
    int left;
    int right;
  };

  // --------------------------------------------------------------------------
  // Helper function for sorting subarrays in pthread_create
  
  void* sorter(void *input) {
    struct args* arg = (struct args*)input;
    singleThreadedMergeSort(arg->subarr, arg->left, arg->right);
    return NULL;
  }

  // --------------------------------------------------------------------------
  // Helper function used to create & return structs

  struct args* struct_helper(int arr[], int left, int right) {
    struct args* Structure = (struct args *)malloc(sizeof(struct args));
    Structure->subarr = arr;
    Structure->left = left;
    Structure->right = right;
    return Structure;
  }

  // --------------------------------------------------------------------------
  // Array subset boundary indexes (very useful)
  int mid = (left+right)/2, l_mid = (left+mid)/2, r_mid = ((mid+1)+right)/2;

  // P creates four threads to sort one 1/4 of the array each
  pthread_t tids[4];
  int lefts[4] = {left, l_mid + 1, mid + 1, r_mid + 1};
  int rights[4] = {l_mid, mid, r_mid, right};
  
  // Create struct args & pthread for each subarray
  for (int i = 0; i < 4; i++) {
    struct args* str_args = struct_helper(arr, lefts[i], rights[i]);
    pthread_create(&tids[i], NULL, sorter, (void *)str_args);
  }

  // --------------------------------------------------------------------------
  // P waits for all four threads to finish
 
  for (int i = 0; i < 4; i++) {
    pthread_join(tids[i], NULL);
  }

  // P merges left-left & left-right
  merge(arr, left, l_mid, mid);

  // P merges right-left & right-right
  merge(arr, mid + 1, r_mid, right);

  // P merges left & right
  merge(arr, left, mid, right);


}
