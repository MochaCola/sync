#include "merge.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

/* LEFT index and RIGHT index of the sub-array of ARR[] to be sorted */
void singleThreadedMergeSort(int arr[], int left, int right) 
{
  if (left < right) {
    int middle = (left+right)/2;
    singleThreadedMergeSort(arr, left, middle); 
    singleThreadedMergeSort(arr, middle+1, right); 
    merge(arr, left, middle, right); 
  } 
}


void multiThreadedMergeSort(int arr[], int left, int right) 
{

  // P_thread create syntax referenced here:
  // http://www.cse.cuhk.edu.hk/~ericlo/teaching/os/lab/9-PThread/Pass.html
  // int pthread_create(pthread_t tid, void optional,
  //                   void function, void pass_value);
  
  // P_thread join syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_join.3.html
  // int pthread_join(pthread_t tid, void **retval);

  // P_thread exit syntax referenced here:
  // https://man7.org/linux/man-pages/man3/pthread_exit.3.html
  // void pthread_exit(void *retval);


  // Defining a structure to pass multiple arguments
  // --------------------------------------------------------------------------

  struct args {
    int* A;       // subarray
    int L;        // left
    int R;        // right
  };

  // --------------------------------------------------------------------------
  // Helper function used to create & return structs

  struct args* strct(int arr[], int left, int right) {
    struct args* Structure = (struct args *)malloc(sizeof(struct args));
    Structure->A = arr, Structure->L = left, Structure->R = right;
    return Structure;
  }

  // --------------------------------------------------------------------------
  // Helper function for sorting subarrays in sorter
  
  void* in_sorter(void *input) {

    // Get the struct argument from input
    struct args* arg = (struct args*)input;

    // If thread left < right, sort the 1/4 of the array!
    if (arg->L < arg->R) { singleThreadedMergeSort(arg->A, arg->L, arg->R); }

    return NULL;
  }

  // --------------------------------------------------------------------------
  // Helper function for sorting subarrays in pthread_create
  
  void* sorter(void *input) {

    // Get the struct argument from input
    struct args* arg = (struct args*)input;

    // Structs for creation of threads
    struct args* strcts[2];
    strcts[0] = strct(arg->A, arg->L, (arg->L + arg->R)/2);
    strcts[1] = strct(arg->A, ((arg->L + arg->R)/2)+1, arg->R);

    // T1 and T2 each create two threads to sort half of their half
    pthread_t tids[2];

    // Create two pthreads (one for each quarter of array)
    for (int i = 0; i < 2; i++) { pthread_create(&tids[i], NULL, in_sorter, (void *)strcts[i]); }

    // T1 and T2 wait for their threads to finish
    for (int i = 0; i < 2; i++) { pthread_join(tids[i], NULL); }

    // T1 and T2 merge their lefts & rights
    merge(arg->A, arg->L, (arg->L+arg->R)/2, arg->R);

    // De-allocate structs (fixes some memory problems)
    free(strcts[0]);
    free(strcts[1]);
    
    return NULL;
  }

  // --------------------------------------------------------------------------
  // Structs for creation of threads
  struct args* strcts[2];
  strcts[0] = strct(arr, left, (left + right)/2);
  strcts[1] = strct(arr, ((left + right)/2)+1, right);

  // P creates two threads to sort half of the array each
  pthread_t tids[2];
  
  // Create two pthreads (one for each half of array)
  for (int i = 0; i < 2; i++) { pthread_create(&tids[i], NULL, sorter, (void *)strcts[i]); }

  // P waits for both threads to finish
  for (int i = 0; i < 2; i++) { pthread_join(tids[i], NULL); }

  // P merges left & right
  merge(arr, left, (left + right)/2, right);

  // De-allocate structs (fixes some memory problems)
  free(strcts[0]);
  free(strcts[1]);

}