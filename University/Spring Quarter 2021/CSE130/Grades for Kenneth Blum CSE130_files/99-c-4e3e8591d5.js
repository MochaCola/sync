(window["canvasWebpackJsonp"]=window["canvasWebpackJsonp"]||[]).push([[99],{CyAq:function(e,t,s){"use strict"
s.d(t,"a",(function(){return a}))
function a(e){var t="".concat(e)
return[parseFloat(t,10),t.match(/[\d.\-\+]*\s*(.*)/)[1]||""]}},HMVb:function(e,t,s){"use strict"
s.d(t,"a",(function(){return c}))
var a=s("ODXe")
var n=s("i/8D")
var o=s("DUTp")
var r=s("IPIv")
var i={}
function _(e,t){if(!n["a"])return 16
var s=e||Object(o["a"])(e).documentElement
if(!t&&i[s])return i[s]
var a=parseInt(Object(r["a"])(s).getPropertyValue("font-size"))
i[s]=a
return a}var l=s("CyAq")
function c(e,t){var s=t||document.body
if(!e||"number"===typeof e)return e
var n=Object(l["a"])(e),o=Object(a["a"])(n,2),r=o[0],i=o[1]
return"rem"===i?r*_():"em"===i?r*_(s):r}},ZbPE:function(e,t,s){"use strict"
s.d(t,"a",(function(){return B}))
var a=s("rePB")
var n=s("1OyB")
var o=s("vuIU")
var r=s("Ji7U")
var i=s("LK+K")
var _=s("q1tI")
var l=s.n(_)
var c=s("17x9")
var u=s.n(c)
var m=s("TSYQ")
var d=s.n(m)
var f=s("J2CL")
var p=s("KgFQ")
var y=s("jtGx")
var g=s("nAyT")
var b=s("VTBJ")
function h(e){var t=e.typography,s=e.colors,a=e.spacing
return Object(b["a"])({},t,{primaryInverseColor:s.textLightest,primaryColor:s.textDarkest,secondaryColor:s.textDark,secondaryInverseColor:s.textLight,warningColor:s.textWarning,brandColor:s.textBrand,errorColor:s.textDanger,dangerColor:s.textDanger,successColor:s.textSuccess,alertColor:s.textAlert,paragraphMargin:"".concat(a.medium," 0")})}h.canvas=function(e){return{primaryColor:e["ic-brand-font-color-dark"],brandColor:e["ic-brand-primary"]}}
var v,k,w,R
var x={componentId:"enRcg",template:function(e){return"\n\n.enRcg_bGBk{font-family:".concat(e.fontFamily||"inherit","}\n\n.enRcg_bGBk sub,.enRcg_bGBk sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}\n\n.enRcg_bGBk sup{top:-0.4em}\n\n.enRcg_bGBk sub{bottom:-0.4em}\n\n.enRcg_bGBk code,.enRcg_bGBk pre{all:initial;animation:none 0s ease 0s 1 normal none running;backface-visibility:visible;background:transparent none repeat 0 0/auto auto padding-box border-box scroll;border:medium none currentColor;border-collapse:separate;border-image:none;border-radius:0;border-spacing:0;bottom:auto;box-shadow:none;box-sizing:content-box;caption-side:top;clear:none;clip:auto;color:#000;column-count:auto;column-fill:balance;column-gap:normal;column-rule:medium none currentColor;column-span:1;column-width:auto;columns:auto;content:normal;counter-increment:none;counter-reset:none;cursor:auto;direction:ltr;display:inline;empty-cells:show;float:none;font-family:serif;font-family:").concat(e.fontFamilyMonospace||"inherit",";font-size:medium;font-stretch:normal;font-style:normal;font-variant:normal;font-weight:400;height:auto;hyphens:none;left:auto;letter-spacing:normal;line-height:normal;list-style:disc outside none;margin:0;max-height:none;max-width:none;min-height:0;min-width:0;opacity:1;orphans:2;outline:medium none invert;overflow:visible;overflow-x:visible;overflow-y:visible;padding:0;page-break-after:auto;page-break-before:auto;page-break-inside:auto;perspective:none;perspective-origin:50% 50%;position:static;right:auto;tab-size:8;table-layout:auto;text-align:left;text-align-last:auto;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;top:auto;transform:none;transform-origin:50% 50% 0;transform-style:flat;transition:none 0s ease 0s;unicode-bidi:normal;vertical-align:baseline;visibility:visible;white-space:normal;widows:2;width:auto;word-spacing:normal;z-index:auto}\n\n.enRcg_bGBk em,.enRcg_bGBk i{font-style:italic}\n\n.enRcg_bGBk b,.enRcg_bGBk strong{font-weight:").concat(e.fontWeightBold||"inherit","}\n\n.enRcg_bGBk p{display:block;margin:").concat(e.paragraphMargin||"inherit",";padding:0}\n\ninput.enRcg_bGBk[type]{-moz-appearance:none;-webkit-appearance:none;appearance:none;background:none;border:none;border-radius:0;box-shadow:none;box-sizing:border-box;color:inherit;display:block;height:auto;line-height:inherit;margin:0;outline:0;padding:0;text-align:start;width:100%}\n\n[dir=ltr] input.enRcg_bGBk[type]{text-align:left}\n\n[dir=rtl] input.enRcg_bGBk[type]{text-align:right}\n\n.enRcg_bGBk:focus,input.enRcg_bGBk[type]:focus{outline:none}\n\n.enRcg_bGBk.enRcg_qFsi,input.enRcg_bGBk[type].enRcg_qFsi{color:").concat(e.primaryColor||"inherit","}\n\n.enRcg_bGBk.enRcg_bLsb,input.enRcg_bGBk[type].enRcg_bLsb{color:").concat(e.secondaryColor||"inherit","}\n\n.enRcg_bGBk.enRcg_ezBQ,input.enRcg_bGBk[type].enRcg_ezBQ{color:").concat(e.primaryInverseColor||"inherit","}\n\n.enRcg_bGBk.enRcg_dlnd,input.enRcg_bGBk[type].enRcg_dlnd{color:").concat(e.secondaryInverseColor||"inherit","}\n\n.enRcg_bGBk.enRcg_cJLh,input.enRcg_bGBk[type].enRcg_cJLh{color:").concat(e.successColor||"inherit","}\n\n.enRcg_bGBk.enRcg_fpfC,input.enRcg_bGBk[type].enRcg_fpfC{color:").concat(e.brandColor||"inherit","}\n\n.enRcg_bGBk.enRcg_eHcp,input.enRcg_bGBk[type].enRcg_eHcp{color:").concat(e.warningColor||"inherit","}\n\n.enRcg_bGBk.enRcg_dwua,input.enRcg_bGBk[type].enRcg_dwua{color:").concat(e.errorColor||"inherit","}\n\n.enRcg_bGBk.enRcg_NQMb,input.enRcg_bGBk[type].enRcg_NQMb{color:").concat(e.dangerColor||"inherit","}\n\n.enRcg_bGBk.enRcg_eZgl,input.enRcg_bGBk[type].enRcg_eZgl{color:").concat(e.alertColor||"inherit","}\n\n.enRcg_bGBk.enRcg_fbNi,input.enRcg_bGBk[type].enRcg_fbNi{-ms-hyphens:auto;-webkit-hyphens:auto;hyphens:auto;overflow-wrap:break-word;word-break:break-word}\n\n.enRcg_bGBk.enRcg_drST,input.enRcg_bGBk[type].enRcg_drST{font-weight:").concat(e.fontWeightNormal||"inherit","}\n\n.enRcg_bGBk.enRcg_pEgL,input.enRcg_bGBk[type].enRcg_pEgL{font-weight:").concat(e.fontWeightLight||"inherit","}\n\n.enRcg_bGBk.enRcg_bdMA,input.enRcg_bGBk[type].enRcg_bdMA{font-weight:").concat(e.fontWeightBold||"inherit","}\n\n.enRcg_bGBk.enRcg_ijuF,input.enRcg_bGBk[type].enRcg_ijuF{font-style:normal}\n\n.enRcg_bGBk.enRcg_fetN,input.enRcg_bGBk[type].enRcg_fetN{font-style:italic}\n\n.enRcg_bGBk.enRcg_dfBC,input.enRcg_bGBk[type].enRcg_dfBC{font-size:").concat(e.fontSizeXSmall||"inherit","}\n\n.enRcg_bGBk.enRcg_doqw,input.enRcg_bGBk[type].enRcg_doqw{font-size:").concat(e.fontSizeSmall||"inherit","}\n\n.enRcg_bGBk.enRcg_ycrn,input.enRcg_bGBk[type].enRcg_ycrn{font-size:").concat(e.fontSizeMedium||"inherit","}\n\n.enRcg_bGBk.enRcg_cMDj,input.enRcg_bGBk[type].enRcg_cMDj{font-size:").concat(e.fontSizeLarge||"inherit","}\n\n.enRcg_bGBk.enRcg_eoMd,input.enRcg_bGBk[type].enRcg_eoMd{font-size:").concat(e.fontSizeXLarge||"inherit","}\n\n.enRcg_bGBk.enRcg_fdca,input.enRcg_bGBk[type].enRcg_fdca{font-size:").concat(e.fontSizeXXLarge||"inherit","}\n\n.enRcg_bGBk.enRcg_fEWX,input.enRcg_bGBk[type].enRcg_fEWX{line-height:").concat(e.lineHeight||"inherit","}\n\n.enRcg_bGBk.enRcg_fNIu,input.enRcg_bGBk[type].enRcg_fNIu{line-height:").concat(e.lineHeightFit||"inherit","}\n\n.enRcg_bGBk.enRcg_dfDs,input.enRcg_bGBk[type].enRcg_dfDs{line-height:").concat(e.lineHeightCondensed||"inherit","}\n\n.enRcg_bGBk.enRcg_bDjL,input.enRcg_bGBk[type].enRcg_bDjL{line-height:").concat(e.lineHeightDouble||"inherit","}\n\n.enRcg_bGBk.enRcg_eQnG,input.enRcg_bGBk[type].enRcg_eQnG{letter-spacing:").concat(e.letterSpacingNormal||"inherit","}\n\n.enRcg_bGBk.enRcg_bbUA,input.enRcg_bGBk[type].enRcg_bbUA{letter-spacing:").concat(e.letterSpacingCondensed||"inherit","}\n\n.enRcg_bGBk.enRcg_bRWU,input.enRcg_bGBk[type].enRcg_bRWU{letter-spacing:").concat(e.letterSpacingExpanded||"inherit","}\n\n.enRcg_bGBk.enRcg_wZsr,input.enRcg_bGBk[type].enRcg_wZsr{text-transform:none}\n\n.enRcg_bGBk.enRcg_fCZK,input.enRcg_bGBk[type].enRcg_fCZK{text-transform:capitalize}\n\n.enRcg_bGBk.enRcg_dsRi,input.enRcg_bGBk[type].enRcg_dsRi{text-transform:uppercase}\n\n.enRcg_bGBk.enRcg_bLtD,input.enRcg_bGBk[type].enRcg_bLtD{text-transform:lowercase}")},root:"enRcg_bGBk","color-primary":"enRcg_qFsi","color-secondary":"enRcg_bLsb","color-primary-inverse":"enRcg_ezBQ","color-secondary-inverse":"enRcg_dlnd","color-success":"enRcg_cJLh","color-brand":"enRcg_fpfC","color-warning":"enRcg_eHcp","color-error":"enRcg_dwua","color-danger":"enRcg_NQMb","color-alert":"enRcg_eZgl","wrap-break-word":"enRcg_fbNi","weight-normal":"enRcg_drST","weight-light":"enRcg_pEgL","weight-bold":"enRcg_bdMA","style-normal":"enRcg_ijuF","style-italic":"enRcg_fetN","x-small":"enRcg_dfBC",small:"enRcg_doqw",medium:"enRcg_ycrn",large:"enRcg_cMDj","x-large":"enRcg_eoMd","xx-large":"enRcg_fdca","lineHeight-default":"enRcg_fEWX","lineHeight-fit":"enRcg_fNIu","lineHeight-condensed":"enRcg_dfDs","lineHeight-double":"enRcg_bDjL","letterSpacing-normal":"enRcg_eQnG","letterSpacing-condensed":"enRcg_bbUA","letterSpacing-expanded":"enRcg_bRWU","transform-none":"enRcg_wZsr","transform-capitalize":"enRcg_fCZK","transform-uppercase":"enRcg_dsRi","transform-lowercase":"enRcg_bLtD"}
var B=(v=Object(f["l"])(h,x),v(k=(R=w=function(e){Object(r["a"])(s,e)
var t=Object(i["a"])(s)
function s(){Object(n["a"])(this,s)
return t.apply(this,arguments)}Object(o["a"])(s,[{key:"render",value:function(){var e
var t=this.props,n=t.wrap,o=t.weight,r=t.fontStyle,i=t.size,_=t.lineHeight,c=t.letterSpacing,u=t.transform,m=t.color,f=t.children
var g=Object(p["a"])(s,this.props)
return l.a.createElement(g,Object.assign({},Object(y["b"])(this.props),{className:d()((e={},Object(a["a"])(e,x.root,true),Object(a["a"])(e,x[i],i),Object(a["a"])(e,x["wrap-".concat(n)],n),Object(a["a"])(e,x["weight-".concat(o)],o),Object(a["a"])(e,x["style-".concat(r)],r),Object(a["a"])(e,x["transform-".concat(u)],u),Object(a["a"])(e,x["lineHeight-".concat(_)],_),Object(a["a"])(e,x["letterSpacing-".concat(c)],c),Object(a["a"])(e,x["color-".concat(m)],m),e)),ref:this.props.elementRef}),f)}}])
s.displayName="Text"
return s}(_["Component"]),w.propTypes={as:u.a.elementType,children:u.a.node,color:g["a"].deprecatePropValues(u.a.oneOf(["primary","secondary","brand","success","warning","error","danger","alert","primary-inverse","secondary-inverse"]),["error"],"It will be removed in version 8.0.0. Use `danger` instead."),elementRef:u.a.func,fontStyle:u.a.oneOf(["italic","normal"]),letterSpacing:u.a.oneOf(["normal","condensed","expanded"]),lineHeight:u.a.oneOf(["default","fit","condensed","double"]),size:u.a.oneOf(["x-small","small","medium","large","x-large","xx-large"]),transform:u.a.oneOf(["none","capitalize","uppercase","lowercase"]),weight:u.a.oneOf(["normal","light","bold"]),wrap:u.a.oneOf(["normal","break-word"])},w.defaultProps={as:"span",wrap:"normal",size:"medium",letterSpacing:"normal",children:null,elementRef:void 0,color:void 0,transform:void 0,lineHeight:void 0,fontStyle:void 0,weight:void 0},R))||k)},cClk:function(e,t,s){"use strict"
s.d(t,"a",(function(){return a}))
function a(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"onChange"
var s=arguments.length>2&&void 0!==arguments[2]?arguments[2]:"defaultValue"
return function(a,n,o){var r=e.apply(null,arguments)
if(r)return r
if(a[n]&&"function"!==typeof a[t])return new Error(["You provided a '".concat(n,"' prop without an '").concat(t,"' handler on '").concat(o,"'. This will render a controlled component. If the component should be uncontrolled and manage its own state, use '").concat(s,"'. Otherwise, set '").concat(t,"'.")].join(""))}}},dbrX:function(e,t,s){"use strict"
var a=s("uYtQ")
const n={}
const o={getCssVariant(){const e=window.ENV.use_responsive_layout?"responsive_layout":"new_styles"
const t=window.ENV.use_high_contrast?"high_contrast":"normal_contrast"
const s=Object(a["c"])()?"_rtl":""
return`${e}_${t}${s}`},urlFor(e,{combinedChecksum:t,includesNoVariables:s}){const a=s?"no_variables":o.getCssVariant()
return[window.ENV.ASSET_HOST||"","dist","brandable_css",a,`${e}-${t}.css`].join("/")},loadStylesheet(e,t){if(e in n)return
const s=document.createElement("link")
s.rel="stylesheet"
s.href=o.urlFor(e,t)
s.setAttribute("data-loaded-by-brandableCss",true)
document.head.appendChild(s)}}
t["a"]=o},eGSd:function(e,t,s){"use strict"
s.d(t,"a",(function(){return a}))
function a(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0
var s=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{}
var a,n,o,r
var i=0
var _=[]
var l=false
if("function"!==typeof e)throw new TypeError("Expected a function")
var c=!!s.leading
var u="maxWait"in s
var m=!("trailing"in s)||!!s.trailing
var d=u?Math.max(+s.maxWait||0,t):0
function f(t){var s=a
var r=n
a=n=void 0
i=t
if(true!==l){o=e.apply(r,s)
return o}}function p(e){i=e
_.push(setTimeout(b,t))
return c?f(e):o}function y(e){var s=e-r
var a=e-i
var n=t-s
return u?Math.min(n,d-a):n}function g(e){var s=e-r
var a=e-i
return"undefined"===typeof r||s>=t||s<0||u&&a>=d}function b(){var e=Date.now()
if(g(e))return h(e)
_.push(setTimeout(b,y(e)))}function h(e){w()
if(m&&a)return f(e)
a=n=void 0
return o}function v(){l=true
w()
i=0
a=r=n=void 0}function k(){return 0===_.length?o:h(Date.now())}function w(){_.forEach((function(e){return clearTimeout(e)}))
_=[]}function R(){var e=Date.now()
var s=g(e)
for(var i=arguments.length,l=new Array(i),c=0;c<i;c++)l[c]=arguments[c]
a=l
n=this
r=e
if(s){if(0===_.length)return p(r)
if(u){_.push(setTimeout(b,t))
return f(r)}}0===_.length&&_.push(setTimeout(b,t))
return o}R.cancel=v
R.flush=k
return R}},f40T:function(e,t,s){"use strict"
s.d(t,"a",(function(){return l}))
var a=s("ouhR")
var n=s.n(a)
function o(e,t){e.top<0&&(e.top=0)
return n()(this).css(e).toggleClass("carat-bottom","bottom"===t.vertical)}let r=0
const i=[]
function _(e){const t=e.querySelectorAll('a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select')
const s=t[0]
const a=t[t.length-1]
const n=9
e.addEventListener("keydown",e=>{const t="Tab"===e.key||e.keyCode===n
if(!t)return
if(e.shiftKey){if(document.activeElement===s){a.focus()
e.preventDefault()}}else if(document.activeElement===a){setTimeout(()=>{s.focus()})
e.preventDefault()}})}class l{constructor(e,t,s={}){this.ignoreOutsideClickSelector=".ui-dialog"
this.outsideClickHandler=e=>{n()(e.target).closest(this.el.add(this.trigger).add(this.ignoreOutsideClickSelector)).length||this.hide()}
this.position=()=>this.el.position({my:"center "+("bottom"===this.options.verticalSide?"top":"bottom"),at:"center "+(this.options.verticalSide||"top"),of:this.trigger,offset:`0px ${this.offsetPx()}px`,within:"body",collision:"flipfit "+(this.options.verticalSide?"none":"flipfit"),using:o})
this.content=t
this.options=s
this.trigger=n()(e.currentTarget)
this.triggerAction=e.type
this.focusTrapped=false
this.el=n()(this.content).addClass("carat-bottom").data("popover",this).keydown(e=>{e.keyCode===n.a.ui.keyCode.ESCAPE&&this.hide()
if(e.keyCode!==n.a.ui.keyCode.TAB)return
const t=n()(":tabbable",this.el)
const s=n.a.inArray(e.target,t)
if(-1===s)return
e.shiftKey?this.focusTrapped||0!==s||this.hide():this.focusTrapped||s!==t.length-1||this.hide()})
this.el.delegate(".popover_close","keyclick click",e=>{e.preventDefault()
this.hide()})
this.show(e)}trapFocus(e){this.focusTrapped=true
_(e)}show(e){let t
n.a.screenReaderFlashMessageExclusive("")
while(t=i.pop())t.hide()
i.push(this)
const s="popover-"+r++
this.trigger.attr({"aria-expanded":true,"aria-controls":s})
this.previousTarget=e.currentTarget
this.el.attr({id:s}).appendTo(document.body).show()
this.position()
if("mouseenter"!==e.type){this.el.find(":tabbable").first().focus()
setTimeout(()=>this.el.find(":tabbable").first().focus(),100)}document.querySelector("#application").setAttribute("aria-hidden","true")
this.el.find(".ui-menu-carat").remove()
const a=this.options.manualOffset||0
const o=this.trigger.offset().left-this.el.offset().left
const _=e.pageX-this.trigger.offset().left
const l=Math.max(0,this.trigger.width()/2-this.el.width()/2)+20
const c=this.trigger.width()-l
const u=Math.min(Math.max(l,_),c)+o+a
n()('<span class="ui-menu-carat"><span /></span>').css("left",u).prependTo(this.el)
this.positionInterval=setInterval(this.position,200)
n()(window).click(this.outsideClickHandler)}hide(){for(let e=0;e<i.length;e++){const t=i[e]
this===t&&i.splice(e,1)}this.el.detach()
this.trigger.attr("aria-expanded",false)
clearInterval(this.positionInterval)
n()(window).unbind("click",this.outsideClickHandler)
this.restoreFocus()
0===i.length&&document.querySelector("#application").setAttribute("aria-hidden","false")}offsetPx(){const e="bottom"===this.options.verticalSide?10:-10
return this.options.invertOffset?-1*e:e}restoreFocus(){this.previousTarget&&n()(this.previousTarget).is(":visible")&&this.previousTarget.focus()}}},fIUB:function(e,t,s){"use strict"
var a=s("HGxv")
var n=s("8WeW")
Object(n["a"])(JSON.parse('{"ar":{"exceeds_mastery_1f995dce":"تجاوز الإتقان","meets_mastery_754e1c06":"يلبي الإتقان","near_mastery_f25174a4":"قريب من الإتقان","unstarted_932f2990":"لم تبدأ","well_below_mastery_37664bdc":"أقل بكثير من الإتقان"},"ca":{"exceeds_mastery_1f995dce":"Supera el domini","meets_mastery_754e1c06":"Assoleix el domini","near_mastery_f25174a4":"Domini gairebé assolit","unstarted_932f2990":"No començat","well_below_mastery_37664bdc":"Molt per sota del domini"},"cy":{"exceeds_mastery_1f995dce":"Yn well na’r lefel meistroli","meets_mastery_754e1c06":"Wedi Meistroli","near_mastery_f25174a4":"Bron wedi Meistroli","unstarted_932f2990":"Heb ddechrau","well_below_mastery_37664bdc":"Yn bell iawn o’r lefel Meistroli"},"da":{"exceeds_mastery_1f995dce":"Overstiger opfyldelse af læringsmål","meets_mastery_754e1c06":"Opfylder mål","near_mastery_f25174a4":"Tæt på opfyldelse af læringsmål","unstarted_932f2990":"Ikke startet","well_below_mastery_37664bdc":"Langt under opfyldelse af læringsmål"},"da-x-k12":{"exceeds_mastery_1f995dce":"Overstiger opfyldelse af læringsmål","meets_mastery_754e1c06":"Opfylder mål","near_mastery_f25174a4":"Tæt på opfyldelse af læringsmål","unstarted_932f2990":"Ikke startet","well_below_mastery_37664bdc":"Langt under opfyldelse af læringsmål"},"de":{"exceeds_mastery_1f995dce":"Übertrifft Leistungsziel","meets_mastery_754e1c06":"Leistungsziel erreicht","near_mastery_f25174a4":"Leistungsziel knapp erreicht","unstarted_932f2990":"Nicht gestartet","well_below_mastery_37664bdc":"Deutlich unter dem Leistungsziel"},"el":{"exceeds_mastery_1f995dce":"Υπερβαίνει την Μαεστρία/Τεχνογνωσία","meets_mastery_754e1c06":"Είναι σε Επίπεδο Επιδεξιότητας","near_mastery_f25174a4":"Κοντά στο Επίπεδο Επιδεξιότητας","unstarted_932f2990":"Δεν έχει ξεκινήσει","well_below_mastery_37664bdc":"Αρκετά Κάτω από το Επίπεδο Επιδεξιότητας"},"en-AU":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-AU-x-unimelb":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-CA":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-GB":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-GB-x-lbs":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-GB-x-ukhe":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"es":{"exceeds_mastery_1f995dce":"Excede el dominio","meets_mastery_754e1c06":"Reúne el dominio","near_mastery_f25174a4":"Cerca del dominio","unstarted_932f2990":"Sin comenzar","well_below_mastery_37664bdc":"Muy por debajo del dominio"},"fa":{"exceeds_mastery_1f995dce":"فراتر از تسلط","meets_mastery_754e1c06":"با تسلط مطابقت دارد","near_mastery_f25174a4":"نزدیک به تسلط","unstarted_932f2990":"شروع نشده","well_below_mastery_37664bdc":"پایین تر از سطح تسلط"},"fi":{"exceeds_mastery_1f995dce":"Erinomainen osaaminen","meets_mastery_754e1c06":"Hyvä osaaminen","near_mastery_f25174a4":"Tyydyttävä osaaminen","unstarted_932f2990":"Aloittamaton","well_below_mastery_37664bdc":"Heikko osaaminen"},"fr":{"exceeds_mastery_1f995dce":"Surpasse le niveau de maîtrise","meets_mastery_754e1c06":"Niveau de maîtrise atteint","near_mastery_f25174a4":"Proche du niveau de maîtrise","unstarted_932f2990":"Non commencé","well_below_mastery_37664bdc":"Bien inférieur au niveau de maîtrise"},"fr-CA":{"exceeds_mastery_1f995dce":"Surpasse le niveau de Maîtrise","meets_mastery_754e1c06":"Niveau de Maîtrise atteint","near_mastery_f25174a4":"Proche du niveau de maîtrise","unstarted_932f2990":"Non commencé","well_below_mastery_37664bdc":"Bien inférieur au niveau de maîtrise"},"he":{"exceeds_mastery_1f995dce":"מעבר לרף הנדרש למומחיות","meets_mastery_754e1c06":"עונה על דרישות ההתמחות","near_mastery_f25174a4":"קרוב להתמחות","unstarted_932f2990":"טרם החל","well_below_mastery_37664bdc":"רחוק מאד מהתמחות"},"ht":{"exceeds_mastery_1f995dce":"Depase Metriz","meets_mastery_754e1c06":"Respekte Metriz","near_mastery_f25174a4":"Pwòch Metriz","unstarted_932f2990":"Pa kòmanse","well_below_mastery_37664bdc":"Pi ba Metriz"},"hu":{"exceeds_mastery_1f995dce":"A jártassági szint fölött","meets_mastery_754e1c06":"Megfelel a jártassági szintnek","near_mastery_f25174a4":"Közel a jártassági szinthez","unstarted_932f2990":"Nincs elkezdve","well_below_mastery_37664bdc":"Jóval a jártassági szint alatt"},"hy":{"exceeds_mastery_1f995dce":"Գերազանցում է անցողիկ միավորը","meets_mastery_754e1c06":"Համապատասխանում է անցողիկ միավորին","near_mastery_f25174a4":"Մոտ է անցողիկ միավորին","unstarted_932f2990":"Չի սկսվել","well_below_mastery_37664bdc":"Բավական ցածր է անցողիկ միավորներից "},"is":{"exceeds_mastery_1f995dce":"Fer fram úr tileinkun","meets_mastery_754e1c06":"Uppfyllir tileinkun","near_mastery_f25174a4":"Nálægt tileinkun","unstarted_932f2990":"Óbyrjað","well_below_mastery_37664bdc":"Langt undir tileinkun"},"it":{"exceeds_mastery_1f995dce":"Supera il livello di padronanza","meets_mastery_754e1c06":"Soddisfa il livello di padronanza","near_mastery_f25174a4":"Padronanza quasi completa","unstarted_932f2990":"Non iniziato","well_below_mastery_37664bdc":"Ben al di sotto del livello di padronanza"},"ja":{"exceeds_mastery_1f995dce":"熟達を上回る","meets_mastery_754e1c06":"熟達を満たしている","near_mastery_f25174a4":"熟達に近い","unstarted_932f2990":"開始前","well_below_mastery_37664bdc":"熟達を大きく下回る"},"mi":{"exceeds_mastery_1f995dce":"Nui atu te mana","meets_mastery_754e1c06":"Tutuki Mana","near_mastery_f25174a4":"Tata Mātatau","unstarted_932f2990":"Koare i tīmata","well_below_mastery_37664bdc":"Kei raro rawa atu Mātatau"},"nb":{"exceeds_mastery_1f995dce":"Overgår ekspertise","meets_mastery_754e1c06":"Innfrir forventningene","near_mastery_f25174a4":"Nær ekspertise","unstarted_932f2990":"Ikke påbegynt","well_below_mastery_37664bdc":"Innfrir ikke ekspertise"},"nb-x-k12":{"exceeds_mastery_1f995dce":"Overgår ekspertise","meets_mastery_754e1c06":"Innfrir forventningene","near_mastery_f25174a4":"Nær ekspertise","unstarted_932f2990":"Ikke påbegynt","well_below_mastery_37664bdc":"Innfrir ikke ekspertise"},"nl":{"exceeds_mastery_1f995dce":"Overtreft Meesterschap","meets_mastery_754e1c06":"Voldoende vakbeheersing","near_mastery_f25174a4":"Bijna Meesterschap","unstarted_932f2990":"Nog niet begonnen","well_below_mastery_37664bdc":"Ruim onder vakbeheersing"},"nn":{"exceeds_mastery_1f995dce":"Overskrid meistringsnivå","meets_mastery_754e1c06":"Møter meistringsnivå","near_mastery_f25174a4":"Nær meistringsnivå","unstarted_932f2990":"Ikkje starta","well_below_mastery_37664bdc":"Godt under meistringsnivå"},"pl":{"exceeds_mastery_1f995dce":"Przekracza poziom biegłości","meets_mastery_754e1c06":"Spełnia poziom biegłości","near_mastery_f25174a4":"Blisko biegłości","unstarted_932f2990":"Nierozpoczęto","well_below_mastery_37664bdc":"Znacznie poniżej poziomu biegłości"},"pt":{"exceeds_mastery_1f995dce":"Excede a Excelência","meets_mastery_754e1c06":"Completa o domínio","near_mastery_f25174a4":"Perto do Domínio","unstarted_932f2990":"Não iniciado","well_below_mastery_37664bdc":"Muito abaixo de Domínio"},"pt-BR":{"exceeds_mastery_1f995dce":"Excede Domínio","meets_mastery_754e1c06":"Encontra com Domínio","near_mastery_f25174a4":"Quase Domínio","unstarted_932f2990":"Não iniciado","well_below_mastery_37664bdc":"Muito abaixo de Domínio"},"ru":{"exceeds_mastery_1f995dce":"Превышает проходной балл","meets_mastery_754e1c06":"Соответствует проходному баллу","near_mastery_f25174a4":"Почти усвоено","unstarted_932f2990":"Не начато","well_below_mastery_37664bdc":"Гораздо ниже проходного балла"},"sl":{"exceeds_mastery_1f995dce":"Presega odličnost","meets_mastery_754e1c06":"Odličen","near_mastery_f25174a4":"Delno obvladano","unstarted_932f2990":"Nezačeto","well_below_mastery_37664bdc":"Precej pod odličnostjo"},"sv":{"exceeds_mastery_1f995dce":"Överträffar måluppfyllelse","meets_mastery_754e1c06":"Möter måluppfyllelse","near_mastery_f25174a4":"Nära måluppfyllelse","unstarted_932f2990":"Ej påbörjad","well_below_mastery_37664bdc":"Långt under måluppfyllelse"},"sv-x-k12":{"exceeds_mastery_1f995dce":"Överträffar måluppfyllelse","meets_mastery_754e1c06":"Möter måluppfyllelse","near_mastery_f25174a4":"Nära måluppfyllelse","unstarted_932f2990":"Ej påbörjad","well_below_mastery_37664bdc":"Långt under måluppfyllelse"},"tr":{"exceeds_mastery_1f995dce":"Yeterliğin Üstünde","meets_mastery_754e1c06":"Yeterliği Karşılıyor","near_mastery_f25174a4":"Yeterliğe Yakın","unstarted_932f2990":"Başlamamış","well_below_mastery_37664bdc":"Yeterliğin Epey Altında"},"uk":{"exceeds_mastery_1f995dce":"Перевищує Майстерність","meets_mastery_754e1c06":"Відповідає майстерності","near_mastery_f25174a4":"Майже майстерність","unstarted_932f2990":"Не розпочато","well_below_mastery_37664bdc":"Суттєво нижча майстерність"},"zh-Hans":{"exceeds_mastery_1f995dce":"超过掌握","meets_mastery_754e1c06":"达到掌握","near_mastery_f25174a4":"最接近的掌握","unstarted_932f2990":"未开始","well_below_mastery_37664bdc":"远低于掌握"},"zh-Hant":{"exceeds_mastery_1f995dce":"超過掌握程度","meets_mastery_754e1c06":"達到掌握程度","near_mastery_f25174a4":"接近掌握程度","unstarted_932f2990":"未開始","well_below_mastery_37664bdc":"遠低於掌握程度"}}'))
s("jQeR")
s("0sPK")
var o=a["default"].scoped("grade_summaryOutcome")
var r=s("Y/W1")
var i=s.n(r)
s("mX+G")
var _=s("A5yp")
var l=s("3H9/")
var c,u=function(e,t){for(var s in t)m.call(t,s)&&(e[s]=t[s])
function a(){this.constructor=e}a.prototype=t.prototype
e.prototype=new a
e.__super__=t.prototype
return e},m={}.hasOwnProperty
c={}
t["a"]=c.Outcome=function(e){u(t,e)
function t(){return t.__super__.constructor.apply(this,arguments)}t.prototype.initialize=function(){t.__super__.initialize.apply(this,arguments)
this.set("friendly_name",this.get("display_name")||this.get("title"))
this.set("hover_name",this.get("display_name")?this.get("title"):void 0)
return this.set("scaled_score",this.scaledScore())}
t.prototype.parse=function(e){var s,a
return t.__super__.parse.call(this,i.a.extend(e,{submitted_or_assessed_at:l["a"].parse(e.submitted_or_assessed_at),question_bank_result:null!=(s=e.links)&&null!=(a=s.alignment)?a.includes("assessment_question_bank"):void 0}))}
t.prototype.status=function(){var e,t
if(this.scoreDefined()){t=this.score()
e=this.get("mastery_points")
return t>=e+e/2?"exceeds":t>=e?"mastery":t>=e/2?"near":"remedial"}return"undefined"}
t.prototype.statusTooltip=function(){return{undefined:o.t("Unstarted"),remedial:o.t("Well Below Mastery"),near:o.t("Near Mastery"),mastery:o.t("Meets Mastery"),exceeds:o.t("Exceeds Mastery")}[this.status()]}
t.prototype.roundedScore=function(){return this.scoreDefined()?Math.round(100*this.score())/100:null}
t.prototype.scoreDefined=function(){return i.a.isNumber(this.get("score"))}
t.prototype.scaledScore=function(){var e
e=this.get("question_bank_result")
if(!(this.scoreDefined()&&e))return
return this.get("points_possible")>0?this.get("percent")*this.get("points_possible"):this.get("percent")*this.get("mastery_points")}
t.prototype.score=function(){return this.get("scaled_score")||this.get("score")}
t.prototype.percentProgress=function(){if(!this.scoreDefined())return 0
return this.get("percent")?100*this.get("percent"):this.score()/this.get("points_possible")*100}
t.prototype.masteryPercent=function(){return this.get("mastery_points")/this.get("points_possible")*100}
t.prototype.toJSON=function(){return i.a.extend(t.__super__.toJSON.apply(this,arguments),{status:this.status(),statusTooltip:this.statusTooltip(),roundedScore:this.roundedScore(),scoreDefined:this.scoreDefined(),percentProgress:this.percentProgress(),masteryPercent:this.masteryPercent()})}
return t}(_["a"])},gCYW:function(e,t,s){"use strict"
s.d(t,"a",(function(){return i}))
var a=s("QF4Q")
var n=s("i/8D")
var o=s("EgqM")
var r=s("DUTp")
function i(e){var t={top:0,left:0,height:0,width:0}
if(!n["a"])return t
var s=Object(a["a"])(e)
if(!s)return t
if(s===window)return{left:window.pageXOffset,top:window.pageYOffset,width:window.innerWidth,height:window.innerHeight,right:window.innerWidth+window.pageXOffset,bottom:window.innerHeight+window.pageYOffset}
var _=e===document?document:Object(r["a"])(s)
var l=_&&_.documentElement
if(!l||!Object(o["a"])(l,s))return t
var c=s.getBoundingClientRect()
var u
for(u in c)t[u]=c[u]
if(_!==document){var m=_.defaultView.frameElement
if(m){var d=i(m)
t.top+=d.top
t.bottom+=d.top
t.left+=d.left
t.right+=d.left}}return{top:t.top+(window.pageYOffset||l.scrollTop)-(l.clientTop||0),left:t.left+(window.pageXOffset||l.scrollLeft)-(l.clientLeft||0),width:(null==t.width?s.offsetWidth:t.width)||0,height:(null==t.height?s.offsetHeight:t.height)||0,right:_.body.clientWidth-t.width-t.left,bottom:_.body.clientHeight-t.height-t.top}}},kR0I:function(e,t,s){"use strict"
s.d(t,"a",(function(){return i}))
var a=s("KQm4")
var n=s("QF4Q")
var o=s("xm5c")
var r=s("IPIv")
function i(e,t,s){var r=Object(n["a"])(e)
if(!r||"function"!==typeof r.querySelectorAll)return[]
var i="a[href],frame,iframe,object,input:not([type=hidden]),select,textarea,button,*[tabindex]"
var _=Array.from(r.querySelectorAll(i))
s&&Object(o["a"])(r,i)&&(_=[].concat(Object(a["a"])(_),[r]))
return _.filter((function(e){return"function"===typeof t?t(e)&&u(e):u(e)}))}function _(e){var t=Object(r["a"])(e)
return"inline"!==t.display&&e.offsetWidth<=0&&e.offsetHeight<=0||"none"===t.display}function l(e){var t=["fixed","absolute"]
if(t.includes(e.style.position.toLowerCase()))return true
if(t.includes(Object(r["a"])(e).getPropertyValue("position").toLowerCase()))return true
return false}function c(e){while(e){if(e===document.body)break
if(_(e))return false
if(l(e))break
e=e.parentNode}return true}function u(e){return!e.disabled&&c(e)}},niMM:function(e,t,s){"use strict"
var a=s("3O+N")
var n=s.n(a)
s("RwoB")
var o=s("HGxv")
var r=s("8WeW")
Object(r["a"])(JSON.parse('{"ar":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"درجة الإتقان الحالية: %{score} من %{mastery_points}","latest_assessment_a85280ef":"آخر تقييم","no_score_yet_c94e919b":"لا توجد درجة حتى الآن","not_available_until_next_submission_2175158":"غير متوفر قبل الإرسال القادم","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"النتيجة: %{title} بالحالة %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"الإتقان عند","no_results":"(بلا نتائج)"}}},"ca":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Puntuació de domini actual: %{score} de %{mastery_points}","latest_assessment_a85280ef":"Última avaluació","no_score_yet_c94e919b":"Encara no hi ha cap puntuació","not_available_until_next_submission_2175158":"No disponible fins a la propera entrega","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Resultat: %{title} té l\'estat %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Domini establert a","no_results":"(cap resultat)"}}},"cy":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Sgôr Meistroli bresennol: %{score} allan o %{mastery_points}","latest_assessment_a85280ef":"Asesiad diweddaraf","no_score_yet_c94e919b":"Dim sgôr eto","not_available_until_next_submission_2175158":"Ddim ar gael tan y cyflwyniad nesaf","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Deilliant: %{title} â statws %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Lefel meistroli ar","no_results":"(dim canlyniadau)"}}},"da":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Aktuelt resultat for opfyldelse af læringsmål: %{score} ud af %{mastery_points}","latest_assessment_a85280ef":"Seneste bedømmelse","no_score_yet_c94e919b":"Intet resultat endnu","not_available_until_next_submission_2175158":"Ikke tilgængelig før næste aflevering","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsudbytte: %{title} har en status for %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Målopfyldelse sat til","no_results":"(ingen resultater)"}}},"da-x-k12":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Aktuelt resultat for opfyldelse af læringsmål: %{score} ud af %{mastery_points}","latest_assessment_a85280ef":"Seneste bedømmelse","no_score_yet_c94e919b":"Intet resultat endnu","not_available_until_next_submission_2175158":"Ikke tilgængelig før næste aflevering","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsudbytte: %{title} har en status for %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Målopfyldelse sat til","no_results":"(ingen resultater)"}}},"de":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Derzeitige Leistungsziel-Punktzahl: %{score} von %{mastery_points}","latest_assessment_a85280ef":"Letzte Bewertung","no_score_yet_c94e919b":"Noch keine Punkte vorhanden","not_available_until_next_submission_2175158":"Vor der nächsten Abgabe nicht verfügbar","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Lernziel: %{title} hat den Status %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Leistungsziel festgelegt bei","no_results":"(keine Ergebnisse)"}}},"el":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Τρέχουσα Βαθμολογία Επιδεξιότητας: %{score} από %{mastery_points}","latest_assessment_a85280ef":"Πιο Πρόσφατη Αξιολόγηση","no_score_yet_c94e919b":"Δεν υπάρχει ακόμη βαθμολογία","not_available_until_next_submission_2175158":"Μη διαθέσιμο έως την επόμενη υποβολή","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Αποτέλεσμα: Η κατάσταση του %{title} είναι %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Η Επιδεξιότητα έχει οριστεί σε","no_results":"(δεν υπάρχουν αποτελέσματα)"}}},"en-AU":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Current Mastery Score: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Latest Assessment","no_score_yet_c94e919b":"No score yet","not_available_until_next_submission_2175158":"Not available until next submission","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Mastery set at","no_results":"(no results)"}}},"en-AU-x-unimelb":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Current Mastery Score: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Latest Assessment","no_score_yet_c94e919b":"No score yet","not_available_until_next_submission_2175158":"Not available until next submission","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Mastery set at","no_results":"(no results)"}}},"en-CA":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Current Mastery Score: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Latest Assessment","no_score_yet_c94e919b":"No score yet","not_available_until_next_submission_2175158":"Not available until next submission","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Mastery set at","no_results":"(no results)"}}},"en-GB":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Current Mastery Score: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Latest Assessment","no_score_yet_c94e919b":"No score yet","not_available_until_next_submission_2175158":"Not available until next submission","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Mastery set at","no_results":"(no results)"}}},"en-GB-x-lbs":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Current Mastery Score: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Latest Assessment","no_score_yet_c94e919b":"No score yet","not_available_until_next_submission_2175158":"Not available until next submission","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Mastery set at","no_results":"(no results)"}}},"en-GB-x-ukhe":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Current Mastery Score: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Latest Assessment","no_score_yet_c94e919b":"No score yet","not_available_until_next_submission_2175158":"Not available until next submission","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Mastery set at","no_results":"(no results)"}}},"es":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Puntaje de dominio actual: %{score} de %{mastery_points}","latest_assessment_a85280ef":"Última evaluación","no_score_yet_c94e919b":"Aún sin puntaje","not_available_until_next_submission_2175158":"No está disponible hasta la próxima entrega","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Competencia: el estado de %{title} es %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Dominio establecido en","no_results":"(sin resultados)"}}},"fa":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"نمره تسلط فعلی: %{score} از %{mastery_points}","latest_assessment_a85280ef":"آخرین ارزیابی","no_score_yet_c94e919b":"هنوز هیچ نمره ای موجود نیست","not_available_until_next_submission_2175158":"تا مورد ارسالی بعدی در دسترس نیست","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"نتیجه: %{title} دارای وضعیت %{statusTooltip} است","outcomes":{"outcome_popover":{"mastery_set_at":"تسلط تعیین شده در","no_results":"(نتیجه موجود نیست)"}}},"fi":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Nykyinen osaamisen hallinnan pistemäärä: %{score}/%{mastery_points}","latest_assessment_a85280ef":"Tuorein arviointi","no_score_yet_c94e919b":"Ei vielä pistemäärää","not_available_until_next_submission_2175158":"Ei saatavissa ennen seuraavaa tehtäväpalautusta","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Osaamistulos: %{title} on tilassa %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Osaamistulos saavutettu pistemäärällä: ","no_results":"(ei tuloksia)"}}},"fr":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Score actuel de maîtrise : %{score} sur %{mastery_points}","latest_assessment_a85280ef":"Dernière évaluation","no_score_yet_c94e919b":"Pas encore de résultat","not_available_until_next_submission_2175158":"Non disponible avant l\'envoi suivant","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Acquis : %{title} a le statut %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Niveau de maîtrise défini à","no_results":"(pas de résultats)"}}},"fr-CA":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Score actuel de maîtrise : %{score} sur %{mastery_points}","latest_assessment_a85280ef":"Dernière évaluation","no_score_yet_c94e919b":"Pas encore de résultat","not_available_until_next_submission_2175158":"Non disponible avant l\'envoi suivant","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Résultat : %{title} a le statut %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Niveau de maîtrise défini à","no_results":"(pas de résultats)"}}},"he":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"ציון רמת מומחיות נוכחית: %{score}  מתוך %{mastery_points}","latest_assessment_a85280ef":"הערכה אחרונה","no_score_yet_c94e919b":"אין ציון עדיין","not_available_until_next_submission_2175158":"לא זמין עד ההגשה הבאה","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"תוצאות למידה: %{title} בסטטוס של %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"התמחות הוגדרה ל","no_results":"(אין תוצאות)"}}},"ht":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Nòt Mwetriz Aktyèl: %{score} sou %{mastery_points}","latest_assessment_a85280ef":"Dènye Evalyasyon","no_score_yet_c94e919b":"Poko gen nòt","not_available_until_next_submission_2175158":"Pa Disponib","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Rezilta: %{title} gen estati %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Metriz fikse a","no_results":"(okenn rezilta)"}}},"hu":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Aktuális jártassági pontszám: %{score}, ennyi pontból: %{mastery_points}","latest_assessment_a85280ef":"Utolsó értékelés","no_score_yet_c94e919b":"Még nincs pontszám","not_available_until_next_submission_2175158":"A következő beadásig nem érhető el","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Tanulási eredmény: %{title} a következő státuszú %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Elfogadási szint beállítva erre:","no_results":"(nincsenek eredmények)"}}},"hy":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Ընթացիկ անցողիկ միավոր՝ %{mastery_points}-ից %{score} ","latest_assessment_a85280ef":"Վերջին գնահատումը","no_score_yet_c94e919b":"Գնահատականներ դեռևս չկան","not_available_until_next_submission_2175158":"Հասանելի չէ մինչև հաջորդ հանձնարարության հանձնումը","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Արդյունք՝ %{title} ունի %{statusTooltip} կարգավիճակ","outcomes":{"outcome_popover":{"mastery_set_at":"Յուրացման մակարդակը սահմանվել է՝","no_results":"(ոչ մի արդյունք)"}}},"is":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Núverandi tileinkunareinkunn: %{score} af %{mastery_points}","latest_assessment_a85280ef":"Nýjasta mat","no_score_yet_c94e919b":"Engin einkunn enn","not_available_until_next_submission_2175158":"Ekki tiltækt fyrr en við næstu skil","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Niðurstaða: %{title} hefur stöðu %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Tileinkun ákvörðuð","no_results":"(engar niðurstöður)"}}},"it":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Punteggio livello di padronanza attuale: %{score} su %{mastery_points}","latest_assessment_a85280ef":"Valutazione più recente","no_score_yet_c94e919b":"Ancora nessun punteggio","not_available_until_next_submission_2175158":"Non disponibile fino alla consegna successiva","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Esito: %{title} è in stato %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Livello di padronanza impostato su","no_results":"(nessun esito)"}}},"ja":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"現在の熟達スコア: %{mastery_points}のうちの%{score}","latest_assessment_a85280ef":"最新の課題","no_score_yet_c94e919b":"まだスコアがありません","not_available_until_next_submission_2175158":"次回の提出まで使用不可です","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"成果: %{title} の状況は %{statusTooltip} です","outcomes":{"outcome_popover":{"mastery_set_at":"達成の設定:","no_results":"(結果なし)"}}},"mi":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Kaute Mana nāianei: %{score} i roto i te %{mastery_points}","latest_assessment_a85280ef":"aromatawai Latest","no_score_yet_c94e919b":"Kaore anō he Kaute","not_available_until_next_submission_2175158":"Kāore i te wātea tae noa ki te tāpaetanga muri mai","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Putanga: %{title} he mana o te %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Whakaturia Mātatau i","no_results":"(kahore he hua)"}}},"nb":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Nåværende ekspertise resultat: %{score} av %{mastery_points}","latest_assessment_a85280ef":"Siste vurdering","no_score_yet_c94e919b":"Ingen resultat ennå","not_available_until_next_submission_2175158":"Ikke tilgjengelig før neste innsending","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsmål: %{title} har status som %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Ekspertisenivå","no_results":"(ingen resultater)"}}},"nb-x-k12":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Nåværende ekspertise resultat: %{score} av %{mastery_points}","latest_assessment_a85280ef":"Siste vurdering","no_score_yet_c94e919b":"Ingen resultat ennå","not_available_until_next_submission_2175158":"Ikke tilgjengelig før neste innsending","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Mål: %{title} har status som %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Ekspertisenivå","no_results":"(ingen resultater)"}}},"nl":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Huidige Meesterschap Score: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Laatste beoordeling","no_score_yet_c94e919b":"Geen score gegeven","not_available_until_next_submission_2175158":"Niet beschikbaar tot volgende inzending","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Resultaat: %{title}heeft een status van %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Meesterschap ingesteld bij","no_results":"(geen resultaten)"}}},"nn":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Gjeldande meistringsnivå: %{score} av %{mastery_points}","latest_assessment_a85280ef":"Siste innlevering","no_score_yet_c94e919b":"Ingen resultat","not_available_until_next_submission_2175158":"Ikkje tilgjengeleg før neste innlevering","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsmål: %{title} har status som %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Meistringsnivå sett til","no_results":"(utan resultat)"}}},"pl":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Obecny wynik poziomu biegłości: %{score} out of %{mastery_points}","latest_assessment_a85280ef":"Najnowsza ocena","no_score_yet_c94e919b":"Jeszcze nie ma wyniku punktowego","not_available_until_next_submission_2175158":"Niedostępne do kolejnego zgłoszenia","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Wynik: %{title} ma status %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Biegłość ustawiono na","no_results":"(brak wyników)"}}},"pt":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Pontuação atual de Domínio: %{score} de %{mastery_points}","latest_assessment_a85280ef":"Última Avaliação","no_score_yet_c94e919b":"Ainda sem pontuação","not_available_until_next_submission_2175158":"Não disponível até à próxima entrega","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Resultado: %{title} tem um estado de %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Domínio definido em","no_results":"(nenhum resultado)"}}},"pt-BR":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Pontuação atual de domínio: %{score} de %{mastery_points}","latest_assessment_a85280ef":"Avaliação Mais Recente","no_score_yet_c94e919b":"Sem pontuação ainda","not_available_until_next_submission_2175158":"Não está disponível até o próximo envio","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Objetivo: %{title} tem um status de %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Domínio definido em","no_results":"(sem resultados)"}}},"ru":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Текущий проходной балл: %{score} из %{mastery_points}","latest_assessment_a85280ef":"Последнее оценивание","no_score_yet_c94e919b":"Оценок пока нет","not_available_until_next_submission_2175158":"Не доступно до следующей сдачи домашнего задания","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Результат: %{title} имеет статус %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Усвоение установлено на","no_results":"(без результата)"}}},"sl":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Trenutni rezultat odličnosti: %{score} od %{mastery_points}","latest_assessment_a85280ef":"Najnovejše ocenjevanje","no_score_yet_c94e919b":"Nobene ocene še ni.","not_available_until_next_submission_2175158":"Ni na voljo do naslednje oddaje.","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Izid: %{title} ima status %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Odličnost na","no_results":"(ni rezultatov)"}}},"sv":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Nuvarande måluppfyllelsepoäng: %{score} av %{mastery_points}","latest_assessment_a85280ef":"Senaste bedömning","no_score_yet_c94e919b":"Inga resultat ännu","not_available_until_next_submission_2175158":"Inte tillgänglig förrän nästa inlämning","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Lärandemål: %{title} har statusen %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Måluppfyllelse satt vid","no_results":"(inga resultat)"}}},"sv-x-k12":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Nuvarande måluppfyllelsepoäng: %{score} av %{mastery_points}","latest_assessment_a85280ef":"Senaste bedömning","no_score_yet_c94e919b":"Inga resultat ännu","not_available_until_next_submission_2175158":"Inte tillgänglig förrän nästa inlämning","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Lärandemål: %{title} har statusen %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Måluppfyllelse satt vid","no_results":"(inga resultat)"}}},"tr":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Geçerli Yeterlik Puanı: %{mastery_points} üzerinden %{score}","latest_assessment_a85280ef":"En son Değerlendirme","no_score_yet_c94e919b":"Henüz puan yok","not_available_until_next_submission_2175158":"Bir sonraki gönderime kadar kullanılamaz","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Öğrenme çıktısı: %{title} nin durumu %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Yeterlik şuna ayarlandı","no_results":"(sonuç yok)"}}},"uk":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"Поточний основний бал: %{score} з %{mastery_points}","latest_assessment_a85280ef":"Остання оцінка","no_score_yet_c94e919b":"Оцінка відсутня ще","not_available_until_next_submission_2175158":"Не доступно до наступного відправлення","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Вихідний: %{title} має статус %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"Майстерність встановлена на","no_results":"(результати відсутні)"}}},"zh-Hans":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"目前的掌握得分: %{score} 超出 %{mastery_points}","latest_assessment_a85280ef":"最新的评估","no_score_yet_c94e919b":"尚无分数","not_available_until_next_submission_2175158":"不可用，直到下次提交","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"结果：%{title} 的状态为 %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"掌握程度设置为","no_results":"(无结果)"}}},"zh-Hant":{"current_mastery_score_score_out_of_mastery_points_fedd343f":"目前的精通分數：總分 %{mastery_points}， 得分 %{score}","latest_assessment_a85280ef":"最新的評估","no_score_yet_c94e919b":"尚無分數","not_available_until_next_submission_2175158":"下次提交前不可用","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"成果：%{title} 的狀態為 %{statusTooltip}","outcomes":{"outcome_popover":{"mastery_set_at":"精通標準設置於","no_results":"（沒有結果）"}}}}'))
s("jQeR")
s("0sPK")
o["default"].scoped("outcomes.outcome_popover")
s("BGrI")
var i=s("dbrX")
var _=n.a.default
var l=_.template,c=_.templates=_.templates||{}
var u="/work/canvas-deploy/generated/ui/shared/outcomes/jst/outcomePopover"
c[u]=l((function(e,t,s,a,n){this.compilerInfo=[4,">= 1.0.0"]
s=this.merge(s,e.helpers)
a=this.merge(a,e.partials)
n=n||{}
var o,r="",i="function",_=this.escapeExpression,l=this,c=s.helperMissing
function u(e,t){return"-dialog"}function m(e,t){var a,n,o,r=""
r+='\n    <div class="title">'
if(n=s.path)a=n.call(e,{hash:{},data:t})
else{n=e&&e.path
a=typeof n===i?n.call(e,{hash:{},data:t}):n}r+=_(a)+'</div>\n    <div class="chart">\n      '
a=s["if"].call(e,(a=e&&e.ratings,null==a||false===a?a:a.result_count),{hash:{},inverse:l.program(10,g,t),fn:l.program(4,d,t),data:t});(a||0===a)&&(r+=a)
r+='\n    </div>\n    <div class="outcome-info">\n      <div class="mastery">'+_((n=s.t||e&&e.t,o={hash:{scope:"outcomes.outcome_popover"},data:t},n?n.call(e,"mastery_set_at","Mastery set at",o):c.call(e,"t","mastery_set_at","Mastery set at",o)))+": "+_((n=s.nf||e&&e.nf,o={hash:{format:"outcomeScore"},data:t},n?n.call(e,e&&e.mastery_points,o):c.call(e,"nf",e&&e.mastery_points,o)))+"</div>\n      "
a=s["if"].call(e,e&&e.description,{hash:{},inverse:l.noop,fn:l.program(12,b,t),data:t});(a||0===a)&&(r+=a)
r+="\n    </div>\n    "
return r}function d(e,t){var a,n=""
n+='\n        <div class="chart-image pull-left"></div>\n        <ol class="ratings pull-left">\n        '
a=s.each.call(e,e&&e.ratings,{hash:{},inverse:l.noop,fn:l.programWithDepth(5,f,t,e),data:t});(a||0===a)&&(n+=a)
n+="\n        </ol>\n      "
return n}function f(e,t,a){var n,o,r=""
r+='\n          <li class="rating"><span class="legend-color"\n            style="background-color: '
n=s["if"].call(e,a&&a.account_level_scales,{hash:{},inverse:l.program(8,y,t),fn:l.program(6,p,t),data:t});(n||0===n)&&(r+=n)
r+='"\n          />\n            '
if(o=s.description)n=o.call(e,{hash:{},data:t})
else{o=e&&e.description
n=typeof o===i?o.call(e,{hash:{},data:t}):o}r+=_(n)+"\n          </li>\n        "
return r}function p(e,t){var a,n,o=""
o+=" #"
if(n=s.color)a=n.call(e,{hash:{},data:t})
else{n=e&&e.color
a=typeof n===i?n.call(e,{hash:{},data:t}):n}o+=_(a)+" "
return o}function y(e,t){var a,n,o=""
o+=" "
if(n=s.color)a=n.call(e,{hash:{},data:t})
else{n=e&&e.color
a=typeof n===i?n.call(e,{hash:{},data:t}):n}o+=_(a)+" "
return o}function g(e,t){var a,n,o=""
o+='\n        <p><img src="/images/pie-chart-disabled.png" /></p>\n        <p>'+_((a=s.t||e&&e.t,n={hash:{scope:"outcomes.outcome_popover"},data:t},a?a.call(e,"no_results","(no results)",n):c.call(e,"t","no_results","(no results)",n)))+"</p>\n      "
return o}function b(e,t){var a,n,o=""
o+='<div class="description">'
if(n=s.description)a=n.call(e,{hash:{},data:t})
else{n=e&&e.description
a=typeof n===i?n.call(e,{hash:{},data:t}):n}(a||0===a)&&(o+=a)
o+="</div>"
return o}function h(e,t){var a,n,o,r=""
r+='\n    <div class="mastery-details pull-right" aria-hidden="true">\n      '
a=s["if"].call(e,e&&e.scoreDefined,{hash:{},inverse:l.program(17,k,t),fn:l.program(15,v,t),data:t});(a||0===a)&&(r+=a)
r+='\n    </div>\n    <div class="recent-details">\n      <div class="last-assessment">'+_((n=s.t||e&&e.t,o={hash:{i18n_inferred_key:true},data:t},n?n.call(e,"latest_assessment_a85280ef","Latest Assessment",o):c.call(e,"t","latest_assessment_a85280ef","Latest Assessment",o)))+":&nbsp;\n        "
a=s["if"].call(e,e&&e.result_title,{hash:{},inverse:l.program(21,R,t),fn:l.program(19,w,t),data:t});(a||0===a)&&(r+=a)
r+='\n      </div>\n      <div class="submission-time">'+_((n=s.datetimeFormatted||e&&e.datetimeFormatted,o={hash:{},data:t},n?n.call(e,e&&e.submission_time,o):c.call(e,"datetimeFormatted",e&&e.submission_time,o)))+'</div>\n    </div>\n    <div class="screenreader-only">\n      '
a=s["if"].call(e,e&&e.score,{hash:{},inverse:l.program(25,B,t),fn:l.program(23,x,t),data:t});(a||0===a)&&(r+=a)
r+="\n    </div>\n    "
return r}function v(e,t){var a,n,o=""
o+="\n        <span>"+_((a=s.nf||e&&e.nf,n={hash:{format:"outcomeScore"},data:t},a?a.call(e,e&&e.roundedScore,n):c.call(e,"nf",e&&e.roundedScore,n)))+" / "+_((a=s.nf||e&&e.nf,n={hash:{format:"outcomeScore"},data:t},a?a.call(e,e&&e.mastery_points,n):c.call(e,"nf",e&&e.mastery_points,n)))+'</span>\n        <span class="screenreader-only">\n          '+_((a=s.t||e&&e.t,n={hash:{title:e&&e.title,statusTooltip:e&&e.statusTooltip,i18n_inferred_key:true},data:t},a?a.call(e,"outcome_title_has_a_status_of_statustooltip_82d1e6d9","Outcome: %{title} has a status of %{statusTooltip}",n):c.call(e,"t","outcome_title_has_a_status_of_statustooltip_82d1e6d9","Outcome: %{title} has a status of %{statusTooltip}",n)))+"\n        </span>\n        "+_((a=s.addMasteryIcon||e&&e.addMasteryIcon,n={hash:{},data:t},a?a.call(e,e&&e.status,n):c.call(e,"addMasteryIcon",e&&e.status,n)))+"\n      "
return o}function k(e,t){var a,n,o=""
o+="\n        <span>"+_((a=s.t||e&&e.t,n={hash:{i18n_inferred_key:true},data:t},a?a.call(e,"no_score_yet_c94e919b","No score yet",n):c.call(e,"t","no_score_yet_c94e919b","No score yet",n)))+"</span>\n      "
return o}function w(e,t){var a,n,o=""
o+="\n          "
if(n=s.result_title)a=n.call(e,{hash:{},data:t})
else{n=e&&e.result_title
a=typeof n===i?n.call(e,{hash:{},data:t}):n}o+=_(a)+"\n        "
return o}function R(e,t){var a,n,o=""
o+="\n          "+_((a=s.t||e&&e.t,n={hash:{i18n_inferred_key:true},data:t},a?a.call(e,"not_available_until_next_submission_2175158","Not available until next submission",n):c.call(e,"t","not_available_until_next_submission_2175158","Not available until next submission",n)))+"\n        "
return o}function x(e,t){var a,n,o=""
o+="\n        "+_((a=s.t||e&&e.t,n={hash:{score:e&&e.score,mastery_points:e&&e.mastery_points,i18n_inferred_key:true},data:t},a?a.call(e,"current_mastery_score_score_out_of_mastery_points_fedd343f","Current Mastery Score: %{score} out of %{mastery_points}",n):c.call(e,"t","current_mastery_score_score_out_of_mastery_points_fedd343f","Current Mastery Score: %{score} out of %{mastery_points}",n)))+"\n      "
return o}function B(e,t){var a,n,o=""
o+="\n        "+_((a=s.t||e&&e.t,n={hash:{i18n_inferred_key:true},data:t},a?a.call(e,"no_score_yet_c94e919b","No score yet",n):c.call(e,"t","no_score_yet_c94e919b","No score yet",n)))+"\n      "
return o}r+='<div class="outcome-details'
o=s["if"].call(t,t&&t.dialog,{hash:{},inverse:l.noop,fn:l.program(1,u,n),data:n});(o||0===o)&&(r+=o)
r+='">\n  <div class="line-graph">\n    <div class="screenreader-only"></div>\n  </div>\n  <div class="head-content" tabindex="0">\n    '
o=s["if"].call(t,t&&t.path,{hash:{},inverse:l.program(14,h,n),fn:l.program(3,m,n),data:n});(o||0===o)&&(r+=o)
r+='\n  </div>\n  <div class="calculation-method-example method-details content-box pad-box-mini border border-trbl border-round">\n    '
o=l.invokePartial(a["ui/shared/outcomes/jst/_calculationMethodExample.handlebars"],"ui/shared/outcomes/jst/_calculationMethodExample.handlebars",t,s,a,n);(o||0===o)&&(r+=o)
r+="\n  </div>\n</div>\n"
return r}))
i["a"].loadStylesheet("jst/outcomes/outcomePopover",{new_styles_normal_contrast:{combinedChecksum:"538a4912e7"},new_styles_high_contrast:{combinedChecksum:"538a4912e7"},responsive_layout_normal_contrast:{combinedChecksum:"538a4912e7"},responsive_layout_high_contrast:{combinedChecksum:"538a4912e7"},new_styles_normal_contrast_rtl:{combinedChecksum:"4d51acaf87"},new_styles_high_contrast_rtl:{combinedChecksum:"4d51acaf87"},responsive_layout_normal_contrast_rtl:{combinedChecksum:"4d51acaf87"},responsive_layout_high_contrast_rtl:{combinedChecksum:"4d51acaf87"}}[i["a"].getCssVariant()])
t["a"]=c[u]},xm5c:function(e,t,s){"use strict"
s.d(t,"a",(function(){return n}))
var a=s("QF4Q")
function n(e,t){var s=e&&Object(a["a"])(e)
if(!s)return false
return s.matches?s.matches(t):s.msMatchesSelector(t)}}}])

//# sourceMappingURL=99-c-4e3e8591d5.js.map