(window["canvasWebpackJsonp"]=window["canvasWebpackJsonp"]||[]).push([[18],{"/656":function(e,t,n){"use strict"
n.d(t,"a",(function(){return P}))
var a=n("rePB")
var r=n("VTBJ")
var i=n("1OyB")
var s=n("vuIU")
var o=n("Ji7U")
var c=n("LK+K")
n("DEX3")
var h=n("q1tI")
var u=n.n(h)
var l=n("17x9")
var d=n.n(l)
var v=n("TSYQ")
var f=n.n(v)
var p=n("J2CL")
var g=n("eGSd")
var _=n("8o96")
var b=n("i/8D")
var m=n("KI51")
var x=n("sQ3tx")
var O=n("II2N")
var y=n("oXx0")
var j=n("dKDz")
var T=n.n(j)
var k=n("7/OM")
var w=n("IPIv")
var I=n("gCYW")
var S=n("OI1/")
function C(e,t){var n=0
for(var a=0;a<e.length;a++){var r=e[a]
n+=L(r.textContent,1===r.nodeType?r:t)}return n}function L(e,t){var n=Object(w["a"])(t)
var a=document.createElement("canvas")
document.createDocumentFragment().appendChild(a)
var r=a.getContext("2d")
if(!r||!e)return 0
var i=e
var s=0
var o=0
var c=[n["font-weight"],n["font-style"],n["font-size"],n["font-family"]].join(" ")
r.font=c
"uppercase"===n["text-transform"]?i=e.toUpperCase():"lowercase"===n["text-transform"]?i=e.toLowerCase():"capitalize"===n["text-transform"]&&(i=e.replace(/\b\w/g,(function(e){return e.toUpperCase()})))
"normal"!==n["letter-spacing"]&&(s=i.length*parseInt(n["letter-spacing"]))
o=r.measureText(i).width+s
return o}var E=C
function H(e,t){var n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2]
var a=!(arguments.length>3&&void 0!==arguments[3])||arguments[3]
var r=arguments.length>4&&void 0!==arguments[4]&&arguments[4]
var i=e
var s=i.charAt(0)
var o=i.slice(-1)
n&&-1!==t.indexOf(s)&&(i=i.slice(1))
a&&-1!==t.indexOf(o)&&(i=i.slice(0,-1))
r&&(i=H(i,t,n,a,false))
return i}var D=H
function M(e,t){var n=arguments.length>2&&void 0!==arguments[2]&&arguments[2]
var a=t.truncate,r=t.ignore,i=t.ellipsis
var s=Object(k["a"])(e)
var o=-1
var c=-1
var h=function(){for(var e=0;e<s.length;e++){var t=s[e]
if(-1!==t.indexOf(i)){o=e
c=t.indexOf(i)}}}
if("character"===a){h()
var u=s[o]
if(u){var l=u[c-1]
l&&-1!==r.indexOf(l)&&s[o].splice(c-1,1)
if(!l){var d=null
var v=o-1
while(v>=0){d=s[v]
if(d.length>0)break
v--}if(d){var f=String(d.slice(-1));-1!==r.indexOf(f)&&(s[v].length-=1)}}}h()
u=s[o]
if(u){var p=u[c+1]
p&&-1!==r.indexOf(p)&&s[o].splice(c+1,1)
if(!p){var g=null
var _=o+1
while(_<s.length){g=s[_]
if(g.length>0)break
_++}if(g){var b=String(g[0]);-1!==r.indexOf(b)&&s[_].shift()}}}}else{h()
var m=s[o]
if(m){var x=m[c-1]
x&&-1!==r.indexOf(x.slice(-1))&&(1===x.length?s[o].splice([c-1],1):s[o][c-1]=x.slice(0,-1))
if(!x){var O=null
var y=o-1
while(y>=0){O=s[y]
if(O.length>0)break
y--}if(O){var j=String(O.slice(-1)).slice(-1)
if(-1!==r.indexOf(j)){var T=O.length-1
s[y][T]=O[T].slice(0,-1)}}}}}n&&(s=M(s,t,false))
return s}var U=M
function N(e,t){var n=new W(e,t)
if(n)return n.truncate()}var W=function(){function e(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{}
Object(i["a"])(this,e)
this._options={parent:n.parent||t.parentElement,maxLines:n.maxLines||1,position:n.position||"end",truncate:n.truncate||"character",ellipsis:n.ellipsis||"…",ignore:n.ignore||[" ",".",","],lineHeight:n.lineHeight||1.2,shouldTruncateWhenInvisible:n.shouldTruncateWhenInvisible}
if(!t)return
this._stage=t
n.parent?this._parent=this._options.parent:this._parent="auto"===this._options.maxLines?this._stage.parentElement:this._stage
this._truncatedText=this._parent.textContent
this._isTruncated=false
this.setup()}Object(s["a"])(e,[{key:"setup",value:function(){if(!this._stage)return
var e=this._options,t=e.maxLines,n=e.truncate,a=e.lineHeight
var r=Object(w["a"])(this._parent)
var i="normal"===r.lineHeight?a*parseFloat(r.fontSize):parseFloat(r.lineHeight)
var s=this._stage.firstChild.children?this._stage.firstChild:this._stage
var o=[]
var c=[]
this._nodeMap=this.getNodeMap(s)
for(var h=0;h<this._nodeMap.length;h++){var u=this._nodeMap[h]
"word"===n&&" "===u.data[u.data.length-1]&&(u.data.length-=1)
c[h]=u.data
for(var l=0;l<u.data.length;l++)o.push(h)}this._defaultStringData=Object(k["a"])(c)
this._nodeDataIndexes=Object(k["a"])(o)
this._maxHeight="auto"===t?Object(I["a"])(this._parent).height:t*i
this._maxWidth=E(this._nodeMap.map((function(e){return e.node})),this._parent)
this._maxLines="auto"===t?Math.round(this._maxHeight/i):t}},{key:"getNodeMap",value:function(e){var t=this._options,n=t.shouldTruncateWhenInvisible,a=t.truncate
var r=Array.from(e.childNodes)
var i=[]
r.forEach((function(e){if(1===e.nodeType||3===e.nodeType){var t=!!n||Object(S["a"])(e,false)
var r=e.textContent+" "
i.push({node:e,data:"word"===a?t?r.match(/.*?[\.\s\/]+?/g):"":t?e.textContent.split(""):[]})}}))
return i}},{key:"getNodeIndexes",value:function(e){var t=[]
for(var n=0;n<e.length;n++)for(var a=0;a<e[n].length;a++)t.push([n,a])
return t}},{key:"domString",value:function(e){var t=Object.keys(e)
var n=""
for(var a=0;a<t.length;a++){var r=t[a]
var i=this._nodeMap[r]
var s=e[r].join("")
var o=T()(s)
if(1===i.node.nodeType){var c=i.node.nodeName
var h=i.node.attributes
var u=""
for(var l=0;l<h.length;l++){var d=h[l]
u+=" ".concat(d.nodeName,'="').concat(d.nodeValue,'"')}n+="<".concat(c).concat(u,">").concat(o,"</").concat(c,">")}else 3===i.node.nodeType&&(n+=o)}return n}},{key:"checkFit",value:function(e){var t=this.domString(e)
var n="auto"===this._options.maxLines?this._stage:this._parent
var a=true
this._stage.innerHTML=t
Object(I["a"])(n).height-this._maxHeight>.5&&(a=false)
return a}},{key:"truncate",value:function(){var e=this._options,t=e.ellipsis,n=e.ignore,a=e.position
var r="middle"===a
var i=false
var s=false
var o=0
var c=0
var h=""
var u=null
var l=null
if(!this._stage)return
u=Object(k["a"])(this._defaultStringData)
c=r?0:this._nodeDataIndexes.length-1
while(!s){if(c<0)break
if(r){var d=this.getNodeIndexes(u)
var v=Math.floor(d.length/2)
l=d[v]
c>0&&u[l[0]].splice(l[1],1,t)}else{o=this._nodeDataIndexes[c]
if(c<this._nodeDataIndexes.length-1){u[o]=u[o].slice(0,-1)
u[o].push(t)}}s=this.checkFit(u)
if(s){for(var f=0;f<u.length;f++){var p=u[f]
h+=p.join("")}break}i=true
if(r){u[l[0]].splice(l[1],1)
c++}else{u[o]=u[o].slice(0,-1)
c--}}u=U(u,this._options,true)
if(i&&!r){h=D(h.split(t)[0],n,false,true,true)
h+=t}else if(i&&r){var g=h.split(t)
h=D(g[0],n,false,true,true)+t+D(g[1],n,true,false,true)}return{isTruncated:i,text:h,data:u,constraints:{width:this._maxWidth,height:this._maxHeight,lines:this._maxLines}}}}])
return e}()
var X=N
function F(e){var t=e.typography
return{fontFamily:t.fontFamily,lineHeight:t.lineHeight}}var K,z,R,B,J,q
var A={componentId:"bjXfh",template:function(e){return"\n\n.bjXfh_daKB{display:block;font-family:".concat(e.fontFamily||"inherit",";overflow:hidden;word-wrap:break-word}\n\n.bjXfh_uUeq{height:100%}\n\n.bjXfh_exvv{display:block;max-height:0;visibility:hidden}")},truncated:"bjXfh_daKB",auto:"bjXfh_uUeq",spacer:"bjXfh_exvv"}
var P=(K=Object(y["a"])(),z=Object(p["l"])(F,A),R=Object(m["a"])(["shouldTruncateWhenInvisible"]),K(B=z(B=R(B=(q=J=function(e){Object(o["a"])(n,e)
var t=Object(c["a"])(n)
function n(e){var a
Object(i["a"])(this,n)
a=t.call(this,e)
a.update=function(){a._ref&&a.setState(a.initialState)}
a.state=a.initialState
return a}Object(s["a"])(n,[{key:"componentDidMount",value:function(){var e=this.props.children
if(e){this.checkChildren()
this._text=Object(x["a"])(e)
this.truncate()
if(0===this.props.debounce)this._resizeListener=Object(_["a"])(this._ref,this.update)
else{this._debounced=Object(g["a"])(this.update,this.props.debounce,{leading:true,trailing:true})
this._resizeListener=Object(_["a"])(this._ref,this._debounced)}}}},{key:"componentWillUnmount",value:function(){this._resizeListener&&this._resizeListener.remove()
this._debounced&&this._debounced.cancel()}},{key:"componentDidUpdate",value:function(e,t){var n=this.props,a=n.children,r=n.onUpdate
var i=this.state,s=i.isTruncated,o=i.needsSecondRender,c=i.truncatedText
if(a){if(e!==this.props){if(e.children!==this.props.children){this.checkChildren()
this._text=Object(x["a"])(a)}this.setState(this.initialState)
return}if(o||!s&&!this._wasTruncated)this.truncate()
else{r(s,c)
this._wasTruncated=s}}}},{key:"checkChildren",value:function(){}},{key:"truncate",value:function(){if(!this.state.needsSecondRender)return
if(b["a"]){var e=X(this._stage,Object(r["a"])({},this.props,{parent:this._ref,lineHeight:this.theme.lineHeight}))
if(e){var t=this.renderChildren(e.isTruncated,e.data,e.constraints.width)
this.setState({needsSecondRender:false,isTruncated:e.isTruncated,truncatedElement:t,truncatedText:e.text})}}else this.setState({needsSecondRender:false,isTruncated:false,truncatedElement:this._text,truncatedText:this._ref.textContent})}},{key:"renderChildren",value:function(e,t,n){if(!e)return this._text
var a=[]
for(var r=0;r<t.length;r++){var i=t[r]
var s=this._text.props.children[r]
var o=i.join("")
s&&s.props?a.push(Object(O["a"])(s,s.props,o)):a.push(o)}a.push(u.a.createElement("span",{className:A.spacer,style:{width:n||null}}))
var c=u.a.Children.map(a,(function(e){return e}))
return this._text.props?Object(O["a"])(this._text,this._text.props,c):c}},{key:"render",value:function(){var e,t=this
var n=this.state.truncatedElement
var r=this.props,i=r.maxLines,s=r.children
return u.a.createElement("span",{className:f()((e={},Object(a["a"])(e,A.truncated,true),Object(a["a"])(e,A.auto,"auto"===i),e)),ref:function(e){t._ref=e}},s&&(n?null:u.a.createElement("span",{ref:function(e){t._stage=e}},Object(x["a"])(s))),n)}},{key:"initialState",get:function(){return{isTruncated:false,needsSecondRender:true,truncatedElement:null,truncatedText:null}}}])
n.displayName="TruncateText"
return n}(h["Component"]),J.propTypes={children:d.a.node.isRequired,maxLines:d.a.oneOfType([d.a.string,d.a.number]),position:d.a.oneOf(["end","middle"]),truncate:d.a.oneOf(["character","word"]),ellipsis:d.a.string,ignore:d.a.arrayOf(d.a.string),debounce:d.a.number,onUpdate:d.a.func,shouldTruncateWhenInvisible:d.a.bool},J.defaultProps={maxLines:1,ellipsis:"…",truncate:"character",position:"end",ignore:[" ",".",","],debounce:0,onUpdate:function(e,t){}},q))||B)||B)||B)},"7/OM":function(e,t,n){"use strict"
n.d(t,"a",(function(){return a}))
function a(e){var t
if(Array.isArray(e)){t=e.slice(0)
for(var n=0;n<t.length;n++)t[n]=a(t[n])
return t}return e}},KI51:function(e,t,n){"use strict"
n.d(t,"a",(function(){return a}))
n("1OyB")
n("vuIU")
n("foSv")
n("ReuC")
n("Ji7U")
n("LK+K")
n("DEX3")
n("jcII")
var a=function(){return function(e){return e}}},"OI1/":function(e,t,n){"use strict"
n.d(t,"a",(function(){return i}))
var a=n("QF4Q")
var r=n("IPIv")
function i(e){var t=!(arguments.length>1&&void 0!==arguments[1])||arguments[1]
var n=e&&Object(a["a"])(e)
if(n===window||n===document||n===document.body)return true
var s=n.parentNode
if(3===n.nodeType)return i(s,t)
var o=Object(r["a"])(n)
if("none"===o.display)return false
if("hidden"===o.visibility||"0"===o.opacity)return false
if("hidden"===o.overflow&&"absolute"===o.position&&"auto"!==o.clip){var c=o.clip.substring(5).slice(0,-1).split(", ")
var h=true
c.forEach((function(e){"0px"!==e&&(h=false)}))
if(h)return false}return!t||!s||i(s)}}}])

//# sourceMappingURL=18-c-5bf9d96323.js.map