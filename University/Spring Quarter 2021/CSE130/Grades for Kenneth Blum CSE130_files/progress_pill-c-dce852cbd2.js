(window["canvasWebpackJsonp"]=window["canvasWebpackJsonp"]||[]).push([[527],{"+ayY":function(e,n,i){"use strict"
i.d(n,"a",(function(){return f}))
var t=i("VTBJ")
var a=i("1OyB")
var o=i("vuIU")
var s=i("Ji7U")
var r=i("LK+K")
var u=i("q1tI")
var d=i.n(u)
var c=i("CSQ8")
var l=d.a.createElement("path",{d:"M960,0 C1490.19336,0 1920,429.80664 1920,960 C1920,1490.19336 1490.19336,1920 960,1920 C429.80664,1920 0,1490.19336 0,960 C0,429.80664 429.80664,0 960,0 Z M960,101.052632 C485.616468,101.052632 101.052632,485.616468 101.052632,960 C101.052632,1434.38353 485.616468,1818.94737 960,1818.94737 C1434.38353,1818.94737 1818.94737,1434.38353 1818.94737,960 C1818.94737,485.616468 1434.38353,101.052632 960,101.052632 Z M950.679927,1322.54386 C870.655911,1322.54386 805.551855,1387.64792 805.551855,1467.67193 C805.551855,1547.69595 870.655911,1612.8 950.679927,1612.8 C1030.70394,1612.8 1095.808,1547.69595 1095.808,1467.67193 C1095.808,1387.64792 1030.70394,1322.54386 950.679927,1322.54386 Z M1143.46512,353.684211 L757.894737,353.684211 L851.795993,1205.0114 L1049.56386,1205.0114 L1143.46512,353.684211 Z",fillRule:"evenodd",stroke:"none",strokeWidth:"1"})
var f=function(e){Object(s["a"])(i,e)
var n=Object(r["a"])(i)
function i(){Object(a["a"])(this,i)
return n.apply(this,arguments)}Object(o["a"])(i,[{key:"render",value:function(){return d.a.createElement(c["a"],Object.assign({},this.props,{name:"IconWarning",viewBox:"0 0 1920 1920"}),l)}}])
i.displayName="IconWarningLine"
return i}(u["Component"])
f.glyphName="warning"
f.variant="Line"
f.propTypes=Object(t["a"])({},c["a"].propTypes)},CyAq:function(e,n,i){"use strict"
i.d(n,"a",(function(){return t}))
function t(e){var n="".concat(e)
return[parseFloat(n,10),n.match(/[\d.\-\+]*\s*(.*)/)[1]||""]}},HMVb:function(e,n,i){"use strict"
i.d(n,"a",(function(){return c}))
var t=i("ODXe")
var a=i("i/8D")
var o=i("DUTp")
var s=i("IPIv")
var r={}
function u(e,n){if(!a["a"])return 16
var i=e||Object(o["a"])(e).documentElement
if(!n&&r[i])return r[i]
var t=parseInt(Object(s["a"])(i).getPropertyValue("font-size"))
r[i]=t
return t}var d=i("CyAq")
function c(e,n){var i=n||document.body
if(!e||"number"===typeof e)return e
var a=Object(d["a"])(e),o=Object(t["a"])(a,2),s=o[0],r=o[1]
return"rem"===r?s*u():"em"===r?s*u(i):s}},Vzc5:function(e,n,i){"use strict"
i.r(n)
var t=i("An8g")
var a=i("HGxv")
var o=i("8WeW")
Object(o["a"])(JSON.parse('{"ar":{"submission_failed_to_submit_8de4efcd":"تعذر إرسال الإرسال","uploading_submission_4c8d944b":"جارٍ تحميل الإرسال"},"ca":{"submission_failed_to_submit_8de4efcd":"No s\'ha pogut enviar l\'entrega","uploading_submission_4c8d944b":"S\'està carregant l\'entrega"},"cy":{"submission_failed_to_submit_8de4efcd":"Wedi methu cyflwyno’r Cyflwyniad","uploading_submission_4c8d944b":"Llwytho cyflwyniad i fyny"},"da":{"submission_failed_to_submit_8de4efcd":"Afleveringen blev ikke afleveret","uploading_submission_4c8d944b":"Uploader aflevering"},"da-x-k12":{"submission_failed_to_submit_8de4efcd":"Afleveringen blev ikke afleveret","uploading_submission_4c8d944b":"Uploader aflevering"},"de":{"submission_failed_to_submit_8de4efcd":"Abgabe fehlgeschlagen","uploading_submission_4c8d944b":"Abgabe wird hochgeladen"},"el":{"uploading_submission_4c8d944b":"Γίνεται Μεταφόρτωση της Υποβολής"},"en-AU":{"submission_failed_to_submit_8de4efcd":"Submission Failed to Submit","uploading_submission_4c8d944b":"Uploading Submission"},"en-AU-x-unimelb":{"submission_failed_to_submit_8de4efcd":"Submission Failed to Submit","uploading_submission_4c8d944b":"Uploading Submission"},"en-CA":{"submission_failed_to_submit_8de4efcd":"Submission Failed to Submit","uploading_submission_4c8d944b":"Uploading Submission"},"en-GB":{"submission_failed_to_submit_8de4efcd":"Submission Failed to Submit","uploading_submission_4c8d944b":"Uploading submission"},"en-GB-x-lbs":{"submission_failed_to_submit_8de4efcd":"Submission Failed to Submit","uploading_submission_4c8d944b":"Uploading submission"},"en-GB-x-ukhe":{"submission_failed_to_submit_8de4efcd":"Submission Failed to Submit","uploading_submission_4c8d944b":"Uploading submission"},"es":{"submission_failed_to_submit_8de4efcd":"Entrega no realizada","uploading_submission_4c8d944b":"Cargando la entrega"},"fa":{"submission_failed_to_submit_8de4efcd":"ارسال موارد ارسالی انجام نشد","uploading_submission_4c8d944b":"در حال بارگذاری مورد ارسالی"},"fi":{"submission_failed_to_submit_8de4efcd":"Tehtäväpalautuksen lähettäminen epäonnistui","uploading_submission_4c8d944b":"Ladataan tehtäväpalautusta"},"fr":{"submission_failed_to_submit_8de4efcd":"La soumission a échoué","uploading_submission_4c8d944b":"Envoi en cours"},"fr-CA":{"submission_failed_to_submit_8de4efcd":"Envoi non effectué","uploading_submission_4c8d944b":"Envoi en cours"},"he":{"uploading_submission_4c8d944b":"מעלה הגשה"},"ht":{"submission_failed_to_submit_8de4efcd":"Demand lan Pa Soumèt","uploading_submission_4c8d944b":"Chajman Soumisyon"},"hu":{"uploading_submission_4c8d944b":"Beadandó feltöltése"},"hy":{"uploading_submission_4c8d944b":"Ուղարկված ֆայլի ներբեռնում"},"is":{"submission_failed_to_submit_8de4efcd":"Ekki tókst að senda inn skila","uploading_submission_4c8d944b":"Hlaða upp skilum"},"it":{"submission_failed_to_submit_8de4efcd":"Invio consegna non riuscito","uploading_submission_4c8d944b":"Caricamento consegna"},"ja":{"submission_failed_to_submit_8de4efcd":"提出物を提出できませんでした","uploading_submission_4c8d944b":"提出物をアップロードしています"},"ko":{"uploading_submission_4c8d944b":"제출물 업로드"},"mi":{"submission_failed_to_submit_8de4efcd":"I hapa te tuku i te tāpaetanga","uploading_submission_4c8d944b":"E Tukuake ana te Tapaetanga"},"nb":{"submission_failed_to_submit_8de4efcd":"Innlevering ble ikke levert inn","uploading_submission_4c8d944b":"Laster opp innlevering"},"nb-x-k12":{"submission_failed_to_submit_8de4efcd":"Innlevering ble ikke levert inn","uploading_submission_4c8d944b":"Laster opp innlevering"},"nl":{"submission_failed_to_submit_8de4efcd":"Inlevering mislukt","uploading_submission_4c8d944b":"Bezig met uploaden van inzending"},"nn":{"submission_failed_to_submit_8de4efcd":"Innlevering mislukkast","uploading_submission_4c8d944b":"Lastar opp innlevering"},"pl":{"submission_failed_to_submit_8de4efcd":"Nie udało się przesłać","uploading_submission_4c8d944b":"Trwa przesyłanie zadania"},"pt":{"submission_failed_to_submit_8de4efcd":"A submissão falhou ao enviar","uploading_submission_4c8d944b":"A carregar o envio"},"pt-BR":{"submission_failed_to_submit_8de4efcd":"Falhou em enviar","uploading_submission_4c8d944b":"Realizando upload"},"ru":{"submission_failed_to_submit_8de4efcd":"Не удалось выполнить отправку","uploading_submission_4c8d944b":"Загрузка отправки"},"sl":{"submission_failed_to_submit_8de4efcd":"Pošiljanje oddaje ni uspelo.","uploading_submission_4c8d944b":"Nalaganje oddaje"},"sv":{"submission_failed_to_submit_8de4efcd":"Det gick inte att skicka in","uploading_submission_4c8d944b":"Laddar upp inlämning"},"sv-x-k12":{"submission_failed_to_submit_8de4efcd":"Det gick inte att skicka in","uploading_submission_4c8d944b":"Laddar upp inlämning"},"tr":{"uploading_submission_4c8d944b":"Gönderi Yükleniyor"},"uk":{"uploading_submission_4c8d944b":"Завантаження матеріалу"},"zh-Hans":{"submission_failed_to_submit_8de4efcd":"提交项提交失败","uploading_submission_4c8d944b":"正在上传提交作业"},"zh-Hant":{"submission_failed_to_submit_8de4efcd":"提交項目提交失敗","uploading_submission_4c8d944b":"正在上傳提交項目"}}'))
i("jQeR")
i("0sPK")
var s=a["default"].scoped("progress_pill")
i("q1tI")
var r=i("i8i4")
var u=i.n(r)
var d=i("bZJi")
var c=i("whu9")
var l=i("+ayY")
var f=i("3lLS")
var b=i.n(f)
var m,p
b()(()=>{const e=document.querySelectorAll(".assignment_presenter_for_submission")
const n=e=>{switch(e.innerText){case"pending":return[m||(m=Object(t["a"])(c["a"],{})),s.t("Uploading Submission")]
case"failed":return[p||(p=Object(t["a"])(l["a"],{})),s.t("Submission Failed to Submit")]
default:return null}}
const i=document.querySelectorAll(".react_pill_container")
for(let a=0;a<i.length;a++){const o=n(e[a])
null!==o&&u.a.render(Object(t["a"])(d["a"],{tip:o[1]},void 0,o[0]),i[a])}})},bZJi:function(e,n,i){"use strict"
i.d(n,"a",(function(){return A}))
var t=i("Ff2n")
var a=i("VTBJ")
var o=i("1OyB")
var s=i("vuIU")
var r=i("Ji7U")
var u=i("LK+K")
var d=i("q1tI")
var c=i.n(d)
var l=i("17x9")
var f=i.n(l)
var b=i("nAyT")
var m=i("KgFQ")
var p=i("jtGx")
var _=i("sQ3tx")
var v=i("E+IV")
var g=i("UCAh")
var h=i("BTe1")
var y=i("J2CL")
var w=i("oXx0")
var O=i("jsCG")
var j=i("AdN2")
var S=function(e){var n=e.typography,i=e.spacing
return{fontFamily:n.fontFamily,fontWeight:n.fontWeightNormal,fontSize:n.fontSizeSmall,padding:i.small}}
var C,k,T,L,I,x
var F={componentId:"eZLSb",template:function(e){return"\n\n.eZLSb_bGBk{box-sizing:border-box;display:block;font-family:".concat(e.fontFamily||"inherit",";font-size:").concat(e.fontSize||"inherit",";font-weight:").concat(e.fontWeight||"inherit",";padding:").concat(e.padding||"inherit","}")},root:"eZLSb_bGBk"}
var A=(C=Object(b["a"])("8.0.0",{tip:"renderTip",variant:"color"}),k=Object(w["a"])(),T=Object(y["l"])(S,F),C(L=k(L=T(L=(x=I=function(e){Object(r["a"])(i,e)
var n=Object(u["a"])(i)
function i(){var e
Object(o["a"])(this,i)
for(var t=arguments.length,a=new Array(t),s=0;s<t;s++)a[s]=arguments[s]
e=n.call.apply(n,[this].concat(a))
e._id=Object(h["a"])("Tooltip")
e.state={hasFocus:false}
e.handleFocus=function(n){e.setState({hasFocus:true})}
e.handleBlur=function(n){e.setState({hasFocus:false})}
return e}Object(s["a"])(i,[{key:"renderTrigger",value:function(){var e=this.props,n=e.children,t=e.as
var o=this.state.hasFocus
var s={"aria-describedby":this._id}
if(t){var r=Object(m["a"])(i,this.props)
var u=Object(p["a"])(this.props,i.propTypes)
return c.a.createElement(r,Object.assign({},u,s),n)}return"function"===typeof n?n({focused:o,getTriggerProps:function(e){return Object(a["a"])({},s,{},e)}}):Object(_["a"])(this.props.children,s)}},{key:"render",value:function(){var e=this
var n=this.props,i=n.renderTip,a=n.isShowingContent,o=n.defaultIsShowingContent,s=n.on,r=n.placement,u=n.mountNode,d=n.constrain,l=n.offsetX,f=n.offsetY,b=n.positionTarget,m=n.onShowContent,_=n.onHideContent,g=n.tip,h=(n.variant,Object(t["a"])(n,["renderTip","isShowingContent","defaultIsShowingContent","on","placement","mountNode","constrain","offsetX","offsetY","positionTarget","onShowContent","onHideContent","tip","variant"]))
var y=this.props.variant
y=y?"default"===y?"primary-inverse":"primary":this.props.color
return c.a.createElement(O["a"],Object.assign({},Object(p["b"])(h),{isShowingContent:a,defaultIsShowingContent:o,on:s,shouldRenderOffscreen:true,shouldReturnFocus:false,placement:r,color:"primary"===y?"primary-inverse":"primary",mountNode:u,constrain:d,shadow:"none",offsetX:l,offsetY:f,positionTarget:b,renderTrigger:function(){return e.renderTrigger()},onShowContent:m,onHideContent:_,onFocus:this.handleFocus,onBlur:this.handleBlur}),c.a.createElement("span",{id:this._id,className:F.root,role:"tooltip"},i?Object(v["a"])(i):g))}}])
i.displayName="Tooltip"
return i}(d["Component"]),I.propTypes={children:f.a.oneOfType([f.a.node,f.a.func]).isRequired,renderTip:f.a.oneOfType([f.a.node,f.a.func]),isShowingContent:f.a.bool,defaultIsShowingContent:f.a.bool,as:f.a.elementType,on:f.a.oneOfType([f.a.oneOf(["click","hover","focus"]),f.a.arrayOf(f.a.oneOf(["click","hover","focus"]))]),color:f.a.oneOf(["primary","primary-inverse"]),placement:g["a"].placement,mountNode:g["a"].mountNode,constrain:g["a"].constrain,offsetX:f.a.oneOfType([f.a.string,f.a.number]),offsetY:f.a.oneOfType([f.a.string,f.a.number]),positionTarget:f.a.oneOfType([j["a"],f.a.func]),onShowContent:f.a.func,onHideContent:f.a.func,tip:f.a.node,variant:f.a.oneOf(["default","inverse"])},I.defaultProps={renderTip:void 0,isShowingContent:void 0,defaultIsShowingContent:false,on:void 0,color:"primary",placement:"top",mountNode:null,constrain:"window",offsetX:0,offsetY:0,positionTarget:void 0,onShowContent:function(e){},onHideContent:function(e,n){n.documentClick}},x))||L)||L)||L)},eGSd:function(e,n,i){"use strict"
i.d(n,"a",(function(){return t}))
function t(e){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0
var i=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{}
var t,a,o,s
var r=0
var u=[]
var d=false
if("function"!==typeof e)throw new TypeError("Expected a function")
var c=!!i.leading
var l="maxWait"in i
var f=!("trailing"in i)||!!i.trailing
var b=l?Math.max(+i.maxWait||0,n):0
function m(n){var i=t
var s=a
t=a=void 0
r=n
if(true!==d){o=e.apply(s,i)
return o}}function p(e){r=e
u.push(setTimeout(g,n))
return c?m(e):o}function _(e){var i=e-s
var t=e-r
var a=n-i
return l?Math.min(a,b-t):a}function v(e){var i=e-s
var t=e-r
return"undefined"===typeof s||i>=n||i<0||l&&t>=b}function g(){var e=Date.now()
if(v(e))return h(e)
u.push(setTimeout(g,_(e)))}function h(e){O()
if(f&&t)return m(e)
t=a=void 0
return o}function y(){d=true
O()
r=0
t=s=a=void 0}function w(){return 0===u.length?o:h(Date.now())}function O(){u.forEach((function(e){return clearTimeout(e)}))
u=[]}function j(){var e=Date.now()
var i=v(e)
for(var r=arguments.length,d=new Array(r),c=0;c<r;c++)d[c]=arguments[c]
t=d
a=this
s=e
if(i){if(0===u.length)return p(s)
if(l){u.push(setTimeout(g,n))
return m(s)}}0===u.length&&u.push(setTimeout(g,n))
return o}j.cancel=y
j.flush=w
return j}},gCYW:function(e,n,i){"use strict"
i.d(n,"a",(function(){return r}))
var t=i("QF4Q")
var a=i("i/8D")
var o=i("EgqM")
var s=i("DUTp")
function r(e){var n={top:0,left:0,height:0,width:0}
if(!a["a"])return n
var i=Object(t["a"])(e)
if(!i)return n
if(i===window)return{left:window.pageXOffset,top:window.pageYOffset,width:window.innerWidth,height:window.innerHeight,right:window.innerWidth+window.pageXOffset,bottom:window.innerHeight+window.pageYOffset}
var u=e===document?document:Object(s["a"])(i)
var d=u&&u.documentElement
if(!d||!Object(o["a"])(d,i))return n
var c=i.getBoundingClientRect()
var l
for(l in c)n[l]=c[l]
if(u!==document){var f=u.defaultView.frameElement
if(f){var b=r(f)
n.top+=b.top
n.bottom+=b.top
n.left+=b.left
n.right+=b.left}}return{top:n.top+(window.pageYOffset||d.scrollTop)-(d.clientTop||0),left:n.left+(window.pageXOffset||d.scrollLeft)-(d.clientLeft||0),width:(null==n.width?i.offsetWidth:n.width)||0,height:(null==n.height?i.offsetHeight:n.height)||0,right:u.body.clientWidth-n.width-n.left,bottom:u.body.clientHeight-n.height-n.top}}},kR0I:function(e,n,i){"use strict"
i.d(n,"a",(function(){return r}))
var t=i("KQm4")
var a=i("QF4Q")
var o=i("xm5c")
var s=i("IPIv")
function r(e,n,i){var s=Object(a["a"])(e)
if(!s||"function"!==typeof s.querySelectorAll)return[]
var r="a[href],frame,iframe,object,input:not([type=hidden]),select,textarea,button,*[tabindex]"
var u=Array.from(s.querySelectorAll(r))
i&&Object(o["a"])(s,r)&&(u=[].concat(Object(t["a"])(u),[s]))
return u.filter((function(e){return"function"===typeof n?n(e)&&l(e):l(e)}))}function u(e){var n=Object(s["a"])(e)
return"inline"!==n.display&&e.offsetWidth<=0&&e.offsetHeight<=0||"none"===n.display}function d(e){var n=["fixed","absolute"]
if(n.includes(e.style.position.toLowerCase()))return true
if(n.includes(Object(s["a"])(e).getPropertyValue("position").toLowerCase()))return true
return false}function c(e){while(e){if(e===document.body)break
if(u(e))return false
if(d(e))break
e=e.parentNode}return true}function l(e){return!e.disabled&&c(e)}},whu9:function(e,n,i){"use strict"
i.d(n,"a",(function(){return f}))
var t=i("VTBJ")
var a=i("1OyB")
var o=i("vuIU")
var s=i("Ji7U")
var r=i("LK+K")
var u=i("q1tI")
var d=i.n(u)
var c=i("CSQ8")
var l=d.a.createElement("path",{d:"M1838.86029,1451.57632 L1920,1532.94589 L1558.4341,1894.51179 L361.565904,1894.51179 L0,1532.94589 L81.2546391,1451.57632 L409.146414,1779.58302 L1510.85359,1779.58302 L1838.86029,1451.57632 Z M962.333054,25 L1462.61798,525.284928 L1381.47827,606.654495 L1019.68251,244.973662 L1019.68251,1432.53262 L904.86867,1432.53262 L904.86867,244.973662 L543.187837,606.654495 L462.048126,525.284928 L962.333054,25 Z",fillRule:"evenodd",stroke:"none",strokeWidth:"1"})
var f=function(e){Object(s["a"])(i,e)
var n=Object(r["a"])(i)
function i(){Object(a["a"])(this,i)
return n.apply(this,arguments)}Object(o["a"])(i,[{key:"render",value:function(){return d.a.createElement(c["a"],Object.assign({},this.props,{name:"IconUpload",viewBox:"0 0 1920 1920"}),l)}}])
i.displayName="IconUploadLine"
return i}(u["Component"])
f.glyphName="upload"
f.variant="Line"
f.propTypes=Object(t["a"])({},c["a"].propTypes)},xm5c:function(e,n,i){"use strict"
i.d(n,"a",(function(){return a}))
var t=i("QF4Q")
function a(e,n){var i=e&&Object(t["a"])(e)
if(!i)return false
return i.matches?i.matches(n):i.msMatchesSelector(n)}}}])

//# sourceMappingURL=progress_pill-c-dce852cbd2.js.map