(window["canvasWebpackJsonp"]=window["canvasWebpackJsonp"]||[]).push([[336],{"08kA":function(e,t,a){"use strict"
var s=a("s4NR"),n=a("CxY0"),r=a("U6jy")
function i(e){return e&&e.rel}function o(e,t){function a(a){e[a]=r(t,{rel:a})}t.rel.split(/\s+/).forEach(a)
return e}function _(e,t){var a=t.match(/\s*(.+)\s*=\s*"?([^"]+)"?/)
a&&(e[a[1]]=a[2])
return e}function l(e){try{var t=e.match(/<?([^>]*)>(.*)/),a=t[1],i=t[2].split(";"),o=n.parse(a),l=s.parse(o.query)
i.shift()
var d=i.reduce(_,{})
d=r(l,d)
d.url=a
return d}catch(e){return null}}e.exports=function(e){if(!e)return null
return e.split(/,\s*</).map(l).filter(i).reduce(o,{})}},"4M30":function(e,t,a){"use strict"
a.d(t,"a",(function(){return n}))
a.d(t,"b",(function(){return r}))
const s={}.hasOwnProperty
function n(e,t){for(const a in t)s.call(t,a)&&(e[a]=t[a])
function a(){this.constructor=e}a.prototype=t.prototype
e.prototype=new a
e.__super__=t.prototype
return e}function r(e,t){Object.keys(t).forEach(a=>Object.defineProperty(e,a,{get:t[a],enumerable:true,configurable:true}))
return e}},"8o96":function(e,t,a){"use strict"
a.d(t,"a",(function(){return i}))
var s=a("QF4Q")
var n=a("gCYW")
var r=a("ISHz")
function i(e,t){var a=arguments.length>2&&void 0!==arguments[2]?arguments[2]:["width"]
var i=Object(s["a"])(e)
var o=Object(n["a"])(i)
var _=false
var l
var d=function e(){if(_)return
var s=Object(n["a"])(i)
var d={width:s.width,height:s.height}
a.some((function(e){return d[e]!=o[e]}))&&"function"===typeof t&&t(d)
o=d
l=Object(r["a"])(e)}
d()
return{remove:function(){_=true
l.cancel()}}}},"8ruJ":function(e,t,a){"use strict"
a.d(t,"a",(function(){return u}))
var s=a("VTBJ")
var n=a("1OyB")
var r=a("vuIU")
var i=a("Ji7U")
var o=a("LK+K")
var _=a("q1tI")
var l=a.n(_)
var d=a("CSQ8")
var c=l.a.createElement("path",{d:"M1468.2137,0 L1468.2137,564.697578 L1355.27419,564.697578 L1355.27419,112.939516 L112.939516,112.939516 L112.939516,1807.03225 L1355.27419,1807.03225 L1355.27419,1581.15322 L1468.2137,1581.15322 L1468.2137,1919.97177 L2.5243549e-29,1919.97177 L2.5243549e-29,0 L1468.2137,0 Z M1597.64239,581.310981 C1619.77853,559.174836 1655.46742,559.174836 1677.60356,581.310981 L1677.60356,581.310981 L1903.4826,807.190012 C1925.5058,829.213217 1925.5058,864.902104 1903.4826,887.038249 L1903.4826,887.038249 L1225.8455,1564.67534 C1215.22919,1575.17872 1200.88587,1581.16451 1185.86491,1581.16451 L1185.86491,1581.16451 L959.985883,1581.16451 C928.814576,1581.16451 903.516125,1555.86606 903.516125,1524.69475 L903.516125,1524.69475 L903.516125,1298.81572 C903.516125,1283.79477 909.501919,1269.45145 920.005294,1258.94807 L920.005294,1258.94807 Z M1442.35055,896.29929 L1016.45564,1322.1942 L1016.45564,1468.225 L1162.48643,1468.225 L1588.38135,1042.33008 L1442.35055,896.29929 Z M677.637094,1242.34597 L677.637094,1355.28548 L338.818547,1355.28548 L338.818547,1242.34597 L677.637094,1242.34597 Z M903.516125,1016.46693 L903.516125,1129.40645 L338.818547,1129.40645 L338.818547,1016.46693 L903.516125,1016.46693 Z M1637.62298,701.026867 L1522.19879,816.451052 L1668.22958,962.481846 L1783.65377,847.057661 L1637.62298,701.026867 Z M1129.39516,338.829841 L1129.39516,790.587903 L338.818547,790.587903 L338.818547,338.829841 L1129.39516,338.829841 Z M1016.45564,451.769356 L451.758062,451.769356 L451.758062,677.648388 L1016.45564,677.648388 L1016.45564,451.769356 Z",fillRule:"evenodd",stroke:"none",strokeWidth:"1"})
var u=function(e){Object(i["a"])(a,e)
var t=Object(o["a"])(a)
function a(){Object(n["a"])(this,a)
return t.apply(this,arguments)}Object(r["a"])(a,[{key:"render",value:function(){return l.a.createElement(d["a"],Object.assign({},this.props,{name:"IconAssignment",viewBox:"0 0 1920 1920",bidirectional:true}),c)}}])
a.displayName="IconAssignmentLine"
return a}(_["Component"])
u.glyphName="assignment"
u.variant="Line"
u.propTypes=Object(s["a"])({},d["a"].propTypes)},HknL:function(e,t,a){"use strict"
var s=a("HGxv")
var n=a("8WeW")
Object(n["a"])(JSON.parse('{"ar":{"CollectionView":{"no_items":"لا توجد عناصر."}},"ca":{"CollectionView":{"no_items":"No hi ha elements."}},"cy":{"CollectionView":{"no_items":"Dim eitemau."}},"da":{"CollectionView":{"no_items":"Ingen elementer."}},"da-x-k12":{"CollectionView":{"no_items":"Ingen elementer."}},"de":{"CollectionView":{"no_items":"Keine Elemente"}},"el":{"CollectionView":{"no_items":"Δεν υπάρχουν στοιχεία."}},"en-AU":{"CollectionView":{"no_items":"No items."}},"en-AU-x-unimelb":{"CollectionView":{"no_items":"No items."}},"en-CA":{"CollectionView":{"no_items":"No items."}},"en-GB":{"CollectionView":{"no_items":"No items."}},"en-GB-x-ukhe":{"CollectionView":{"no_items":"No items."}},"es":{"CollectionView":{"no_items":"No hay items."}},"fa":{"CollectionView":{"no_items":"هیچ موردی یافت نشد."}},"fi":{"CollectionView":{"no_items":"Ei kohteita."}},"fr":{"CollectionView":{"no_items":"Aucun élément"}},"fr-CA":{"CollectionView":{"no_items":"Aucun élément"}},"he":{"CollectionView":{"no_items":"אין פריטים"}},"ht":{"CollectionView":{"no_items":"Okenn eleman."}},"hu":{"CollectionView":{"no_items":"Nincsenek elemek."}},"hy":{"CollectionView":{"no_items":"Տարրերը բացակայում են:"}},"is":{"CollectionView":{"no_items":"Engin atriði."}},"it":{"CollectionView":{"no_items":"Nessun elemento."}},"ja":{"CollectionView":{"no_items":"アイテムがありません。"}},"ko":{"CollectionView":{"no_items":"항목이 없습니다."}},"mi":{"CollectionView":{"no_items":"Kāore he tūemi."}},"nb":{"CollectionView":{"no_items":"Ingen elementer."}},"nb-x-k12":{"CollectionView":{"no_items":"Ingen elementer."}},"nl":{"CollectionView":{"no_items":"Geen items."}},"nn":{"CollectionView":{"no_items":"Ingen element."}},"pl":{"CollectionView":{"no_items":"Brak elementów."}},"pt":{"CollectionView":{"no_items":"Nenhum item."}},"pt-BR":{"CollectionView":{"no_items":"Nenhum item."}},"ru":{"CollectionView":{"no_items":"Элементы отсутствуют."}},"sv":{"CollectionView":{"no_items":"Inga objekt."}},"sv-x-k12":{"CollectionView":{"no_items":"Inga objekt."}},"tr":{"CollectionView":{"no_items":"Öğe bulunamadı."}},"uk":{"CollectionView":{"no_items":"Елементи не знайдено."}},"zh-Hans":{"CollectionView":{"no_items":"没有项目。"}},"zh-Hant":{"CollectionView":{"no_items":"沒有項目。"}}}'))
a("jQeR")
a("0sPK")
var r=s["default"].scoped("CollectionView")
var i=a("ouhR")
var o=a.n(i)
var _=a("Y/W1")
var l=a.n(_)
var d=a("mX+G")
var c=a.n(d)
var u=a("3O+N")
var p=a.n(u)
a("BGrI")
var m=p.a.default
var h=m.template,f=m.templates=m.templates||{}
var g="/work/canvas-deploy/generated/ui/shared/backbone-collection-view/jst/index"
f[g]=h((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i="",o="function",_=this.escapeExpression,l=this
function d(e,t){var s,n,r=""
r+='\n<ul class="collectionViewItems '
if(n=a.listClassName)s=n.call(e,{hash:{},data:t})
else{n=e&&e.listClassName
s=typeof n===o?n.call(e,{hash:{},data:t}):n}r+=_(s)+'"></ul>\n'
return r}function c(e,t){var s,n,r=""
r+='\n<p class="emptyMessage">'
if(n=a.emptyMessage)s=n.call(e,{hash:{},data:t})
else{n=e&&e.emptyMessage
s=typeof n===o?n.call(e,{hash:{},data:t}):n}r+=_(s)+"</p>\n"
return r}r=a["if"].call(t,(r=t&&t.collection,null==r||false===r?r:r.length),{hash:{},inverse:l.program(3,c,n),fn:l.program(1,d,n),data:n});(r||0===r)&&(i+=r)
i+="\n"
return i}))
var b=f[g]
var y=a("4M30")
var w=function(e,t){return function(){return e.apply(t,arguments)}},v=function(e,t){for(var a in t)k.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},k={}.hasOwnProperty
t["a"]=function(e){v(t,e)
function t(){this.renderItem=w(this.renderItem,this)
this.renderOnAdd=w(this.renderOnAdd,this)
this.removeItem=w(this.removeItem,this)
this.renderOnReset=w(this.renderOnReset,this)
this.removePreviousItems=w(this.removePreviousItems,this)
this.reorder=w(this.reorder,this)
this.render=w(this.render,this)
return t.__super__.constructor.apply(this,arguments)}t.optionProperty("itemView")
t.optionProperty("itemViewOptions")
t.optionProperty("emptyMessage")
t.optionProperty("listClassName")
t.prototype.className="collectionView"
t.prototype.els={".collectionViewItems":"$list"}
t.prototype.defaults=Object(y["b"])({itemViewOptions:{}},{emptyMessage:function(){return r.t("no_items","No items.")}})
t.prototype.template=b
t.prototype.initialize=function(e){t.__super__.initialize.apply(this,arguments)
return this.attachCollection()}
t.prototype.render=function(){t.__super__.render.apply(this,arguments)
this.empty||this.renderItems()
return this}
t.prototype.toJSON=function(){return l.a.extend(this.options,{emptyMessage:this.emptyMessage,listClassName:this.listClassName,ENV:ENV})}
t.prototype.reorder=function(){var e,t,a
this.collection.sort()
this.$list.children().detach()
e=function(){var e,a,s,n
s=this.collection.models
n=[]
for(e=0,a=s.length;e<a;e++){t=s[e]
n.push(t.itemView.$el)}return n}.call(this)
return(a=this.$list).append.apply(a,e)}
t.prototype.attachCollection=function(){this.listenTo(this.collection,"reset",this.renderOnReset)
this.listenTo(this.collection,"add",this.renderOnAdd)
this.listenTo(this.collection,"remove",this.removeItem)
return this.empty=!this.collection.length}
t.prototype.detachCollection=function(){return this.stopListening(this.collection)}
t.prototype.switchCollection=function(e){this.detachCollection()
this.collection=e
return this.attachCollection()}
t.prototype.removePreviousItems=function(e){var t,a,s,n,r
r=[]
for(t=0,a=e.length;t<a;t++){s=e[t]
r.push(null!=(n=s.view)?n.remove():void 0)}return r}
t.prototype.renderOnReset=function(e,t){this.empty=!this.collection.length
this.removePreviousItems(t.previousModels)
return this.render()}
t.prototype.renderItems=function(){this.collection.each(this.renderItem.bind(this))
return this.trigger("renderedItems")}
t.prototype.removeItem=function(e){this.empty=!this.collection.length
return this.empty?this.render():e.view.remove()}
t.prototype.renderOnAdd=function(e){this.empty&&this.render()
this.empty=false
return this.renderItem(e)}
t.prototype.renderItem=function(e){var t
t=this.createItemView(e)
t.render()
"function"===typeof this.attachItemView&&this.attachItemView(e,t)
return this.insertView(t)}
t.prototype.createItemView=function(e){var t
t=new this.itemView(o.a.extend({},this.itemViewOptions||{},{model:e}))
e.itemView=t
return t}
t.prototype.insertView=function(e){var t
t=this.collection.indexOf(e.model)
return 0===t?this.prependView(e):t===this.collection.length-1?this.appendView(e):this.insertViewAtIndex(e,t)}
t.prototype.insertViewAtIndex=function(e,t){var a
a=this.$list.children().eq(t)
return a.length?a.before(e.el):this.$list.append(e.el)}
t.prototype.prependView=function(e){return this.$list.prepend(e.el)}
t.prototype.appendView=function(e){return this.$list.append(e.el)}
return t}(c.a.View)},KIYd:function(e,t,a){"use strict"
var s=a("HGxv")
var n=a("8WeW")
Object(n["a"])(JSON.parse('{"ar":{"buttons":{"cancel":"إلغاء","update":"تحديث"}},"ca":{"buttons":{"cancel":"Cancel·la","update":"Actualitza"}},"cy":{"buttons":{"cancel":"Canslo","update":"Diweddaru"}},"da":{"buttons":{"cancel":"Annullér","update":"Opdatering"}},"da-x-k12":{"buttons":{"cancel":"Annullér","update":"Opdatering"}},"de":{"buttons":{"cancel":"Abbrechen","update":"Aktualisieren"}},"el":{"buttons":{"cancel":"Ακύρωση","update":"Ενημέρωση"}},"en-AU":{"buttons":{"cancel":"Cancel","update":"Update"}},"en-AU-x-unimelb":{"buttons":{"cancel":"Cancel","update":"Update"}},"en-CA":{"buttons":{"cancel":"Cancel","update":"Update"}},"en-GB":{"buttons":{"cancel":"Cancel","update":"Update"}},"en-GB-x-lbs":{"buttons":{"cancel":"Cancel","update":"Update"}},"en-GB-x-ukhe":{"buttons":{"cancel":"Cancel","update":"Update"}},"es":{"buttons":{"cancel":"Cancelar","update":"Actualizar"}},"fa":{"buttons":{"cancel":"لغو","update":"بهنگام سازی"}},"fi":{"buttons":{"cancel":"Peruuta","update":"Päivitä"}},"fr":{"buttons":{"cancel":"Annuler","update":"Mettre à jour"}},"fr-CA":{"buttons":{"cancel":"Annuler","update":"Mettre à jour"}},"he":{"buttons":{"cancel":"ביטול","update":"עדכון"}},"ht":{"buttons":{"cancel":"Anile","update":"Aktyalize"}},"hu":{"buttons":{"cancel":"Mégse","update":"Frissítés"}},"hy":{"buttons":{"cancel":"Չեղյալ համարել","update":"Թարմացնել"}},"is":{"buttons":{"cancel":"Hætta við","update":"Uppfæra"}},"it":{"buttons":{"cancel":"Annulla","update":"Aggiorna"}},"ja":{"buttons":{"cancel":"キャンセル","update":"更新"}},"ko":{"buttons":{"cancel":"취소","update":"업데이트"}},"mi":{"buttons":{"cancel":"Whakakore","update":"Whakahōu"}},"nb":{"buttons":{"cancel":"Avbryt","update":"Oppdater"}},"nb-x-k12":{"buttons":{"cancel":"Avbryt","update":"Oppdater"}},"nl":{"buttons":{"cancel":"Annuleren","update":"Bijwerken"}},"nn":{"buttons":{"cancel":"Avbryt","update":"Oppdatering"}},"pl":{"buttons":{"cancel":"Anuluj","update":"Zaktualizuj"}},"pt":{"buttons":{"cancel":"Cancelar","update":"Atualizar"}},"pt-BR":{"buttons":{"cancel":"Cancelar","update":"Atualizar"}},"ru":{"buttons":{"cancel":"Отменить","update":"Обновить"}},"sl":{"buttons":{"cancel":"Prekliči","update":"Posodobi"}},"sv":{"buttons":{"cancel":"Avbryt","update":"Uppdatera"}},"sv-x-k12":{"buttons":{"cancel":"Avbryt","update":"Uppdatera"}},"tr":{"buttons":{"cancel":"İptal","update":"Güncelle"}},"uk":{"buttons":{"cancel":"Скасувати","update":"Оновлення"}},"zh-Hans":{"buttons":{"cancel":"取消","update":"更新"}},"zh-Hant":{"buttons":{"cancel":"取消","update":"更新"}}}'))
a("jQeR")
a("0sPK")
var r=s["default"].scoped("dialog")
var i=a("ouhR")
var o=a.n(i)
var _=a("Y/W1")
var l=a.n(_)
var d=a("mX+G")
var c=a.n(d)
a("ESjL")
var u=function(e,t){return function(){return e.apply(t,arguments)}},p=function(e,t){for(var a in t)m.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},m={}.hasOwnProperty
t["a"]=function(e){p(t,e)
function t(){this.cancel=u(this.cancel,this)
return t.__super__.constructor.apply(this,arguments)}t.prototype.initialize=function(){t.__super__.initialize.apply(this,arguments)
this.initDialog()
return this.setElement(this.dialog)}
t.prototype.defaultOptions=function(){return{autoOpen:false,width:420,resizable:false,buttons:[],destroy:false}}
t.prototype.initDialog=function(){var e
e=l.a.extend({},this.defaultOptions(),{buttons:[{text:r.t("#buttons.cancel","Cancel"),class:"cancel_button",click:(t=this,function(e){return t.cancel(e)})},{text:r.t("#buttons.update","Update"),class:"btn-primary",click:function(e){return function(t){return e.update(t)}}(this)}]},l.a.result(this,"dialogOptions"))
var t
this.dialog=o()('<div id="'+e.id+'"></div>').appendTo("body").dialog(e)
e.containerId&&this.dialog.parent().attr("id",e.containerId)
o()(".ui-resizable-handle").attr("aria-hidden",true)
return this.dialog}
t.prototype.show=function(){return this.dialog.dialog("open")}
t.prototype.close=function(){return this.options.destroy?this.dialog.dialog("destroy"):this.dialog.dialog("close")}
t.prototype.update=function(e){throw"Not yet implemented"}
t.prototype.cancel=function(e){e.preventDefault()
return this.close()}
return t}(c.a.View)},Q0Ww:function(e,t,a){"use strict"
a.d(t,"a",(function(){return u}))
var s=a("VTBJ")
var n=a("1OyB")
var r=a("vuIU")
var i=a("Ji7U")
var o=a("LK+K")
var _=a("q1tI")
var l=a.n(_)
var d=a("CSQ8")
var c=l.a.createElement("g",{fillRule:"evenodd",stroke:"none",strokeWidth:"1"},l.a.createElement("path",{d:"M746.255375,1466.76417 L826.739372,1547.47616 L577.99138,1796.11015 L497.507383,1715.51216 L746.255375,1466.76417 Z M580.35118,1300.92837 L660.949178,1381.52637 L329.323189,1713.15236 L248.725192,1632.55436 L580.35118,1300.92837 Z M414.503986,1135.20658 L495.101983,1215.80457 L80.5979973,1630.30856 L0,1549.71056 L414.503986,1135.20658 Z M1119.32036,264.600006 C1475.79835,-91.8779816 1844.58834,86.3040124 1848.35034,88.1280123 L1848.35034,88.1280123 L1865.45034,96.564012 L1873.88634,113.664011 C1875.71034,117.312011 2053.89233,486.101999 1697.30034,842.693987 L1697.30034,842.693987 L1550.69635,989.297982 L1548.07435,1655.17196 L1325.43235,1877.81395 L993.806366,1546.30196 L415.712386,968.207982 L84.0863971,636.467994 L306.72839,413.826001 L972.602367,411.318001 Z M1436.24035,1103.75398 L1074.40436,1465.70397 L1325.43235,1716.61796 L1434.30235,1607.74796 L1436.24035,1103.75398 Z M1779.26634,182.406009 C1710.18234,156.41401 1457.90035,87.1020124 1199.91836,345.198004 L1199.91836,345.198004 L576.90838,968.207982 L993.806366,1385.10597 L1616.70235,762.095989 C1873.65834,505.139998 1804.68834,250.920007 1779.26634,182.406009 Z M858.146371,525.773997 L354.152388,527.597997 L245.282392,636.467994 L496.310383,887.609985 L858.146371,525.773997 Z"}),l.a.createElement("path",{d:"M1534.98715,372.558003 C1483.91515,371.190003 1403.31715,385.326002 1321.69316,466.949999 L1281.22316,507.305998 L1454.61715,680.585992 L1494.97315,640.343994 C1577.16715,558.035996 1591.87315,479.033999 1589.82115,427.164001 L1587.65515,374.610003 L1534.98715,372.558003 Z"}))
var u=function(e){Object(i["a"])(a,e)
var t=Object(o["a"])(a)
function a(){Object(n["a"])(this,a)
return t.apply(this,arguments)}Object(r["a"])(a,[{key:"render",value:function(){return l.a.createElement(d["a"],Object.assign({},this.props,{name:"IconQuiz",viewBox:"0 0 1920 1920",bidirectional:true}),c)}}])
a.displayName="IconQuizLine"
return a}(_["Component"])
u.glyphName="quiz"
u.variant="Line"
u.propTypes=Object(s["a"])({},d["a"].propTypes)},TnsN:function(e,t,a){"use strict"
var s=a("ouhR")
var n=a.n(s)
var r=a("mX+G")
var i=a.n(r)
var o=a("Y/W1")
var _=a.n(o)
var l,d=function(e,t){return function(){return e.apply(t,arguments)}},c=function(e,t){for(var a in t)u.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},u={}.hasOwnProperty,p=[].slice,m=[].indexOf||function(e){for(var t=0,a=this.length;t<a;t++)if(t in this&&this[t]===e)return t
return-1}
l=function(e){null==e&&(e="")
return e.charAt(0).toUpperCase()+e.substring(1).toLowerCase()}
t["a"]=function(e){c(t,e)
function t(){this._setStateAfterFetch=d(this._setStateAfterFetch,this)
return t.__super__.constructor.apply(this,arguments)}t.prototype.nameRegex=/rel="([a-z]+)/
t.prototype.linkRegex=/^<([^>]+)/
t.prototype.pageRegex=/\Wpage=(\d+)/
t.prototype.perPageRegex=/\Wper_page=(\d+)/
t.prototype.initialize=function(){t.__super__.initialize.apply(this,arguments)
return this.urls={}}
t.prototype.fetch=function(e){var a,s,r,i
null==e&&(e={})
e=_.a.clone(e)
this.loadedAll=false
s="fetching"+l(e.page)+"Page"
this[s]=true
if(null!=e.page){null==e.remove&&(e.remove=false)
if(null!=(r=this.urls)?r[e.page]:void 0){e.url=this.urls[e.page]
e.data=""}}else null==e.reset&&(e.reset=true)
null!=e.fetchOptions&&(e.data=e.fetchOptions)
this.trigger("beforeFetch",this,e)
null!=e.page&&this.trigger("beforeFetch:"+e.page,this,e)
i=null
e.dataFilter=(o=this,function(t){o[s]=false
o._setStateAfterFetch(i,e)
return t})
var o
a=e.dfd||n.a.Deferred()
i=t.__super__.fetch.call(this,e).done(function(t){return function(s,n,r){var i
t.trigger("fetch",t,s,e)
null!=e.page&&t.trigger("fetch:"+e.page,t,s,e);(null!=(i=t.urls)?i.next:void 0)||t.trigger.apply(t,["fetched:last"].concat(p.call(arguments)))
return t.loadAll&&null!=t.urls.next?setTimeout((function(){return t.fetch({page:"next",dfd:a})})):a.resolve(s,n,r)}}(this))
a.abort=i.abort
a.success=a.done
a.error=a.fail
return a}
t.prototype.canFetch=function(e){return null!=this.urls&&null!=this.urls[e]}
t.prototype._setStateAfterFetch=function(e,t){var a,s,n,r,i,o,_,l,d,c,u,p,h,f
null==this._urlCache&&(this._urlCache=[])
f=(o=t.url,m.call(this._urlCache,o)<0)
f||this._urlCache.push(t.url)
s=!this.atLeastOnePageFetched
u=s||("next"===(_=t.page)||"bottom"===_)&&f
p=s||("prev"===(l=t.page)||"top"===l)&&f
r=this.urls
this.urls=this._parsePageLinks(e)
u&&null!=this.urls.next?this.urls.bottom=this.urls.next:this.urls.next?this.urls.bottom=r.bottom:delete this.urls.bottom
p&&null!=this.urls.prev?this.urls.top=this.urls.prev:this.urls.prev?this.urls.top=r.top:delete this.urls.top
h=null!=(d=this.urls.first)?d:this.urls.next
if(null!=h){i=parseInt(h.match(this.perPageRegex)[1],10);(null!=(a=null!=this.options?this.options:this.options={}).params?a.params:a.params={}).per_page=i}this.urls.last&&(n=this.urls.last.match(this.pageRegex))&&(this.totalPages=parseInt(n[1],10));(null!=(c=this.urls)?c.next:void 0)||(this.loadedAll=true)
return this.atLeastOnePageFetched=true}
t.prototype._parsePageLinks=function(e){var t,a
t=null!=(a=e.getResponseHeader("link"))?a.split(","):void 0
null==t&&(t=[])
return _.a.reduce(t,(s=this,function(e,t){var a,n
a=t.match(s.nameRegex)[1]
n=t.match(s.linkRegex)[1]
e[a]=n
return e}),{})
var s}
return t}(i.a.Collection)},TvTI:function(e,t,a){"use strict"
var s=a("ouhR")
var n=a.n(s)
var r=a("gI0r")
a("8JEM")
n.a.fn.fillTemplateData=function(e){if(this.length&&e){e.iterator&&this.find("*").andSelf().each((function(){const t=n()(this)
n.a.each(["name","id","class"],(a,s)=>{t.attr(s)&&t.attr(s,t.attr(s).replace(/-iterator-/,e.iterator))})}))
e.id&&this.attr("id",e.id)
let s=false
if(e.data)for(var t in e.data){if(e.except&&-1!=n.a.inArray(t,e.except))continue
e.data[t]&&e.dataValues&&-1!=n.a.inArray(t,e.dataValues)&&this.data(t,e.data[t].toString())
const i=this.find("."+t)
var a=e.avoid||""
i.each((function(){const i=n()(this)
if(i.length>0&&0===i.closest(a).length){"undefined"!==typeof e.data[t]&&null!==e.data[t]||(e.data[t]="")
if(e.htmlValues&&-1!=n.a.inArray(t,e.htmlValues)){i.html(n.a.raw(e.data[t].toString()))
if(i.hasClass("user_content")){s=true
i.removeClass("enhanced")
i.data("unenhanced_content_html",e.data[t].toString())}}else if("INPUT"==i[0].tagName.toUpperCase())i.val(e.data[t])
else try{const a=e.data[t].toString()
i.html(Object(r["a"])(a))}catch(e){}}}))}e.hrefValues&&e.data&&this.find("a,span[rel]").each((function(){let t,a,s,r=n()(this)
for(const i in e.hrefValues){if(!e.hrefValues.hasOwnProperty(i))continue
const o=e.hrefValues[i]
if(t=r.attr("href")){const a=n.a.replaceTags(t,o,encodeURIComponent(e.data[o]))
const s=r.text()===r.html()?r.text():null
if(t!==a){r.attr("href",a)
s&&r.text(s)}}(a=r.attr("rel"))&&r.attr("rel",n.a.replaceTags(a,o,e.data[o]));(s=r.attr("name"))&&r.attr("name",n.a.replaceTags(s,o,e.data[o]))}}))
s&&n()(document).triggerHandler("user_content_change")}return this}
n.a.fn.fillTemplateData.defaults={htmlValues:null,hrefValues:null}
n.a.fn.getTemplateData=function(e){if(!this.length||!e)return{}
var t,a={}
if(e.textValues){const t=this
e.textValues.forEach(e=>{const r=t.find("."+e.replace(/\[/g,"\\[").replace(/\]/g,"\\]")+":first")
s=n.a.trim(r.text())
"&nbsp;"===r.html()&&(s="")
1===s.length&&160===s.charCodeAt(0)&&(s="")
a[e]=s})}if(e.dataValues)for(t in e.dataValues){var s=this.data(e.dataValues[t])
s&&(a[e.dataValues[t]]=s)}if(e.htmlValues)for(t in e.htmlValues){const r=this.find("."+e.htmlValues[t].replace(/\[/g,"\\[").replace(/\]/g,"\\]")+":first")
s=null
s=r.hasClass("user_content")&&r.data("unenhanced_content_html")?r.data("unenhanced_content_html"):n.a.trim(r.html())
a[e.htmlValues[t]]=s}return a}
n.a.fn.getTemplateValue=function(e,t){const a=n.a.extend({},t,{textValues:[e]})
return this.getTemplateData(a)[e]}},U6jy:function(e,t){e.exports=s
var a=Object.prototype.hasOwnProperty
function s(){var e={}
for(var t=0;t<arguments.length;t++){var s=arguments[t]
for(var n in s)a.call(s,n)&&(e[n]=s[n])}return e}},ZELe:function(e,t,a){"use strict"
a.d(t,"a",(function(){return s}))
function s(e){return function(t,a,s){var n=t[a]
if(null===n||"undefined"===typeof n)return new Error("The prop `".concat(a,"` is marked as required in `").concat(s,"`, but its value is `").concat(n,"`"))
for(var r=arguments.length,i=new Array(r>3?r-3:0),o=3;o<r;o++)i[o-3]=arguments[o]
return e.apply(void 0,[t,a,s].concat(i))}}},bbn0:function(e,t,a){"use strict"
var s=a("HGxv")
var n=a("8WeW")
Object(n["a"])(JSON.parse('{"ar":{"play_media_comment_35257210":"تشغيل تعليق الوسائط.","play_media_comment_by_name_from_createdat_515b3b":"تشغيل تعليق الوسائط بواسطة %{name} من %{createdAt}."},"ca":{"play_media_comment_35257210":"Reprodueix el comentari multimèdia.","play_media_comment_by_name_from_createdat_515b3b":"Reprodueix el comentari multimèdia de %{name} enviat el %{createdAt}."},"cy":{"play_media_comment_35257210":"Chwarae sylw ar gyfryngau.","play_media_comment_by_name_from_createdat_515b3b":"Chwarae sylw ar gyfryngau gan %{name} o %{createdAt}."},"da":{"play_media_comment_35257210":"Afspil medie kommentar.","play_media_comment_by_name_from_createdat_515b3b":"Afspil mediekommentar af %{name} fra %{createdAt}."},"da-x-k12":{"play_media_comment_35257210":"Afspil medie kommentar.","play_media_comment_by_name_from_createdat_515b3b":"Afspil mediekommentar af %{name} fra %{createdAt}."},"de":{"play_media_comment_35257210":"Medienkommentar wiedergeben","play_media_comment_by_name_from_createdat_515b3b":"Medienkommentar durch %{name} von %{createdAt} wiedergeben."},"en-AU":{"play_media_comment_35257210":"Play media comment.","play_media_comment_by_name_from_createdat_515b3b":"Play media comment by %{name} from %{createdAt}."},"en-AU-x-unimelb":{"play_media_comment_35257210":"Play media comment.","play_media_comment_by_name_from_createdat_515b3b":"Play media comment by %{name} from %{createdAt}."},"en-CA":{"play_media_comment_35257210":"Play media comment.","play_media_comment_by_name_from_createdat_515b3b":"Play media comment by %{name} from %{createdAt}."},"en-GB":{"play_media_comment_35257210":"Play media comment.","play_media_comment_by_name_from_createdat_515b3b":"Play media comment by %{name} from %{createdAt}."},"en-GB-x-lbs":{"play_media_comment_35257210":"Play media comment.","play_media_comment_by_name_from_createdat_515b3b":"Play media comment by %{name} from %{createdAt}."},"en-GB-x-ukhe":{"play_media_comment_35257210":"Play media comment.","play_media_comment_by_name_from_createdat_515b3b":"Play media comment by %{name} from %{createdAt}."},"es":{"play_media_comment_35257210":"Reproducir comentario de multimedia.","play_media_comment_by_name_from_createdat_515b3b":"Reproducir comentario de multimedia de %{name} desde %{createdAt}."},"fa":{"play_media_comment_35257210":"پخش نظر رسانه ای","play_media_comment_by_name_from_createdat_515b3b":" نظر رسانه‌ای %{name} را  از %{createdAt} پخش کنید."},"fi":{"play_media_comment_35257210":"Toista mediakommentti.","play_media_comment_by_name_from_createdat_515b3b":"Toista mediakommentti %{name} kohteesta %{createdAt}."},"fr":{"play_media_comment_35257210":"Lancer le commentaire sur média.","play_media_comment_by_name_from_createdat_515b3b":"Lancer le commentaire sur média par %{name} à partir de %{createdAt}."},"fr-CA":{"play_media_comment_35257210":"Lire le commentaire du support","play_media_comment_by_name_from_createdat_515b3b":"Lire le commentaire du support par %{name} du %{createdAt}."},"ht":{"play_media_comment_35257210":"Jwe kòmantè medya.","play_media_comment_by_name_from_createdat_515b3b":"Jwe kòmantè medya pa %{name} de %{createdAt}."},"is":{"play_media_comment_35257210":"Spila miðilsathugasemd.","play_media_comment_by_name_from_createdat_515b3b":"Spila miðilsathugasemd eftir %{name} frá %{createdAt}."},"it":{"play_media_comment_35257210":"Commento sulla riproduzione del contenuto multimediale.","play_media_comment_by_name_from_createdat_515b3b":"Commento sulla riproduzione del contenuto multimediale da parte di %{name} da %{createdAt}."},"ja":{"play_media_comment_35257210":"メディア コメントの再生。","play_media_comment_by_name_from_createdat_515b3b":"%{name}まで%{createdAt}からのメディアのコメントを再生します。"},"mi":{"play_media_comment_35257210":"Mahia ngā korero pāpāho","play_media_comment_by_name_from_createdat_515b3b":"Mahia ngā korero pāpāho ma %{name} mai te %{createdAt}."},"nb":{"play_media_comment_35257210":"Spill mediainnhold","play_media_comment_by_name_from_createdat_515b3b":"Spill mediainnhold av %{name} fra %{createdAt}."},"nb-x-k12":{"play_media_comment_35257210":"Spill mediainnhold","play_media_comment_by_name_from_createdat_515b3b":"Spill mediainnhold av %{name} fra %{createdAt}."},"nl":{"play_media_comment_35257210":"Media-opmerking afspelen","play_media_comment_by_name_from_createdat_515b3b":"Speel media-opmerking door %{name} van %{createdAt} af."},"nn":{"play_media_comment_35257210":"Spel av medieinnhald.","play_media_comment_by_name_from_createdat_515b3b":"Spel av mediekommentar frå %{name} den %{createdAt}."},"pl":{"play_media_comment_35257210":"Odtwórz komentarz multimedialny.","play_media_comment_by_name_from_createdat_515b3b":"Odtwórz komentarz multimedialny %{name} z %{createdAt}."},"pt":{"play_media_comment_35257210":"Reproduzir comentário de mídia.","play_media_comment_by_name_from_createdat_515b3b":"Reproduzir comentários de mídia %{name} de %{createdAt}."},"pt-BR":{"play_media_comment_35257210":"Reproduzir comentário em mídia.","play_media_comment_by_name_from_createdat_515b3b":"Reproduzir comentário em mídia por %{name} de %{createdAt}."},"ru":{"play_media_comment_35257210":"Воспроизведение мультимедийного комментария.","play_media_comment_by_name_from_createdat_515b3b":"Воспроизвести мультимедийный комментарии %{name} из %{createdAt}."},"sl":{"play_media_comment_35257210":"Predvajaj komentar v obliki medija.","play_media_comment_by_name_from_createdat_515b3b":"Predvajaj komentar v obliki medija %{name}, ki je bil ustvarjen ob/dne %{createdAt}."},"sv":{"play_media_comment_35257210":"Spela upp mediekommentar.","play_media_comment_by_name_from_createdat_515b3b":"Spela upp mediekommentar av %{name} från %{createdAt}."},"sv-x-k12":{"play_media_comment_35257210":"Spela upp mediekommentar.","play_media_comment_by_name_from_createdat_515b3b":"Spela upp mediekommentar av %{name} från %{createdAt}."},"zh-Hans":{"play_media_comment_35257210":"播放媒体评论。","play_media_comment_by_name_from_createdat_515b3b":"播放%{name}的来自%{createdAt}的媒体评论。"},"zh-Hant":{"play_media_comment_35257210":"播放媒體評論。","play_media_comment_by_name_from_createdat_515b3b":"播放%{name}於%{createdAt}創建的媒體評論。"}}'))
a("jQeR")
a("0sPK")
var r=s["default"].scoped("mediaCommentThumbnail")
var i=a("Y/W1")
var o=a.n(i)
var _=a("gI0r")
var l=a("ouhR")
var d=a.n(l)
var c=a("6NVu")
const u={normal:{width:140,height:100},small:{width:70,height:50}}
function p(e,t,a){if(!INST.kalturaSettings)return console.log("Kaltura has not been enabled for this account")
let s,n
const i=d()(e)
try{const e=document.createElement("a")
e.href=i.attr("href")
n=e}catch(e){}if(n&&Object(c["a"])(n.search).no_preview)return
const o=u[t]||u.normal
const l=i.data("media_comment_id")||d.a.trim(i.find(".media_comment_id:first").text())||(s=i.attr("id"))&&s.match(/^media_comment_/)&&s.substring(14)||d.a.trim(i.parent().find(".media_comment_id:first").text())
const p=i.data("author")
const m=i.data("created_at")
let h
h=p&&m?r.t("Play media comment by %{name} from %{createdAt}.",{name:p,createdAt:m}):r.t("Play media comment.")
if(l){const e="https://"+INST.kalturaSettings.resource_domain
const s=`${e}/p/${INST.kalturaSettings.partner_id}/thumbnail/entry_id/${l}/width/${o.width}/height/${o.height}/bgcolor/000000/type/2/vid_sec/5`
const n=d()(`<span\n        style='background-image: url(${Object(_["a"])(s)});'\n        class='media_comment_thumbnail media_comment_thumbnail-${Object(_["a"])(t)}'\n      >\n        <span class='media_comment_thumbnail_play_button'>\n          <span class='screenreader-only'>\n            ${Object(_["a"])(h)}\n          </span>\n        </span>\n      </span>`)
const r=i.closest("p")
r.attr("title")||r.attr("title",Object(_["a"])(h))
let c=i
if(a){c=i.clone().empty().removeClass("instructure_file_link")
const e=i.parent(".instructure_file_link_holder")
e.length?c.appendTo(e):i.after(c)}else i.empty()
c.data("download",c.attr("href")).prop("href","#").addClass("instructure_inline_media_comment").append(n).css({backgroundImage:"",padding:0})}}d.a.fn.mediaCommentThumbnail=function(e="normal",t){return this.each((function(){o.a.defer(p,this,e,t)}))}},bt35:function(e,t,a){"use strict"
var s=a("An8g")
a("q1tI")
var n=a("i8i4")
var r=a.n(n)
var i=a("thi/")
var o=a("HGxv")
var _=a("8WeW")
Object(_["a"])(JSON.parse('{"ar":{"late_8bd684b6":"متأخر","missing_d59852a7":"مفقود"},"ca":{"late_8bd684b6":"endarrerit","missing_d59852a7":"falta"},"cy":{"late_8bd684b6":"yn hwyr","missing_d59852a7":"ar goll"},"da":{"late_8bd684b6":"sen","missing_d59852a7":"mangler"},"da-x-k12":{"late_8bd684b6":"sen","missing_d59852a7":"mangler"},"de":{"late_8bd684b6":"verspätet","missing_d59852a7":"fehlt"},"en-AU":{"late_8bd684b6":"late","missing_d59852a7":"missing"},"en-AU-x-unimelb":{"late_8bd684b6":"late","missing_d59852a7":"missing"},"en-CA":{"late_8bd684b6":"late","missing_d59852a7":"missing"},"en-GB":{"late_8bd684b6":"late","missing_d59852a7":"missing"},"en-GB-x-lbs":{"late_8bd684b6":"late","missing_d59852a7":"missing"},"en-GB-x-ukhe":{"late_8bd684b6":"late","missing_d59852a7":"missing"},"es":{"late_8bd684b6":"con atraso","missing_d59852a7":"faltante"},"fa":{"late_8bd684b6":"با تأخیر","missing_d59852a7":"از دست رفته"},"fi":{"late_8bd684b6":"myöhässä","missing_d59852a7":"puuttuu"},"fr":{"late_8bd684b6":"En retard","missing_d59852a7":"manquant"},"fr-CA":{"late_8bd684b6":"en retard","missing_d59852a7":"manquant"},"he":{"late_8bd684b6":"מאוחר","missing_d59852a7":"חסר"},"ht":{"late_8bd684b6":"an reta","missing_d59852a7":"manke"},"hu":{"late_8bd684b6":"késői","missing_d59852a7":"hiányzó"},"is":{"late_8bd684b6":"seint","missing_d59852a7":"vantar"},"it":{"late_8bd684b6":"in ritardo","missing_d59852a7":"mancante"},"ja":{"late_8bd684b6":"遅延","missing_d59852a7":"欠如"},"mi":{"late_8bd684b6":"Tōmuri","missing_d59852a7":"ngaro"},"nb":{"late_8bd684b6":"sen","missing_d59852a7":"mangler"},"nb-x-k12":{"late_8bd684b6":"sen","missing_d59852a7":"mangler"},"nl":{"late_8bd684b6":"te laat","missing_d59852a7":"ontbreekt"},"nn":{"late_8bd684b6":"for sein","missing_d59852a7":"manglar"},"pl":{"late_8bd684b6":"po czasie","missing_d59852a7":"brak"},"pt":{"late_8bd684b6":"atrasada","missing_d59852a7":"em falta"},"pt-BR":{"late_8bd684b6":"atrasado","missing_d59852a7":"em falta"},"ru":{"late_8bd684b6":"поздно","missing_d59852a7":"отсутствует"},"sl":{"late_8bd684b6":"zamuda","missing_d59852a7":"manjkajoče"},"sv":{"late_8bd684b6":"sena","missing_d59852a7":"saknas"},"sv-x-k12":{"late_8bd684b6":"sena","missing_d59852a7":"saknas"},"uk":{"late_8bd684b6":"пізно","missing_d59852a7":"відсутній"},"zh-Hans":{"late_8bd684b6":"迟交","missing_d59852a7":"缺失"},"zh-Hant":{"late_8bd684b6":"逾期","missing_d59852a7":"缺失"}}'))
a("jQeR")
a("0sPK")
var l=o["default"].scoped("gradingStatusPill")
function d(e,t){for(let a=0;a<e.length;a+=1)t(e[a])}t["a"]={renderPills(){const e=document.querySelectorAll(".submission-missing-pill")
const t=document.querySelectorAll(".submission-late-pill")
d(e,e=>{r.a.render(Object(s["a"])(i["a"],{variant:"danger",text:l.t("missing")}),e)})
d(t,e=>{r.a.render(Object(s["a"])(i["a"],{variant:"danger",text:l.t("late")}),e)})}}},dKDz:function(e,t,a){"use strict"
var s=/["'&<>]/
e.exports=n
function n(e){var t=""+e
var a=s.exec(t)
if(!a)return t
var n
var r=""
var i=0
var o=0
for(i=a.index;i<t.length;i++){switch(t.charCodeAt(i)){case 34:n="&quot;"
break
case 38:n="&amp;"
break
case 39:n="&#39;"
break
case 60:n="&lt;"
break
case 62:n="&gt;"
break
default:continue}o!==i&&(r+=t.substring(o,i))
o=i+1
r+=n}return o!==i?r+t.substring(o,i):r}},deAH:function(e,t,a){"use strict"
var s=a("ouhR")
var n=a.n(s)
a("8JEM")
const r={globalEnv:window.ENV}
function i(e,...t){return function(a,s){const n=JSON.stringify(s)
const i=t.map(e=>r.globalEnv[e]).join("_")
try{const t=localStorage[e+"Item"](`_${i}_${a}`,n)
if("undefined"===t)return
if(t)return JSON.parse(t)}catch(e){return}}}["get","set","remove"].forEach(e=>{r[e]=i(e,"current_user_id")
r["context"+n.a.capitalize(e)]=i(e,"current_user_id","context_asset_string")})
t["a"]=r},dpqJ:function(e,t,a){"use strict"
a.d(t,"a",(function(){return i}))
var s=a("q1tI")
var n=a.n(s)
var r=a("ZELe")
var i={oneOf:function(e){function t(t,a,s){var r=n.a.Children.toArray(t[a])
var i=e.map((function(e){return e?o(e):e}))
for(var _=0;_<r.length;_++){var l=r[_]
if(l&&l.type){var d=o(l.type)
if(i.indexOf(d)<0)return new Error("Expected one of ".concat(i.join(", ")," in ").concat(s," but found '").concat(d,"'"))}else if(l)return new Error("Expected one of ".concat(i.join(", ")," in ").concat(s," but found an element with unknown type: ").concat(l))}}t.isRequired=Object(r["a"])(t)
return t},oneOfEach:function(e){return function(t,a,s){var r=n.a.Children.toArray(t[a])
var i={}
var _=e.map((function(e){var t=o(e)
i[t]=0
return t}))
for(var l=0;l<r.length;l++){var d=r[l]
if(d&&d.type){var c=o(d.type)
if(_.indexOf(c)<0)return new Error("Expected one of ".concat(_.join(", ")," in ").concat(s," but found '").concat(c,"'"))
i[c]=(i[c]||0)+1}else if(d)return new Error("Expected one of ".concat(_.join(", ")," in ").concat(s," but found an element of unknown type: ").concat(d))}var u=[]
Object.keys(i).forEach((function(e){i[e]>1&&u.push("".concat(i[e]," children of type ").concat(e))
0===i[e]&&u.push("0 children of type ".concat(e))}))
if(u.length>0)return new Error("Expected exactly one of each ".concat(_.join(", ")," in ").concat(s," but found:\n  ").concat(u.join("\n")))}},enforceOrder:function(){for(var e=arguments.length,t=new Array(e),a=0;a<e;a++)t[a]=arguments[a]
function s(e,t){for(var a=0;a<e.length;a++)if(e[a]!==t[a])return false
return true}function i(e,t){return t.map((function(t){return _(e,t)})).join("\n\n")}function _(e,t){var a=t.map((function(e){return e?o(e):"??"})).map((function(e){return"  <".concat(e," />")})).join("\n")
return"<".concat(e,">\n").concat(a,"\n</").concat(e,">")}function l(e,a,r){var l=n.a.Children.toArray(e[a]).map((function(e){if(e&&e.type)return o(e.type)
if(e)return null}))
for(var d=0;d<t.length;d++){var c=t[d].map((function(e){return e?o(e):"??"}))
if(s(l,c))return}return new Error("Expected children of ".concat(r," in one of the following formats:\n  ").concat(i(r,t),"\n\n\n  Instead of:\n  ").concat(_(r,l)))}l.isRequired=Object(r["a"])(l)
return l}}
var o=function(e){return"string"===typeof e?e:e.displayName||e.name}},mYH6:function(e,t,a){"use strict"
a.r(t)
var s=a("ouhR")
var n=a.n(s)
var r=a("mX+G")
var i=a.n(r)
var o=a("deAH")
var _=a("Y/W1")
var l=a.n(_)
var d=a("plYi")
var c=function(e,t){for(var a in t)u.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},u={}.hasOwnProperty
var p=function(e){c(t,e)
function t(){return t.__super__.constructor.apply(this,arguments)}t.prototype.initialize=function(){return this.set("groups",new r["Collection"]([],{comparator:d["a"].byGet("title")}))}
return t}(r["Model"])
var m=function(e,t){for(var a in t)h.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},h={}.hasOwnProperty
var f=function(e){m(t,e)
function t(){return t.__super__.constructor.apply(this,arguments)}t.prototype.initialize=function(){return this.set("outcomes",new r["Collection"]([],{comparator:d["a"].byGet("friendly_name")}))}
t.prototype.count=function(){return this.get("outcomes").length}
t.prototype.statusCount=function(e){return this.get("outcomes").filter((function(t){return t.status()===e})).length}
t.prototype.mastery_count=function(){return this.statusCount("mastery")+this.statusCount("exceeds")}
t.prototype.remedialCount=function(){return this.statusCount("remedial")}
t.prototype.undefinedCount=function(){return this.statusCount("undefined")}
t.prototype.status=function(){return this.remedialCount()>0?"remedial":this.mastery_count()===this.count()?"mastery":this.undefinedCount()===this.count()?"undefined":"near"}
t.prototype.started=function(){return true}
t.prototype.toJSON=function(){return l.a.extend(t.__super__.toJSON.apply(this,arguments),{count:this.count(),mastery_count:this.mastery_count(),started:this.started(),status:this.status()})}
return t}(r["Model"])
var g=a("fIUB")
var b=a("TnsN")
var y=function(e,t){for(var a in t)w.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},w={}.hasOwnProperty
var v=function(e){y(t,e)
function t(){return t.__super__.constructor.apply(this,arguments)}t.optionProperty("key")
t.prototype.parse=function(e){this.linked=e.linked
return e[this.key]}
return t}(b["a"])
class k extends b["a"]{url(){return`/api/v1/courses/${this.course_id}/outcome_groups`}}k.optionProperty("course_id")
k.prototype.model=f
class x extends b["a"]{url(){return`/api/v1/courses/${this.course_id}/outcome_group_links?outcome_style=full`}}x.optionProperty("course_id")
class A extends v{url(){return`/api/v1/courses/${this.course_id}/outcome_rollups?user_ids[]=${this.user_id}`}}A.optionProperty("course_id")
A.optionProperty("user_id")
A.prototype.key="rollups"
class j extends r["Collection"]{constructor(...e){super(...e)
this.fetch=()=>{const e=n.a.Deferred()
const t=l.a.values(this.rawCollections).map(e=>{e.loadAll=true
return e.fetch()})
n.a.when(...t).done(()=>this.processCollections(e))
return e}}initialize(){super.initialize(...arguments)
this.rawCollections={groups:new k([],{course_id:this.course_id}),links:new x([],{course_id:this.course_id}),rollups:new A([],{course_id:this.course_id,user_id:this.user_id})}
return this.outcomeCache=new r["Collection"]}rollups(){const e=this.rawCollections.rollups.at(0).get("scores")
const t=e.map(e=>[e.links.outcome,e])
return l.a.object(t)}populateGroupOutcomes(){const e=this.rollups()
this.outcomeCache.reset()
this.rawCollections.links.each(t=>{const a=new g["a"](t.get("outcome"))
const s=this.rawCollections.groups.get(t.get("outcome_group").id)
const n=e[a.id]
a.set("score",null!=n?n.score:void 0)
a.set("result_title",null!=n?n.title:void 0)
a.set("submission_time",null!=n?n.submitted_at:void 0)
a.set("count",(null!=n?n.count:void 0)||0)
a.group=s
s.get("outcomes").add(a)
this.outcomeCache.add(a)})}populateSectionGroups(){const e=new r["Collection"]
this.rawCollections.groups.each(t=>{let a
if(!t.get("outcomes").length)return
const s=t.get("parent_outcome_group")
const n=s?s.id:t.id;(a=e.get(n))||(a=e.add(new p({id:n,path:this.getPath(n)})))
a.get("groups").add(t)})
return this.reset(e.models)}processCollections(e){this.populateGroupOutcomes()
this.populateSectionGroups()
return e.resolve(this.models)}getPath(e){const t=this.rawCollections.groups.get(e)
const a=t.get("parent_outcome_group")
if(!a)return""
const s=this.getPath(a.id)
return(s?s+": ":"")+t.get("title")}}j.optionProperty("course_id")
j.optionProperty("user_id")
j.prototype.comparator=d["a"].byGet("path")
var V=a("HknL")
var C=a("HGxv")
var S=a("8WeW")
Object(S["a"])(JSON.parse('{"ar":{"exceeds_mastery_1f995dce":"تجاوز الإتقان","meets_mastery_754e1c06":"يلبي الإتقان","near_mastery_f25174a4":"قريب من الإتقان","unstarted_932f2990":"لم تبدأ","well_below_mastery_37664bdc":"أقل بكثير من الإتقان"},"ca":{"exceeds_mastery_1f995dce":"Supera el domini","meets_mastery_754e1c06":"Assoleix el domini","near_mastery_f25174a4":"Domini gairebé assolit","unstarted_932f2990":"No començat","well_below_mastery_37664bdc":"Molt per sota del domini"},"cy":{"exceeds_mastery_1f995dce":"Yn well na’r lefel meistroli","meets_mastery_754e1c06":"Wedi Meistroli","near_mastery_f25174a4":"Bron wedi Meistroli","unstarted_932f2990":"Heb ddechrau","well_below_mastery_37664bdc":"Yn bell iawn o’r lefel Meistroli"},"da":{"exceeds_mastery_1f995dce":"Overstiger opfyldelse af læringsmål","meets_mastery_754e1c06":"Opfylder mål","near_mastery_f25174a4":"Tæt på opfyldelse af læringsmål","unstarted_932f2990":"Ikke startet","well_below_mastery_37664bdc":"Langt under opfyldelse af læringsmål"},"da-x-k12":{"exceeds_mastery_1f995dce":"Overstiger opfyldelse af læringsmål","meets_mastery_754e1c06":"Opfylder mål","near_mastery_f25174a4":"Tæt på opfyldelse af læringsmål","unstarted_932f2990":"Ikke startet","well_below_mastery_37664bdc":"Langt under opfyldelse af læringsmål"},"de":{"exceeds_mastery_1f995dce":"Übertrifft Leistungsziel","meets_mastery_754e1c06":"Leistungsziel erreicht","near_mastery_f25174a4":"Leistungsziel knapp erreicht","unstarted_932f2990":"Nicht gestartet","well_below_mastery_37664bdc":"Deutlich unter dem Leistungsziel"},"el":{"exceeds_mastery_1f995dce":"Υπερβαίνει την Μαεστρία/Τεχνογνωσία","meets_mastery_754e1c06":"Είναι σε Επίπεδο Επιδεξιότητας","near_mastery_f25174a4":"Κοντά στο Επίπεδο Επιδεξιότητας","unstarted_932f2990":"Δεν έχει ξεκινήσει","well_below_mastery_37664bdc":"Αρκετά Κάτω από το Επίπεδο Επιδεξιότητας"},"en-AU":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-AU-x-unimelb":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-CA":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-GB":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-GB-x-lbs":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"en-GB-x-ukhe":{"exceeds_mastery_1f995dce":"Exceeds Mastery","meets_mastery_754e1c06":"Meets Mastery","near_mastery_f25174a4":"Near Mastery","unstarted_932f2990":"Unstarted","well_below_mastery_37664bdc":"Well Below Mastery"},"es":{"exceeds_mastery_1f995dce":"Excede el dominio","meets_mastery_754e1c06":"Reúne el dominio","near_mastery_f25174a4":"Cerca del dominio","unstarted_932f2990":"Sin comenzar","well_below_mastery_37664bdc":"Muy por debajo del dominio"},"fa":{"exceeds_mastery_1f995dce":"فراتر از تسلط","meets_mastery_754e1c06":"با تسلط مطابقت دارد","near_mastery_f25174a4":"نزدیک به تسلط","unstarted_932f2990":"شروع نشده","well_below_mastery_37664bdc":"پایین تر از سطح تسلط"},"fi":{"exceeds_mastery_1f995dce":"Erinomainen osaaminen","meets_mastery_754e1c06":"Hyvä osaaminen","near_mastery_f25174a4":"Tyydyttävä osaaminen","unstarted_932f2990":"Aloittamaton","well_below_mastery_37664bdc":"Heikko osaaminen"},"fr":{"exceeds_mastery_1f995dce":"Surpasse le niveau de maîtrise","meets_mastery_754e1c06":"Niveau de maîtrise atteint","near_mastery_f25174a4":"Proche du niveau de maîtrise","unstarted_932f2990":"Non commencé","well_below_mastery_37664bdc":"Bien inférieur au niveau de maîtrise"},"fr-CA":{"exceeds_mastery_1f995dce":"Surpasse le niveau de Maîtrise","meets_mastery_754e1c06":"Niveau de Maîtrise atteint","near_mastery_f25174a4":"Proche du niveau de maîtrise","unstarted_932f2990":"Non commencé","well_below_mastery_37664bdc":"Bien inférieur au niveau de maîtrise"},"he":{"exceeds_mastery_1f995dce":"מעבר לרף הנדרש למומחיות","meets_mastery_754e1c06":"עונה על דרישות ההתמחות","near_mastery_f25174a4":"קרוב להתמחות","unstarted_932f2990":"טרם החל","well_below_mastery_37664bdc":"רחוק מאד מהתמחות"},"ht":{"exceeds_mastery_1f995dce":"Depase Metriz","meets_mastery_754e1c06":"Respekte Metriz","near_mastery_f25174a4":"Pwòch Metriz","unstarted_932f2990":"Pa kòmanse","well_below_mastery_37664bdc":"Pi ba Metriz"},"hu":{"exceeds_mastery_1f995dce":"A jártassági szint fölött","meets_mastery_754e1c06":"Megfelel a jártassági szintnek","near_mastery_f25174a4":"Közel a jártassági szinthez","unstarted_932f2990":"Nincs elkezdve","well_below_mastery_37664bdc":"Jóval a jártassági szint alatt"},"hy":{"exceeds_mastery_1f995dce":"Գերազանցում է անցողիկ միավորը","meets_mastery_754e1c06":"Համապատասխանում է անցողիկ միավորին","near_mastery_f25174a4":"Մոտ է անցողիկ միավորին","unstarted_932f2990":"Չի սկսվել","well_below_mastery_37664bdc":"Բավական ցածր է անցողիկ միավորներից "},"is":{"exceeds_mastery_1f995dce":"Fer fram úr tileinkun","meets_mastery_754e1c06":"Uppfyllir tileinkun","near_mastery_f25174a4":"Nálægt tileinkun","unstarted_932f2990":"Óbyrjað","well_below_mastery_37664bdc":"Langt undir tileinkun"},"it":{"exceeds_mastery_1f995dce":"Supera il livello di padronanza","meets_mastery_754e1c06":"Soddisfa il livello di padronanza","near_mastery_f25174a4":"Padronanza quasi completa","unstarted_932f2990":"Non iniziato","well_below_mastery_37664bdc":"Ben al di sotto del livello di padronanza"},"ja":{"exceeds_mastery_1f995dce":"熟達を上回る","meets_mastery_754e1c06":"熟達を満たしている","near_mastery_f25174a4":"熟達に近い","unstarted_932f2990":"開始前","well_below_mastery_37664bdc":"熟達を大きく下回る"},"mi":{"exceeds_mastery_1f995dce":"Nui atu te mana","meets_mastery_754e1c06":"Tutuki Mana","near_mastery_f25174a4":"Tata Mātatau","unstarted_932f2990":"Koare i tīmata","well_below_mastery_37664bdc":"Kei raro rawa atu Mātatau"},"nb":{"exceeds_mastery_1f995dce":"Overgår ekspertise","meets_mastery_754e1c06":"Innfrir forventningene","near_mastery_f25174a4":"Nær ekspertise","unstarted_932f2990":"Ikke påbegynt","well_below_mastery_37664bdc":"Innfrir ikke ekspertise"},"nb-x-k12":{"exceeds_mastery_1f995dce":"Overgår ekspertise","meets_mastery_754e1c06":"Innfrir forventningene","near_mastery_f25174a4":"Nær ekspertise","unstarted_932f2990":"Ikke påbegynt","well_below_mastery_37664bdc":"Innfrir ikke ekspertise"},"nl":{"exceeds_mastery_1f995dce":"Overtreft Meesterschap","meets_mastery_754e1c06":"Voldoende vakbeheersing","near_mastery_f25174a4":"Bijna Meesterschap","unstarted_932f2990":"Nog niet begonnen","well_below_mastery_37664bdc":"Ruim onder vakbeheersing"},"nn":{"exceeds_mastery_1f995dce":"Overskrid meistringsnivå","meets_mastery_754e1c06":"Møter meistringsnivå","near_mastery_f25174a4":"Nær meistringsnivå","unstarted_932f2990":"Ikkje starta","well_below_mastery_37664bdc":"Godt under meistringsnivå"},"pl":{"exceeds_mastery_1f995dce":"Przekracza poziom biegłości","meets_mastery_754e1c06":"Spełnia poziom biegłości","near_mastery_f25174a4":"Blisko biegłości","unstarted_932f2990":"Nierozpoczęto","well_below_mastery_37664bdc":"Znacznie poniżej poziomu biegłości"},"pt":{"exceeds_mastery_1f995dce":"Excede a Excelência","meets_mastery_754e1c06":"Completa o domínio","near_mastery_f25174a4":"Perto do Domínio","unstarted_932f2990":"Não iniciado","well_below_mastery_37664bdc":"Muito abaixo de Domínio"},"pt-BR":{"exceeds_mastery_1f995dce":"Excede Domínio","meets_mastery_754e1c06":"Encontra com Domínio","near_mastery_f25174a4":"Quase Domínio","unstarted_932f2990":"Não iniciado","well_below_mastery_37664bdc":"Muito abaixo de Domínio"},"ru":{"exceeds_mastery_1f995dce":"Превышает проходной балл","meets_mastery_754e1c06":"Соответствует проходному баллу","near_mastery_f25174a4":"Почти усвоено","unstarted_932f2990":"Не начато","well_below_mastery_37664bdc":"Гораздо ниже проходного балла"},"sl":{"exceeds_mastery_1f995dce":"Presega odličnost","meets_mastery_754e1c06":"Odličen","near_mastery_f25174a4":"Delno obvladano","unstarted_932f2990":"Nezačeto","well_below_mastery_37664bdc":"Precej pod odličnostjo"},"sv":{"exceeds_mastery_1f995dce":"Överträffar måluppfyllelse","meets_mastery_754e1c06":"Möter måluppfyllelse","near_mastery_f25174a4":"Nära måluppfyllelse","unstarted_932f2990":"Ej påbörjad","well_below_mastery_37664bdc":"Långt under måluppfyllelse"},"sv-x-k12":{"exceeds_mastery_1f995dce":"Överträffar måluppfyllelse","meets_mastery_754e1c06":"Möter måluppfyllelse","near_mastery_f25174a4":"Nära måluppfyllelse","unstarted_932f2990":"Ej påbörjad","well_below_mastery_37664bdc":"Långt under måluppfyllelse"},"tr":{"exceeds_mastery_1f995dce":"Yeterliğin Üstünde","meets_mastery_754e1c06":"Yeterliği Karşılıyor","near_mastery_f25174a4":"Yeterliğe Yakın","unstarted_932f2990":"Başlamamış","well_below_mastery_37664bdc":"Yeterliğin Epey Altında"},"uk":{"exceeds_mastery_1f995dce":"Перевищує Майстерність","meets_mastery_754e1c06":"Відповідає майстерності","near_mastery_f25174a4":"Майже майстерність","unstarted_932f2990":"Не розпочато","well_below_mastery_37664bdc":"Суттєво нижча майстерність"},"zh-Hans":{"exceeds_mastery_1f995dce":"超过掌握","meets_mastery_754e1c06":"达到掌握","near_mastery_f25174a4":"最接近的掌握","unstarted_932f2990":"未开始","well_below_mastery_37664bdc":"远低于掌握"},"zh-Hant":{"exceeds_mastery_1f995dce":"超過掌握程度","meets_mastery_754e1c06":"達到掌握程度","near_mastery_f25174a4":"接近掌握程度","unstarted_932f2990":"未開始","well_below_mastery_37664bdc":"遠低於掌握程度"}}'))
a("jQeR")
a("0sPK")
var z=C["default"].scoped("grade_summaryGroupView")
var O=a("3O+N")
var N=a.n(O)
a("BGrI")
var P=N.a.default
var I=P.template,T=P.templates=P.templates||{}
var E="/work/canvas-deploy/generated/ui/features/grade-summary/jst/progress-bar"
T[E]=I((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i,o="",_="function",l=this.escapeExpression
o+='<div class="bar-marker" style="left: '
if(i=a.masteryPercent)r=i.call(t,{hash:{},data:n})
else{i=t&&t.masteryPercent
r=typeof i===_?i.call(t,{hash:{},data:n}):i}o+=l(r)+'%"></div>\n<div class="bar-progress '
if(i=a.status)r=i.call(t,{hash:{},data:n})
else{i=t&&t.status
r=typeof i===_?i.call(t,{hash:{},data:n}):i}o+=l(r)+'" style="width: '
if(i=a.percentProgress)r=i.call(t,{hash:{},data:n})
else{i=t&&t.percentProgress
r=typeof i===_?i.call(t,{hash:{},data:n}):i}o+=l(r)+'%"></div>\n'
return o}))
var D=T[E]
var L=function(e,t){for(var a in t)M.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},M={}.hasOwnProperty
var G=function(e){L(t,e)
function t(){return t.__super__.constructor.apply(this,arguments)}t.prototype.className="bar"
t.prototype.template=D
return t}(i.a.View)
var B=a("f40T")
var R=function(e,t){return function(){return e.apply(t,arguments)}},W=function(e,t){for(var a in t)U.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},U={}.hasOwnProperty
var K=function(e){W(t,e)
function t(){this.handleAdd=R(this.handleAdd,this)
this.handleReset=R(this.handleReset,this)
return t.__super__.constructor.apply(this,arguments)}t.prototype.key="outcome_results"
t.prototype.model=g["a"]
t.optionProperty("outcome")
t.prototype.url=function(){return"/api/v1/courses/"+this.course_id+"/outcome_results?user_ids[]="+this.user_id+"&outcome_ids[]="+this.outcome.id+"&include[]=alignments&per_page=100"}
t.prototype.loadAll=true
t.prototype.comparator=function(e){return-1*e.get("submitted_or_assessed_at").getTime()}
t.prototype.initialize=function(){var e
t.__super__.initialize.apply(this,arguments)
this.model=g["a"].extend({defaults:{points_possible:this.outcome.get("points_possible"),mastery_points:this.outcome.get("mastery_points")}})
this.course_id=null!=(e=ENV.context_asset_string)?e.replace("course_",""):void 0
this.user_id=ENV.student_id
this.on("reset",this.handleReset)
return this.on("add",this.handleAdd)}
t.prototype.handleReset=function(){return this.each(this.handleAdd)}
t.prototype.handleAdd=function(e){var t,a
t=e.get("links").alignment
e.set("alignment_name",null!=(a=this.alignments.get(t))?a.get("name"):void 0)
return e.get("points_possible")>0?e.set("score",e.get("points_possible")*e.get("percent")):e.set("score",e.get("mastery_points")*e.get("percent"))}
t.prototype.parse=function(e){var t
null==this.alignments&&(this.alignments=new i.a.Collection([]))
this.alignments.add((null!=e&&null!=(t=e.linked)?t.alignments:void 0)||[])
return e[this.key]}
return t}(v)
var H=a("bljh")
var q=a.n(H)
Object(S["a"])(JSON.parse('{"ar":{"date_ee500367":"التاريخ","score_f7ac9e08":"الدرجة","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"أحدث درجات المستخدم %{current_user_name} ونتيجة %{outcome_name}."},"ca":{"date_ee500367":"Data","score_f7ac9e08":"Puntuació","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Les puntuacions més recents de l\'usuari %{current_user_name} i el resultat %{outcome_name}."},"cy":{"date_ee500367":"Dyddiad","score_f7ac9e08":"Sgôr","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Y sgoriau diweddaraf ar gyfer y defnyddiwr %{current_user_name} a’r deilliant %{outcome_name}."},"da":{"date_ee500367":"Dato","score_f7ac9e08":"Resultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"De seneste resultater for bruger %{current_user_name} og læringsudbytte %{outcome_name}."},"da-x-k12":{"date_ee500367":"Dato","score_f7ac9e08":"Resultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"De seneste resultater for bruger %{current_user_name} og læringsudbytte %{outcome_name}."},"de":{"date_ee500367":"Datum","score_f7ac9e08":"Punktestand","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Die letzten Punkte für den Benutzer %{current_user_name} sowie das Lernziel %{outcome_name}"},"el":{"date_ee500367":"Ημερομηνία","score_f7ac9e08":"Βαθμολογία","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Οι πιο πρόσφατες βαθμολογίες του χρήστη %{current_user_name} και του αποτελέσματος %{outcome_name}."},"en-AU":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"The most recent scores for user %{current_user_name} and outcome %{outcome_name}."},"en-AU-x-unimelb":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"The most recent scores for user %{current_user_name} and outcome %{outcome_name}."},"en-CA":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"The most recent scores for user %{current_user_name} and outcome %{outcome_name}."},"en-GB":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Most recent scores for user %{current_user_name} and outcome %{outcome_name}."},"en-GB-x-lbs":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Most recent scores for user %{current_user_name} and outcome %{outcome_name}."},"en-GB-x-ukhe":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Most recent scores for user %{current_user_name} and outcome %{outcome_name}."},"es":{"date_ee500367":"Fecha","score_f7ac9e08":"Puntaje","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Los puntajes más recientes para el usuario %{current_user_name} y la competencia %{outcome_name}."},"fa":{"date_ee500367":"تاریخ","score_f7ac9e08":"نمره","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"جدیدترین نمره ها برای کاربر %{current_user_name} و نتیجه %{outcome_name}."},"fi":{"date_ee500367":"Päivämäärä","score_f7ac9e08":"Pistemäärä","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Viimeisimmät pistemäärät käyttäjälle %{current_user_name} ja tulokselle %{outcome_name}."},"fr":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Les notes les plus récentes pour l\'utilisateur %{current_user_name} et l\'acquis %{outcome_name}."},"fr-CA":{"date_ee500367":"Date","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Les notes les plus récentes pour l\'utilisateur %{current_user_name} et le résultat %{outcome_name}."},"he":{"date_ee500367":"תאריך","score_f7ac9e08":"ציון","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"הציונים העדכניים ביותר של משתמש %{current_user_name} ותוצאות %{outcome_name}."},"ht":{"date_ee500367":"Dat","score_f7ac9e08":"Nòt","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Nòt ki pi resan pou %{current_user_name} e rezilta %{outcome_name}."},"hu":{"date_ee500367":"Dátum","score_f7ac9e08":"Eredmény","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"A legfrissebb pontszámok a/az %{current_user_name} felhasználó esetén, valamint a tanulási eredmények %{outcome_name}."},"hy":{"the_most_recent_scores_for_user_current_user_name__ea82f0b6":" %{current_user_name} օգտատիրոջ ամենավերջին միավորները և %{outcome_name} արդյունքները:"},"is":{"date_ee500367":"Dagsetning","score_f7ac9e08":"Einkunn","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Nýjustu einkunnir fyrir %{current_user_name} og niðurstaða %{outcome_name}."},"it":{"date_ee500367":"Data","score_f7ac9e08":"Punteggio","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"I punteggi più recenti per l\'utente %{current_user_name} e l\'esito %{outcome_name}."},"ja":{"date_ee500367":"日付","score_f7ac9e08":"スコア","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"ユーザ %{current_user_name} の最近のスコアと結果 %{outcome_name}。"},"mi":{"date_ee500367":"Rā","score_f7ac9e08":"Koinga","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Ko te tatau tino tata mō te kaiwhakamahi %{current_user_name} me te putanga %{outcome_name}."},"nb":{"date_ee500367":"Dato","score_f7ac9e08":"Resultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"De nyligste resultat for brukeren %{current_user_name} og læringsmålet %{outcome_name}."},"nb-x-k12":{"date_ee500367":"Dato","score_f7ac9e08":"Resultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"De siste resultatene for brukeren %{current_user_name} og målet %{outcome_name}."},"nl":{"date_ee500367":"Datum","score_f7ac9e08":"Score","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"De meest recente scores voor gebruiker %{current_user_name} en %{outcome_name} resultaat."},"nn":{"date_ee500367":"Dato","score_f7ac9e08":"Resultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Det siste resultata for brukar %{current_user_name} og læringsmål %{outcome_name}."},"pl":{"date_ee500367":"Data","score_f7ac9e08":"Wynik","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Najnowsze wyniki użytkownika %{current_user_name} i rezultat %{outcome_name}."},"pt":{"date_ee500367":"Data","score_f7ac9e08":"Pontuação","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"As classificações mais recentes por utilizador %{current_user_name} e resultado %{outcome_name}."},"pt-BR":{"date_ee500367":"Data","score_f7ac9e08":"Pontuação","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"As pontuações mais recentes para o usuário %{current_user_name} e objetivo %{outcome_name}."},"ru":{"date_ee500367":"Дата","score_f7ac9e08":"Оценка","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Самые последние баллы для пользователя %{current_user_name} и результат %{outcome_name}."},"sl":{"date_ee500367":"Datum","score_f7ac9e08":"Rezultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Najnovejši rezultati za uporabnika %{current_user_name} in izid %{outcome_name}."},"sv":{"date_ee500367":"Datum","score_f7ac9e08":"Resultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"De senaste resultaten för användaren %{current_user_name} och lärandemålen %{outcome_name}."},"sv-x-k12":{"date_ee500367":"Datum","score_f7ac9e08":"Resultat","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"De senaste resultaten för användaren %{current_user_name} och lärandemålen %{outcome_name}."},"tr":{"date_ee500367":"Tarih","score_f7ac9e08":"Puan","the_most_recent_scores_for_user_current_user_name__ea82f0b6":" %{current_user_name} için en son puan ve %{outcome_name} öğrenme çıktısı."},"uk":{"date_ee500367":"Дата","score_f7ac9e08":"Оцінка","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"Останні оцінки для користувача %{current_user_name}та результат %{outcome_name}"},"zh-Hans":{"date_ee500367":"日期","score_f7ac9e08":"评分","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"为用户的最新得分 %{current_user_name} 和结果 %{outcome_name}。"},"zh-Hant":{"date_ee500367":"日期","score_f7ac9e08":"分數","the_most_recent_scores_for_user_current_user_name__ea82f0b6":"用户 %{current_user_name} 的最新分数和成果 %{outcome_name}。"}}'))
C["default"].scoped("outcomes.accessible_line_graph")
var F=N.a.default
var J=F.template,Z=F.templates=F.templates||{}
var Y="/work/canvas-deploy/generated/ui/features/grade-summary/jst/accessibleLineGraph"
Z[Y]=J((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i,o,_="",l=a.helperMissing,d=this.escapeExpression,c=this
function u(e,t){var s,n,r=""
r+='\n    <tr>\n      <td scope="row">'+d((s=a.tDateToString||e&&e.tDateToString,n={hash:{},data:t},s?s.call(e,e&&e.date,"medium",n):l.call(e,"tDateToString",e&&e.date,"medium",n)))+"</td>\n      <td>"+d((s=a.nf||e&&e.nf,n={hash:{format:"outcomeScore"},data:t},s?s.call(e,e&&e.y,n):l.call(e,"nf",e&&e.y,n)))+"</td>\n    </tr>\n  "
return r}_+="<table>\n  <caption>\n    "+d((i=a.t||t&&t.t,o={hash:{current_user_name:t&&t.current_user_name,outcome_name:t&&t.outcome_name,i18n_inferred_key:true},data:n},i?i.call(t,"the_most_recent_scores_for_user_current_user_name__ea82f0b6","The most recent scores for user %{current_user_name} and outcome %{outcome_name}.",o):l.call(t,"t","the_most_recent_scores_for_user_current_user_name__ea82f0b6","The most recent scores for user %{current_user_name} and outcome %{outcome_name}.",o)))+'\n  </caption>\n  <tr>\n    <th scope="col">'+d((i=a.t||t&&t.t,o={hash:{i18n_inferred_key:true},data:n},i?i.call(t,"date_ee500367","Date",o):l.call(t,"t","date_ee500367","Date",o)))+'</th>\n    <th scope="col">'+d((i=a.t||t&&t.t,o={hash:{i18n_inferred_key:true},data:n},i?i.call(t,"score_f7ac9e08","Score",o):l.call(t,"t","score_f7ac9e08","Score",o)))+"</th>\n  </tr>\n  "
r=a.each.call(t,t&&t.data,{hash:{},inverse:c.noop,fn:c.program(1,u,n),data:n});(r||0===r)&&(_+=r)
_+="\n</table>\n"
return _}))
var Q=Z[Y]
l.a.mixin({sum:(e,t=null,a=0)=>l.a.reduce(e,(e,a)=>(null!=t?t(a):a)+e,a)})
var X,ee=function(e,t){return function(){return e.apply(t,arguments)}},te=function(e,t){for(var a in t)ae.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},ae={}.hasOwnProperty
X=function(){function e(e){this.rawData=e}e.prototype.data=function(){return[[this.xValue(this.rawData[0]),this.yIntercept(),this.xValue(l.a.last(this.rawData)),this.slope()*this.xValue(l.a.last(this.rawData))+this.yIntercept()]]}
e.prototype.slope=function(){return(this.a()-this.b())/(this.c()-this.d())}
e.prototype.yIntercept=function(){return(this.e()-this.f())/this.n()}
e.prototype.n=function(){return this.rawData.length}
e.prototype.a=function(){return this.n()*l.a.sum(this.rawData,(e=this,function(t){return e.xValue(t)*e.yValue(t)}))
var e}
e.prototype.b=function(){return l.a.sum(this.rawData,this.xValue)*l.a.sum(this.rawData,this.yValue)}
e.prototype.c=function(){return this.n()*l.a.sum(this.rawData,(e=this,function(t){return Math.pow(e.xValue(t),2)}))
var e}
e.prototype.d=function(){return Math.pow(l.a.sum(this.rawData,this.xValue),2)}
e.prototype.e=function(){return l.a.sum(this.rawData,this.yValue)}
e.prototype.f=function(){return this.slope()*l.a.sum(this.rawData,this.xValue)}
e.prototype.xValue=function(e){return e.x}
e.prototype.yValue=function(e){return e.y}
return e}()
var se=function(e){te(t,e)
function t(){this.yValue=ee(this.yValue,this)
this.xValue=ee(this.xValue,this)
return t.__super__.constructor.apply(this,arguments)}t.optionProperty("el")
t.optionProperty("height")
t.optionProperty("limit")
t.optionProperty("margin")
t.optionProperty("model")
t.optionProperty("timeFormat")
t.prototype.defaults={height:200,limit:8,margin:{top:20,right:20,bottom:30,left:40},timeFormat:"%Y-%m-%dT%XZ"}
t.prototype.initialize=function(){t.__super__.initialize.apply(this,arguments)
this.deferred=$.Deferred()
this.collection=new K([],{outcome:this.model})
this.collection.on("fetched:last",(e=this,function(){return e.deferred.resolve()}))
var e
return this.collection.fetch()}
t.prototype.render=function(){if(this.deferred.isResolved()){if(this.collection.isEmpty())return this
this._prepareScales()
this._prepareAxes()
this._prepareLines()
this.svg=q.a.select(this.el).append("svg").attr("width",this.width()+this.margin.left+this.margin.right).attr("height",this.height+this.margin.top+this.margin.bottom).attr("aria-hidden",true).append("g").attr("transform","translate("+this.margin.left+", "+this.margin.top+")")
this._appendAxes()
this._appendLines()
this.$(".screenreader-only").append(Q(this.toJSON()))}else this.deferred.done(this.render)
return this}
t.prototype.toJSON=function(){return{current_user_name:ENV.current_user.display_name,data:this.data(),outcome_name:this.model.get("friendly_name")}}
t.prototype.data=function(){return null!=this._data?this._data:this._data=this.collection.chain().last(this.limit).map((e=this,function(t,a){return{x:a,y:e.percentageFor(t.get("score")),date:t.get("submitted_or_assessed_at")}})).value()
var e}
t.prototype.masteryPercentage=function(){return this.model.get("points_possible")>0?this.model.get("mastery_points")/this.model.get("points_possible")*100:100}
t.prototype.percentageFor=function(e){return this.model.get("points_possible")>0?e/this.model.get("points_possible")*100:e/this.model.get("mastery_points")*100}
t.prototype.xValue=function(e){return this.x(e.x)}
t.prototype.yValue=function(e){return this.y(e.y)}
t.prototype._appendAxes=function(){this.svg.append("g").attr("class","x axis").attr("transform","translate(0,"+this.height+")").call(this.xAxis)
this.svg.append("g").attr("class","date-guides").attr("transform","translate(0,"+this.height+")").call(this.dateGuides)
this.svg.append("g").attr("class","y axis").call(this.yAxis)
this.svg.append("g").attr("class","guides").call(this.yGuides)
return this.svg.append("g").attr("class","mastery-percentage-guide").style("stroke-dasharray","3, 3").call(this.masteryPercentageGuide)}
t.prototype._appendLines=function(){this.svg.selectAll("circle").data(this.data()).enter().append("circle").attr("fill","black").attr("r",3).attr("cx",this.xValue).attr("cy",this.yValue)
this.svg.append("path").datum(this.data()).attr("d",this.line).attr("class","line").attr("stroke","black").attr("stroke-width",1).attr("fill","none")
null!=this.trend&&this.svg.selectAll(".trendline").data(this.trend.data()).enter().append("line").attr("class","trendline").attr("x1",(e=this,function(t){return e.x(t[0])})).attr("y1",function(e){return function(t){return e.y(t[1])}}(this)).attr("x2",function(e){return function(t){return e.x(t[2])}}(this)).attr("y2",function(e){return function(t){return e.y(t[3])}}(this)).attr("stroke-width",1)
var e
return this.svg}
t.prototype._prepareAxes=function(){this.xAxis=q.a.svg.axis().scale(this.x).tickFormat("")
this.dateGuides=q.a.svg.axis().scale(this.xTimeScale).tickValues([l.a.first(this.data()).date,l.a.last(this.data()).date]).tickFormat((function(e){return Intl.DateTimeFormat(C["default"].currentLocale(),{day:"numeric",month:"numeric"}).format(e)}))
this.yAxis=q.a.svg.axis().scale(this.y).orient("left").tickFormat((function(e){return C["default"].n(e,{percentage:true})})).tickValues([0,50,100])
this.yGuides=q.a.svg.axis().scale(this.y).orient("left").tickValues([50,100]).tickSize(-this.width(),0,0).tickFormat("")
return this.masteryPercentageGuide=q.a.svg.axis().scale(this.y).orient("left").tickValues([this.masteryPercentage()]).tickSize(-this.width(),0,0).tickFormat("")}
t.prototype._prepareLines=function(){this.data().length>=3&&(this.trend=new X(this.data()))
return this.line=q.a.svg.line().x(this.xValue).y(this.yValue).interpolate("linear")}
t.prototype._prepareScales=function(){this.x=q.a.scale.linear().range([0,this.width()]).domain([0,this.limit-1])
this.xTimeScale=q.a.time.scale().range([0,this.xTimeScaleWidth()]).domain([l.a.first(this.data()).date,l.a.last(this.data()).date])
return this.y=q.a.scale.linear().range([this.height,this.margin.bottom]).domain([0,100])}
t.prototype.width=function(){return this.$el.width()-this.margin.left-this.margin.right-10}
t.prototype.xTimeScaleWidth=function(){return this.width()-this.width()/(this.limit-1)*(this.limit-this.data().length)}
return t}(i.a.View)
var ne=a("niMM")
var re=function(e,t){return function(){return e.apply(t,arguments)}},ie=function(e,t){for(var a in t)oe.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},oe={}.hasOwnProperty
var _e=function(e){ie(t,e)
function t(){this.mouseleave=re(this.mouseleave,this)
this.mouseenter=re(this.mouseenter,this)
return t.__super__.constructor.apply(this,arguments)}t.prototype.TIMEOUT_LENGTH=50
t.optionProperty("el")
t.optionProperty("model")
t.prototype.events={"click i":"mouseleave","mouseenter i":"mouseenter","mouseleave i":"mouseleave"}
t.prototype.inside=false
t.prototype.initialize=function(){t.__super__.initialize.apply(this,arguments)
return this.outcomeLineGraphView=new se({model:this.model})}
t.prototype.render=function(){return Object(ne["a"])(this.toJSON())}
t.prototype.closePopover=function(e){null!=e&&e.preventDefault()
if(null==this.popover)return true
this.popover.hide()
return delete this.popover}
t.prototype.mouseenter=function(e){this.openPopover(e)
return this.inside=true}
t.prototype.mouseleave=function(e){this.inside=false
return setTimeout((t=this,function(){if(t.inside||!t.popover)return
return t.closePopover()}),this.TIMEOUT_LENGTH)
var t}
t.prototype.openPopover=function(e){this.closePopover()&&(this.popover=new B["a"](e,this.render(),{verticalSide:"bottom",manualOffset:14}))
this.outcomeLineGraphView.setElement(this.popover.el.find("div.line-graph"))
return this.outcomeLineGraphView.render()}
return t}(i.a.View)
var le=a("KIYd")
var de=function(e,t){return function(){return e.apply(t,arguments)}},ce=function(e,t){for(var a in t)ue.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},ue={}.hasOwnProperty
var pe=function(e){ce(t,e)
function t(){this._getKey=de(this._getKey,this)
this.onClose=de(this.onClose,this)
return t.__super__.constructor.apply(this,arguments)}t.optionProperty("model")
t.prototype.$target=null
t.prototype.template=ne["a"]
t.prototype.initialize=function(){t.__super__.initialize.apply(this,arguments)
return this.outcomeLineGraphView=new se({model:this.model})}
t.prototype.afterRender=function(){this.outcomeLineGraphView.setElement(this.$("div.line-graph"))
return this.outcomeLineGraphView.render()}
t.prototype.dialogOptions=function(){return{containerId:"outcome_results_dialog",close:this.onClose,buttons:[],width:460}}
t.prototype.show=function(e){if(!("click"===e.type||this._getKey(e.keyCode)))return
this.$target=n()(e.target)
e.preventDefault()
this.$el.dialog("option","title",this.model.get("title"))
t.__super__.show.apply(this,arguments)
return this.render()}
t.prototype.onClose=function(){this.$target.focus()
return delete this.$target}
t.prototype.toJSON=function(){var e
e=t.__super__.toJSON.apply(this,arguments)
return l.a.extend(e,{dialog:true})}
t.prototype._getKey=function(e){var t
t={13:"enter",32:"spacebar"}
return t[e]}
return t}(le["a"])
Object(S["a"])(JSON.parse('{"ar":{"click_for_more_details_on_this_outcome_8c018d2f":"انقر للحصول على مزيد من التفاصيل عن هذه النتيجة","n_count_alignments_a45bebe7":"*%{n_count}* محاذاة","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"النتيجة: %{title} بالحالة %{statusTooltip}"},"ca":{"click_for_more_details_on_this_outcome_8c018d2f":"feu clic per obtenir més informació sobre aquest resultat","n_count_alignments_a45bebe7":"*%{n_count}* alineacions","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Resultat: %{title} té l\'estat %{statusTooltip}"},"cy":{"click_for_more_details_on_this_outcome_8c018d2f":"cliciwch i gael rhagor o fanylion am y deilliant hwn","n_count_alignments_a45bebe7":"*%{n_count}* Cysoniad","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Deilliant: %{title} â statws %{statusTooltip}"},"da":{"click_for_more_details_on_this_outcome_8c018d2f":"klik for yderligere oplysninger om dette læringsudbytte","n_count_alignments_a45bebe7":"*%{n_count}* Tilpasninger","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsudbytte: %{title} har en status for %{statusTooltip}"},"da-x-k12":{"click_for_more_details_on_this_outcome_8c018d2f":"klik for yderligere oplysninger om dette læringsudbytte","n_count_alignments_a45bebe7":"*%{n_count}* Tilpasninger","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsudbytte: %{title} har en status for %{statusTooltip}"},"de":{"click_for_more_details_on_this_outcome_8c018d2f":"Für mehr Details zu diesem Ergebnis klicken","n_count_alignments_a45bebe7":"*%{n_count}* Ausrichtungen","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Lernziel: %{title} hat den Status %{statusTooltip}"},"el":{"click_for_more_details_on_this_outcome_8c018d2f":"Κάντε κλικ για περισσότερες λεπτομέρειες σχετικά με αυτό το αποτέλεσμα","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Αποτέλεσμα: Η κατάσταση του %{title} είναι %{statusTooltip}"},"en-AU":{"click_for_more_details_on_this_outcome_8c018d2f":"Click for more details on this outcome.","n_count_alignments_a45bebe7":"*%{n_count}* Alignments","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}"},"en-AU-x-unimelb":{"click_for_more_details_on_this_outcome_8c018d2f":"Click for more details on this outcome.","n_count_alignments_a45bebe7":"*%{n_count}* Alignments","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}"},"en-CA":{"click_for_more_details_on_this_outcome_8c018d2f":"click for more details on this outcome","n_count_alignments_a45bebe7":"*%{n_count}* Alignments","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}"},"en-GB":{"click_for_more_details_on_this_outcome_8c018d2f":"click for more details on this outcome","n_count_alignments_a45bebe7":"*%{n_count}* Alignments","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}"},"en-GB-x-lbs":{"click_for_more_details_on_this_outcome_8c018d2f":"click for more details on this outcome","n_count_alignments_a45bebe7":"*%{n_count}* Alignments","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}"},"en-GB-x-ukhe":{"click_for_more_details_on_this_outcome_8c018d2f":"click for more details on this outcome","n_count_alignments_a45bebe7":"*%{n_count}* Alignments","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Outcome: %{title} has a status of %{statusTooltip}"},"es":{"click_for_more_details_on_this_outcome_8c018d2f":"haga clic para más detalles de esta competencia","n_count_alignments_a45bebe7":"*%{n_count}* Alineamientos","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Competencia: el estado de %{title} es %{statusTooltip}"},"fa":{"click_for_more_details_on_this_outcome_8c018d2f":"برای اطلاعات بیشتر درباره این نتیجه، کلیک کنید","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"نتیجه: %{title} دارای وضعیت %{statusTooltip} است"},"fi":{"click_for_more_details_on_this_outcome_8c018d2f":"lisätietoja tästä osaamistuloksesta napsauttamalla","n_count_alignments_a45bebe7":"*%{n_count}* Kohdistamiset","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Osaamistulos: %{title} on tilassa %{statusTooltip}"},"fr":{"click_for_more_details_on_this_outcome_8c018d2f":"cliquer pour plus de détails sur cet acquis","n_count_alignments_a45bebe7":"*%{n_count}* Alignements","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Acquis : %{title} a le statut %{statusTooltip}"},"fr-CA":{"click_for_more_details_on_this_outcome_8c018d2f":"cliquer pour plus de détails sur ce résultat","n_count_alignments_a45bebe7":"*%{n_count}* Alignements","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Résultat : %{title} a le statut %{statusTooltip}"},"he":{"click_for_more_details_on_this_outcome_8c018d2f":"הקלק/י לפירוט נוסף של התוצאות האלה","n_count_alignments_a45bebe7":"*%{n_count}* עימודים","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"תוצאות למידה: %{title} בסטטוס של %{statusTooltip}"},"ht":{"click_for_more_details_on_this_outcome_8c018d2f":"klike pou plis detay sou rezilta sa a","n_count_alignments_a45bebe7":"*%{n_count}* Aliyman","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Rezilta: %{title} gen estati %{statusTooltip}"},"hu":{"click_for_more_details_on_this_outcome_8c018d2f":"kattintson további részletekért ezen tanulási eredményre vonatkozóan","n_count_alignments_a45bebe7":"*%{n_count}* illesztés","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Tanulási eredmény: %{title} a következő státuszú %{statusTooltip}"},"hy":{"click_for_more_details_on_this_outcome_8c018d2f":"Սեղմել այս արդյունքի մանրամասները տեսնելու համար","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Արդյունք՝ %{title} ունի %{statusTooltip} կարգավիճակ"},"is":{"click_for_more_details_on_this_outcome_8c018d2f":"smelltu til að fá meiri upplýsingar um þessa niðurstöðu","n_count_alignments_a45bebe7":"*%{n_count}* Jafnanir","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Niðurstaða: %{title} hefur stöðu %{statusTooltip}"},"it":{"click_for_more_details_on_this_outcome_8c018d2f":"fai clic per maggiori dettagli su questo esito","n_count_alignments_a45bebe7":"*%{n_count}* Allineamenti","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Esito: %{title} è in stato %{statusTooltip}"},"ja":{"click_for_more_details_on_this_outcome_8c018d2f":"クリックして、結果の更なる詳細をみる","n_count_alignments_a45bebe7":"*%{n_count}* アライメント","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"成果: %{title} の状況は %{statusTooltip} です"},"mi":{"click_for_more_details_on_this_outcome_8c018d2f":"pāwhiri mo te Whakapā mai He kōrero i runga i tēnei wāhanga","n_count_alignments_a45bebe7":"*%{n_count}* Ngā Tīaroaro","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Putanga: %{title} he mana o te %{statusTooltip}"},"nb":{"click_for_more_details_on_this_outcome_8c018d2f":"klikk for flere opplysninger om dette læringsmålet","n_count_alignments_a45bebe7":"*%{n_count}* Oppstillinger","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsmål: %{title} har status som %{statusTooltip}"},"nb-x-k12":{"click_for_more_details_on_this_outcome_8c018d2f":"klikk for flere opplysninger om dette målet","n_count_alignments_a45bebe7":"*%{n_count}* Oppstillinger","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Mål: %{title} har status som %{statusTooltip}"},"nl":{"click_for_more_details_on_this_outcome_8c018d2f":"klik voor meer informatie over dit resultaat","n_count_alignments_a45bebe7":"*%{n_count}* Verbindingen","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Resultaat: %{title}heeft een status van %{statusTooltip}"},"nn":{"click_for_more_details_on_this_outcome_8c018d2f":"klikk for meir informasjon om dette resultatet","n_count_alignments_a45bebe7":"*%{n_count}* justeringar","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Læringsmål: %{title} har status som %{statusTooltip}"},"pl":{"click_for_more_details_on_this_outcome_8c018d2f":"kliknij aby uzyskać więcej szczegółów na temat tego wyniku","n_count_alignments_a45bebe7":"*%{n_count}* Wyrównania","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Wynik: %{title} ma status %{statusTooltip}"},"pt":{"click_for_more_details_on_this_outcome_8c018d2f":"clique para mais detalhes sobre este assunto","n_count_alignments_a45bebe7":"*%{n_count}* Alinhamentos","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Resultado: %{title} tem um estado de %{statusTooltip}"},"pt-BR":{"click_for_more_details_on_this_outcome_8c018d2f":"clique para mais detalhes sobre esse objetivo","n_count_alignments_a45bebe7":"*%{n_count}* Alinhamentos","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Objetivo: %{title} tem um status de %{statusTooltip}"},"ru":{"click_for_more_details_on_this_outcome_8c018d2f":"нажмите для более подробной информации по этому результату","n_count_alignments_a45bebe7":"*%{n_count}* Выравнивание","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Результат: %{title} имеет статус %{statusTooltip}"},"sl":{"click_for_more_details_on_this_outcome_8c018d2f":"kliknite za več podrobnosti o tem izidu","n_count_alignments_a45bebe7":"*%{n_count}* Poravnave","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Izid: %{title} ima status %{statusTooltip}"},"sv":{"click_for_more_details_on_this_outcome_8c018d2f":"klicka för mer detaljer om det här lärandemålet","n_count_alignments_a45bebe7":"*%{n_count}* Justeringar","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Lärandemål: %{title} har statusen %{statusTooltip}"},"sv-x-k12":{"click_for_more_details_on_this_outcome_8c018d2f":"klicka för mer detaljer om det här lärandemålet","n_count_alignments_a45bebe7":"*%{n_count}* Justeringar","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Lärandemål: %{title} har statusen %{statusTooltip}"},"tr":{"click_for_more_details_on_this_outcome_8c018d2f":"Bu öğrenme çıktısı hakkında daha fazla ayrıntı için tıklayın","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Öğrenme çıktısı: %{title} nin durumu %{statusTooltip}"},"uk":{"click_for_more_details_on_this_outcome_8c018d2f":"натисніть для отримання більш докладної інформації з цього результату","n_count_alignments_a45bebe7":"*%{n_count}* Відповідності","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"Вихідний: %{title} має статус %{statusTooltip}"},"zh-Hans":{"click_for_more_details_on_this_outcome_8c018d2f":"点击此结果的详细信息","n_count_alignments_a45bebe7":"*%{n_count}* 对齐","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"结果：%{title} 的状态为 %{statusTooltip}"},"zh-Hant":{"click_for_more_details_on_this_outcome_8c018d2f":"按一下以了解關於此結果的更多詳情","n_count_alignments_a45bebe7":"*%{n_count}* 對齊","outcome_title_has_a_status_of_statustooltip_82d1e6d9":"成果：%{title} 的狀態為 %{statusTooltip}"}}'))
C["default"].scoped("grade_summary.outcome")
var me=N.a.default
var he=me.template,fe=me.templates=me.templates||{}
var ge="/work/canvas-deploy/generated/ui/features/grade-summary/jst/outcome"
fe[ge]=he((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i,o,_="",l=a.helperMissing,d=this.escapeExpression,c="function",u=this
function p(e,t){var s,n,r=""
r+="\n    "+d((s=a.addMasteryIcon||e&&e.addMasteryIcon,n={hash:{},data:t},s?s.call(e,e&&e.status,n):l.call(e,"addMasteryIcon",e&&e.status,n)))+"\n  "
return r}function m(e,t){return'\n    <i aria-hidden="true" class="icon-empty"></i>\n  '}function h(e,t){var s,n
if(n=a.hover_name)s=n.call(e,{hash:{},data:t})
else{n=e&&e.hover_name
s=typeof n===c?n.call(e,{hash:{},data:t}):n}return d(s)}function f(e,t){var s,n,r=""
r+='\n        <span class="screenreader-only">\n          '
if(n=a.hover_name)s=n.call(e,{hash:{},data:t})
else{n=e&&e.hover_name
s=typeof n===c?n.call(e,{hash:{},data:t}):n}r+=d(s)+"\n        </span>\n      "
return r}_+='<div class="outcome-icon '
if(i=a.status)r=i.call(t,{hash:{},data:n})
else{i=t&&t.status
r=typeof i===c?i.call(t,{hash:{},data:n}):i}_+=d(r)+'" data-tooltip="top" title="'
if(i=a.statusTooltip)r=i.call(t,{hash:{},data:n})
else{i=t&&t.statusTooltip
r=typeof i===c?i.call(t,{hash:{},data:n}):i}_+=d(r)+'">\n  <span class="screenreader-only">\n    '+d((i=a.t||t&&t.t,o={hash:{title:t&&t.title,statusTooltip:t&&t.statusTooltip,i18n_inferred_key:true},data:n},i?i.call(t,"outcome_title_has_a_status_of_statustooltip_82d1e6d9","Outcome: %{title} has a status of %{statusTooltip}",o):l.call(t,"t","outcome_title_has_a_status_of_statustooltip_82d1e6d9","Outcome: %{title} has a status of %{statusTooltip}",o)))+"\n  </span>\n  "
r=a["if"].call(t,t&&t.scoreDefined,{hash:{},inverse:u.program(3,m,n),fn:u.program(1,p,n),data:n});(r||0===r)&&(_+=r)
_+='\n</div>\n<div class="outcome-properties">\n  <div class="title" data-tooltip title="'
r=a["if"].call(t,t&&t.hover_name,{hash:{},inverse:u.noop,fn:u.program(5,h,n),data:n});(r||0===r)&&(_+=r)
_+='">\n    <h4>\n      '
if(i=a.friendly_name)r=i.call(t,{hash:{},data:n})
else{i=t&&t.friendly_name
r=typeof i===c?i.call(t,{hash:{},data:n}):i}_+=d(r)+"\n      "
r=a["if"].call(t,t&&t.hover_name,{hash:{},inverse:u.noop,fn:u.program(7,f,n),data:n});(r||0===r)&&(_+=r)
_+='\n    </h4>\n  </div>\n  <div class="description">'
if(i=a.description)r=i.call(t,{hash:{},data:n})
else{i=t&&t.description
r=typeof i===c?i.call(t,{hash:{},data:n}):i}(r||0===r)&&(_+=r)
_+='</div>\n</div>\n<div class="alignment-info">\n  <a href="#tab-outcomes/'
if(i=a.id)r=i.call(t,{hash:{},data:n})
else{i=t&&t.id
r=typeof i===c?i.call(t,{hash:{},data:n}):i}_+=d(r)+"\" role=\"button\" aria-haspopup='true' aria-controls='outcome_detail'>\n    "
r=(i=a.n||t&&t.n,o={hash:{},data:n},i?i.call(t,t&&t.count,o):l.call(t,"n",t&&t.count,o))
_+=d((i=a.t||t&&t.t,o={hash:{n_count:r,w0:"<strong>$1</strong>",i18n_inferred_key:true},data:n},i?i.call(t,"n_count_alignments_a45bebe7","*%{n_count}* Alignments",o):l.call(t,"t","n_count_alignments_a45bebe7","*%{n_count}* Alignments",o)))+'\n  </a>\n  <a class="more-details"\n    href="#"\n    role="button"\n    tabindex="0">\n    <i class="icon-more" role="presentation"></i>\n    <span class="screenreader-only">\n      '+d((i=a.t||t&&t.t,o={hash:{i18n_inferred_key:true},data:n},i?i.call(t,"click_for_more_details_on_this_outcome_8c018d2f","click for more details on this outcome",o):l.call(t,"t","click_for_more_details_on_this_outcome_8c018d2f","click for more details on this outcome",o)))+"\n    </span>\n  </a>\n</div>\n"
return _}))
var be=fe[ge]
var ye=function(e,t){for(var a in t)we.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},we={}.hasOwnProperty
var ve=function(e){ye(t,e)
function t(){return t.__super__.constructor.apply(this,arguments)}t.prototype.className="outcome"
t.prototype.events={"click .more-details":"show","keydown .more-details":"show"}
t.prototype.tagName="li"
t.prototype.template=be
t.prototype.initialize=function(){t.__super__.initialize.apply(this,arguments)
return this.progress=new G({model:this.model})}
t.prototype.afterRender=function(){this.popover=new _e({el:this.$(".more-details"),model:this.model})
return this.dialog=new pe({model:this.model})}
t.prototype.show=function(e){return this.dialog.show(e)}
t.prototype.toJSON=function(){var e
e=t.__super__.toJSON.apply(this,arguments)
return l.a.extend(e,{progress:this.progress})}
return t}(i.a.View)
Object(S["a"])(JSON.parse('{"ar":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"توسيع/طي مجموعة نواتج %{title}، يفي الطالب بمستوى إجادة %{mastery_count} من نواتج %{count}"},"ca":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Desplega/redueix el grup de resultats de %{title}, l\'assoliment del domini dels estudiants de %{mastery_count} de %{count} resultats"},"cy":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Ehangu/crebachu Grŵp Deilliannau %{title}, mae’r myfyriwr wedi cyrraedd y lefel meistroli %{mastery_count}, sef o %{count} deilliant"},"da":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Udvid/skjul %{title} resultatgruppe, studerende opfylder mestring af %{mastery_count} af %{count} læringsudbytter"},"da-x-k12":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Udvid/skjul %{title} resultatgruppe, elev opfylder mestring af %{mastery_count} af %{count} læringsudbytter"},"de":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"%{title}-Ergebnisgruppe erweitern/reduzieren, Student hat das Leistungsziel unter %{mastery_count} von %{count}-Ergebnisse erreicht"},"en-AU":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes"},"en-AU-x-unimelb":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes"},"en-CA":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes"},"en-GB":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes"},"en-GB-x-lbs":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes"},"en-GB-x-ukhe":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes"},"es":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expandir/colapsar %{title} Grupo de competencia, el estudiante cuenta con el dominio de %{mastery_count} de %{count} resultados"},"fa":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"باز کردن/ جمع کردن  گروه نتیجه %{title}، دانشجو تسلط %{mastery_count} از  %{count} نتیجه را دارد"},"fi":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Laajenna/kutista %{title} Osaamisen hallinnan tulosryhmä, opiskelijalla on hyvä osaaminen %{mastery_count} / %{count} tuloksesta"},"fr":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Développer/réduire le groupe d\'acquis %{title}, l\'élève atteint les critères de réussite pour %{mastery_count} sur %{count} résultats"},"fr-CA":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Agrandir/réduire le groupe d’acquis %{title}, l’étudiant atteint le niveau de Maîtrise de %{mastery_count} sur %{count} acquis"},"he":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"הסתרה/הרחבה של %{title} השגי למידה, התלמיד השיג מומחיות ב %{mastery_count} של  %{count} תוצאות"},"ht":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Elaji/redwi Rezilta Gwoup %{title} , elèv la rive jwenn metriz %{mastery_count} sou %{count} rezilta"},"is":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Stækka/fella %{title} níðurstöðuhóp, nemandi nær tileinkuninni %{mastery_count} af %{count} niðurstöðum"},"it":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Espandi/comprimi gruppo di esiti %{title}. Lo studente soddisfa la padronanza di %{mastery_count} di %{count} esiti"},"ja":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"%{title} 結果グループを展開/折りたたみする、受講生は%{count} 結果の%{mastery_count} の達成を満たす"},"mi":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Whakawhānui/ hinga %{title} Rōpū Putanga, ka tutaki te ākonga ki te hinganga ō %{mastery_count} o %{count} ngā putanga"},"nb":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Utvid/minsk %{title} læringsmålgruppen, studenten har oppnådd ekspertise i %{mastery_count} av %{count} læringsmål"},"nb-x-k12":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Utvid/minsk %{title} målgruppen, eleven har oppnådd ekspertise i %{mastery_count} av %{count} mål"},"nl":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Leerdoelgroep %{title} uit-/samenvouwen, cursist beheerst %{mastery_count} van %{count} leerdoelen"},"nn":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Vis/skjul %{title} læringsmålgruppe. Studenten oppnår meistring ved %{mastery_count} av %{count} læringsmål"},"pl":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Rozwiń/zwiń grupę wyników %{title}, uczestnik wykazuje opanowanie %{mastery_count} z %{count} wyników"},"pt":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expandir/encolher %{title} Grupo de resultados, o aluno atende ao domínio do %{mastery_count} dos %{count} resultados"},"pt-BR":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Expandir/recolher Grupo de objetivos de %{title}, aluno alcança o domínio de %{mastery_count} de %{count} objetivos"},"ru":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Развернуть/свернуть %{title} группу результатов, учащийся соответствует критериям владения по %{mastery_count} из %{count} результатов"},"sl":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Razširi/strni skupino izidov %{title}, študent dosega odličnost pri %{mastery_count} od %{count} učnih izidov."},"sv":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Ta fram/dölj %{title} lärandemålgrupp, studenten uppfyller målen på  %{mastery_count} av %{count} lärandemål"},"sv-x-k12":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Ta fram/dölj %{title} lärandemålgrupp, eleven uppfyller målen på  %{mastery_count} av %{count} lärandemål"},"uk":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"Розгорнути/згорнути %{title} Результат Групи, студент відповідає майстерності %{mastery_count} з результатами %{count}"},"zh-Hans":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"展开/折叠 %{title}结果小组，学生掌握了%{mastery_count}/%{count}个结果"},"zh-Hant":{"expand_collapse_title_outcome_group_student_meets__46ffef48":"展開/收起 %{title} 結果組合，學生可充分掌握 %{mastery_count} 結果組合 %{count}"}}'))
C["default"].scoped("grade_summary.group")
var ke=N.a.default
var xe=ke.template,Ae=ke.templates=ke.templates||{}
var je="/work/canvas-deploy/generated/ui/features/grade-summary/jst/group"
Ae[je]=xe((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i,o,_="",l="function",d=this.escapeExpression,c=a.helperMissing,u=this
function p(e,t){return'\n        class="has-mastery"\n      '}function m(e,t){return'\n        class="no-mastery"\n      '}_+='<div id="group_header-'
if(i=a.id)r=i.call(t,{hash:{},data:n})
else{i=t&&t.id
r=typeof i===l?i.call(t,{hash:{},data:n}):i}_+=d(r)+"\" class=\"group-description\" role='button' tabindex='0' aria-controls=\"group_detail-"
if(i=a.id)r=i.call(t,{hash:{},data:n})
else{i=t&&t.id
r=typeof i===l?i.call(t,{hash:{},data:n}):i}_+=d(r)+'" aria-expanded=\'false\'>\n  <label class="screenreader-only" for="group_header-'
if(i=a.id)r=i.call(t,{hash:{},data:n})
else{i=t&&t.id
r=typeof i===l?i.call(t,{hash:{},data:n}):i}_+=d(r)+'">\n    '+d((i=a.t||t&&t.t,o={hash:{title:t&&t.title,mastery_count:t&&t.mastery_count,count:t&&t.count,i18n_inferred_key:true},data:n},i?i.call(t,"expand_collapse_title_outcome_group_student_meets__46ffef48","Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes",o):c.call(t,"t","expand_collapse_title_outcome_group_student_meets__46ffef48","Expand/collapse %{title} Outcome Group, student meets mastery of %{mastery_count} of %{count} outcomes",o)))+'\n  </label>\n  <div class="outcome-icon">\n    <i class="icon-mini-arrow-down collapsed-arrow"></i>\n    <i class="icon-mini-arrow-up expanded-arrow"></i>\n  </div>\n  <h3 class="group-title" aria-hidden="true">'
if(i=a.title)r=i.call(t,{hash:{},data:n})
else{i=t&&t.title
r=typeof i===l?i.call(t,{hash:{},data:n}):i}_+=d(r)+'</h3>\n  <div class="group-status" aria-hidden="true">\n    <ul class="pill">\n      <li\n      '
r=a["if"].call(t,t&&t.mastery_count,{hash:{},inverse:u.program(3,m,n),fn:u.program(1,p,n),data:n});(r||0===r)&&(_+=r)
_+="\n        >"+d((i=a.n||t&&t.n,o={hash:{},data:n},i?i.call(t,t&&t.mastery_count,o):c.call(t,"n",t&&t.mastery_count,o)))+"</li>\n      <li>"+d((i=a.n||t&&t.n,o={hash:{},data:n},i?i.call(t,t&&t.count,o):c.call(t,"n",t&&t.count,o)))+'</li>\n    </ul>\n  </div>\n</div>\n<div id="group_detail-'
if(i=a.id)r=i.call(t,{hash:{},data:n})
else{i=t&&t.id
r=typeof i===l?i.call(t,{hash:{},data:n}):i}_+=d(r)+'" class="outcomes">\n</div>\n'
return _}))
var Ve=Ae[je]
var Ce=function(e,t){for(var a in t)Se.call(t,a)&&(e[a]=t[a])
function s(){this.constructor=e}s.prototype=t.prototype
e.prototype=new s
e.__super__=t.prototype
return e},Se={}.hasOwnProperty
var ze=function(e){Ce(t,e)
function t(){return t.__super__.constructor.apply(this,arguments)}t.prototype.tagName="li"
t.prototype.className="group"
t.prototype.els={".outcomes":"$outcomes"}
t.prototype.events={"click .group-description":"expand","keyclick .group-description":"expand"}
t.prototype.template=Ve
t.prototype.render=function(){var e
t.__super__.render.apply(this,arguments)
e=new V["a"]({el:this.$outcomes,collection:this.model.get("outcomes"),itemView:ve})
return e.render()}
t.prototype.expand=function(){var e,t
this.$el.toggleClass("expanded")
this.$el.hasClass("expanded")?this.$el.children("div.group-description").attr("aria-expanded","true"):this.$el.children("div.group-description").attr("aria-expanded","false")
e=$("div.outcome-toggles a.icon-collapse")
if(0===$("li.group.expanded").length){e.attr("disabled","disabled")
e.attr("aria-disabled","true")}else{e.removeAttr("disabled")
e.attr("aria-disabled","false")}t=$("div.outcome-toggles a.icon-expand")
if(0===$("li.group:not(.expanded)").length){t.attr("disabled","disabled")
return t.attr("aria-disabled","true")}t.removeAttr("disabled")
return t.attr("aria-disabled","false")}
t.prototype.statusTooltip=function(){switch(this.model.status()){case"undefined":return z.t("Unstarted")
case"remedial":return z.t("Well Below Mastery")
case"near":return z.t("Near Mastery")
case"mastery":return z.t("Meets Mastery")
case"exceeds":return z.t("Exceeds Mastery")}}
t.prototype.toJSON=function(){var e
e=t.__super__.toJSON.apply(this,arguments)
return l.a.extend(e,{statusTooltip:this.statusTooltip()})}
return t}(r["View"])
var Oe=N.a.default
var Ne=Oe.template,Pe=Oe.templates=Oe.templates||{}
var Ie="/work/canvas-deploy/generated/ui/features/grade-summary/jst/section"
Pe[Ie]=Ne((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i,o="",_="function",l=this.escapeExpression
o+="<h2>"
if(i=a.path)r=i.call(t,{hash:{},data:n})
else{i=t&&t.path
r=typeof i===_?i.call(t,{hash:{},data:n}):i}o+=l(r)+'</h2>\n<div class="groups">\n</div>\n'
return o}))
var Te=Pe[Ie]
class Ee extends r["View"]{static initClass(){this.prototype.tagName="li"
this.prototype.className="section"
this.prototype.els={".groups":"$groups"}
this.prototype.template=Te}render(){super.render(...arguments)
const e=new V["a"]({el:this.$groups,collection:this.model.get("groups"),itemView:ze})
return e.render()}}Ee.initClass()
var De=a("VTBJ")
var Le=N.a.default
var Me=Le.template,Ge=Le.templates=Le.templates||{}
var Be="/work/canvas-deploy/generated/ui/features/grade-summary/jst/alignment"
Ge[Be]=Me((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i,o,_="",l="function",d=this.escapeExpression,c=a.helperMissing
_+='<div class="title">'
if(i=a.alignment_name)r=i.call(t,{hash:{},data:n})
else{i=t&&t.alignment_name
r=typeof i===l?i.call(t,{hash:{},data:n}):i}_+=d(r)+'</div>\n<div class="outcome-bar-wrapper">\n  <div class="score"><strong>'+d((i=a.nf||t&&t.nf,o={hash:{format:"outcomeScore"},data:n},i?i.call(t,t&&t.roundedScore,o):c.call(t,"nf",t&&t.roundedScore,o)))+"</strong>/"+d((i=a.nf||t&&t.nf,o={hash:{format:"outcomeScore"},data:n},i?i.call(t,t&&t.mastery_points,o):c.call(t,"nf",t&&t.mastery_points,o)))+"</div>\n  "+d((i=a.view||t&&t.view,o={hash:{},data:n},i?i.call(t,t&&t.progress,o):c.call(t,"view",t&&t.progress,o)))+"\n</div>\n"
return _}))
var Re=Ge[Be]
class We extends i.a.View{static initClass(){this.prototype.tagName="li"
this.prototype.className="alignment"
this.prototype.template=Re}initialize(){super.initialize(...arguments)
return this.progress=new G({model:this.model})}toJSON(){const e=super.toJSON(...arguments)
return Object(De["a"])(Object(De["a"])({},e),{},{progress:this.progress})}}We.initClass()
var Ue=N.a.default
var Ke=Ue.template,He=Ue.templates=Ue.templates||{}
var $e="/work/canvas-deploy/generated/ui/features/grade-summary/jst/outcome-detail"
He[$e]=Ke((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
var r,i,o,_="",l="function",d=this.escapeExpression,c=a.helperMissing,u=this
function p(e,t){var s,n,r=""
r+=' data-tooltip title="'
if(n=a.hover_name)s=n.call(e,{hash:{},data:t})
else{n=e&&e.hover_name
s=typeof n===l?n.call(e,{hash:{},data:t}):n}r+=d(s)+'"'
return r}function m(e,t){var s,n
return d((s=a.nf||e&&e.nf,n={hash:{format:"outcomeScore"},data:t},s?s.call(e,e&&e.roundedScore,n):c.call(e,"nf",e&&e.roundedScore,n)))}function h(e,t){return"-"}_+='<div class="outcome-modal">\n  <div class="title"'
r=a["if"].call(t,t&&t.hover_name,{hash:{},inverse:u.noop,fn:u.program(1,p,n),data:n});(r||0===r)&&(_+=r)
_+=">\n    "
if(i=a.friendly_name)r=i.call(t,{hash:{},data:n})
else{i=t&&t.friendly_name
r=typeof i===l?i.call(t,{hash:{},data:n}):i}_+=d(r)+'\n  </div>\n  <div class="outcome-bar-wrapper">\n    <div class="score"><strong>'
r=a["if"].call(t,t&&t.scoreDefined,{hash:{},inverse:u.program(5,h,n),fn:u.program(3,m,n),data:n});(r||0===r)&&(_+=r)
_+="</strong>/"+d((i=a.nf||t&&t.nf,o={hash:{format:"outcomeScore"},data:n},i?i.call(t,t&&t.mastery_points,o):c.call(t,"nf",t&&t.mastery_points,o)))+"</div>\n    "+d((i=a.view||t&&t.view,o={hash:{},data:n},i?i.call(t,t&&t.progress,o):c.call(t,"view",t&&t.progress,o)))+'\n  </div>\n  <div class="description">'
if(i=a.description)r=i.call(t,{hash:{},data:n})
else{i=t&&t.description
r=typeof i===l?i.call(t,{hash:{},data:n}):i}(r||0===r)&&(_+=r)
_+='</div>\n\n  <div class="alignments"></div>\n</div>\n'
return _}))
var qe=He[$e]
class Fe extends le["a"]{static initClass(){this.prototype.template=qe}dialogOptions(){return{containerId:"outcome_detail",close:this.onClose,buttons:[],width:640}}initialize(){this.alignmentsForView=new i.a.Collection([])
this.alignmentsView=new V["a"]({collection:this.alignmentsForView,itemView:We})
return super.initialize(...arguments)}onClose(){return window.location.hash="tab-outcomes"}render(){super.render(...arguments)
this.alignmentsView.setElement(this.$(".alignments"))
this.allAlignments=new K([],{outcome:this.model})
this.allAlignments.on("fetched:last",()=>this.alignmentsForView.reset(this.allAlignments.toArray()))
return this.allAlignments.fetch()}show(e){this.model=e
this.$el.dialog("option","title",this.model.group.get("title")).css("maxHeight",340)
this.progress=new G({model:this.model})
this.render()
return super.show(...arguments)}toJSON(){const e=super.toJSON(...arguments)
return Object(De["a"])(Object(De["a"])({},e),{},{progress:this.progress})}}Fe.initClass()
class Je extends V["a"]{static initClass(){this.optionProperty("toggles")
this.prototype.itemView=Ee}initialize(){super.initialize(...arguments)
this.outcomeDetailView=new Fe
return this.bindToggles()}show(e){this.fetch()
if(!e)return this.outcomeDetailView.close()
{const t=parseInt(e)
const a=this.collection.outcomeCache.get(t)
if(a)return this.outcomeDetailView.show(a)}}fetch(){this.fetch=n.a.noop
return this.collection.fetch()}bindToggles(){const e=n()("div.outcome-toggles a.icon-collapse")
const t=n()("div.outcome-toggles a.icon-expand")
this.toggles.find(".icon-expand").click(()=>{this.$("li.group").addClass("expanded")
this.$("div.group-description").attr("aria-expanded","true")
t.attr("disabled","disabled")
t.attr("aria-disabled","true")
e.removeAttr("disabled")
e.attr("aria-disabled","false")
n()("div.groups").focus()})
return this.toggles.find(".icon-collapse").click(()=>{this.$("li.group").removeClass("expanded")
this.$("div.group-description").attr("aria-expanded","false")
e.attr("disabled","disabled")
e.attr("aria-disabled","true")
t.removeAttr("disabled")
return t.attr("aria-disabled","false")})}}Je.initClass()
var Ze=a("An8g")
var Ye=a("q1tI")
var Qe=a.n(Ye)
var Xe=a("i8i4")
var et=a.n(Xe)
var tt=a("m2A4")
var at=N.a.default
var st=at.template,nt=at.templates=at.templates||{}
var rt="/work/canvas-deploy/generated/ui/features/grade-summary/jst/individual-student-view"
nt[rt]=st((function(e,t,a,s,n){this.compilerInfo=[4,">= 1.0.0"]
a=this.merge(a,e.helpers)
n=n||{}
return'<div class="individualStudentView"></div>'}))
var it=nt[rt]
class ot extends i.a.View{static initClass(){this.optionProperty("course_id")
this.optionProperty("student_id")
this.prototype.template=it}initialize(){super.initialize(...arguments)
return this.bindToggles()}show(){super.show(...arguments)
this.render()
const e=Object(Ze["a"])(tt["a"],{courseId:this.course_id,studentId:this.student_id,onExpansionChange:this.updateToggles,outcomeProficiency:ENV.outcome_proficiency})
return this.reactView=et.a.render(e,n()(".individualStudentView").get(0))}updateToggles(e,t){const a=n()("div.outcome-toggles a.icon-collapse")
const s=n()("div.outcome-toggles a.icon-expand")
if(e){a.removeAttr("disabled")
a.attr("aria-disabled","false")}else{a.attr("disabled","disabled")
a.attr("aria-disabled","true")}if(t){s.removeAttr("disabled")
return s.attr("aria-disabled","false")}s.attr("disabled","disabled")
return s.attr("aria-disabled","true")}bindToggles(){const e=n()("div.outcome-toggles a.icon-collapse")
const t=n()("div.outcome-toggles a.icon-expand")
t.click(e=>{e.preventDefault()
return this.reactView.expand()})
return e.click(e=>{e.preventDefault()
return this.reactView.contract()})}}ot.initClass()
a("dhbk")
a("8JEM")
a("aq8L")
a("TvTI")
a("bbn0")
a("Dh7j")
var _t=a("BrAc")
var lt=a("Kr7l")
var dt=a("KNu5")
var ct=a("gI0r")
Object(S["a"])(JSON.parse('{"ar":{"assignment_details_collapsed_55b67150":"تم طي تفاصيل المهمة","assignment_details_expanded_20c6f0c7":"تم توسيع تفاصيل المهمة","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"بناءً على نقاط ماذا لو، فإن الدرجة الإجمالية الجديدة الآن هي %{grade}","click_to_test_a_different_score_2e0bb6f0":"انقر لاختبار نقطة مختلفة","enter_a_what_if_score_668c91a3":"إدخال درجة مفترضة.","grades_are_now_reverted_to_original_scores_e6deade1":"أُعيدت الدرجات إلى الدرجات الأصلية","grades_are_now_showing_what_if_scores_711a8be2":"تُعرض الآن درجات ماذا لو","hide_all_details_61d7ebb":"إخفاء كل التفاصيل","instructor_has_not_posted_this_grade_3b4c4124":"لم ينشر المعلم هذا التقدير","n_a_d6f6c42f":"لا يوجد","show_all_details_9879980c":"عرض جميع التفاصيل","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"تم حذف هذه المهمة ولن تؤخذ في الاعتبار أثناء الحساب الإجمالي","this_is_a_what_if_score_cbdc04f9":"هذا مجموع نقاط ماذا لو"},"ca":{"assignment_details_collapsed_55b67150":"detalls de la tasca reduïts","assignment_details_expanded_20c6f0c7":"detalls de la tasca desplegats","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Segons les puntuacions Què passaria si, el nou total és ara %{grade}","click_to_test_a_different_score_2e0bb6f0":"Feu clic per provar una altra puntuació","enter_a_what_if_score_668c91a3":"Introduïu una puntuació Què passaria si.","grades_are_now_reverted_to_original_scores_e6deade1":"Les qualificacions s\'han revertit a les puntuacions originals","grades_are_now_showing_what_if_scores_711a8be2":"Les qualificacions mostren les puntuacions Què passaria si","hide_all_details_61d7ebb":"Amaga tots els detalls","instructor_has_not_posted_this_grade_3b4c4124":"L’instructor no ha publicat aquesta qualificació","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Mostra tots els detalls","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Aquesta tasca s\'ha descartat i no es tindrà en compte al càlcul total","this_is_a_what_if_score_cbdc04f9":"Això és una puntuació Què passaria si"},"cy":{"assignment_details_collapsed_55b67150":"manylion aseiniad wedi’u crebachu","assignment_details_expanded_20c6f0c7":"manylion aseiniad wedi’u hehangu","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Ar sail y sgôr Beth-os, y cyfanswm newydd yw %{grade}","click_to_test_a_different_score_2e0bb6f0":"Cliciwch i brofi sgôr wahanol","enter_a_what_if_score_668c91a3":"Rhowch sgôr Beth-os.","grades_are_now_reverted_to_original_scores_e6deade1":"Mae’r graddau wedi’u newid yn ôl i’r sgoriau gwreiddiol","grades_are_now_showing_what_if_scores_711a8be2":"Mae graddau’n dangos y sgoriau Beth-os nawr","hide_all_details_61d7ebb":"Cuddio’r holl fanylion","instructor_has_not_posted_this_grade_3b4c4124":"Dydy’r addysgwr ddim wedi postio’r radd hon","n_a_d6f6c42f":"Amherthnasol","show_all_details_9879980c":"Dangos yr Holl Fanylion","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Mae’r aseiniad hwn wedi’i ollwng, ac ni fydd yn cael ei ystyried wrth gyfrifo’r cyfanswm","this_is_a_what_if_score_cbdc04f9":"Dyma’r sgôr Beth-os"},"da":{"assignment_details_collapsed_55b67150":"opgavedetaljer skjult","assignment_details_expanded_20c6f0c7":"opgavedetaljer udvidet","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Baseret på hvad-nu-hvis-resultater, bliver det nye samlede resultat %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klik for at teste et andet resultat","enter_a_what_if_score_668c91a3":"Indtast et Hvad-nu-hvis-resultat.","grades_are_now_reverted_to_original_scores_e6deade1":"Karakterer er nu vendt tilbage til originale resultater","grades_are_now_showing_what_if_scores_711a8be2":"Karakterer viser nu hvad-nu-hvis-resultat","hide_all_details_61d7ebb":"Skjul alle detaljer","instructor_has_not_posted_this_grade_3b4c4124":"Instruktøren har ikke slået denne karakter op","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Vis alle detaljer","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Opgaven blev droppet og tages ikke i betragtning i den samlede beregning","this_is_a_what_if_score_cbdc04f9":"Dette er et \'Hvad-nu-hvis-resultat\\""},"da-x-k12":{"assignment_details_collapsed_55b67150":"opgavedetaljer skjult","assignment_details_expanded_20c6f0c7":"opgavedetaljer udvidet","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Baseret på hvad-nu-hvis-resultater, bliver det nye samlede resultat %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klik for at teste et andet resultat","enter_a_what_if_score_668c91a3":"Indtast et Hvad-nu-hvis-resultat.","grades_are_now_reverted_to_original_scores_e6deade1":"Vurderinger er nu vendt tilbage til originale resultater","grades_are_now_showing_what_if_scores_711a8be2":"Vurderinger viser nu hvad-nu-hvis-resultat","hide_all_details_61d7ebb":"Skjul alle detaljer","instructor_has_not_posted_this_grade_3b4c4124":"Instruktøren har ikke slået denne vurdering op","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Vis alle detaljer","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Opgaven blev droppet og tages ikke i betragtning i den samlede beregning","this_is_a_what_if_score_cbdc04f9":"Dette er et \'Hvad-nu-hvis-resultat\\""},"de":{"assignment_details_collapsed_55b67150":"Aufgabendetails ausgeblendet","assignment_details_expanded_20c6f0c7":"Aufgabendetails eingeblendet","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Basierend auf hypothetischen Punkteständen ist die Gesamtsumme jetzt %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klicken, um andere Punktzahl zu testen","enter_a_what_if_score_668c91a3":"Geben Sie eine hypothetische Punktzahl ein.","grades_are_now_reverted_to_original_scores_e6deade1":"Die Noten werden jetzt auf die ursprüngliche Punktezahl zurückgesetzt.","grades_are_now_showing_what_if_scores_711a8be2":"Die Noten zeigen jetzt hypothetische Punktestände","hide_all_details_61d7ebb":"Alle Details ausblenden","instructor_has_not_posted_this_grade_3b4c4124":"Lehrer hat diese Note nicht veröffentlicht","n_a_d6f6c42f":"k. A.","show_all_details_9879980c":"Alle Details zeigen","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Diese Aufgabe wurde gestrichen und fließt nicht in die Gesamtpunktzahl ein.","this_is_a_what_if_score_cbdc04f9":"Dies ist ein hypothetischer Punktestand"},"el":{"hide_all_details_61d7ebb":"Απόκρυψη Όλων των Λεπτομερειών","n_a_d6f6c42f":"Δεν εφαρμόζεται","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Αυτή η εργασία δεν θα συνυπολογιστεί στην τελική βαθμολογία"},"en-AU":{"assignment_details_collapsed_55b67150":"assignment details collapsed","assignment_details_expanded_20c6f0c7":"assignment details expanded","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Based on What-If scores, the new total is now %{grade}","click_to_test_a_different_score_2e0bb6f0":"Click to test a different score","enter_a_what_if_score_668c91a3":"Enter a What-If score.","grades_are_now_reverted_to_original_scores_e6deade1":"Grades are now reverted to original scores.","grades_are_now_showing_what_if_scores_711a8be2":"Grades are now showing what-if scores.","hide_all_details_61d7ebb":"Hide All Details","instructor_has_not_posted_this_grade_3b4c4124":"Instructor has not posted this grade","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Show All Details","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"This assignment is dropped and will not be considered in the total calculation.","this_is_a_what_if_score_cbdc04f9":"This is a What-If score"},"en-AU-x-unimelb":{"assignment_details_collapsed_55b67150":"assignment details collapsed","assignment_details_expanded_20c6f0c7":"assignment details expanded","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Based on What-If scores, the new total is now %{grade}","click_to_test_a_different_score_2e0bb6f0":"Click to test a different score","enter_a_what_if_score_668c91a3":"Enter a What-If score.","grades_are_now_reverted_to_original_scores_e6deade1":"Grades are now reverted to original scores.","grades_are_now_showing_what_if_scores_711a8be2":"Grades are now showing what-if scores.","hide_all_details_61d7ebb":"Hide All Details","instructor_has_not_posted_this_grade_3b4c4124":"Instructor has not posted this grade","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Show All Details","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"This assignment is dropped and will not be considered in the total calculation.","this_is_a_what_if_score_cbdc04f9":"This is a What-If score"},"en-CA":{"assignment_details_collapsed_55b67150":"assignment details collapsed","assignment_details_expanded_20c6f0c7":"assignment details expanded","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Based on What-If scores, the new total is now %{grade}","click_to_test_a_different_score_2e0bb6f0":"Click to test a different score","enter_a_what_if_score_668c91a3":"Enter a What-If score.","grades_are_now_reverted_to_original_scores_e6deade1":"Grades are now reverted to original scores","grades_are_now_showing_what_if_scores_711a8be2":"Grades are now showing what-if scores","hide_all_details_61d7ebb":"Hide All Details","instructor_has_not_posted_this_grade_3b4c4124":"Instructor has not posted this grade","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Show All Details","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"This assignment is dropped and will not be considered in the total calculation","this_is_a_what_if_score_cbdc04f9":"This is a What-If score"},"en-GB":{"assignment_details_collapsed_55b67150":"assignment details collapsed","assignment_details_expanded_20c6f0c7":"assignment details expanded","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Based on What-if scores, the new total is now %{grade}","click_to_test_a_different_score_2e0bb6f0":"Click to test a different score","enter_a_what_if_score_668c91a3":"Enter a What-if score.","grades_are_now_reverted_to_original_scores_e6deade1":"Grades are now reverted to original scores","grades_are_now_showing_what_if_scores_711a8be2":"Grades are now showing what-if scores","hide_all_details_61d7ebb":"Hide all details","instructor_has_not_posted_this_grade_3b4c4124":"Instructor has not posted this grade","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Show all details","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"This assignment has been dropped and will not be considered in the total calculation","this_is_a_what_if_score_cbdc04f9":"This is a what-if score"},"en-GB-x-lbs":{"assignment_details_collapsed_55b67150":"task details collapsed","assignment_details_expanded_20c6f0c7":"task details expanded","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Based on What-if scores, the new total is now %{grade}","click_to_test_a_different_score_2e0bb6f0":"Click to test a different score","enter_a_what_if_score_668c91a3":"Enter a What-if score.","grades_are_now_reverted_to_original_scores_e6deade1":"Grades are now reverted to original scores","grades_are_now_showing_what_if_scores_711a8be2":"Grades are now showing what-if scores","hide_all_details_61d7ebb":"Hide all details","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Show all details","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"This task has been dropped and will not be considered in the total calculation","this_is_a_what_if_score_cbdc04f9":"This is a what-if score"},"en-GB-x-ukhe":{"assignment_details_collapsed_55b67150":"assignment details collapsed","assignment_details_expanded_20c6f0c7":"assignment details expanded","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Based on What-if scores, the new total is now %{grade}","click_to_test_a_different_score_2e0bb6f0":"Click to test a different score","enter_a_what_if_score_668c91a3":"Enter a What-if score.","grades_are_now_reverted_to_original_scores_e6deade1":"Grades are now reverted to original scores","grades_are_now_showing_what_if_scores_711a8be2":"Grades are now showing what-if scores","hide_all_details_61d7ebb":"Hide all details","instructor_has_not_posted_this_grade_3b4c4124":"Instructor has not posted this grade","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Show all details","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"This assignment has been dropped and will not be considered in the total calculation","this_is_a_what_if_score_cbdc04f9":"This is a what-if score"},"es":{"assignment_details_collapsed_55b67150":"detalles de tarea colapsados","assignment_details_expanded_20c6f0c7":"detalles de tarea expandidos","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Según las puntuaciones \\"qué pasa si\\", ahora el total nuevo es %{grade}","click_to_test_a_different_score_2e0bb6f0":"Haga clic para probar un puntaje diferente","enter_a_what_if_score_668c91a3":"Ingresar un nuevo puntaje hipotético","grades_are_now_reverted_to_original_scores_e6deade1":"Las calificaciones han sido revertidas ahora a sus puntuaciones originales","grades_are_now_showing_what_if_scores_711a8be2":"Las calificaciones ahora muestran las puntuaciones \\"Que pasaría si\\"","hide_all_details_61d7ebb":"Ocultar todos los detalles","instructor_has_not_posted_this_grade_3b4c4124":"El profesor no ha publicado esta calificación","n_a_d6f6c42f":"N. a.","show_all_details_9879980c":"Mostrar todos los detalles","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Esta tarea está cancelada y no será considerada en el cálculo total","this_is_a_what_if_score_cbdc04f9":"Esta es un puntaje \\"que pasa si\\""},"fa":{"assignment_details_collapsed_55b67150":"اطلاعات تکلیف باز شد","assignment_details_expanded_20c6f0c7":"جزئیات تکلیف گسترش یافته است","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"بر اساس نمره های احتمالی، مجموع جدید اکنون %{grade} است","click_to_test_a_different_score_2e0bb6f0":"برای آزمایش نمره دیگر، کلیک کنید","enter_a_what_if_score_668c91a3":"یک نمره احتمالی وارد کنید.","grades_are_now_reverted_to_original_scores_e6deade1":"نمره ها اکنون به نمره های اصلی تبدیل شده اند","grades_are_now_showing_what_if_scores_711a8be2":"نمره ها اکنون نمره های احتمالی را نشان می دهند","hide_all_details_61d7ebb":"مخفی کردن همه جزئیات","n_a_d6f6c42f":"موجود نیست","show_all_details_9879980c":"نمایش همه اطلاعات","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"این تکلیف حذف شده است و در محاسبه کل در نظر گرفته نخواهد شد","this_is_a_what_if_score_cbdc04f9":"این یک نمره احتمالی است"},"fi":{"assignment_details_collapsed_55b67150":"Tehtävän tiedot kutistettu","assignment_details_expanded_20c6f0c7":"Tehtävän tiedot laajennettu","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Mitä jos -pistemäärien perusteella uusi kokonaismäärä on nyt %{grade}","click_to_test_a_different_score_2e0bb6f0":"Testaa eri pistemääriä napsauttamalla","enter_a_what_if_score_668c91a3":"Syötä Mitä jos -pistemäärä.","grades_are_now_reverted_to_original_scores_e6deade1":"Arvosanat on nyt palautettu alkuperäisiin pistemääriin","grades_are_now_showing_what_if_scores_711a8be2":"Arvosanat näyttävät nyt mitä jos -pistemäärät","hide_all_details_61d7ebb":"Piilota kaikki tiedot","instructor_has_not_posted_this_grade_3b4c4124":"Ohjaaja ei ole lähettänyt tätä arvosanaa","n_a_d6f6c42f":"Ei sovellettavissa","show_all_details_9879980c":"Näytä kaikki tiedot","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Tämä tehtävä on pudotettu eikä sitä oteta huomioon summan laskelmassa","this_is_a_what_if_score_cbdc04f9":"Tämä on Mitä jos -pistemäärä"},"fr":{"assignment_details_collapsed_55b67150":"détails de la tâche réduits","assignment_details_expanded_20c6f0c7":"détails de la tâche étendus","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Basé sur les scores What-if, le nouveau total est désormais de %{grade}","click_to_test_a_different_score_2e0bb6f0":"Cliquer pour tester une note différente","enter_a_what_if_score_668c91a3":"Entrez un score conditionnel.","grades_are_now_reverted_to_original_scores_e6deade1":"Les notes sont maintenant rétablies sur les scores d\'origine","grades_are_now_showing_what_if_scores_711a8be2":"Les notes montrent maintenant des scores hypothétiques","hide_all_details_61d7ebb":"Masquer tous les détails","instructor_has_not_posted_this_grade_3b4c4124":"L\'instructeur n\'a pas publié cette note","n_a_d6f6c42f":"NA","show_all_details_9879980c":"Montrer tous les détails","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Ce devoir est abandonné et ne sera pas pris en compte dans le calcul du total","this_is_a_what_if_score_cbdc04f9":"Ceci est une note hypothétique"},"fr-CA":{"assignment_details_collapsed_55b67150":"détails de la tâche minimisés","assignment_details_expanded_20c6f0c7":"détails de la tâche maximisés","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Selon les scores « What If », le nouveau total est maintenant %{grade}","click_to_test_a_different_score_2e0bb6f0":"Cliquer pour tester une note différente","enter_a_what_if_score_668c91a3":"Saisir un score de simulation What If.","grades_are_now_reverted_to_original_scores_e6deade1":"Les notes sont maintenant rétablies sur les scores d\'origine","grades_are_now_showing_what_if_scores_711a8be2":"Les notes montrent maintenant des scores hypothétiques","hide_all_details_61d7ebb":"Masquer tous les détails","instructor_has_not_posted_this_grade_3b4c4124":"L’instructeur n’a pas publié cette note","n_a_d6f6c42f":"N/D","show_all_details_9879980c":"Afficher tous les détails","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Cette tâche est abandonné et ne sera pas pris en compte dans le calcul du total","this_is_a_what_if_score_cbdc04f9":"Ceci est une note hypothétique (What If)"},"he":{"assignment_details_collapsed_55b67150":"פרטי מטלה הוסתרו","assignment_details_expanded_20c6f0c7":"פרטי מטלה נגלו","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"בהתבסס על ציוני  What-If , הסיכום החדש הוא כעת %{grade}","click_to_test_a_different_score_2e0bb6f0":"הקש/י כדי לבחון ציון שונה","enter_a_what_if_score_668c91a3":"הקלדת ציון ל\\"מה יקרה אם\\"","grades_are_now_reverted_to_original_scores_e6deade1":"הציונים כעת חזרו לניקוד המקורי","grades_are_now_showing_what_if_scores_711a8be2":"ההערכות מראות כעת ציוני מה-היה-אילו","hide_all_details_61d7ebb":"הסתרת כל הפרטים","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"הצגת כל הפרטים","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"משימה זו הושמטה ולא תחשב בתחשיב הכולל.","this_is_a_what_if_score_cbdc04f9":"זהו ציון של \\"מה יקרה אם...\\""},"ht":{"assignment_details_collapsed_55b67150":"detay sesyon efondre","assignment_details_expanded_20c6f0c7":"detay sesyon elaji","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Daprè What-If scores, kounye a nouvo total la se %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klike pou teste yon lòt nòt","enter_a_what_if_score_668c91a3":"Antre yon What-If score.","grades_are_now_reverted_to_original_scores_e6deade1":"Nòt yo retounen nan patisyon orijinal yo kounye a","grades_are_now_showing_what_if_scores_711a8be2":"Kounye a kategori yo afiche kisa-si nòt yo","hide_all_details_61d7ebb":"Kache tout Detay","instructor_has_not_posted_this_grade_3b4c4124":"Enstriktè a pa afiche nòt sa a","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Afiche tout Detay","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Yo abandone sesyon sa a ni yo pa p konsidere li nan kalkil total la.","this_is_a_what_if_score_cbdc04f9":"Sa se yon Kisa-Si nòt"},"hu":{"assignment_details_collapsed_55b67150":"feladat részletei elrejtve","assignment_details_expanded_20c6f0c7":"feladat részletei megjelenítve","grades_are_now_reverted_to_original_scores_e6deade1":"Az értékelések vissza lettek állítva az eredeti értékekre","grades_are_now_showing_what_if_scores_711a8be2":"Az értékelések most a \\"Mi lenne, ha?\\" állapotot mutatják","hide_all_details_61d7ebb":"Minden részlet elrejtése","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Minden részlet megjelenítése ","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Ez a feladat el lett dobva, nem lesz figyelembe véve a teljes számításban","this_is_a_what_if_score_cbdc04f9":"Ez csak egy \\"Mi lenne ha?\\" pontszám"},"hy":{"n_a_d6f6c42f":"Չկա","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Այս հանձնարարությունը բաց է թողնվել և հաշվի չի առնվելու ընդհանուր գնահատականը հաշվարկելիս"},"is":{"assignment_details_collapsed_55b67150":"upplýsingar um verkefni felldar","assignment_details_expanded_20c6f0c7":"upplýsingar um verkefni stækkaðar","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Á grunni spáðra einkunna, er nýja samtalan %{grade}","click_to_test_a_different_score_2e0bb6f0":"Smelltu hér til að prófa aðra einkunn","enter_a_what_if_score_668c91a3":"Færa inn spáða einkunn.","grades_are_now_reverted_to_original_scores_e6deade1":"Einkunnum er nú breytt aftur í upprunaleg stig","grades_are_now_showing_what_if_scores_711a8be2":"Einkunnir sýna nú spáða einkunn","hide_all_details_61d7ebb":"Fela allar upplýsingar","instructor_has_not_posted_this_grade_3b4c4124":"Kennari hefur ekki birt þessa einkunn","n_a_d6f6c42f":"Á ekki við","show_all_details_9879980c":"Sýna allar upplýsingar","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Þetta verkefni er fellt niður og verður ekki með í heildarútreikningum","this_is_a_what_if_score_cbdc04f9":"Þetta er spáð einkunn"},"it":{"assignment_details_collapsed_55b67150":"dettagli compito compressi","assignment_details_expanded_20c6f0c7":"dettagli compiti espansi","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"In base ai punteggi What-If, il nuovo totale è ora %{grade}","click_to_test_a_different_score_2e0bb6f0":"Fai clic per testare un punteggio diverso","enter_a_what_if_score_668c91a3":"Inserisci punteggio What-If.","grades_are_now_reverted_to_original_scores_e6deade1":"I voti sono ora ripristinati ai punteggi originali","grades_are_now_showing_what_if_scores_711a8be2":"I voti al momento mostrano punteggi What-If","hide_all_details_61d7ebb":"Nascondi tutti i dettagli","instructor_has_not_posted_this_grade_3b4c4124":"L’istruttore non ha pubblicato questo voto","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Mostra tutti i dettagli","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Questo compito è stato eliminato e non verrà considerato nel calcolo totale","this_is_a_what_if_score_cbdc04f9":"Questo è un punteggio What-If"},"ja":{"assignment_details_collapsed_55b67150":"課題の詳細が表示されない","assignment_details_expanded_20c6f0c7":"課題詳細が拡張されました","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"What-If スコアに基づいた、新しい合計は現在%{grade}","click_to_test_a_different_score_2e0bb6f0":"クリックして、別のスコアをテスト","enter_a_what_if_score_668c91a3":"仮説スコアを入力します。","grades_are_now_reverted_to_original_scores_e6deade1":"成績は現在、元のスコアに戻っています","grades_are_now_showing_what_if_scores_711a8be2":"成績は現在、仮のスコアを表示しています","hide_all_details_61d7ebb":"詳細をすべて非表示","instructor_has_not_posted_this_grade_3b4c4124":"講師はこの成績を提出していません","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"詳細をすべて表示","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"この課題は削除されました。合計の計算で考慮されません。","this_is_a_what_if_score_cbdc04f9":"これは what-if (仮の事態を想定した) スコアです"},"ko":{"hide_all_details_61d7ebb":"모든 세부 정보 숨기기","n_a_d6f6c42f":"해당 없음","show_all_details_9879980c":"모든 세부 정보 표시하기"},"mi":{"assignment_details_collapsed_55b67150":"I hinga ngā taipitopito whakataunga","assignment_details_expanded_20c6f0c7":"I whakanuia ngā taipitopito whakataunga","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"I runga i te Aha ki te kaute, koinei te katoa hou %{grade}","click_to_test_a_different_score_2e0bb6f0":"Pāwhiritia ki te whakamātau i te kaute rerekē","enter_a_what_if_score_668c91a3":"Whakauru he Ki te aha kaute","grades_are_now_reverted_to_original_scores_e6deade1":"Na e hoki ngā kōeke ki kaute taketake","grades_are_now_showing_what_if_scores_711a8be2":"Kei te whakātu ngā kōeke ināianei te mea-ki te kaute","hide_all_details_61d7ebb":"Huna ngā Taipitopito katoa","instructor_has_not_posted_this_grade_3b4c4124":"Kaore te Kaitohutohu i whakairi i tēnei kōeke","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Whakāturia Ngā Taipitopito Katoa","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Maturuturu iho ana tēnei whakataunga te a kore e whakaarohia i roto i te tātai katoa","this_is_a_what_if_score_cbdc04f9":"Ko te kaute Aha-Ki tēnei"},"nb":{"assignment_details_collapsed_55b67150":"oppgavedetaljer sammentrukket","assignment_details_expanded_20c6f0c7":"oppgavedetaljer utvidet","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Basert på What-If-resultater, er den nye totalen nå %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klikk for å teste et annet resultat","enter_a_what_if_score_668c91a3":"Skriv inn en Hva-om-vurdering.","grades_are_now_reverted_to_original_scores_e6deade1":"Karakterene er nå satt tilbake til opprinnelig resultat","grades_are_now_showing_what_if_scores_711a8be2":"Karakterene viser nå hva-hvis resultat","hide_all_details_61d7ebb":"Skjul alle detaljer","instructor_has_not_posted_this_grade_3b4c4124":"Instruktøren har ikke publisert denne karakteren","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Vis alle detaljer","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Denne oppgaven er droppet og blir ikke tatt med i beregningen","this_is_a_what_if_score_cbdc04f9":"Dette er et What-if-resultat"},"nb-x-k12":{"assignment_details_collapsed_55b67150":"oppgavedetaljer sammentrukket","assignment_details_expanded_20c6f0c7":"oppgavedetaljer utvidet","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Basert på What-If-resultater, er den nye totalen nå %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klikk for å teste et annet resultat","enter_a_what_if_score_668c91a3":"Skriv inn et Hva-om-resultat.","grades_are_now_reverted_to_original_scores_e6deade1":"Vurderingene er nå satt tilbake til opprinnelig resultat","grades_are_now_showing_what_if_scores_711a8be2":"Vurderingene viser nå hva-hvis resultat","hide_all_details_61d7ebb":"Skjul alle detaljer","instructor_has_not_posted_this_grade_3b4c4124":"Instruktøren har ikke publisert denne vurderingen","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Vis alle detaljer","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Denne oppgaven er droppet og blir ikke tatt med i beregningen","this_is_a_what_if_score_cbdc04f9":"Dette er et What-if-resultat"},"nl":{"assignment_details_collapsed_55b67150":"opdrachtdetails samengevouwen","assignment_details_expanded_20c6f0c7":"opdrachtdetails uitgevouwen","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Op basis van wat-als-scores is het nieuwe totaal nu %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klikken om een andere score te testen","enter_a_what_if_score_668c91a3":"Voer een wat-als-score in.","grades_are_now_reverted_to_original_scores_e6deade1":"Cijfers zijn nu teruggekeerd naar de oorspronkelijke scores","grades_are_now_showing_what_if_scores_711a8be2":"Cijfers tonen nu stel-dat scores","hide_all_details_61d7ebb":"Alle artefacten verbergen","instructor_has_not_posted_this_grade_3b4c4124":"Cursusleider heeft dit cijfer niet gepost","n_a_d6f6c42f":"Niet van toepassing","show_all_details_9879980c":"Alle details tonen","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Deze opdracht is vervallen en wordt niet in overweging genomen voor de totale berekening","this_is_a_what_if_score_cbdc04f9":"Dit is een wat-als-score"},"nn":{"assignment_details_collapsed_55b67150":"oppgåvedetaljar er skjult","assignment_details_expanded_20c6f0c7":"oppgåvedetaljar viser","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Basert på kva-skjer-dersom-resultata, er den nye totalen no %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klikk for å teste eit anna resultat","enter_a_what_if_score_668c91a3":"Oppgi eit kva-skjer-dersom-resultat.","grades_are_now_reverted_to_original_scores_e6deade1":"Vurderingane blir no tilbakestilt til det opphavlege resultatet","grades_are_now_showing_what_if_scores_711a8be2":"Vurderingane viser no kva-skjer-dersom-resultat","hide_all_details_61d7ebb":"Skjul alle detaljar","n_a_d6f6c42f":"Gjeld ikkje","show_all_details_9879980c":"Vis alle detaljar","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Oppgåva er fjerna og er ikkje med i den totale utrekninga","this_is_a_what_if_score_cbdc04f9":"Dette er kva-skjer-dersom-resultat"},"pl":{"assignment_details_collapsed_55b67150":"zwinięto szczegóły materiałów","assignment_details_expanded_20c6f0c7":"rozwinięto szczegóły materiałów","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"W oparciu o wyniki teoretyczne nowa suma to %{grade}","click_to_test_a_different_score_2e0bb6f0":"Kliknij, aby sprawdzić inny wynik punktowy","enter_a_what_if_score_668c91a3":"Wprowadź Prawdopodobny wynik.","grades_are_now_reverted_to_original_scores_e6deade1":"Oceny już powróciły do oryginalnego wyniku punktowego","grades_are_now_showing_what_if_scores_711a8be2":"Oceny teraz wykazują prawdopodobne wyniki punktowe","hide_all_details_61d7ebb":"Ukryj wszystkie informacje szczegółowe","instructor_has_not_posted_this_grade_3b4c4124":"Instruktor nie opublikował tej oceny","n_a_d6f6c42f":"Brak danych","show_all_details_9879980c":"Pokaż wszystkie informacje szczegółowe","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"To zadanie zostało opuszczone i nie zostanie wliczone w ostateczny wynik","this_is_a_what_if_score_cbdc04f9":"To wynik hipotetyczny"},"pt":{"assignment_details_collapsed_55b67150":"Detalhes da atribuição recolhidos","assignment_details_expanded_20c6f0c7":"Detalhes de atribuição expandidos","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Com base nas pontuações What-If, o novo total está agora %{grade}","click_to_test_a_different_score_2e0bb6f0":"Clique para testar uma pontuação diferente","enter_a_what_if_score_668c91a3":"Inserir pontuação de Hipóteses","grades_are_now_reverted_to_original_scores_e6deade1":"As notas foram revertidas para as classificações originais","grades_are_now_showing_what_if_scores_711a8be2":"As notas apresentam potenciais classiicações","hide_all_details_61d7ebb":"Ocultar todos os detalhes","instructor_has_not_posted_this_grade_3b4c4124":"O instrutor não afixou esta nota","n_a_d6f6c42f":"ND","show_all_details_9879980c":"Exibir todos os detalhes","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Esta tarefa foi cancelada e não será considerada no cálculo total","this_is_a_what_if_score_cbdc04f9":"Esta é uma pontuação \\"e se\\""},"pt-BR":{"assignment_details_collapsed_55b67150":"detalhes da tarefa recolhidos","assignment_details_expanded_20c6f0c7":"detalhes da tarefa ampliados","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Baseado em previsões de pontuações, o novo total agora é %{grade}","click_to_test_a_different_score_2e0bb6f0":"Clique para testar uma pontuação diferente","enter_a_what_if_score_668c91a3":"Insira uma previsão de pontuação.","grades_are_now_reverted_to_original_scores_e6deade1":"Notas está agora revertida para a pontuação original","grades_are_now_showing_what_if_scores_711a8be2":"Notas está agora mostrando a previsão de pontuação","hide_all_details_61d7ebb":"Ocultar todos os detalhes","instructor_has_not_posted_this_grade_3b4c4124":"O instrutor não postou esta nota","n_a_d6f6c42f":"N/A","show_all_details_9879980c":"Exibir todos os detalhes","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Esta tarefa foi excluída e não fará parte do cálculo total","this_is_a_what_if_score_cbdc04f9":"Esta é uma previsão de pontuação"},"ru":{"assignment_details_collapsed_55b67150":"подробности задания свернуты","assignment_details_expanded_20c6f0c7":"подробности задания развернуты","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Судя по оценкам в режиме «что, если», новый итог в настоящее время %{grade}","click_to_test_a_different_score_2e0bb6f0":"Щелкните для проверки другой оценки","enter_a_what_if_score_668c91a3":"Введите оценку «что, если».","grades_are_now_reverted_to_original_scores_e6deade1":"Оценки были возвращены к первоначальным баллам","grades_are_now_showing_what_if_scores_711a8be2":"Оценки теперь показывают моделируемые баллы","hide_all_details_61d7ebb":"Скрыть все сведения","instructor_has_not_posted_this_grade_3b4c4124":"Инструктор не отправил эту отметку","n_a_d6f6c42f":"Н/Д","show_all_details_9879980c":"Показать все сведения","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Это задание было пропущено и не будет учитываться в итоговом подсчете","this_is_a_what_if_score_cbdc04f9":"Это оценка в режиме «что, если»"},"sl":{"assignment_details_collapsed_55b67150":"strnjene podrobnosti naloge","assignment_details_expanded_20c6f0c7":"razširjene podrobnosti naloge","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Na podlagi simulacije rezultatov je nova vsota zdaj %{grade}","click_to_test_a_different_score_2e0bb6f0":"Kliknite, da preizkusite drug rezultat.","enter_a_what_if_score_668c91a3":"Vnesite simulacijo rezultatov","grades_are_now_reverted_to_original_scores_e6deade1":"Ocene so zdaj povrnjene na izvirne rezultate.","grades_are_now_showing_what_if_scores_711a8be2":"Ocene zdaj prikazujejo simulacijo rezultatov","hide_all_details_61d7ebb":"Skrij vse podrobnosti","n_a_d6f6c42f":"Ni na voljo.","show_all_details_9879980c":"Prikaži vse podrobnosti","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Ta naloga je izpuščena in ne bo upoštevana v končnem izračunu.","this_is_a_what_if_score_cbdc04f9":"To je simulirani rezultat"},"sv":{"assignment_details_collapsed_55b67150":"uppgiftsdetaljer stängda","assignment_details_expanded_20c6f0c7":"uppgiftsdetaljer expanderade","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Baserad på tänk om-resultat, är den nya totalsumman nu %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klicka för att testa ett annat resultat","enter_a_what_if_score_668c91a3":"Skriv in tänk om-resultat.","grades_are_now_reverted_to_original_scores_e6deade1":"Omdömen har nu återgått till ursprungliga resultat","grades_are_now_showing_what_if_scores_711a8be2":"Omdömen visar nu tänk om-resultat","hide_all_details_61d7ebb":"Dölj alla detaljer","instructor_has_not_posted_this_grade_3b4c4124":"Instruktören har inte publicerat det här omdömet","n_a_d6f6c42f":"Ej tillämpbart","show_all_details_9879980c":"Visa alla detaljer","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Den här uppgiften har utelämnats och kommer inte att räknas in i den totala uträkningen.","this_is_a_what_if_score_cbdc04f9":"Det här är ett tänk om-resultat"},"sv-x-k12":{"assignment_details_collapsed_55b67150":"uppgiftsdetaljer stängda","assignment_details_expanded_20c6f0c7":"uppgiftsdetaljer expanderade","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"Baserad på tänk om-resultat, är den nya totalsumman nu %{grade}","click_to_test_a_different_score_2e0bb6f0":"Klicka för att testa ett annat resultat","enter_a_what_if_score_668c91a3":"Skriv in tänk om-resultat.","grades_are_now_reverted_to_original_scores_e6deade1":"Omdömen har nu återgått till ursprungliga resultat","grades_are_now_showing_what_if_scores_711a8be2":"Omdömen visar nu tänk om-resultat","hide_all_details_61d7ebb":"Dölj alla detaljer","instructor_has_not_posted_this_grade_3b4c4124":"Instruktören har inte publicerat det här bedömning","n_a_d6f6c42f":"Ej tillämpbart","show_all_details_9879980c":"Visa alla detaljer","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Den här uppgiften har utelämnats och kommer inte att räknas in i den totala uträkningen.","this_is_a_what_if_score_cbdc04f9":"Det här är ett tänk om-resultat"},"tr":{"assignment_details_collapsed_55b67150":"ödev ayrıntılıları daraltıldı","assignment_details_expanded_20c6f0c7":"ödev ayrıntıları genişletildi","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"... olsaydı ne olurdu puanlarına bağlı olarak yeni toplam artık %{grade}","click_to_test_a_different_score_2e0bb6f0":"Farklı bir puanı test etmek için tıklayın","hide_all_details_61d7ebb":"Tüm Ayrıntıları Gizle","n_a_d6f6c42f":"Yok","show_all_details_9879980c":"Tüm Ayrıntıları Göster"},"uk":{"assignment_details_collapsed_55b67150":"Деталі завдання зникли","assignment_details_expanded_20c6f0c7":"Деталі завдання розширено","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"На основі балів що-якщо, новий підсумок зараз становить %{grade}","click_to_test_a_different_score_2e0bb6f0":"Натисніть, щоб перевірити різні оцінки","enter_a_what_if_score_668c91a3":"Введіть оцінку Що-якщо ","grades_are_now_reverted_to_original_scores_e6deade1":"Оцінки зараз повернуті до оригінальних балів","grades_are_now_showing_what_if_scores_711a8be2":"Системи оцінювання зараз відображають можливі бали","hide_all_details_61d7ebb":"Сховати всі подробиці","n_a_d6f6c42f":"Недоступно","show_all_details_9879980c":"Показати всі деталі","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"Це завдання відхилено і не буде розглядатися в загальному підрахунку","this_is_a_what_if_score_cbdc04f9":"Це оцінка типу \\"Що-якщо\\""},"zh-Hans":{"assignment_details_collapsed_55b67150":"作业详情已折叠","assignment_details_expanded_20c6f0c7":"作业详情已展开","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"基于假设分数，新总分现在为%{grade}","click_to_test_a_different_score_2e0bb6f0":"单击以测试不同分数","enter_a_what_if_score_668c91a3":"输入假设分数。","grades_are_now_reverted_to_original_scores_e6deade1":"成绩现在已还原为原始得分","grades_are_now_showing_what_if_scores_711a8be2":"成绩现在显示为假设的得分","hide_all_details_61d7ebb":"隐藏所有详细信息","instructor_has_not_posted_this_grade_3b4c4124":"讲师未发布此评分","n_a_d6f6c42f":"不适用","show_all_details_9879980c":"显示全部详情","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"这个作业被丢弃，但不会影响总计算。","this_is_a_what_if_score_cbdc04f9":"这是假设分数"},"zh-Hant":{"assignment_details_collapsed_55b67150":"收起作業詳情","assignment_details_expanded_20c6f0c7":"展開作業詳情","based_on_what_if_scores_the_new_total_is_now_grade_d223be10":"根據『假設分析』分數，新的總分會出現%{grade}","click_to_test_a_different_score_2e0bb6f0":"按一下以測試不同的成績","enter_a_what_if_score_668c91a3":"輸入預期得分。","grades_are_now_reverted_to_original_scores_e6deade1":"成績正在還原至原始分數","grades_are_now_showing_what_if_scores_711a8be2":"成績正在顯示『假設分析』的分數","hide_all_details_61d7ebb":"隱藏所有詳細資料","instructor_has_not_posted_this_grade_3b4c4124":"導師未公佈此評分","n_a_d6f6c42f":"不適用","show_all_details_9879980c":"顯示所有詳細資料","this_assignment_is_dropped_and_will_not_be_conside_35c3e198":"該作業已被取消，不予計入總評分。","this_is_a_what_if_score_cbdc04f9":"这是『假設分析』分数"}}'))
var ut=C["default"].scoped("gradingGradeSummary")
var pt=a("xvQv")
var mt=a("FdVj")
var ht=a("Gzzm")
var ft=a("ShK5")
var gt=a("+yat")
var bt=a("5Pfy")
var yt=a("bt35")
a("17x9")
var wt=a("Xx/m")
var vt=a("TstA")
var kt=a("CO+y")
var xt=a("6SzX")
var At=a("ZbPE")
var jt=a("JGuX")
var Vt=a("uloQ")
Object(S["a"])(JSON.parse('{"ar":{"all_grading_periods_77974940":"كل فترات تقييم الدرجات","an_error_occurred_please_try_again_95361e05":"حدث خطأ ما. يرجى إعادة المحاولة.","apply_781a2546":"تطبيق","apply_filters_note_clicking_this_button_will_cause_753f04":"تطبيق عوامل تصفية. ملاحظة: سيؤدي النقر فوق هذا الزر إلى إعادة تحميل الصفحة.","arrange_by_a121617c":"ترتيب حسب","course_8a63b4a3":"المساق","grading_period_8b0a4a10":"فترة تقييم الدرجات","student_5da6bfd1":"الطالب"},"ca":{"all_grading_periods_77974940":"Tots els períodes de qualificació","an_error_occurred_please_try_again_95361e05":"S\'ha produït un error. Torneu-ho a provar.","apply_781a2546":"Aplica","apply_filters_note_clicking_this_button_will_cause_753f04":"Aplica els filtres. Nota: en fer clic en aquest botó, la pàgina es tornarà a carregar.","arrange_by_a121617c":"Ordena per","course_8a63b4a3":"Curs","grading_period_8b0a4a10":"Període de qualificació","student_5da6bfd1":"Estudiant"},"cy":{"all_grading_periods_77974940":"Pob Cyfnod Graddio","an_error_occurred_please_try_again_95361e05":"Gwall. Rhowch gynnig arall arni.","apply_781a2546":"Rhoi ar waith","apply_filters_note_clicking_this_button_will_cause_753f04":"Rhowch hidlyddion ar waith. Nodyn: wrth glicio’r botwm hwn, bydd y dudalen yn ail-lwytho.","arrange_by_a121617c":"Trefnu yn ôl","course_8a63b4a3":"Cwrs","grading_period_8b0a4a10":"Cyfnod Graddio","student_5da6bfd1":"Myfyriwr"},"da":{"all_grading_periods_77974940":"Alle karakterperioder","an_error_occurred_please_try_again_95361e05":"En fejl opstod. Prøv igen.","apply_781a2546":"Tildel","apply_filters_note_clicking_this_button_will_cause_753f04":"Anvend filtre. Bemærk: Ved at klikke på denne knap bliver siden genindlæst.","arrange_by_a121617c":"Organiser efter","course_8a63b4a3":"Fag","grading_period_8b0a4a10":"Karakterperiode","student_5da6bfd1":"Studerende"},"da-x-k12":{"all_grading_periods_77974940":"Alle vurderingsperioder","an_error_occurred_please_try_again_95361e05":"En fejl opstod. Prøv igen.","apply_781a2546":"Tildel","apply_filters_note_clicking_this_button_will_cause_753f04":"Anvend filtre. Bemærk: Ved at klikke på denne knap bliver siden genindlæst.","arrange_by_a121617c":"Organiser efter","course_8a63b4a3":"Fag","grading_period_8b0a4a10":"Vurderingsperiode","student_5da6bfd1":"Elev"},"de":{"all_grading_periods_77974940":"Alle Benotungszeiträume","an_error_occurred_please_try_again_95361e05":"Ein Fehler ist aufgetreten. Bitte versuchen Sie es noch einmal.","apply_781a2546":"Anwenden","apply_filters_note_clicking_this_button_will_cause_753f04":"Filter anwenden.. Hinweis: Mit dieser Schaltfläche wird die Seite erneut geladen.","arrange_by_a121617c":"Anordnen nach","course_8a63b4a3":"Kurs","grading_period_8b0a4a10":"Benotungszeitraum","student_5da6bfd1":"Student"},"el":{"all_grading_periods_77974940":"Όλες οι περίοδοι βαθμολογήσεων","apply_781a2546":"Εφαρμογή","course_8a63b4a3":"Μάθημα","grading_period_8b0a4a10":"Περίδος Βαθμολόγησης","student_5da6bfd1":"Μαθητής "},"en-AU":{"all_grading_periods_77974940":"All Grading Periods","an_error_occurred_please_try_again_95361e05":"An error occurred. Please try again.","apply_781a2546":"Apply","apply_filters_note_clicking_this_button_will_cause_753f04":"Apply filters. Note: clicking this button will cause the page to reload.","arrange_by_a121617c":"Arrange By","course_8a63b4a3":"Course","grading_period_8b0a4a10":"Grading Period","student_5da6bfd1":"Student"},"en-AU-x-unimelb":{"all_grading_periods_77974940":"All Grading Periods","an_error_occurred_please_try_again_95361e05":"An error occurred. Please try again.","apply_781a2546":"Apply","apply_filters_note_clicking_this_button_will_cause_753f04":"Apply filters. Note: clicking this button will cause the page to reload.","arrange_by_a121617c":"Arrange By","course_8a63b4a3":"Subject","grading_period_8b0a4a10":"Grading Period","student_5da6bfd1":"Student"},"en-CA":{"all_grading_periods_77974940":"All Grading Periods","an_error_occurred_please_try_again_95361e05":"An error occurred. Please try again.","apply_781a2546":"Apply","apply_filters_note_clicking_this_button_will_cause_753f04":"Apply filters. Note: clicking this button will cause the page to reload.","arrange_by_a121617c":"Arrange By","course_8a63b4a3":"Course","grading_period_8b0a4a10":"Grading Period","student_5da6bfd1":"Student"},"en-GB":{"all_grading_periods_77974940":"All grading periods","an_error_occurred_please_try_again_95361e05":"An error occurred. Please try again.","apply_781a2546":"Apply","apply_filters_note_clicking_this_button_will_cause_753f04":"Apply filters. Note: clicking this button will cause the page to reload.","arrange_by_a121617c":"Arrange by","course_8a63b4a3":"Course","grading_period_8b0a4a10":"Grading period","student_5da6bfd1":"Student"},"en-GB-x-lbs":{"all_grading_periods_77974940":"All grading periods","an_error_occurred_please_try_again_95361e05":"An error occurred. Please try again.","apply_781a2546":"Apply","apply_filters_note_clicking_this_button_will_cause_753f04":"Apply filters. Note: clicking this button will cause the page to reload.","arrange_by_a121617c":"Arrange by","course_8a63b4a3":"Programme","grading_period_8b0a4a10":"Grading period","student_5da6bfd1":"Student"},"en-GB-x-ukhe":{"all_grading_periods_77974940":"All grading periods","an_error_occurred_please_try_again_95361e05":"An error occurred. Please try again.","apply_781a2546":"Apply","apply_filters_note_clicking_this_button_will_cause_753f04":"Apply filters. Note: clicking this button will cause the page to reload.","arrange_by_a121617c":"Arrange by","course_8a63b4a3":"Module","grading_period_8b0a4a10":"Grading period","student_5da6bfd1":"Student"},"es":{"all_grading_periods_77974940":"Todos los períodos de calificación","an_error_occurred_please_try_again_95361e05":"Ha ocurrido un error. Inténtelo de nuevo.","apply_781a2546":"Aplicar","apply_filters_note_clicking_this_button_will_cause_753f04":"Aplicar filtros. Nota: al hacer clic en este botón, se recargará la página.","arrange_by_a121617c":"Organizar por","course_8a63b4a3":"Curso","grading_period_8b0a4a10":"Período de Calificación","student_5da6bfd1":"Estudiante"},"fa":{"all_grading_periods_77974940":"همه دوره های نمره گذازی","an_error_occurred_please_try_again_95361e05":"یک خطا رخ داد. لطفا دوباره سعی کنید.","apply_781a2546":"اعمال","apply_filters_note_clicking_this_button_will_cause_753f04":"اعمال فیلترها. توجه: با کلیک کردن روی این دکمه، صفحه دوباره بارگذاری می شود.","arrange_by_a121617c":"مرتب سازی بر اساس","course_8a63b4a3":"درس","grading_period_8b0a4a10":"دوره نمره گذاری","student_5da6bfd1":"دانشجو"},"fi":{"all_grading_periods_77974940":"Kaikki arvosanojen antojaksot","an_error_occurred_please_try_again_95361e05":"Tapahtui virhe. Yritä uudelleen.","apply_781a2546":"Käytä","apply_filters_note_clicking_this_button_will_cause_753f04":"Käytä suodattimia. Huomautus: tämän painikkeen painaminen lataa sivun uudelleen.","arrange_by_a121617c":"Järjestysperuste","course_8a63b4a3":"Kurssi","grading_period_8b0a4a10":"Arvosanojen antojakso","student_5da6bfd1":"Opiskelija"},"fr":{"all_grading_periods_77974940":"Toutes les périodes de notation","an_error_occurred_please_try_again_95361e05":"Une erreur est survenue. Veuillez réessayer.","apply_781a2546":"Appliquer","apply_filters_note_clicking_this_button_will_cause_753f04":"Appliquer les filtres. Note : cliquer sur ce bouton entraînera le rafraichissement de la page.","arrange_by_a121617c":"Arranger par","course_8a63b4a3":"Le cours","grading_period_8b0a4a10":"Période de notation","student_5da6bfd1":"Étudiant"},"fr-CA":{"all_grading_periods_77974940":"Toutes les périodes de notation","an_error_occurred_please_try_again_95361e05":"Une erreur est survenue. Veuillez réessayer.","apply_781a2546":"Appliquer","apply_filters_note_clicking_this_button_will_cause_753f04":"Appliquer filtres. Remarque : cliquer sur ce bouton déclenche le rechargement de la page.","arrange_by_a121617c":"Trier par","course_8a63b4a3":"Cours","grading_period_8b0a4a10":"Période de notation","student_5da6bfd1":"Étudiant"},"he":{"all_grading_periods_77974940":"כל תקופת חישוב הציונים","an_error_occurred_please_try_again_95361e05":"אירעה שגיאה. נא לנסות שוב.","apply_781a2546":"החל","apply_filters_note_clicking_this_button_will_cause_753f04":"הפעלת מסננים. שימו לב: הקשה על כפתור זה תגרום לטעינת הדף מחדש","arrange_by_a121617c":"סידור לפי","course_8a63b4a3":"קורס","grading_period_8b0a4a10":"תקופת הערכה","student_5da6bfd1":"תלמיד"},"ht":{"all_grading_periods_77974940":"Tout Peryòd Klasman","an_error_occurred_please_try_again_95361e05":"Yon erè fèt. Tanpri eseye ankò.","apply_781a2546":"Aplike","apply_filters_note_clicking_this_button_will_cause_753f04":"Aplike filtè. Nòt: klike sou bouton sa a ap rechaje paj la.","arrange_by_a121617c":"Aranje Pa","course_8a63b4a3":"Kou","grading_period_8b0a4a10":"Peryòd Klasman","student_5da6bfd1":"Elèv"},"hu":{"all_grading_periods_77974940":"Összes osztályzási időszak","apply_781a2546":"Alkalmazás","arrange_by_a121617c":"Rendezés ez alapján","course_8a63b4a3":"Kurzus","grading_period_8b0a4a10":"Osztályzási időszak","student_5da6bfd1":"Hallgató"},"hy":{"all_grading_periods_77974940":"Գնահատման բոլոր ժամանակահատվածները","apply_781a2546":"Կիրառել","course_8a63b4a3":"Դասընթաց","student_5da6bfd1":"Ունկնդիր"},"is":{"all_grading_periods_77974940":"Öll einkunnatímabil","an_error_occurred_please_try_again_95361e05":"Villa kom upp Vinsamlegast reynið aftur.","apply_781a2546":"Virkja","apply_filters_note_clicking_this_button_will_cause_753f04":"Setja upp síur. Ath: síðan endurhleðst ef smellt er á þennan hnapp.","arrange_by_a121617c":"Raða eftir","course_8a63b4a3":"Námskeið","grading_period_8b0a4a10":"Einkunnatímabil","student_5da6bfd1":"Nemandi"},"it":{"all_grading_periods_77974940":"Tutti i periodi di valutazione","an_error_occurred_please_try_again_95361e05":"Si è verificato un errore. Riprova.","apply_781a2546":"Applica","apply_filters_note_clicking_this_button_will_cause_753f04":"Applica filtri. Nota: se fai clic su questo pulsante, la pagina verrà ricaricata.","arrange_by_a121617c":"Disponi per","course_8a63b4a3":"Corso","grading_period_8b0a4a10":"Periodo di valutazione","student_5da6bfd1":"Studente"},"ja":{"all_grading_periods_77974940":"すべての採点期間","an_error_occurred_please_try_again_95361e05":"エラーが発生しました。再度試してください。","apply_781a2546":"適用","apply_filters_note_clicking_this_button_will_cause_753f04":"フィルターを適用する。注：このボタンをクリックすると、ページが再読み込みされます。","arrange_by_a121617c":"～で並べ替え","course_8a63b4a3":"コース","grading_period_8b0a4a10":"採点期間","student_5da6bfd1":"受講生"},"ko":{"apply_781a2546":"적용","arrange_by_a121617c":"정렬 기준","student_5da6bfd1":"학생"},"mi":{"all_grading_periods_77974940":"Takiwā kōeke katoa","an_error_occurred_please_try_again_95361e05":"Kua puta tētahi hapa Tēnā koa ngana anō.","apply_781a2546":"Tono","apply_filters_note_clicking_this_button_will_cause_753f04":"Hoatuhia ngā tātari. Tuhipoka: ma te pāwhiri i tēnei pātene ka meinga te whārangi ki te uta anō.","arrange_by_a121617c":"Whakarite e","course_8a63b4a3":"akoranga","grading_period_8b0a4a10":"Wā kōeke","student_5da6bfd1":"Ākonga"},"nb":{"all_grading_periods_77974940":"Alle vurderingsperioder","an_error_occurred_please_try_again_95361e05":"Det oppstod en feil. Vennligst prøv igjen.","apply_781a2546":"Bruk","apply_filters_note_clicking_this_button_will_cause_753f04":"Bruk filtre. Merk: Ved å klikke på denne tasten vil siden bli lastet opp på nytt.","arrange_by_a121617c":"Sorter etter","course_8a63b4a3":"Emne","grading_period_8b0a4a10":"Vurderingsperiode","student_5da6bfd1":"Student"},"nb-x-k12":{"all_grading_periods_77974940":"Alle vurderingsperioder","an_error_occurred_please_try_again_95361e05":"Det oppstod en feil. Vennligst prøv igjen.","apply_781a2546":"Søk","apply_filters_note_clicking_this_button_will_cause_753f04":"Bruk filtre. Merk: Ved å klikke på denne tasten vil siden bli lastet opp på nytt.","arrange_by_a121617c":"Sorter etter","course_8a63b4a3":"Fag","grading_period_8b0a4a10":"Vurderingsperiode","student_5da6bfd1":"Elev"},"nl":{"all_grading_periods_77974940":"Alle beoordelingsperioden","an_error_occurred_please_try_again_95361e05":"Er is een fout opgetreden. Probeer het opnieuw.","apply_781a2546":"Toepassen","apply_filters_note_clicking_this_button_will_cause_753f04":"Pas filters toe. Opmerking: als je op deze knop klikt, wordt de pagina opnieuw geladen.","arrange_by_a121617c":"Rangschikken op","course_8a63b4a3":"Cursus","grading_period_8b0a4a10":"Beoordelingsperiode","student_5da6bfd1":"Cursist"},"nn":{"all_grading_periods_77974940":"Alle periodar for karaktersetjing","an_error_occurred_please_try_again_95361e05":"Det oppstod ein feil. Prøv på nytt seinare.","apply_781a2546":"Bruk","apply_filters_note_clicking_this_button_will_cause_753f04":"Legg til filter. Merk: ved å klikke på knappen vil sida laste på nytt.","arrange_by_a121617c":"Sorter etter","course_8a63b4a3":"Emne","grading_period_8b0a4a10":"Karaktersetjingsperiode","student_5da6bfd1":"Student"},"pl":{"all_grading_periods_77974940":"Wszystkie okresy oceniania","an_error_occurred_please_try_again_95361e05":"Wystąpił błąd. Spróbuj ponownie.","apply_781a2546":"Zastosuj","apply_filters_note_clicking_this_button_will_cause_753f04":"Zastosuj filtry! Uwaga: kliknięcie tego przycisku spowoduje ponowne wczytanie strony.","arrange_by_a121617c":"Układ","course_8a63b4a3":"Kurs","grading_period_8b0a4a10":"Okres oceniania","student_5da6bfd1":"Uczestnik"},"pt":{"all_grading_periods_77974940":"Todos os períodos de avaliação","an_error_occurred_please_try_again_95361e05":"Um erro ocorreu. É favor tentar novamente.","apply_781a2546":"Aplicar","apply_filters_note_clicking_this_button_will_cause_753f04":"Aplicar filtros. Nota: clicar neste botão fará com que a página seja recarregada.","arrange_by_a121617c":"Organizar por","course_8a63b4a3":"Disciplina","grading_period_8b0a4a10":"Período de classificação","student_5da6bfd1":"Aluno"},"pt-BR":{"all_grading_periods_77974940":"Todos os períodos de avaliação","an_error_occurred_please_try_again_95361e05":"Ocorreu um erro. Por favor, tente novamente.","apply_781a2546":"Aplicar","apply_filters_note_clicking_this_button_will_cause_753f04":"Aplicar filtros. Nota: Clicar nesse botão causará o recarregamento da página.","arrange_by_a121617c":"Ordenar por","course_8a63b4a3":"Curso","grading_period_8b0a4a10":"Período de Pontuação","student_5da6bfd1":"Aluno"},"ru":{"all_grading_periods_77974940":"Все периоды выставления оценок","an_error_occurred_please_try_again_95361e05":"Возникла ошибка. Попробуйте еще раз.","apply_781a2546":"Применить","apply_filters_note_clicking_this_button_will_cause_753f04":"Примените фильтры. Примечание: нажатие этой кнопки приведет к перезагрузке страницы.","arrange_by_a121617c":"Упорядочить по","course_8a63b4a3":"Курс","grading_period_8b0a4a10":"Период оценивания","student_5da6bfd1":"Студент"},"sl":{"all_grading_periods_77974940":"Vsa ocenjevalna obdobja","an_error_occurred_please_try_again_95361e05":"Prišlo je do napake. Poskusite znova.","apply_781a2546":"Uporabi","apply_filters_note_clicking_this_button_will_cause_753f04":"Uporabi filtre Opomba: s klikom tega gumba se bo stran ponovno naložila.","arrange_by_a121617c":"Razvrsti glede na","course_8a63b4a3":"Predmet","grading_period_8b0a4a10":"Ocenjevalno obdobje","student_5da6bfd1":"Študent"},"sv":{"all_grading_periods_77974940":"Alla bedömningsperioder","an_error_occurred_please_try_again_95361e05":"Ett fel uppstod. Försök igen.","apply_781a2546":"Tillämpa","apply_filters_note_clicking_this_button_will_cause_753f04":"Använd filter. Obs! Om du klickar på den här knappen kommer sidan att laddas om.","arrange_by_a121617c":"Sortera efter","course_8a63b4a3":"Kurs","grading_period_8b0a4a10":"Bedömningsperiod","student_5da6bfd1":"Student"},"sv-x-k12":{"all_grading_periods_77974940":"Alla bedömningsperioder","an_error_occurred_please_try_again_95361e05":"Ett fel uppstod. Försök igen.","apply_781a2546":"Tillämpa","apply_filters_note_clicking_this_button_will_cause_753f04":"Använd filter. Obs! Om du klickar på den här knappen kommer sidan att laddas om.","arrange_by_a121617c":"Sortera efter","course_8a63b4a3":"Kurs","grading_period_8b0a4a10":"Bedömningsperiod","student_5da6bfd1":"Elev"},"tr":{"all_grading_periods_77974940":"Tüm Notlandırma Periyotları","an_error_occurred_please_try_again_95361e05":"Hata oluştu. Lütfen tekrar deneyin.","apply_781a2546":"Uygula","apply_filters_note_clicking_this_button_will_cause_753f04":"Filtreleri uygula. Not: bu butona tıkladığınızda sayfa yeniden yüklenecek.","arrange_by_a121617c":"Göre düzenle","course_8a63b4a3":"Ders","grading_period_8b0a4a10":"Notlandırma Periyodu","student_5da6bfd1":"Öğrenci"},"uk":{"all_grading_periods_77974940":"Усі періоди оцінювання","an_error_occurred_please_try_again_95361e05":"Виникла помилка. Будь ласка, спробуйте ще раз.","apply_781a2546":"Застосувати","apply_filters_note_clicking_this_button_will_cause_753f04":"Застосуйте фільтри. Зауваження: натискання на кнопки викличе перевантаження сторінки.","arrange_by_a121617c":"Впорядкувати по","course_8a63b4a3":"Курс","grading_period_8b0a4a10":"Період оцінювання","student_5da6bfd1":"Студент"},"zh-Hans":{"all_grading_periods_77974940":"所有评分时段","an_error_occurred_please_try_again_95361e05":"发生错误。请重试。","apply_781a2546":"申请","apply_filters_note_clicking_this_button_will_cause_753f04":"应用筛选条件。注释：点击此按钮将重载页面。","arrange_by_a121617c":"整理顺序","course_8a63b4a3":"课程","grading_period_8b0a4a10":"评分周期","student_5da6bfd1":"学生"},"zh-Hant":{"all_grading_periods_77974940":"所有評分時段","an_error_occurred_please_try_again_95361e05":"發生了一個錯誤。請重試。","apply_781a2546":"應用","apply_filters_note_clicking_this_button_will_cause_753f04":"應用篩選。注意：點擊此按鈕將導致內容頁重新載入。","arrange_by_a121617c":"排列規則為","course_8a63b4a3":"課程","grading_period_8b0a4a10":"評分時段","student_5da6bfd1":"學生"}}'))
var Ct=C["default"].scoped("grade_summary")
var St=a("2zZe")
function zt(e){const t=e.options.map(t=>{const a=t[e.textAttribute]
const s=t[e.valueAttribute]
return Object(Ze["a"])(St["a"].Option,{id:s,value:s},s,a)})
return Object(Ze["a"])(St["a"],{defaultValue:e.defaultValue,interaction:e.disabled?"disabled":"enabled",id:e.id,isInline:true,renderLabel:e.label,onChange:e.onChange,width:"15rem"},void 0,t)}class Ot extends Qe.a.Component{constructor(e){super(e)
this.onSelection=(e,t,{value:a})=>{this.setState({[e]:a})}
this.onSubmit=()=>{this.setState({processing:true},()=>{this.state.assignmentSortOrder!==this.props.selectedAssignmentSortOrder?this.props.saveAssignmentOrder(this.state.assignmentSortOrder).then(this.reloadPage).catch(e=>{Object(Vt["b"])(Ct.t("An error occurred. Please try again."))(e)
this.setState({processing:false})}):this.reloadPage()})}
this.reloadPage=()=>{const e=this.state.courseID,t=this.props.selectedCourseID
const a=this.props.courses.find(e=>e.id===t)
const s=this.props.courses.find(t=>t.id===e)
const n=s.url
const r=this.state.studentID===this.props.currentUserID?"":"/"+this.state.studentID
let i
i=s.gradingPeriodSetId&&a.gradingPeriodSetId===s.gradingPeriodSetId&&this.state.gradingPeriodID?"?grading_period_id="+this.state.gradingPeriodID:""
this.props.goToURL(`${n}${r}${i}`)}
this.onSelectAssignmentSortOrder=this.onSelection.bind(this,"assignmentSortOrder")
this.onSelectCourse=this.onSelection.bind(this,"courseID")
this.onSelectStudent=this.onSelection.bind(this,"studentID")
this.onSelectGradingPeriod=this.onSelection.bind(this,"gradingPeriodID")
this.state={assignmentSortOrder:e.selectedAssignmentSortOrder,courseID:e.selectedCourseID,gradingPeriodID:e.selectedGradingPeriodID,processing:false,studentID:e.selectedStudentID}}componentDidMount(){this.props.displayPageContent()}anySelectMenuChanged(e){const t={assignmentSortOrder:"selectedAssignmentSortOrder",courseID:"selectedCourseID",gradingPeriodID:"selectedGradingPeriodID",studentID:"selectedStudentID"}
return e.some(e=>this.state[e]!==this.props[t[e]])}gradingPeriodOptions(){return[{id:"0",title:Ct.t("All Grading Periods")}].concat(this.props.gradingPeriods)}noSelectMenuChanged(){return!this.anySelectMenuChanged(["courseID","studentID","gradingPeriodID","assignmentSortOrder"])}render(){const e=!this.props.breakpoints.miniTablet
return Object(Ze["a"])(vt["a"],{alignItems:e?"start":"end",wrapItems:true,margin:"0 0 small 0",direction:e?"column":"row"},void 0,Object(Ze["a"])(vt["a"].Item,{},void 0,this.props.students.length>1&&Object(Ze["a"])(zt,{defaultValue:this.props.selectedStudentID,disabled:this.anySelectMenuChanged(["courseID"]),id:"student_select_menu",label:Ct.t("Student"),onChange:this.onSelectStudent,options:this.props.students,textAttribute:"name",valueAttribute:"id"}),this.props.gradingPeriods.length>0&&Object(Ze["a"])(zt,{defaultValue:this.props.selectedGradingPeriodID,disabled:this.anySelectMenuChanged(["courseID"]),id:"grading_period_select_menu",label:Ct.t("Grading Period"),onChange:this.onSelectGradingPeriod,options:this.gradingPeriodOptions(),textAttribute:"title",valueAttribute:"id"}),this.props.courses.length>1&&Object(Ze["a"])(zt,{defaultValue:this.props.selectedCourseID,disabled:this.anySelectMenuChanged(["studentID","gradingPeriodID","assignmentSortOrder"]),id:"course_select_menu",label:Ct.t("Course"),onChange:this.onSelectCourse,options:this.props.courses,textAttribute:"nickname",valueAttribute:"id"}),Object(Ze["a"])(zt,{defaultValue:this.props.selectedAssignmentSortOrder,disabled:this.anySelectMenuChanged(["courseID"]),id:"assignment_sort_order_select_menu",label:Ct.t("Arrange By"),onChange:this.onSelectAssignmentSortOrder,options:this.props.assignmentSortOptions,textAttribute:0,valueAttribute:1})),Object(Ze["a"])(vt["a"].Item,{margin:e?"small 0 0 0":"0 0 0 small"},void 0,Object(Ze["a"])(wt["a"],{disabled:this.state.processing||this.noSelectMenuChanged(),id:"apply_select_menus",onClick:this.onSubmit,type:"submit",size:"medium",variant:"primary"},void 0,Object(Ze["a"])(kt["a"],{},void 0,Object(Ze["a"])(At["a"],{},void 0,Ct.t("Apply"))),Object(Ze["a"])(xt["a"],{},void 0,Ct.t("Apply filters. Note: clicking this button will cause the page to reload.")))))}}Ot.defaultProps={selectedGradingPeriodID:null,breakpoints:{}}
var Nt=Object(jt["a"])(Ot)
var Pt=a("3vji")
const It={getSelectedGradingPeriodId(){const e=ENV.current_grading_period_id
if(!e||"0"===e)return null
return e},getAssignmentId:e=>e.getTemplateData({textValues:["assignment_id"]}).assignment_id,parseScoreText(e,t,a){const s="number"===typeof t?t:null
const n="string"===typeof a?a:"-"
let r=mt["a"].parse(e)
r=isNaN(r)?s:r
return{numericalValue:r,formattedValue:bt["a"].formatGrade(r,{defaultValue:n})}},getOriginalScore(e){let t=parseFloat(e.find(".original_points").text())
t=isNaN(t)?null:t
return{numericalValue:t,formattedValue:e.find(".original_score").text()}},onEditWhatIfScore(e,t){e.find(".grade").data("originalValue")||e.find(".grade").data("originalValue",e.find(".grade").html())
const a=e.find(".screenreader-only").clone(true)
e.find(".grade").data("screenreader_link",a)
e.find(".grade").empty().append(n()("#grade_entry"))
e.find(".score_value").hide()
t.text(ut.t("Enter a What-If score."))
const s=e.parents(".student_assignment").find(".what_if_score").text()
const r=It.parseScoreText(s,0,"0")
n()("#grade_entry").val(r.formattedValue).show().focus().select()},onScoreChange(e,t){const a=It.getOriginalScore(e)
const s=e.find("#grade_entry").val()
let r=It.parseScoreText(s)
if(null==r.numericalValue){const t=e.find(".what_if_score").text()
r=It.parseScoreText(t)}null==r.numericalValue&&(r=a)
const i=r.numericalValue!=a.numericalValue
e.find(".what_if_score").text(r.formattedValue)
let o=t.update
if(e.hasClass("dont_update")){o=false
e.removeClass("dont_update")}const _=It.getAssignmentId(e)
if(o){const t=n.a.replaceTags(n()(".update_submission_url").attr("href"),"assignment_id",_)
const a=i?r.numericalValue:null
n.a.ajaxJSON(t,"PUT",{"submission[student_entered_score]":a},t=>{const a={student_entered_score:t.submission.student_entered_score}
e.fillTemplateData({data:a})},n.a.noop)}n()("#grade_entry").hide().appendTo(n()("body"))
const l=e.find(".grade")
null==r.numericalValue?l.html(l.data("originalValue")):l.html(Object(ct["a"])(r.formattedValue))
Tt(e)
const d=e.find(".assignment_score")
const c=d.find(".score_teaser")
if(i){d.attr("title","")
c.text(ut.t("This is a What-If score"))
const a=n()("#revert_score_template").clone(true).attr("id","").show()
d.find(".score_holder").append(a)
l.addClass("changed")
t.refocus&&setTimeout(()=>{e.find(".revert_score_link").focus()},0)}else{Et(e)
d.attr("title",ut.t("Click to test a different score"))
l.removeClass("changed")
e.find(".revert_score_link").remove()
t.refocus&&setTimeout(()=>{e.find(".grade").focus()},0)}if(!i){const t=e.find(".grade").data("screenreader_link")
e.find(".grade").prepend(t)}It.updateScoreForAssignment(_,r.numericalValue)
It.updateStudentGrades()},onScoreRevert(e,t){const a=e.find(".assignment_score")
const s=a.find(".grade")
const n=Object(De["a"])({refocus:true,skipEval:false},t)
const r=It.getOriginalScore(e)
let i
null==r.numericalValue&&(r.formattedValue=It.parseScoreText(null).formattedValue)
if(e.data("muted")){i=ut.t("Instructor has not posted this grade")
s.html(`<i class="icon-off" aria-hidden="true" title="${i}"=></i>`)}else{i=ut.t("Click to test a different score")
s.text(r.formattedValue)}Et(e)
e.find(".what_if_score").text(r.formattedValue)
e.find(".revert_score_link").remove()
e.find(".score_value").text(r.formattedValue)
a.attr("title",i)
s.removeClass("changed")
const o=e.getTemplateValue("assignment_id")
It.updateScoreForAssignment(o,r.numericalValue)
n.skipEval||It.updateStudentGrades()
const _=e.find(".grade").data("screenreader_link")
s.prepend(_)
n.refocus&&setTimeout(()=>{e.find(".grade").focus()},0)}}
function Tt(e){const t=e.find(".grade")
let a
let s
a=t.find(".tooltip_wrap right")
if(0===a.length){a=n()('<span class="tooltip_wrap right"></span>')
t.append(a)
s=a.find(".tooltip_text score_teaser")
if(0===s.length){s=n()('<span class="tooltip_text score_teaser"></span>')
a.append(s)}}}function Et(e){let t
t=e.data("muted")?ut.t("Instructor has not posted this grade"):ut.t("Click to test a different score")
Tt(e)
const a=e.find(".tooltip_text.score_teaser")
a.text(t)}function Dt(){if(ENV.grading_period_set)return dt["a"].deserializeSet(ENV.grading_period_set)
return null}function Lt(){let e
e=ENV.effective_due_dates&&ENV.grading_period_set?ht["a"].calculate(ENV.submissions,ENV.assignment_groups,ENV.group_weighting_scheme,ENV.grade_calc_ignore_unposted_anonymous_enabled,Dt(),Object(ft["a"])(ENV.effective_due_dates,ENV.student_id)):ht["a"].calculate(ENV.submissions,ENV.assignment_groups,ENV.group_weighting_scheme,ENV.grade_calc_ignore_unposted_anonymous_enabled)
const t=It.getSelectedGradingPeriodId()
if(t)return e.gradingPeriods[t]
return e}function Mt(e,t){return t>0&&!isNaN(e)}function Gt(e,t){const a=Object(Pt["c"])(e,t)
return Object(pt["a"])(a,pt["a"].DEFAULT)}function Bt(e){return ut.n(e,{percentage:true})}function Rt(e,t){if(Mt(e,t))return Bt(Gt(e,t))
return ut.t("N/A")}function Wt(){const e=ENV.grading_period_set
const t=It.getSelectedGradingPeriodId()
return(!t||0===t)&&e&&e.weighted}function Ut(e,t,a){const s=[]
let n
n=e?{bins:ENV.grading_periods,grades:t.gradingPeriods,elementIdPrefix:"#submission_period"}:{bins:ENV.assignment_groups,grades:t.assignmentGroups,elementIdPrefix:"#submission_group"}
if(n.grades)for(let e=0;e<n.bins.length;e++){const t=n.bins[e].id
let r=n.grades[t]
r=r?r[a]:{score:0,possible:0}
const i=ut.n(r.score,{precision:pt["a"].DEFAULT})
const o=ut.n(r.possible,{precision:pt["a"].DEFAULT})
const _={teaserText:`${i} / ${o}`,gradeText:Rt(r.score,r.possible),rowElementId:`${n.elementIdPrefix}-${t}`}
s.push(_)}return s}function Kt(e,t){if("percent"===e)return""
const a=It.getSelectedGradingPeriodId()
const s=Dt()
if(null==a&&s&&s.weighted)return""
return t}function Ht(e,t,a){const s=n()(".grade.changed").length>0
const r=ENV.show_total_grade_as_points
const i=Ut(Wt(),e,t)
for(let e=0;e<i.length;e++){const t=n()(i[e].rowElementId)
t.find(".grade").text(i[e].gradeText)
t.find(".score_teaser").text(i[e].teaserText)
t.find(".points_possible").text(i[e].teaserText)}const o=e[t].score
const _=e[t].possible
const l=`${ut.n(o,{precision:pt["a"].DEFAULT})} / ${ut.n(_,{precision:pt["a"].DEFAULT})}`
const d=Rt(o,_)
let c
let u
if($t()){const e=qt()?ENV.effective_final_score:Gt(o,_)
const t=Object(gt["d"])(e,ENV.grading_scheme)
n()(".final_grade .letter_grade").text(t)}if(!s&&qt()){c=Bt(ENV.effective_final_score)
u=l}else if(r&&"percent"!==a){c=l
u=d}else{c=d
u=l}const p=n()(".student_assignment.final_grade")
p.find(".grade").text(c)
p.find(".score_teaser").text(u)
const m=Kt(a,l)
p.find(".points_possible").text(m)
"percent"===a&&p.find(".score_teaser").hide()
if(s){const e=ut.t("Based on What-If scores, the new total is now %{grade}",{grade:c})
n.a.screenReaderFlashMessageExclusive(e)}n()(".revert_all_scores").showIf(n()("#grades_summary .revert_score_link").length>0)}function $t(){return n()(".final_grade .letter_grade").length>0&&ENV.grading_scheme}function qt(){return null!=ENV.effective_final_score}function Ft(){const e=ut.t("This assignment is dropped and will not be considered in the total calculation")
const t=n()("#only_consider_graded_assignments").attr("checked")
const a=t?"current":"final"
const s=ENV.group_weighting_scheme
const r=!ENV.exclude_total
const i=Lt()
n()(".dropped").attr("aria-label","")
n()(".dropped").attr("title","")
n()(".student_assignment").find(".points_possible").attr("aria-label","")
l.a.forEach(i.assignmentGroups,e=>{l.a.forEach(e[a].submissions,e=>{n()("#submission_"+e.submission.assignment_id).toggleClass("dropped",!!e.drop)})})
n()(".dropped").attr("aria-label",e)
n()(".dropped").attr("title",e)
r&&Ht(i,a,s)}function Jt(e,t){const a=l.a.find(ENV.submissions,t=>""+t.assignment_id===""+e)
a?a.score=t:ENV.submissions.push({assignment_id:e,score:t})}function Zt(e){n()("#show_all_details_button").click(t=>{t.preventDefault()
const a=n()("#show_all_details_button")
a.toggleClass("showAll")
if(a.hasClass("showAll")){a.text(ut.t("Hide All Details"))
n()("tr.student_assignment.editable").each((function(){const e=n()(this).getTemplateValue("assignment_id")
const t=n()(this).data("muted")
if(!t){n()("#comments_thread_"+e).show()
n()("#rubric_"+e).show()
n()("#grade_info_"+e).show()
n()("#final_grade_info_"+e).show()}}))
e.text(ut.t("assignment details expanded"))}else{a.text(ut.t("Show All Details"))
n()("tr.rubric_assessments").hide()
n()("tr.comments").hide()
e.text(ut.t("assignment details collapsed"))}})}function Yt(){document.getElementById("grade-summary-content").style.display=""
document.getElementById("student-grades-right-content").style.display=""}function Qt(e){window.location.href=e}function Xt(e){return _t["a"].post(ENV.save_assignment_order_url,{assignment_order:e})}function ea(){return ENV.courses_with_grades.map(e=>Object(lt["a"])(e))}function ta(){return{assignmentSortOptions:ENV.assignment_sort_options,courses:ea(),currentUserID:ENV.current_user.id,displayPageContent:Yt,goToURL:Qt,gradingPeriods:ENV.grading_periods||[],saveAssignmentOrder:Xt,selectedAssignmentSortOrder:ENV.current_assignment_sort_order,selectedCourseID:ENV.context_asset_string.match(/.*_(\d+)$/)[1],selectedGradingPeriodID:ENV.current_grading_period_id,selectedStudentID:ENV.student_id,students:ENV.students}}function aa(){et.a.render(Qe.a.createElement(Nt,It.getSelectMenuGroupProps()),document.getElementById("GradeSummarySelectMenuGroup"))}function sa(){n()(document).ready((function(){It.updateStudentGrades()
const e=n()(this).find("#student-grades-whatif button")
const t=n()(this).find("#revert-all-to-actual-score")
const a=n()(this).find("#aria-announcer")
n()(".revert_all_scores_link").click(t=>{t.preventDefault()
n()("#grades_summary .revert_score_link").each((function(){n()(this).trigger("click",{skipEval:true,refocus:false})}))
n()("#.show_guess_grades.exists").show()
It.updateStudentGrades()
e.focus()
n.a.screenReaderFlashMessageExclusive(ut.t("Grades are now reverted to original scores"))})
n()(".toggle_comments_link, .toggle_score_details_link, .toggle_rubric_assessments_link, .toggle_final_grade_info").click((function(e){e.preventDefault()
const t=n()("#"+n()(this).attr("aria-controls"))
const a=this
n()(a).attr("aria-expanded","none"===t.css("display"))
t.toggle()
"none"!==t.css("display")&&t.find(".screenreader-toggle").focus()}))
n()(".screenreader-toggle").click((function(e){e.preventDefault()
const t=n()(this).data("aria")
const a=n()(`a[aria-controls='${t}']`)
n()(a).attr("aria-expanded",false)
n()(a).focus()
n()(this).closest(".rubric_assessments, .comments").hide()}))
function s(e){if("click"===e.type||13===e.keyCode){if(0===n()("#grades_summary.editable").length||n()(this).find("#grade_entry").length>0||n()(e.target).closest(".revert_score_link").length>0)return
It.onEditWhatIfScore(n()(this),a)}}n()(".student_assignment.editable .assignment_score").on("click keypress",s)
n()("#grade_entry").keydown((function(e){if(13===e.keyCode){a.text("")
n()(this)[0].blur()}else if(27===e.keyCode){a.text("")
const e=n()(this).parents(".student_assignment").addClass("dont_update").find(".original_score").text()
n()(this).val(e||"")[0].blur()}}))
n()("#grades_summary .student_assignment").bind("score_change",(function(e,t){It.onScoreChange(n()(this),t)}))
n()("#grade_entry").blur((function(){const e=n()(this).parents(".student_assignment")
e.triggerHandler("score_change",{update:true,refocus:true})}))
n()("#grades_summary").delegate(".revert_score_link","click",(function(e,t){e.preventDefault()
e.stopPropagation()
It.onScoreRevert(n()(this).parents(".student_assignment"),t)}))
n()("#grades_summary:not(.editable) .assignment_score").css("cursor","default")
n()("#grades_summary tr").hover((function(){n()(this).find("th.title .context").addClass("context_hover")}),(function(){n()(this).find("th.title .context").removeClass("context_hover")}))
n()(".show_guess_grades_link").click(()=>{n()("#grades_summary .student_entered_score").each((function(){const e=It.parseScoreText(n()(this).text())
if(null!=e.numericalValue){const t=n()(this).parents(".student_assignment")
t.find(".what_if_score").text(e.formattedValue)
t.find(".score_value").hide()
t.triggerHandler("score_change",{update:false,refocus:false})}}))
n()(".show_guess_grades").hide()
t.focus()
n.a.screenReaderFlashMessageExclusive(ut.t("Grades are now showing what-if scores"))})
n()("#grades_summary .student_entered_score").each((function(){const e=It.parseScoreText(n()(this).text())
null!=e.numericalValue&&n()(".show_guess_grades").show().addClass("exists")}))
n()(".comments .play_comment_link").mediaCommentThumbnail("normal")
n()(".play_comment_link").live("click",(function(e){e.preventDefault()
const t=n()(this).parents(".comment_media")
const a=t.getTemplateData({textValues:["media_comment_id"]}).media_comment_id
if(a){let e="any"
n()(this).hasClass("video_comment")?e="video":n()(this).hasClass("audio_comment")&&(e="audio")
t.children(":not(.media_comment_content)").remove()
t.find(".media_comment_content").mediaComment("show_inline",a,e)}}))
n()("#only_consider_graded_assignments").change(()=>{It.updateStudentGrades()}).triggerHandler("change")
Zt(a)
yt["a"].renderPills()}))}var na=l.a.extend(It,{setup:sa,getGradingPeriodSet:Dt,canBeConvertedToGrade:Mt,calculateGrade:Rt,calculateGrades:Lt,calculateTotals:Ht,calculateSubtotals:Ut,calculatePercentGrade:Gt,finalGradePointsPossibleText:Kt,formatPercentGrade:Bt,getSelectMenuGroupProps:ta,renderSelectMenuGroup:aa,updateScoreForAssignment:Jt,updateStudentGrades:Ft})
a("UEsX")
a("sXof")
na.setup()
class ra extends i.a.Router{initialize(){if(!ENV.student_outcome_gradebook_enabled)return
n()("#content").tabs({activate:this.activate})
const e=ENV.context_asset_string.replace("course_","")
const t=ENV.student_id
if(ENV.gradebook_non_scoring_rubrics_enabled)this.outcomeView=new ot({el:n()("#outcomes"),course_id:e,student_id:t})
else{this.outcomes=new j([],{course_id:e,user_id:t})
this.outcomeView=new Je({el:n()("#outcomes"),collection:this.outcomes,toggles:n()(".outcome-toggles")})}}tab(e,t){"outcomes"!==e&&"assignments"!==e&&(e=o["a"].contextGet("grade_summary_tab")||"assignments")
n()(`a[href='#${e}']`).click()
if("outcomes"===e){if(!this.outcomeView)return
this.outcomeView.show(t)
n()(".outcome-toggles").show()}else n()(".outcome-toggles").hide()}activate(e,t){const a=t.newPanel.attr("id")
ia.navigate("#tab-"+a,{trigger:true})
return o["a"].contextSet("grade_summary_tab",a)}}ra.prototype.routes={"":"tab","tab-:route(/*path)":"tab"}
let ia
n()(()=>{na.renderSelectMenuGroup()
ia=new ra
i.a.history.start()})}}])

//# sourceMappingURL=grade_summary-c-0f4359f930.js.map