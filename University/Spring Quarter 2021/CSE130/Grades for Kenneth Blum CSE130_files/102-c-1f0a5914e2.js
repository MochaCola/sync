(window["canvasWebpackJsonp"]=window["canvasWebpackJsonp"]||[]).push([[102,699],{BrAc:function(e,t,r){"use strict"
var a=r("vDqi")
var s=r.n(a)
s.a.defaults.xsrfCookieName="_csrf_token"
s.a.defaults.xsrfHeaderName="X-CSRF-Token"
const n=s.a.defaults.headers.common.Accept
s.a.defaults.headers.common.Accept="application/json+canvas-string-ids, "+n
s.a.defaults.headers.common["X-Requested-With"]="XMLHttpRequest"
t["a"]=s.a},Gzzm:function(e,t,r){"use strict"
var a=r("VTBJ")
var s=r("Y/W1")
var n=r.n(s)
var i=r("xvQv")
var c=r("HatE")
var o=r("3vji")
function d(e,t,r){const s=n.a.map(e,e=>{const r=t?e.final:e.current
return Object(a["a"])(Object(a["a"])({},r),{},{weight:e.assignmentGroupWeight})})
if(r.weightAssignmentGroups){const e=s.filter(e=>e.possible)
const r=Object(o["e"])(e,"weight")
let a=Object(o["a"])(n.a.map(e,o["g"]))
0===r?a=null:r<100&&(a=Object(o["f"])(Object(o["g"])({score:a,possible:r,weight:100})))
const c=Object(o["e"])(e,"submission_count")
const d=c>0||t?100:0
let u=a&&Object(i["a"])(a,2)
u=isNaN(u)?null:u
return{score:u,possible:d}}return{score:Object(o["e"])(s,"score"),possible:Object(o["e"])(s,"possible")}}function u(e,t){let r=n.a.map(e,e=>{const r=t?e.final:e.current
return Object(a["a"])(Object(a["a"])({},r),{},{weight:e.gradingPeriodWeight})})
t||(r=n.a.filter(r,"possible"))
const s=Object(o["a"])(n.a.map(r,o["g"]))
const c=Object(o["e"])(r,"weight")
const d=0===c?0:Object(o["f"])(s.times(100).div(Math.min(c,100)))
return{score:Object(i["a"])(d,2),possible:100}}function l(e,t){const r=n.a.groupBy(e.assignments,e=>t[e.id].grading_period_id)
return n.a.map(r,t=>Object(a["a"])(Object(a["a"])({},e),{},{assignments:t}))}function _(e,t){return n.a.reduce(e,(e,r)=>{const s=n.a.filter(r.assignments,e=>t[e.id])
if(s.length>0){const n=Object(a["a"])(Object(a["a"])({},r),{},{assignments:s})
return[...e,...l(n,t)]}return e},[])}function g(e){const t={}
for(let r=0;r<e.length;r++){const s=e[r]
const n=t[s.assignmentGroupId]
t[s.assignmentGroupId]=n?Object(a["a"])(Object(a["a"])({},n),{},{current:{submission_count:n.current.submission_count+s.current.submission_count,submissions:[...n.current.submissions,...s.current.submissions],score:Object(o["d"])([n.current.score,s.current.score]),possible:Object(o["d"])([n.current.possible,s.current.possible])},final:{submission_count:n.final.submission_count+s.final.submission_count,submissions:[...n.final.submissions,...s.final.submissions],score:Object(o["d"])([n.final.score,s.final.score]),possible:Object(o["d"])([n.final.possible,s.final.possible])}}):s}return t}function f(e,t,r,a,s){const i=_(t,a)
const o=n.a.groupBy(i,e=>{const t=e.assignments[0].id
return a[t].grading_period_id})
const l=n.a.keyBy(r,"id")
const f={}
const p=[]
for(let t=0;t<r.length;t++){const a={}
const i=r[t]
const u=o[i.id]||[]
for(let t=0;t<u.length;t++){const r=u[t]
a[r.id]=c["a"].calculate(e,r,s.ignoreUnpostedAnonymous)
p.push(a[r.id])}const _=n.a.values(a)
f[i.id]={gradingPeriodId:i.id,gradingPeriodWeight:l[i.id].weight||0,assignmentGroups:a,current:d(_,false,s),final:d(_,true,s),scoreUnit:s.weightAssignmentGroups?"percentage":"points"}}if(s.weightGradingPeriods)return{assignmentGroups:g(p),gradingPeriods:f,current:u(f,false,s),final:u(f,true,s),scoreUnit:"percentage"}
const m=n.a.map(t,t=>c["a"].calculate(e,t,s.ignoreUnpostedAnonymous))
return{assignmentGroups:n.a.keyBy(m,e=>e.assignmentGroupId),gradingPeriods:f,current:d(m,false,s),final:d(m,true,s),scoreUnit:s.weightAssignmentGroups?"percentage":"points"}}function p(e,t,r){const a=n.a.map(t,t=>c["a"].calculate(e,t,r.ignoreUnpostedAnonymous))
return{assignmentGroups:n.a.keyBy(a,e=>e.assignmentGroupId),current:d(a,false,r),final:d(a,true,r),scoreUnit:r.weightAssignmentGroups?"percentage":"points"}}function m(e,t,r,a,s,n){const i={weightGradingPeriods:s&&!!s.weighted,weightAssignmentGroups:"percent"===r,ignoreUnpostedAnonymous:a}
if(s&&n)return f(e,t,s.gradingPeriods,n,i)
return p(e,t,i)}t["a"]={calculate:m}},HatE:function(e,t,r){"use strict"
var a=r("ODXe")
var s=r("Y/W1")
var n=r.n(s)
var i=r("3vji")
function c(e,t){const r=n.a.groupBy(e,t)
return[r.true||[],r.false||[]]}function o(e){const t=parseFloat(e)
return t&&isFinite(t)?t:0}function d([e,t],[r,a]){const s=r-e
if(0!==s)return s
return t.submission.assignment_id-a.submission.assignment_id}function u([e,t],[r,a]){const s=e-r
if(0!==s)return s
return t.submission.assignment_id-a.submission.assignment_id}function l(e,t){const r=e.score-t.score
if(0!==r)return r
return e.submission.assignment_id-t.submission.assignment_id}function _({score:e,total:t}){return e/t}function g(e,t,r){if(t.length>0){const r=Object(i["e"])(e,"total")
const a=Math.max(r,Object(i["e"])(e,"score"))
const s=Object(i["e"])(t,"score")
return(a+s)/r}return r[r.length-1]}function f(e,t,r){return function(a,s){const c=n.a.map(s,e=>[e.score-a*e.total,e])
const o=c.sort(r)
const d=o.slice(0,e)
const u=Object(i["e"])(d,([e])=>e)
const l=n.a.map(d,([e,t])=>t)
const _=Object(i["e"])(t,e=>e.score-a*e.total)
return[u+_,l]}}function p(e,t,r,s){const i=n.a.map(e,"total")
const o=Math.max(...i)
function l(e,r,s){const n=Math.max(1,r)
if(e.length<=n)return e
const i=[...e,...t]
const d=c(i,e=>0===e.total),u=Object(a["a"])(d,2),l=u[0],p=u[1]
const m=p.map(_).sort()
let h=g(p,l,m)
let b=m[0]
let v=(b+h)/2
const A=f(n,t,s)
let w=A(v,e),O=Object(a["a"])(w,2),j=O[0],D=O[1]
const y=1/(2*n*o**2)
while(h-b>=y){j<0?h=v:b=v
v=(b+h)/2
if(v===h||v===b)break
var E=A(v,e)
var G=Object(a["a"])(E,2)
j=G[0]
D=G[1]}return D}const p=l(e,r,d)
return l(p,s,u)}function m(e,t,r){const a=e.sort(l)
return n.a.chain(a).last(t).head(r).value()}function h(e,t={}){let r=t.drop_lowest||0
let s=t.drop_highest||0
const i=t.never_drop||[]
if(!(r||s))return e
let o=[]
let d=e
if(i.length>0){var u=c(e,e=>n.a.includes(i,e.submission.assignment_id))
var l=Object(a["a"])(u,2)
o=l[0]
d=l[1]}if(0===d.length)return o
r=Math.min(r,d.length-1)
s=r+s>=d.length?0:s
const _=d.length-r
const g=_-s
const f=n.a.some(d,e=>e.total>0)
let h
h=f?p(d,o,_,g):m(d,_,g)
h=[...h,...o]
n.a.difference(d,h).forEach(e=>{e.drop=true})
return h}function b(e,t,r){const a=n.a.chain(t).filter("hidden").keyBy("assignment_id").value()
const s=e=>e.omit_from_final_grade||a[e.id]||n.a.isEqual(e.submission_types,["not_graded"])||"unpublished"===e.workflow_state||r.ignoreUnpostedAnonymous&&e.anonymize_students
const c=n.a.reject(e.assignments,s)
const d=n.a.keyBy(c,"id")
let u=n.a.filter(t,e=>d[e.assignment_id])
u=n.a.reject(u,"excused")
const l=n.a.map(u,e=>({total:o(d[e.assignment_id].points_possible),score:o(e.score),submitted:null!=e.score&&""!==e.score,pending_review:"pending_review"===e.workflow_state,submission:e}))
let _=l
r.includeUngraded||(_=n.a.filter(l,e=>e.submitted&&!e.pending_review))
const g=h(_,e.rules)
const f=Object(i["d"])(n.a.chain(g).map("score").map(o).value())
const p=Object(i["e"])(g,"total")
return{score:f,possible:p,submission_count:n.a.filter(l,"submitted").length,submissions:n.a.map(l,e=>{const t=e.total?Object(i["b"])(e.score,e.total):0
return{drop:e.drop,percent:o(t),score:o(e.score),possible:e.total,submission:e.submission,submitted:e.submitted}})}}function v(e,t,r){const a=n.a.uniq(e,"assignment_id")
return{assignmentGroupId:t.id,assignmentGroupWeight:t.group_weight,current:b(t,a,{includeUngraded:false,ignoreUnpostedAnonymous:r}),final:b(t,a,{includeUngraded:true,ignoreUnpostedAnonymous:r}),scoreUnit:"points"}}t["a"]={calculate:v}},KNu5:function(e,t,r){"use strict"
var a=r("ouhR")
var s=r.n(a)
var n=r("Y/W1")
var i=r.n(n)
var c=r("BrAc")
r("8JEM")
var o=r("HGxv")
var d=r("8WeW")
Object(d["a"])(JSON.parse('{"ar":{"set_created_createdat_895e2243":"تم إنشاء المجموعة %{createdAt}"},"ca":{"set_created_createdat_895e2243":"Conjunt creat el %{createdAt}"},"cy":{"set_created_createdat_895e2243":"Wedi creu set %{createdAt}"},"da":{"set_created_createdat_895e2243":"Sæt oprettet %{createdAt}"},"da-x-k12":{"set_created_createdat_895e2243":"Sæt oprettet %{createdAt}"},"de":{"set_created_createdat_895e2243":"Set erstellt %{createdAt}"},"en-AU":{"set_created_createdat_895e2243":"Set created %{createdAt}"},"en-AU-x-unimelb":{"set_created_createdat_895e2243":"Set created %{createdAt}"},"en-CA":{"set_created_createdat_895e2243":"Set created %{createdAt}"},"en-GB":{"set_created_createdat_895e2243":"Set created %{createdAt}"},"en-GB-x-lbs":{"set_created_createdat_895e2243":"Set created %{createdAt}"},"en-GB-x-ukhe":{"set_created_createdat_895e2243":"Set created %{createdAt}"},"es":{"set_created_createdat_895e2243":"Conjunto creado %{createdAt}"},"fa":{"set_created_createdat_895e2243":"مجموعه ایجاد شد %{createdAt}"},"fi":{"set_created_createdat_895e2243":"Määritä luotu %{createdAt}"},"fr":{"set_created_createdat_895e2243":"Ensemble créé %{createdAt}"},"fr-CA":{"set_created_createdat_895e2243":"Ensemble créé %{createdAt}"},"he":{"set_created_createdat_895e2243":"נוצר אוגדן %{createdAt}"},"ht":{"set_created_createdat_895e2243":"Ansanm Kreye %{createdAt}"},"hu":{"set_created_createdat_895e2243":"Beállítás létrehozva %{createdAt}"},"is":{"set_created_createdat_895e2243":"Sett stofnað %{createdAt}"},"it":{"set_created_createdat_895e2243":"Set creato %{createdAt}"},"ja":{"set_created_createdat_895e2243":"セットが %{createdAt} に作成されました"},"mi":{"set_created_createdat_895e2243":"Tautuhi hanga %{createdAt}"},"nb":{"set_created_createdat_895e2243":"Settet opprettet %{createdAt}"},"nb-x-k12":{"set_created_createdat_895e2243":"Settet opprettet %{createdAt}"},"nl":{"set_created_createdat_895e2243":"Set aangemaakt %{createdAt}"},"nn":{"set_created_createdat_895e2243":"Sett oppretta %{createdAt}"},"pl":{"set_created_createdat_895e2243":"Zbiór utworzono %{createdAt}"},"pt":{"set_created_createdat_895e2243":"Grupo criado %{createdAt}"},"pt-BR":{"set_created_createdat_895e2243":"Configuração criada %{createdAt}"},"ru":{"set_created_createdat_895e2243":"Установить создание %{createdAt}"},"sl":{"set_created_createdat_895e2243":"Nabor je ustvarjen %{createdAt}"},"sv":{"set_created_createdat_895e2243":"Uppsättning skapades %{createdAt}"},"sv-x-k12":{"set_created_createdat_895e2243":"Uppsättning skapades %{createdAt}"},"uk":{"set_created_createdat_895e2243":"Набір створено %{createdAt}"},"zh-Hans":{"set_created_createdat_895e2243":"集已创建%{createdAt}"},"zh-Hant":{"set_created_createdat_895e2243":"組創建於 %{createdAt}"}}'))
r("jQeR")
r("0sPK")
var u=o["default"].scoped("gradingPeriodSetsApi")
var l=r("hlbo")
var _=r("alKs")
var g=r("xxvzy")
const f=()=>ENV.GRADING_PERIOD_SETS_URL
const p=()=>ENV.GRADING_PERIOD_SETS_URL
const m=e=>s.a.replaceTags(ENV.GRADING_PERIOD_SET_UPDATE_URL,"id",e)
const h=e=>{const t={title:e.title,weighted:e.weighted,display_totals_for_all_grading_periods:e.displayTotalsForAllGradingPeriods}
return{grading_period_set:t,enrollment_term_ids:e.enrollmentTermIDs}}
const b=e=>({id:e.id.toString(),title:v(e),weighted:!!e.weighted,displayTotalsForAllGradingPeriods:e.display_totals_for_all_grading_periods,gradingPeriods:g["a"].deserializePeriods(e.grading_periods),permissions:e.permissions,createdAt:new Date(e.created_at)})
const v=e=>{if(e.title&&e.title.trim())return e.title.trim()
{const t=l["a"].formatDateForDisplay(new Date(e.created_at))
return u.t("Set created %{createdAt}",{createdAt:t})}}
const A=function(e){const t=b(e)
t.enrollmentTermIDs=e.enrollment_term_ids
return t}
const w=e=>i.a.flatten(i.a.map(e,e=>i.a.map(e.grading_period_sets,e=>b(e))))
t["a"]={deserializeSet:A,list:()=>new Promise((e,t)=>{const r=new _["a"]
r.getDepaginated(f()).then(t=>e(w(t))).fail(e=>t(e))}),create:e=>c["a"].post(p(),h(e)).then(e=>A(e.data.grading_period_set)),update:e=>c["a"].patch(m(e.id),h(e)).then(t=>e)}},Kr7l:function(e,t,r){"use strict"
r.d(t,"a",(function(){return n}))
r.d(t,"b",(function(){return i}))
function a(e,t){return(e||"").replace(/(?:^|[-_])(\w)/g,(e,r,a)=>0===a&&t?r?r.toLowerCase():"":r?r.toUpperCase():"")}function s(e){return e.replace(/([A-Z])/g,e=>"_"+e.toLowerCase())}function n(e){let t
const r={}
for(t in e)e.hasOwnProperty(t)&&(r[a(t,true)]=e[t])
return r}function i(e){let t
const r={}
for(t in e)e.hasOwnProperty(t)&&(r[s(t)]=e[t])
return r}},ShK5:function(e,t,r){"use strict"
r.d(t,"a",(function(){return c}))
r.d(t,"b",(function(){return o}))
var a=r("LvDl")
var s=r.n(a)
var n=r("3H9/")
var i=r("tybv")
function c(e,t){const r={}
s.a.forEach(e,(e,a)=>{e[t]&&(r[a]=e[t])})
return r}function o(e,t,r=[]){const a=new i["a"](r)
const c=s.a.sortBy(r,"startDate")
t.forEach(t=>{const s=n["a"].parse(t.cached_due_date)
let i=null
r.length&&(i=s?a.gradingPeriodForDueAt(s):c[c.length-1])
const o=e[t.assignment_id]||{}
o[t.user_id]={due_at:t.cached_due_date,grading_period_id:i?i.id:null,in_closed_grading_period:!!i&&i.isClosed}
e[t.assignment_id]=o})}},alKs:function(e,t,r){"use strict"
r.d(t,"a",(function(){return m}))
var a=r("VTBJ")
var s=r("ouhR")
var n=r.n(s)
r("dhbk")
var i=r("ODXe")
var c=r("Y/W1")
var o=r.n(c)
function d(e,t){const r=[]
let a=1
const s=(n,c)=>{if(c===a){e&&e(n)
o.a.isArray(n)?t.push(...n):t.push(n)
a+=1}else r.push([n,c])
const d=o.a.find(r,([e,t])=>t===a)
if(d){const e=Object(i["a"])(d,2),t=e[0],r=e[1]
s(t,r)}}
return s}function u(e,t,r,s=(()=>{}),i=null){const c=n.a.Deferred()
const o=[]
const u=()=>{s([])
c.reject()}
const l=d(r,o)
n.a.ajaxJSON(e,"GET",t,(r,d)=>{l(r,1)
const _=d.getResponseHeader("Link")
const g=_.match(/<[^>]+>; *rel="last"/)
if(null===g){s([])
c.resolve(o)
return}const f=parseInt(g[0].match(/page=(\d+)/)[1],10)
if(1===f){s([])
c.resolve(o)
return}function p(e){return Object(a["a"])({page:e},t)}function m(e){return t=>l(t,e)}const h=[]
if(null==i){const t=t=>n.a.ajaxJSON(e,"GET",p(t),m(t))
for(let e=2;e<=f;e++)h.push(t(e))}else for(let t=2;t<=f;t++){const r=i.getJSON(e,p(t),m(t))
h.push(r)}s(h)
n.a.when(...h).then(()=>c.resolve(o),u)},u)
return c}var l=u
const _=12
const g=100
const f=10
function p(e){let t=Number.parseInt(e,10)
t=Number.isNaN(t)?_:t
t=Math.min(g,t)
return Math.max(f,t)}class m{constructor(e={}){this.options=Object(a["a"])(Object(a["a"])({},e),{},{activeRequestLimit:p(e.activeRequestLimit)})
this.requests=[]}get activeRequestCount(){return this.requests.filter(e=>e.active).length}get nextPendingRequest(){return this.requests.find(e=>!e.active)}addRequest(e){this.requests.push(e)
this.fillQueue()}clearRequest(e){this.requests=this.requests.filter(t=>t!==e)
this.fillQueue()}fillQueue(){let e=this.nextPendingRequest
while(null!=e&&this.activeRequestCount<this.options.activeRequestLimit){e.start()
e=this.nextPendingRequest}}getDepaginated(e,t,r=(()=>{}),a=(()=>{})){const s={deferred:new n.a.Deferred,active:false}
const i=e=>{this.clearRequest(s)
return a(e)}
s.start=()=>{s.active=true
l(e,t,r,i,this).then(s.deferred.resolve).fail(s.deferred.reject).always(()=>{this.clearRequest(s)})}
this.addRequest(s)
return s.deferred}getJSON(e,t,r,a){const s={deferred:new n.a.Deferred,active:false}
s.start=()=>{s.active=true
n.a.ajaxJSON(e,"GET",t,r,a).then(s.deferred.resolve).fail(s.deferred.reject).always(()=>{this.clearRequest(s)})}
this.addRequest(s)
return s.deferred}}},hlbo:function(e,t,r){"use strict"
var a=r("Y/W1")
var s=r.n(a)
var n=r("ouhR")
var i=r.n(n)
var c=r("3H9/")
r("7svE")
const o={parseDates(e,t){s.a.each(t,t=>{const r=!s.a.isUndefined(e[t])
r&&(e[t]=c["a"].parse(e[t]))})
return e},formatDatetimeForDisplay:(e,t="medium")=>i.a.datetimeString(e,{format:t,timezone:ENV.CONTEXT_TIMEZONE}),formatDatetimeForDiscussions:(e,t="")=>i.a.discussionsDatetimeString(e,{format:t,timezone:ENV.CONTEXT_TIMEZONE}),formatDateForDisplay:e=>i.a.dateString(e,{format:"medium",timezone:ENV.CONTEXT_TIMEZONE}),isMidnight:e=>c["a"].isMidnight(e)}
t["a"]=o},tybv:function(e,t,r){"use strict"
var a=r("Y/W1")
var s=r.n(a)
function n(e,t=false){let r=s.a.isDate(e)
t&&!r&&(r=null===e)
if(!r)throw new Error(`\`${e}\` must be a Date or null`)}function i(e){if(null==e)throw new Error(`'${e}' must be an array or object`)
const t=["startDate","endDate","closeDate"]
const r=s.a.isArray(e)?e:[e]
s.a.each(r,e=>{s.a.each(t,t=>n(e[t]))})
return r}function c(e){const t=s.a.isString(e)
if(!t)throw new Error(`Grading period id \`${e}\` must be a String`)}class o{constructor(e){this.gradingPeriods=i(e)}static isAllGradingPeriods(e){c(e)
return"0"===e}get earliestValidDueDate(){const e=s.a.sortBy(this.gradingPeriods,"startDate")
const t=s.a.find(e,{isClosed:false})
return t?t.startDate:null}gradingPeriodForDueAt(e){n(e,true)
return s.a.find(this.gradingPeriods,t=>this.isDateInGradingPeriod(e,t.id,false))||null}isDateInGradingPeriod(e,t,r=true){if(r){n(e,true)
c(t)}const a=s.a.find(this.gradingPeriods,{id:t})
if(!a)throw new Error(`No grading period has id \`${t}\``)
return null===e?a.isLast:a.startDate<e&&e<=a.endDate}isDateInClosedGradingPeriod(e){const t=this.gradingPeriodForDueAt(e)
return!!t&&t.isClosed}}t["a"]=o},xxvzy:function(e,t,r){"use strict"
var a=r("ouhR")
var s=r.n(a)
var n=r("Y/W1")
var i=r.n(n)
var c=r("BrAc")
r("8JEM")
const o=e=>s.a.replaceTags(ENV.GRADING_PERIODS_UPDATE_URL,"set_id",e)
const d=e=>{const t=i.a.map(e,e=>({id:e.id,title:e.title,start_date:e.startDate,end_date:e.endDate,close_date:e.closeDate,weight:e.weight}))
return{grading_periods:t}}
t["a"]={deserializePeriods:e=>i.a.map(e,e=>({id:e.id,title:e.title,startDate:new Date(e.start_date),endDate:new Date(e.end_date),closeDate:new Date(e.close_date),isLast:e.is_last,isClosed:e.is_closed,weight:e.weight})),batchUpdate(e,t){return new Promise((r,a)=>c["a"].patch(o(e),d(t)).then(e=>r(this.deserializePeriods(e.data.grading_periods))).catch(e=>a(e)))}}}}])

//# sourceMappingURL=102-c-1f0a5914e2.js.map